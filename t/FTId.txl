ID   XXXX_XXXXX  PRELIMINARY;      PRT;     1 AA.
AC   P00001;
DT   01-JAN-2000 (TrEMBLrel. 15, Created)
DT   01-JAN-2000 (TrEMBLrel. 15, Last sequence update)
DT   01-JAN-2000 (TrEMBLrel. 15, Last annotation update)
DE   Foo protein.
OC   1.
FT   TOPIC         1     10       Text.
FT                                /FTId=FOO_0000000001.
FT   TOPIC        11     20
FT                                /FTId=FOO_0000000002.
SQ   SEQUENCE   1 AA;  0 MW;  0 CRC64;
     X
//
