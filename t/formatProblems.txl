ID   PPK5_PERAM     PRELIMINARY;   PRT;   17 AA.
AC   P82617;
DT   01-OCT-2000 (TrEMBLrel. 15, Created)
DT   01-OCT-2000 (TrEMBLrel. 15, Last sequence update)
DT   01-OCT-2000 (TrEMBLrel. 15, Last annotation update)
DE   PYROKININ-5 (PEA-PK-5) (FXPRL-AMIDE).
OS   Periplaneta americana (American cockroach).
OC   Eukaryota; Metazoa; Arthropoda; Tracheata; Hexapoda; Insecta;
OC   Pterygota; Neoptera; Orthopteroidea; Dictyoptera; Blattaria;
OC   Blattoidea; Blattidae; Periplaneta.
OX   NCBI_TaxID=6978;
RN   [1]
RP   SEQUENCE, FUNCTION, AND MASS SPECTROMETRY.
RC   TISSUE=ABDOMINAL PERISYMPATHETIC ORGANS;
RX   MEDLINE=99212469; PubMed=10196736;
RG   The Three Stooges;
RA   Predel R., Kellner R., Nachman R.J., Holman G.M., Rapus J., Gaede G.;
RT   "Differential distribution of pyrokinin-isoforms in cerebral and
RT   abdominal neurohemal organs of the American cockroach.";
RL   Insect Biochem. Mol. Biol. 29:139-144(1999).
RN   [2]
RP   TISSUE SPECIFICITY.
RC   STRAIN=12,714 / SCARLATINA;
RX   MEDLINE=20189894; PubMed=10723010;
RG   The Three
RG   Stooges;
RT   "Tagma-specific distribution of FXPRLamides in the nervous system of
RT   the American cockroach.";
RL   J. Comp. Neurol. 419:352-363(2000).
CC   -!- FUNCTION: MEDIATES VISCERAL MUSCLE CONTRACTILE ACTIVITY
CC       (MYOTROPIC ACTIVITY).
CC   -!- TISSUE SPECIFICITY: MAINLY IN ABDOMINAL PERISYMPATHETIC ORGANS AND
CC       TO A LESSER EXTENT IN RETROCEREBRAL COMPLEX.
CC   -!- MASS SPECTROMETRY: Mass=1651.7; Method=MALDI;
CC   -!- SIMILARITY: BELONGS TO THE PYROKININ FAMILY.
CC   -!- SIMILARITY: BELONGS TO THE PYROKININ FAMILY.
CC   -!- RNA EDITING: Modified_positions=2.
CC   --------------------------------------------------------------------------
CC   Copyrighted by the UniProt Consortium, see http://www.uniprot.org/terms
CC   Distributed under the Creative Commons Attribution-NoDerivs License.
CC   --------------------------------------------------------------------------
DR   INTERPRO; IPR001484; -.
DR   PROSITE; PS00539; PYROKININ; UNKNOWN_1.
KW   Amidation; Neuropeptide; Pyrokinin.
FT   MOD_RES      17     17       AMIDATION.
FT   MOD_RES      17     17       AMIDATIONAMIDATIONAMIDATIONAMIDATIONAMIDATIONAMIDATION.
FT   DOMAIN      638    758       CONTAINS CONSERVED RESIDUES USED FOR
FT                                3' -> 5' EXONUCLEASE ACTIVITIES.
FT   VARIANT      74     74       Q -> R (IN HYPOGONADISM; LACK OF
FT                                RECEPTOR-BINDING).
FT   VARIANT      74     74       Q -> R (IN A VERY SELDOM ENCOUNTERED RARE FORM OF HYPOGONADISMHYPOGONADISMHYPOGONADISMHYPOGONADISM; LACK OF
FT                                RECEPTOR-BINDING).
FT   VARIANT      74     74       Q -> R (IN HYPOGONADISMHYPOGONADISMHYPOGONADISMHYPOGONADISM; LACK OF
FT                                RECEPTOR-BINDING).
FT   VARIANT      74     74       Q -> R (IN HYPOGONADISMHYPOGONADISMHYPOGONADISM-HYPOGONADISM; LACK OF
FT                                RECEPTOR-BINDING).
FT   VARIANT      74     74       Q -> RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR (IN HYPOGONADISM).
FT   VARIANT      67     67       K -> M (IN ALLELE PGM1*7+, ALLELE PGM1*7-
FT                                , ALLELE PGM1*3+ AND ALLELE PGM1*3-).
FT                                /FTId=VAR_006090.
**   #################     SOURCE SECTION     ##################
**   #################    INTERNAL SECTION    ##################
**ZZ CREATED AND FINISHED BY NICO.
**ZZ DS 43484.
**ZZ UPDATED BY NICO (7/8/00). ADDED TISSUE SPECIFICITY.
**ZZ CURATED.
SQ   SEQUENCE   17 AA;  1653 MW;  8527162EA45BBA54 CRC64;
     GGGGSGETSG MWFGPRL
//
ID   PPK5_PERAM     PRELIMINARY;   PRT;   17 AA.
AC   P82617;
DT   01-OCT-2000 (TrEMBLrel. 15, Created)
DT   01-OCT-2000 (TrEMBLrel. 15, Last sequence update)
DT   01-OCT-2000 (TrEMBLrel. 15, Last annotation update)
DE   Light-independent protochlorophyllide reductase subunit B (EC
DE   1.18.-.-) (LI-POR subunit B) (DPOR subunit B).
OS   Periplaneta americana (American cockroach).
OC   Eukaryota; Metazoa; Arthropoda; Tracheata; Hexapoda; Insecta;
OC   Pterygota; Neoptera; Orthopteroidea; Dictyoptera; Blattaria;
OC   Blattoidea; Blattidae; Periplaneta.
OX   NCBI_TaxID=6978;
RN   [1]
RP   SEQUENCE, FUNCTION, AND MASS SPECTROMETRY.
RC   TISSUE=ABDOMINAL PERISYMPATHETIC ORGANS;
RX   MEDLINE=99212469; PubMed=10196736;
RA   Predel R., Kellner R., Nachman R.J., Holman G.M., Rapus J., Gaede G.;
RT   "Differential distribution of pyrokinin-isoforms in cerebral and
RT   abdominal neurohemal organs of the American cockroach.";
RG   French Parkinson's disease genetics study group;
RG   European consortium on genetic susceptibility on Parkinson's disease;
RL   Insect Biochem. Mol. Biol. 29:139-144(1999).
RN   [2]
RP   TISSUE SPECIFICITY.
RX   MEDLINE=20189894; PubMed=10723010;
RA   Predel R., Eckert M.;
RT   "Tagma-specific distribution of FXPRLamides in the nervous system of
RT   the American cockroach.";
RL   J. Comp. Neurol. 419:352-363(2000).
CC   -!- FUNCTION: MEDIATES VISCERAL MUSCLE CONTRACTILE ACTIVITY
CC       (MYOTROPIC ACTIVITY).
CC   -!- TISSUE SPECIFICITY: MAINLY IN ABDOMINAL PERISYMPATHETIC ORGANS AND
CC       TO A LESSER EXTENT IN RETROCEREBRAL COMPLEX.
CC   -!- MASS SPECTROMETRY: MW=1651.7; METHOD=MALDI;
CC   -!- SIMILARITY: BELONGS TO THE PYROKININ FAMILY.
DR   PRODOM; PD010497; -; 1.
DR   INTERPRO; IPR001484; -.
DR   PROSITE; PS00539; PYROKININ; UNKNOWN_1.
**   PROSITE; PS00537; PYROKININ; FALSE_POS_1.
**   #################     SOURCE SECTION     ##################
**   #################    INTERNAL SECTION    ##################
**PM PROSITE; PS00539; PYROKININ; 13; 17; ?; 06-SEP-2000;
**ZZ CREATED AND FINISHED BY NICO.
**ZZ DS 43484.
**ZZ UPDATED BY NICO (7/8/00). ADDED TISSUE SPECIFICITY.
**ZZ CURATED.
SQ   SEQUENCE   17 AA;  1653 MW;  8527162EA45BBA54 CRC64;
     GGGGSGETSG MWFGPRL
//
ID   O54521      PRELIMINARY;      PRT;   144 AA.
AC   O54521;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   SLYA.
GN   SLYA.
OS   Salmonella enterica serovar Typhi.
OC   Bacteria; Proteobacteria; gamma subdivision; Enterobacteriaceae;
OC   Salmonella.
OX   NCBI_TaxID=90370, 119912;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=TY2, RF-1;
RA   Kawakami T., Kaneko A., Sekiya K., Okada N., Imajho-Ohmi S.,
RA   Nonaka T., Matsui H., Kawahara K., Danbara H.;
RL   Submitted (FEB-1998) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; AB010777; BAA24582.1; -.
DR   EMBL; AB010776; BAA24581.1; -.
DR   INTERPRO; IPR000835; -.
DR   PFAM; PF01047; MarR; 1.
DR   PRINTS; PR00598; HTHMARR.
DR   PROSITE; PS01117; HTH_MARR_FAMILY; 1.
**   PROSITE pattern is not entirely correct. Test comment.
**
**   #################     SOURCE SECTION     ##################
**   PSMID1233 STANDARD
**   Salmonella choleraesuis serovar Typhi gene for SlyA, complete cds.
**   [1]
**   1-636
**   Okada N.;
**   ;
**   Submitted (27-JAN-1998) to the EMBL/GenBank/DDBJ databases.
**   Nobuhiko Okada, Kitasato University, School of Pharmaceutical
**   Sciences,
**   Department of Microbiology; 5-9-1 Shirokane, Minato, Tokyo 108-8641,
**   Japan (E-mail:okadan@platinum.pharm.kitasato-u.ac.j p,
**   Tel:03-3444-6161,
**   Fax:03-3444-4831)
**   [2]
**   Kawakami T., Kaneko A., Sekiya K., Okada N., Imajho-Ohmi S., Nonaka
**   T.,
**   Matsui H., Kawahara K., Danbara H.;
**   "Identification of TTG initiation codon in slyA of Salmonella, a gene
**   required for survival within macrophages";
**   Unpublished.
**   [1]
**   1-636
**   Okada N.;
**   ;
**   Submitted (27-JAN-1998) to the EMBL/GenBank/DDBJ databases.
**   Nobuhiko Okada, Kitasato University, School of Pharmaceutical
**   Sciences,
**   Department of Microbiology; 5-9-1 Shirokane, Minato, Tokyo 108-8641,
**   Japan (E-mail:okadan@platinum.pharm.kitasato-u.ac.j p,
**   Tel:03-3444-6161,
**   Fax:03-3444-4831)
**   [2]
**   Kawakami T., Kaneko A., Sekiya K., Okada N., Imajho-Ohmi S., Nonaka
**   T.,
**   Matsui H., Kawahara K., Danbara H.;
**   "Identification of TTG initiation codon in slyA of Salmonella, a gene
**   required for survival within macrophages";
**   Unpublished.
**   source          1..636
**                   /organism="Salmonella choleraesuis serovar Typhi"
**                   /sequenced_mol="DNA"
**                   /strain="Ty2"
**   CDS             154..588
**                   /codon_start=1
**                   /db_xref="PID:d1025502"
**                   /transl_table=11
**                   /gene="slyA"
**                   /product="SlyA"
**   CDS_IN_EMBL_ENTRY 1
**   ORGANISM DOESN'T EXIST IN SP
**   11-FEB-1998 (Rel. 54, Last updated, Version 2)
**   source          1..636
**                   /organism="Salmonella choleraesuis choleraesuis"
**                   /sequenced_mol="DNA"
**                   /strain="RF-1"
**   CDS             154..588
**                   /codon_start=1
**                   /db_xref="PID:d1025501"
**                   /transl_table=11
**                   /gene="slyA"
**                   /product="SlyA"
**   CDS_IN_EMBL_ENTRY 1
**   ORGANISM DOESN'T EXIST IN SP
**   11-FEB-1998 (Rel. 54, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**PM PFAM; PF01047; MarR; 29; 133; T; 28-JAN-2000;
**PM PRINTS; PR00598; HTHMARR; 47; 63; T; 28-JAN-2000;
**PM PRINTS; PR00598; HTHMARR; 64; 79; T; 28-JAN-2000;
**PM PRINTS; PR00598; HTHMARR; 83; 99; T; 28-JAN-2000;
**PM PRINTS; PR00598; HTHMARR; 113; 133; T; 28-JAN-2000;
**PM PROSITE; PS01117; HTH_MARR_FAMILY; 62; 96; T; 02-MAY-2000;
SQ   SEQUENCE   144 AA;  16448 MW;  4647F7704F2D78DE CRC64;
     MESPLGSDLA RLVRIWRALI DHRLKPLELT QTHWVTLHNI HQLPPDQSQI QLAKAIGIEQ
     PSLVRTLDQL EDKGLISRQT CASDRRAKRI KLTEKAEPLI AEMEEVIHKT RGEILAGISS
     EEIELLIKLV AKLEHNIMEL HSHD
//
ID   FXR1_MOUSE     PRELIMINARY;   PRT;   677 AA.
AC   Q61584; Q9R1E2; Q9R1E3; Q9R1E4; Q9R1E5; Q9WUA7; Q9WUA8; Q9WUA9;
DT   01-JUL-1997 (TrEMBLrel. 04, Created)
DT   01-MAR-2001 (TrEMBLrel. 16, Last sequence update)
DT   01-MAR-2001 (TrEMBLrel. 16, Last annotation update)
DE   Fragile X mental retardation syndrome related proteinase precursor domain -
DE   like protein.
GN   FXR1 OR FXR1H.
OS   Mus musculus (Mouse).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Rodentia; Sciurognathi; Muridae; Murinae; Mus.
OX   NCBI_TaxID=010090;
RN   [1]
RP   SEQUENCE FROM N.A. (ISOFORM A).
RC   TISSUE=FETAL BRAIN;
RX   MEDLINE=96177651; PubMed=8634689;
RA   Coy J.F., Sedlacek Z., Baechner D., Hameister H., Joos S., Lichter P.,
RA   Delius H., Poustka A.;
RT   "Highly conserved 3' UTR and expression pattern of FXR1 points to a
RT   divergent gene regulation of FXR1 and FMR1.";
RL   Hum. Mol. Genet. 4:2209-2218(1995).
RN   [2]
RP   SEQUENCE FROM N.A., ALTERNATIVE SPLICING, AND TISSUE SPECIFICITY.
RX   MEDLINE=99339984; PubMed=10409431;
RA   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
RT   "Alternative splicing in the murine and human FXR1 genes.";
RL   Genomics 59:193-202(1999).
CC   -!- FUNCTION: RNA-BINDING PROTEIN. INTERACTS WITH FMR1 AND FXR2 (BY
CC       SIMILARITY).
CC   -!- SUBCELLULAR LOCATION: CYTOPLASMIC (BY SIMILARITY).
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing; Named isoforms=7;
CC       Name=E;
CC         IsoId=Q61584-1; Sequence=Displayed;
CC       Name=A;
CC         IsoId=Q61584-2; Sequence=VSP_02391, VSP_02393, VSP_02395;
CC       Name=B;
CC         IsoId=Q61584-3; Sequence=VSP_02393;
CC       Name=C;
CC         IsoId=Q61584-4; Sequence=VSP_02394;
CC       Name=D;
CC         IsoId=Q61584-5; Sequence=VSP_02391, VSP_02394;
CC       Name=F;
CC         IsoId=Q61584-6; Sequence=VSP_02391;
CC       Name=G;
CC         IsoId=Q61584-7; Sequence=VSP_02392;
CC   -!- TISSUE SPECIFICITY: IN EARLY EMBRYOGENESIS, HIGHEST EXPRESSION IN
CC       SOMITES AND CENTRAL NERVOUS SYSTEM. ALSO EXPRESSED IN SPINAL CORD,
CC       SURROUNDING MESENCHYMAL TISSUE AND UNDIFFERENTIATED GONAD. IN MID-
CC       EMBRYOGENESIS, MOST PROMINENT IN GONAD AND MUSCLE TISSUE. ALSO
CC       EXPRESSED IN LIVER, RETINA, TELENCEPHALON AND MESENCEPHALON. IN
CC       LATE EMBRYOGENESIS, RESTRICTED TO SKELETAL MUSCLE AND
CC       PROLIFERATIVE ACTIVE LAYERS OF BRAIN. AFTER BIRTH, HIGHLY
CC       EXPRESSED IN POSTMEIOTIC SPERMATIDS. INTERMEDIATE LEVELS ARE FOUND
CC       IN HEART, LIVER AND KIDNEY WITH LOWER LEVELS IN BRAIN AND SKELETAL
CC       MUSCLE.
CC   -!- SIMILARITY: BELONGS TO THE FMR1 FAMILY.
DR   EMBL; X90875; CAA62383.1; -.
DR   EMBL; AF124385; AAD30211.1; -.
DR   EMBL; AF124394; AAD30212.1; -.
DR   EMBL; AF124386; AAD30212.1; JOINED.
DR   EMBL; AF124387; AAD30212.1; JOINED.
DR   EMBL; AF124388; AAD30212.1; JOINED.
DR   EMBL; AF124389; AAD30212.1; JOINED.
DR   EMBL; AF124390; AAD30212.1; JOINED.
DR   EMBL; AF124391; AAD30212.1; JOINED.
DR   EMBL; AF124392; AAD30212.1; JOINED.
DR   EMBL; AF124393; AAD30212.1; JOINED.
DR   EMBL; AF124394; AAD30213.1; -.
DR   EMBL; AF124386; AAD30213.1; JOINED.
DR   EMBL; AF124387; AAD30213.1; JOINED.
DR   EMBL; AF124388; AAD30213.1; JOINED.
DR   EMBL; AF124389; AAD30213.1; JOINED.
DR   EMBL; AF124390; AAD30213.1; JOINED.
DR   EMBL; AF124391; AAD30213.1; JOINED.
DR   EMBL; AF124392; AAD30213.1; JOINED.
DR   EMBL; AF124393; AAD30213.1; JOINED.
DR   EMBL; AF124394; AAD30214.1; -.
DR   EMBL; AF124386; AAD30214.1; JOINED.
DR   EMBL; AF124387; AAD30214.1; JOINED.
DR   EMBL; AF124388; AAD30214.1; JOINED.
DR   EMBL; AF124389; AAD30214.1; JOINED.
DR   EMBL; AF124390; AAD30214.1; JOINED.
DR   EMBL; AF124391; AAD30214.1; JOINED.
DR   EMBL; AF124392; AAD30214.1; JOINED.
DR   EMBL; AF124393; AAD30214.1; JOINED.
DR   EMBL; AF124394; AAD30215.1; -.
DR   EMBL; AF124386; AAD30215.1; JOINED.
DR   EMBL; AF124387; AAD30215.1; JOINED.
DR   EMBL; AF124388; AAD30215.1; JOINED.
DR   EMBL; AF124389; AAD30215.1; JOINED.
DR   EMBL; AF124390; AAD30215.1; JOINED.
DR   EMBL; AF124391; AAD30215.1; JOINED.
DR   EMBL; AF124392; AAD30215.1; JOINED.
DR   EMBL; AF124393; AAD30215.1; JOINED.
DR   EMBL; AF124394; AAD30216.1; -.
DR   EMBL; AF124386; AAD30216.1; JOINED.
DR   EMBL; AF124387; AAD30216.1; JOINED.
DR   EMBL; AF124388; AAD30216.1; JOINED.
DR   EMBL; AF124389; AAD30216.1; JOINED.
DR   EMBL; AF124390; AAD30216.1; JOINED.
DR   EMBL; AF124391; AAD30216.1; JOINED.
DR   EMBL; AF124392; AAD30216.1; JOINED.
DR   EMBL; AF124393; AAD30216.1; JOINED.
DR   EMBL; AF124394; AAD30217.1; -.
DR   EMBL; AF124386; AAD30217.1; JOINED.
DR   EMBL; AF124387; AAD30217.1; JOINED.
DR   EMBL; AF124388; AAD30217.1; JOINED.
DR   EMBL; AF124389; AAD30217.1; JOINED.
DR   EMBL; AF124390; AAD30217.1; JOINED.
DR   EMBL; AF124391; AAD30217.1; JOINED.
DR   EMBL; AF124392; AAD30217.1; JOINED.
DR   EMBL; AF124393; AAD30217.1; JOINED.
DR   EMBL; AF124394; AAD30218.1; -.
DR   EMBL; AF124386; AAD30218.1; JOINED.
DR   EMBL; AF124387; AAD30218.1; JOINED.
DR   EMBL; AF124388; AAD30218.1; JOINED.
DR   EMBL; AF124389; AAD30218.1; JOINED.
DR   EMBL; AF124390; AAD30218.1; JOINED.
DR   EMBL; AF124391; AAD30218.1; JOINED.
DR   EMBL; AF124392; AAD30218.1; JOINED.
DR   EMBL; AF124393; AAD30218.1; JOINED.
DR   HSSP; Q06787; 2FMR.{EI1}
DR   MGI; MGI:104860; Fxr1h.{EI2}
DR   INTERPRO; IPR000958; -.
DR   PFAM; PF00013; KH-domain; 2.
KW   Alternative splicing; RNA-binding; Repeat.
FT   COMPBIAS     50     53       POLY-PRO.
FT   DOMAIN      222    251       KH.
FT   DOMAIN      285    314       KH.
FT   DOMAIN      471    490       RNA-BINDING (RGG-BOX).
FT   COMPBIAS    531    539       POLY-ARG.
FT   VAR_SEQ     380    408       Missing (in isoform A, isoform D and
FT                                isoform F).
FT                                /FTId=VSP_02391.
FT   VAR_SEQ     430    455       Missing (in isoform G).
FT                                /FTId=VSP_02392.
FT   VAR_SEQ     564    568       DDSEK -> GKRCD (in isoform A and isoform
FT                                B).
FT                                /FTId=VSP_02393.
FT   VAR_SEQ     564    590       Missing (in isoform C and isoform D).
FT                                /FTId=VSP_02394.
FT   VAR_SEQ     569    677       Missing (in isoform A).
FT                                /FTId=VSP_02395.
FT   VARIANT      79     79       R -> C (IN
FT                                TOURS/ALGER/AMIENS/TOYAMA/PARIS-1/PARIS-
FT                                2/PADUA-2/BARCELONA-2/KUMAMOTO; LACKS
FT                                HEPARIN-BINDING ABILITY).
FT                                /FTId=VAR_007037.
FT   VARIANT     425    425       R -> CDCDCDDCDDCDCDDCDDCDCDCDCDCD
FT                                CDDCDDCCDCDCDCD (IN
FT                                A STRAIN).
FT   CONFLICT    425    425       Missing.
FT   CONFLICT    425    425       R -> CDCDCDDCDDCDCDDCDDCDCDCDCDCD
FT                                CDDCDDCCDCDCDCD (IN
FT                                A STRAIN).
FT   CONFLICT    425    425       RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
FT                                -> C.
FT   VARIANT     425    425       R -> C (IN NORTHWICK/MILANO/
FT                                FRANKFURT/COPENHAGEN/LONDON; TYPE-II).
FT                                /FTId=VAR_007075.
FT   VARIANT     425    425       R -> C (IN NORTHWICK/MILANO/
FT                                FRANKFURT123/COPENHAGEN/LONDON; TYPE-II).
FT   VARIANT     425    425       R -> C (IN NORTHWICK/MILANO/
FT                                FRANKFURT1234/COPENHAGEN/LONDON; TYPE-II).
FT   VARIANT      23     23       R -> H (IN LILLE/TAIPEI/VARESE/KOMAGOME-
FT                                3).
FT   VARIANT     608    608       Missing (in NPD type B; prevalent among
FT                                NPD type B patients from the
FT                                North-African Maghreb region).
FT                                /FTId=VAR_005068.
**
**   #################     SOURCE SECTION     ##################
**   M.musculus mRNA for FXR1 protein
**   [1]
**   Coy J.F., Sedlacek Z., Baechner D., Hameister H., Joos S., Lichter P.,
**   Delius H., Poustka A.;
**   "Highly conserved 3' UTR and expression pattern of FXR1 points to a
**   divergent gene regulation of FXR1 and FMR1";
**   Hum. Mol. Genet. 4:2209-2218(1995).
**   [2]
**   1-2060
**   Coy J.F.;
**   ;
**   Submitted (14-AUG-1995) to the EMBL/GenBank/DDBJ databases.
**   J.F. Coy, DKFZ-Heidelberg, Molekulare Genomanalyse, Im Neuenheimer
**   Feld
**   280, 69120 Heidelberg, FRG
**   MGD; MGI:104860; Fxr1h.
**   [1]
**   1-1200
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   "Alternative Splicing in the Murine and Human FXR1 Genes";
**   Genomics 0:0-0(1999).
**   [2]
**   1-1200
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   ;
**   Submitted (28-JAN-1999) to the EMBL/GenBank/DDBJ databases.
**   Molecular & Human Genetics, Baylor College of Medicine, One Baylor
**   Plaza, Houston, TX 77030, USA
**   source          1..2060
**                   /organism="Mus musculus"
**   CDS             14..1633
**                   /db_xref="PID:e196394"
**                   /db_xref="MGD:MGI:104860"
**                   /gene="FXR1"
**   CDS_IN_EMBL_ENTRY 1
**   08-DEC-1995 (Rel. 46, Last updated, Version 2)
**   source          1..1200
**                   /db_xref="taxon:10090"
**                   /organism="Mus musculus"
**   CDS             join(AF124386.1:971..1021,AF124387.1:387..439,
**                   AF124387.1:1926..2019,AF124388.1:641..712,
**                   AF124388.1:1046..1194,AF124388.1:1575..1668,
**                   AF124388.1:2247..2363,AF124389.1:433..603,
**                   AF124389.1:1355..1433,AF124390.1:422..531,
**                   AF124391.1:187..273,AF124392.1:290..347,
**                   AF124392.1:435..497,AF124393.1:391..594,
**                   AF124393.1:2679..2879,911..927)
**                   /codon_start=1
**                   /db_xref="PID:g4835741"
**                   /gene="Fxr1h"
**                   /product="fragile-X-related protein 1 isoform a"
**                   /protein_id="AAD30213.1"
**   CDS_IN_EMBL_ENTRY 7
**   17-MAY-1999 (Rel. 59, Last updated, Version 1)
**   Mus musculus fragile-X-related protein 1 (Fxr1h) gene, exons 16 and
**   17, alternative splice products, complete cds.
**   [1]
**   1-1200
**   MEDLINE; 99339984.
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   "Alternative splicing in the murine and human FXR1 genes";
**   Genomics 59(2):193-202(1999).
**   [2]
**   1-1200
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   ;
**   Submitted (28-JAN-1999) to the EMBL/GenBank/DDBJ databases.
**   Molecular & Human Genetics, Baylor College of Medicine, One Baylor Plaza,
**   Houston, TX 77030, USA
**   source          1..1200
**                   /db_xref="taxon:10090"
**                   /organism="Mus musculus"
**   CDS             join(AF124386.1:971..1021,AF124387.1:387..439,
**                   AF124387.1:1926..2019,AF124388.1:641..712,
**                   AF124388.1:1046..1194,AF124388.1:1575..1668,
**                   AF124388.1:2247..2363,AF124389.1:433..603,
**                   AF124389.1:1355..1433,AF124390.1:422..531,
**                   AF124391.1:187..273,AF124392.1:290..347,
**                   AF124392.1:435..497,AF124393.1:391..594,
**                   AF124393.1:2679..2879,156..247,911..1081)
**                   /codon_start=1
**                   /db_xref="PID:g4835744"
**                   /gene="Fxr1h"
**                   /product="fragile-X-related protein 1 isoform d"
**                   /protein_id="AAD30216.1"
**   CDS_IN_EMBL_ENTRY 7
**   10-AUG-1999 (Rel. 60, Last updated, Version 2)
**   Mus musculus fragile-X-related protein 1 (Fxr1h) gene, exons 16 and
**   17, alternative splice products, complete cds.
**   [1]
**   1-1200
**   MEDLINE; 99339984.
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   "Alternative splicing in the murine and human FXR1 genes";
**   Genomics 59(2):193-202(1999).
**   [2]
**   1-1200
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   ;
**   Submitted (28-JAN-1999) to the EMBL/GenBank/DDBJ databases.
**   Molecular & Human Genetics, Baylor College of Medicine, One Baylor Plaza,
**   Houston, TX 77030, USA
**   source          1..1200
**                   /db_xref="taxon:10090"
**                   /organism="Mus musculus"
**   CDS             join(AF124386.1:971..1021,AF124387.1:387..439,
**                   AF124387.1:1926..2019,AF124388.1:641..712,
**                   AF124388.1:1046..1194,AF124388.1:1575..1668,
**                   AF124388.1:2247..2363,AF124389.1:433..603,
**                   AF124389.1:1355..1433,AF124390.1:422..531,
**                   AF124391.1:187..273,AF124392.1:290..497,
**                   AF124393.1:391..594,AF124393.1:2679..2879,911..927)
**                   /codon_start=1
**                   /db_xref="PID:g4835742"
**                   /gene="Fxr1h"
**                   /product="fragile-X-related protein 1 isoform b"
**                   /protein_id="AAD30214.1"
**   CDS_IN_EMBL_ENTRY 7
**   10-AUG-1999 (Rel. 60, Last updated, Version 2)
**   Mus musculus fragile-X-related protein 1 (Fxr1h) gene, exons 16 and
**   17, alternative splice products, complete cds.
**   [1]
**   1-1200
**   MEDLINE; 99339984.
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   "Alternative splicing in the murine and human FXR1 genes";
**   Genomics 59(2):193-202(1999).
**   [2]
**   1-1200
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   ;
**   Submitted (28-JAN-1999) to the EMBL/GenBank/DDBJ databases.
**   Molecular & Human Genetics, Baylor College of Medicine, One Baylor Plaza,
**   Houston, TX 77030, USA
**   source          1..1200
**                   /db_xref="taxon:10090"
**                   /organism="Mus musculus"
**   CDS             join(AF124386.1:971..1021,AF124387.1:387..439,
**                   AF124387.1:1926..2019,AF124388.1:641..712,
**                   AF124388.1:1046..1194,AF124388.1:1575..1668,
**                   AF124388.1:2247..2363,AF124389.1:433..603,
**                   AF124389.1:1355..1433,AF124390.1:422..531,
**                   AF124391.1:187..273,AF124392.1:290..347,
**                   AF124392.1:435..497,AF124393.1:391..594,
**                   AF124393.1:2679..2879,AF124393.1:3534..3614,156..247,
**                   911..1081)
**                   /codon_start=1
**                   /db_xref="PID:g4835745"
**                   /gene="Fxr1h"
**                   /product="fragile-X-related protein 1 isoform f"
**                   /protein_id="AAD30217.1"
**   CDS_IN_EMBL_ENTRY 7
**   10-AUG-1999 (Rel. 60, Last updated, Version 2)
**   Mus musculus fragile-X-related protein 1 (Fxr1h) mRNA, partial cds.
**   [1]
**   1-1674
**   MEDLINE; 99339984.
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   "Alternative splicing in the murine and human FXR1 genes";
**   Genomics 59(2):193-202(1999).
**   [2]
**   1-1674
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   ;
**   Submitted (26-JAN-1999) to the EMBL/GenBank/DDBJ databases.
**   Molecular & Human Genetics, Baylor College of Medicine, One Baylor Plaza,
**   Houston, TX 77030, USA
**   source          1..1674
**                   /db_xref="taxon:10090"
**                   /organism="Mus musculus"
**                   /strain="C57BL/6 x DBA"
**                   /clone="IMAGE:559877"
**   CDS             <1..1401
**                   /codon_start=1
**                   /db_xref="PID:g4835729"
**                   /note="FXR1"
**                   /gene="Fxr1h"
**                   /product="fragile-X-related protein 1"
**                   /protein_id="AAD30211.1"
**   CDS_IN_EMBL_ENTRY 1
**   10-AUG-1999 (Rel. 60, Last updated, Version 2)
**   Mus musculus fragile-X-related protein 1 (Fxr1h) gene, exons 16 and
**   17, alternative splice products, complete cds.
**   [1]
**   1-1200
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   "Alternative Splicing in the Murine and Human FXR1 Genes";
**   Genomics 0:0-0(1999).
**   [2]
**   1-1200
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   ;
**   Submitted (28-JAN-1999) to the EMBL/GenBank/DDBJ databases.
**   Molecular & Human Genetics, Baylor College of Medicine, One Baylor
**   Plaza, Houston, TX 77030, USA
**   source          1..1200
**                   /db_xref="taxon:10090"
**                   /organism="Mus musculus"
**   CDS             join(AF124386.1:971..1021,AF124387.1:387..439,
**                   AF124387.1:1926..2019,AF124388.1:641..712,
**                   AF124388.1:1046..1194,AF124388.1:1575..1668,
**                   AF124388.1:2247..2363,AF124389.1:433..603,
**                   AF124389.1:1355..1433,AF124390.1:422..531,
**                   AF124391.1:187..273,AF124392.1:290..497,
**                   AF124393.1:469..594,AF124393.1:2679..2879,
**                   AF124393.1:3534..3614,156..247,911..1081)
**                   /codon_start=1
**                   /db_xref="PID:g4835746"
**                   /gene="Fxr1h"
**                   /product="fragile-X-related protein 1 isoform g"
**                   /protein_id="AAD30218.1"
**   CDS_IN_EMBL_ENTRY 7
**   17-MAY-1999 (Rel. 59, Last updated, Version 1)
**   Mus musculus fragile-X-related protein 1 (Fxr1h) gene, exons 16 and
**   17, alternative splice products, complete cds.
**   [1]
**   1-1200
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   "Alternative Splicing in the Murine and Human FXR1 Genes";
**   Genomics 0:0-0(1999).
**   [2]
**   1-1200
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   ;
**   Submitted (28-JAN-1999) to the EMBL/GenBank/DDBJ databases.
**   Molecular & Human Genetics, Baylor College of Medicine, One Baylor
**   Plaza, Houston, TX 77030, USA
**   source          1..1200
**                   /db_xref="taxon:10090"
**                   /organism="Mus musculus"
**   CDS             join(AF124386.1:971..1021,AF124387.1:387..439,
**                   AF124387.1:1926..2019,AF124388.1:641..712,
**                   AF124388.1:1046..1194,AF124388.1:1575..1668,
**                   AF124388.1:2247..2363,AF124389.1:433..603,
**                   AF124389.1:1355..1433,AF124390.1:422..531,
**                   AF124391.1:187..273,AF124392.1:290..497,
**                   AF124393.1:391..594,AF124393.1:2679..2879,156..247,
**                   911..1081)
**                   /codon_start=1
**                   /db_xref="PID:g4835743"
**                   /gene="Fxr1h"
**                   /product="fragile-X-related protein 1 isoform c"
**                   /protein_id="AAD30215.1"
**   CDS_IN_EMBL_ENTRY 7
**   17-MAY-1999 (Rel. 59, Last updated, Version 1)
**   Mus musculus fragile-X-related protein 1 (Fxr1h) gene, exons 16 and
**   17, alternative splice products, complete cds.
**   [1]
**   1-1200
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   "Alternative Splicing in the Murine and Human FXR1 Genes";
**   Genomics 0:0-0(1999).
**   [2]
**   1-1200
**   Kirkpatrick L.L., McIlwain K.A., Nelson D.L.;
**   ;
**   Submitted (28-JAN-1999) to the EMBL/GenBank/DDBJ databases.
**   Molecular & Human Genetics, Baylor College of Medicine, One Baylor
**   Plaza, Houston, TX 77030, USA
**   source          1..1200
**                   /db_xref="taxon:10090"
**                   /organism="Mus musculus"
**   CDS             join(AF124386.1:971..1021,AF124387.1:387..439,
**                   AF124387.1:1926..2019,AF124388.1:641..712,
**                   AF124388.1:1046..1194,AF124388.1:1575..1668,
**                   AF124388.1:2247..2363,AF124389.1:433..603,
**                   AF124389.1:1355..1433,AF124390.1:422..531,
**                   AF124391.1:187..273,AF124392.1:290..497,
**                   AF124393.1:391..594,AF124393.1:2679..2879,
**                   AF124393.1:3534..3614,156..247,911..1081)
**                   /codon_start=1
**                   /db_xref="PID:g4835740"
**                   /note="FXR1"
**                   /gene="Fxr1h"
**                   /product="fragile-X-related protein 1 isoform e"
**                   /protein_id="AAD30212.1"
**   CDS_IN_EMBL_ENTRY 7
**   17-MAY-1999 (Rel. 59, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**EV EI1; HSSP_ADD; Q06787; 29-SEP-2000.
**EV EI2; MGD_ADD; MGI:104860; 29-SEP-2000.
**ID XXXX_MOUSE
**PM PFAM; PF00013; KH-domain; 222; 269; T; 02-FEB-2000;
**PM PFAM; PF00013; KH-domain; 285; 334; T; 02-FEB-2000;
**TT Test case. Wrongly positioned ** line.
**PM PROSITE; PS50084; KH_DOMAIN; 218; 277; T; 28-JAN-2000;
**PM PROSITE; PS50084; KH_DOMAIN; 281; 331; T; 28-JAN-2000;
**ZZ CREATED AND FINISHED BY Serenella.
**ZZ UPDATED BY Michele M. (10-NOV-2000).
**ZZ Comment: MERGED 7 TREMBL ENTRIES TO THIS ONE.
**ZZ CURATED.
SQ   SEQUENCE   677 AA;  76222 MW;  908104FC95431A11 CRC64;
     MAELTVEVRG SNGAFYKGFI KDVHEDSLTV VFENNWQPER QVPFNEVRLP PPPDIKKEIS
     EGDEVEVYSR ANDQEPCGWW LAKVRMMKGE FYVIEYAACD ATYNEIVTFE RLRPVNQNKT
     VKKNTFFKCT VDVPEDLREA CANENAHKDF KKAVGACRIF YHPETTQLMI LSASEATVKR
     VNILSDMHLR SIRTKLMLMS RNEEATKHLE CTKQLAAAFH EEFVVREDLM GLAIGTHGSN
     IQQARKVPGV TAIELDEDTG TFRIYGESAE AVKKARGFLE FVEDFIQVPR NLVGKVIGKN
     GKVIQEIVDK SGVVRVRIEG DNENKLPRED GMVPFVFVGT KESIGNVQVL LEYHIAYLKE
     VEQLRMERLQ IDEQLRQIGM GFRPSSTRGP EREKGYATDE STVSSVQGSR SYSGRGRGRR
     GPNYTSGYGT NSELSNPSET ESERKDELSD WSLAGEDDRE TRHQRDSRRR PGGRGRSVSG
     GRGRGGPRGG KSSISSVLKD PDSNPYSLLD NTESDQTADT DASESHHSTN RRRRSRRRRT
     DEDAVLMDGL TESDTASVNE NGLDDSEKKP QRRNRSRRRR FRGQAEDRQP VTVADYISRA
     ESQSRQRNLP RETLAKNKKE MAKDVIEEHG PSEKAINGPT SASGDEIPKL PRTLGEEKTK
     TLKEDSTQEA AVLNGVS
//
ID   AATM_RABIT     STANDARD;      PRT;    30 AA.
AC   P12345;
DT   01-OCT-1989 (Rel. 12, Created)
DT   01-OCT-1989 (Rel. 12, Last sequence update)
DT   01-OCT-1996 (Rel. 34, Last annotation update)
DE   Aspartate aminotransferase, mitochondrial (EC 2.6.1.1) (Transaminase
DE   A) (Glutamate oxaloacetate transaminase-2) (Fragment).
GN   GOT2.
OS   Oryctolagus cuniculus (Rabbit).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Lagomorpha; Leporidae; Oryctolagus.
OX   NCBI_TaxID=9986;
RN   [1]
RP   SEQUENCE.
RC   TISSUE=Liver;
RX   MEDLINE=85289123; PubMed=4030726;
RA   Kuramitsu S., Inoue K., Kondo K., Aki K., Kagamiyama H.;
RT   "Aspartate aminotransferase isozymes from rabbit liver. Purification
RT   and properties.";
RL   J. Biochem. 97:1337-1345(1985).
CC   -!- CATALYTIC ACTIVITY: L-ASPARTATE + 2-OXOGLUTARATE = OXALOACETATE +
CC       L-GLUTAMATE.
CC   -!- CATALYTIC ACTIVITY: Catalyzes the reduction and hydrolysis of (1->6)-alpha-D-glucosidic
CC       linkages in pullulan and in amylopectin and glycogen, and the alpha-
CC       and beta-limit dextrins of amylopectin and glycogen.
CC   -!- COFACTOR: PYRIDOXAL PHOSPHATE.
CC   -!- SUBUNIT: HOMODIMER.
CC   -!- SUBCELLULAR LOCATION: MITOCHONDRIAL MATRIX.
CC   -!- MISCELLANEOUS: IN EUKARYOTES THERE ARE TWO ISOZYMES: A CYTOPLASMIC
CC       ONE AND A MITOCHONDRIAL ONE.
CC   -!- SIMILARITY: BELONGS TO CLASS-I OF PYRIDOXAL-PHOSPHATE-DEPENDENT
CC       AMINOTRANSFERASES.
DR   PIR; B27103; B27103.
DR   HSSP; P00508; 1TAT.
DR   InterPro; IPR001511; Aminotran_1.
DR   PROSITE; PS00105; AA_TRANSFER_CLASS_1; PARTIAL.
KW   Transferase; Aminotransferase; Pyridoxal phosphate; Mitochondrion.
FT   NON_TER      30     30
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   30 AA;  3401 MW;  410321530B95B673 CRC64;
     SSWWAHVEMG PPDPILGVTE AYKRDTNSKK
//
ID   AATM_RABIT     STANDARD;      PRT;    30 AA.
AC   P12345;
DT   01-OCT-1989 (Rel. 12, Created)
DT   01-OCT-1989 (Rel. 12, Last sequence update)
DT   01-OCT-1996 (Rel. 34, Last annotation update)
DE   Aspartate aminotransferase, mitochondrial (EC 2.6.1.1) (Transaminase A) (Glutamate oxaloacetate transaminase-2) (Fragment).
GN   abcdefghijklmnopqrstuvwx abcdefghijklmnopqrstuvwx OR abcdefghijkl mnOR A.
OS   Oryctolagus cuniculus (Rabbit).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Lagomorpha; Leporidae; Oryctolagus.
OX   NCBI_TaxID=9986;
RN   [1]
RP   SEQUENCE.
RC   TISSUE=Liver;
RX   MEDLINE=85289123; PubMed=4030726;
RA   Kuramitsu S., Inoue K., Kondo K., Aki K., Kagamiyama H.;
RT   "Aspartate aminotransferase isozymes from rabbit liver. Purification
RT   and properties.";
RL   J. Biochem. 97:1337-1345(1985).
FT   NON_TER      30     30
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   30 AA;  3401 MW;  410321530B95B673 CRC64;
     SSWWAHVEMG PPDPILGVTE AYKRDTNSKK
//
ID   AATM_RABIT     STANDARD;      PRT;    30 AA.
AC   P12345;
DT   01-OCT-1989 (Rel. 12, Created)
DT   01-OCT-1989 (Rel. 12, Last sequence update)
DT   01-OCT-1996 (Rel. 34, Last annotation update)
DE   Aspartate aminotransferase, mitochondrial (EC 2.6.1.1) (Transaminase
DE   A) (Glutamate oxaloacetate transaminase-2) (Fragment).
GN   abcdefghijklmnopqrstuvwx OR abcdefghijklmnopqrstuvwxyzabcdefghikjlmn OR abcdefghijkl mnOR A.
OS   Oryctolagus cuniculus (Rabbit).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Lagomorpha; Leporidae; Oryctolagus.
OX   NCBI_TaxID=9986;
RN   [1]
RP   SEQUENCE.
RC   TISSUE=Liver;
RX   MEDLINE=85289123; PubMed=4030726;
RA   Kuramitsu S., Inoue K., Kondo K., Aki K., Kagamiyama H.;
RT   "Aspartate aminotransferase isozymes from rabbit liver. Purification
RT   and properties.";
RL   J. Biochem. 97:1337-1345(1985).
CC   -!- SIMILARITY: Contains 1 RING-type zinc finger.
CC   -!- SIMILARITY: Contains 4 C3H1-type zinc fingers.
FT   NON_TER      30     30
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   30 AA;  3401 MW;  410321530B95B673 CRC64;
     SSWWAHVEMG PPDPILGVTE AYKRDTNSKK
//
ID   COAT_BPSP      STANDARD;      PRT;   132 AA.
AC   P09673;
DT   01-MAR-1989 (Rel. 10, Created)
DT   01-MAR-1989 (Rel. 10, Last sequence update)
DT   01-FEB-1994 (Rel. 28, Last annotation update)
DE   Coat protein.
OS   Bacteriophage SP.
OC   Viruses; ssRNA positive-strand viruses, no DNA stage; Leviviridae;
OC   Allolevivirus.
OX   NCBI_TaxID=12027, 10685;
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=88289362; PubMed=3399390;
RA   Hirashima A., Hirose T., Inayama S., Inokuchi Y., Jacobson A.B.;
RT   "Analysis of the complete nucleotide sequence of the group IV RNA
RT   coliphage SP.";
RL   Nucleic Acids Res. 16:6205-6221(1988).
CC   -!- FUNCTION: FORMS THE PHAGE SHELL; BINDS TO THE PHAGE RNA.
DR   EMBL; X07489; CAA30374.1; -.
DR   HSSP; P03615; 1QBE.
DR   InterPro; IPR002703; Levi_coat.
DR   Pfam; PF01819; Levi_coat; 1.
KW   Coat protein; RNA-binding.
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   132 AA;  14129 MW;  50B1E6CC6AF0A254 CRC64;
     MAKLNQVTLS KIGKNGDQTL TLTPRGVNPT NGVASLSEAG AVPALEKRVT VSVAQPSRNR
     KNFKVQIKLQ NPTACTRDAC DPSVTRSAFA DVTLSFTSYS TDEERALIRT ELAALLADPL
     IVDAIDNLNP AY
//
ID   X_WHV1         STANDARD;      PRT;   141 AA.
AC   P03167;
DT   21-JUL-1986 (Rel. 01, Created)
DT   21-JUL-1986 (Rel. 01, Last sequence update)
DT   15-DEC-1998 (Rel. 37, Last annotation update)
DE   Trans-activating protein X.
GN   X.
OS   Woodchuck hepatitis virus 1 (WHV 1).
OC   Viruses; Retroid viruses; Hepadnaviridae; Orthohepadnavirus.
OX   NCBI_TaxID=10430;
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=82216969; PubMed=7086958;
RA   Galibert F., Chen T.N., Mandart E.;
RT   "Nucleotide sequence of a cloned woodchuck hepatitis virus genome:
RT   comparison with the hepatitis B virus sequence.";
RL   J. Virol. 41:51-65(1982).
DR   EMBL; J02442; -; NOT_ANNOTATED_CDS.
DR   PIR; A03720; QQVLC1.
DR   InterPro; IPR000236; TransactX.
DR   Pfam; PF00739; X; 1.
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   141 AA;  15221 MW;  4728019844561D85 CRC64;
     MAARLCCQLD PARDVLLLRP FGSQSSGPPF PRPSAGSAAS PASSLSASDE SDLPLGRLPA
     CFASASGPCC LVVTCAELRT MDSTVNFVSW HANRQLGMPS KDLWTPYIRD QLLTKWEEGS
     IDPRLSIFVL GGCRHKCMRL P
//
ID   ROCKY_NICSY     PRELIMINARY;      PRT;   276 AA.
AC   P19682;
DT   01-FEB-1991 (Rel. 17, Created)
DT   01-FEB-1991 (Rel. 17, Last sequence update)
DT   16-OCT-2001 (Rel. 40, Last annotation update)
DE   28 kDa ribonucleoprotein, chloroplast precursor (28RNP).
OS   Nicotiana sylvestris (Wood tobacco).
OC   Eukaryota; Viridiplantae; Streptophyta; Embryophyta; Tracheophyta;
OC   Spermatophyta; Magnoliophyta; eudicotyledons; core eudicots;
OC   Asteridae; euasterids I; Solanales; Solanaceae; Nicotiana.
OX   NCBI_TaxID=4096;
RN   [1]
RP   SEQUENCE FROM N.A., AND SEQUENCE OF 58-78.
RC   STRAIN=CV. BRIGHT YELLOW 4;
RX   MEDLINE=91005997; PubMed=1698606;
RA   Li Y., Sugiura M.;
RT   "Three distinct ribonucleoproteins from tobacco chloroplasts: each
RT   contains a unique amino terminal acidic domain and two
RT   ribonucleoprotein consensus motifs.";
RL   EMBO J. 9:3059-3066(1990).
CC   -!- FUNCTION: PROBABLY INVOLVED IN THE 3'END PROCESSING OF CHLOROPLAST
CC       MRNA'S.
CC   -!- SUBCELLULAR LOCATION: Chloroplast.
CC   -!- SIMILARITY: STRONG, TO PROTEIN X.
CC   -!- SIMILARITY: CONTAINS 2 RNA RECOGNITION MOTIFS (RRM).
CC   -!- SIMILARITY: CONTAINS 3 ABL DOMAINS.
CC   -!- SIMILARITY: IN THE CENTRAL SECTION; BELONGS TO THE RRM FAMILY.
DR   EMBL; X53933; CAA37880.1; -.
DR   PIR; S12109; S12109.
DR   HSSP; P09651; 1UP1.
DR   InterPro; IPR000504; RRM.
DR   Pfam; PF00076; rrm; 2.
DR   SMART; SM00360; RRM; 2.
DR   PROSITE; PS50102; RRM; 2.
DR   PROSITE; PS00030; RRM_RNP_1; 2.
KW   RNA-binding; Ribonucleoprotein; Repeat; mRNA processing; Chloroplast;
KW   Transit peptide.
FT   TRANSIT       1     57       CHLOROPLAST.
FT   CHAIN        58    276       28 kDa RIBONUCLEOPROTEIN.
FT   COMPBIAS     58     96       ASP/GLU-RICH (ACIDIC).
FT   COMPBIAS     92    100       ASP/GLU-RICH (ACIDIC).
FT   COMPBIAS     92   >100       ALA-RICH.
FT   COMPBIAS     80     85       COILED COIL.
FT   DOMAIN       97    175       RNA-BINDING (RRM) 1.
FT   DOMAIN      191    269       RNA-BINDING (RRM) 2.
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   276 AA;  30699 MW;  48FF3DED534133BA CRC64;
     MATNGCLISL PPFFTTTKSI SSYPFLSTQL KPISLSSSLP TLLSLNKRTT QFPTFVSVLS
     EDDNTLVLDD QEQGGDFPSF VGEAGETEEY QEPSEDAKLF VGNLPYDIDS EGLAQLFQQA
     GVVEIAEVIY NRETDRSRGF GFVTMSTVEE ADKAVELYSQ YDLNGRLLTV NKAAPRGSRP
     ERAPRTFQPT YRIYVGNIPW DIDDARLEQV FSEHGKVVSA RVVFDRESGR SRGFGFVTMS
     SEAEMSEAIA NLDGQTLDGR TIRVNAAEER PRRNTY
//
ID   CA54_HUMAN     STANDARD;      PRT;  1685 AA.
AC   P29400; Q16126; Q16006;
DT   01-DEC-1992 (Rel. 24, Created)
DT   01-FEB-1994 (Rel. 28, Last sequence update)
DT   16-OCT-2001 (Rel. 40, Last annotation update)
DE   Collagen alpha 5(IV) chain precursor.
GN   COL4A5.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606;
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=94165049; PubMed=8120014;
RA   Zhou J., Leinonen A., Tryggvason K.;
RT   "Structure of the human type IV collagen COL4A5 gene.";
RL   J. Biol. Chem. 269:6608-6614(1994).
RN   [2]
RP   SEQUENCE OF 1-910 FROM N.A., AND VARIANT AS CYS-521.
RC   TISSUE=Liver, and Kidney;
RX   MEDLINE=92316923; PubMed=1352287;
RA   Zhou J., Hertz J.M., Leinonen A., Tryggvason K.;
RT   "Complete amino acid sequence of the human alpha 5 (IV) collagen
RT   chain and identification of a single-base mutation in exon 23
RT   converting glycine 521 in the collagenous domain to cysteine in an
RT   Alport syndrome patient.";
RL   J. Biol. Chem. 267:12475-12481(1992).
RN   [3]
RP   SEQUENCE OF 85-1685 FROM N.A.
RC   TISSUE=Placenta;
RX   MEDLINE=90337990; PubMed=2380186;
RA   Pihlajaniemi T., Pohjolainen E.R., Myers J.C.;
RT   "Complete primary structure of the triple-helical region and the
RT   carboxyl-terminal domain of a new type IV collagen chain, alpha
RT   5(IV).";
RL   J. Biol. Chem. 265:13758-13766(1990).
RN   [4]
RP   SEQUENCE OF 924-1685 FROM N.A.
RX   MEDLINE=91169491; PubMed=2004755;
RA   Zhou J., Hostikka S.L., Chow L.T., Tryggvason K.;
RT   "Characterization of the 3' half of the human type IV collagen alpha
RT   5 gene that is affected in the Alport syndrome.";
RL   Genomics 9:1-9(1991).
RN   [5]
RP   SEQUENCE OF 914-1685 FROM N.A.
RX   MEDLINE=90160375; PubMed=1689491;
RA   Hostikka S.L., Eddy R.L., Byers M.G., Hoeyhtyae M., Shows T.B.,
RA   Tryggvason K.;
RT   "Identification of a distinct type IV collagen alpha chain with
RT   restricted kidney distribution and assignment of its gene to the
RT   locus of X chromosome-linked Alport syndrome.";
RL   Proc. Natl. Acad. Sci. U.S.A. 87:1606-1610(1990).
RN   [6]
RP   SEQUENCE OF 1442-1471 FROM N.A.
RX   MEDLINE=90252791; PubMed=2339699;
RA   Myers J.C., Jones T.A., Pohjolainen E.R., Kadri A.S., Goddard A.D.,
RA   Sheer D., Solomon E., Pihlajaniemi T.;
RT   "Molecular cloning of alpha 5(IV) collagen and assignment of the gene
RT   to the region of the X chromosome containing the Alport syndrome
RT   locus.";
RL   Am. J. Hum. Genet. 46:1024-1033(1990).
RN   [7]
RP   SEQUENCE OF 1-20 FROM N.A.
RA   Guo C., van Damme B., Vanrenterghem Y., Devriendt K., Cassiman J.-J.,
RA   Marynen P.;
RL   Submitted (SEP-1994) to the EMBL/GenBank/DDBJ databases.
RN   [8]
RP   SEQUENCE OF 1258-1270 FROM N.A. (SPLICED FORM).
RX   MEDLINE=94133540; PubMed=8301933;
RA   Guo C., van Damme B., van Damme-Lombaerts R., van den Berghe H.,
RA   Cassiman J.-J., Marynen P.;
RT   "Differential splicing of COL4A5 mRNA in kidney and white blood
RT   cells: a complex mutation in the COL4A5 gene of an Alport patient
RT   deletes the NC1 domain.";
RL   Kidney Int. 44:1316-1321(1993).
RN   [9]
RP   REVIEW ON VARIANTS.
RX   MEDLINE=97338662; PubMed=9195222;
RA   Lemmink H.H., Schroeder C.H., Monnens L.A.H., Smeets H.J.M.;
RT   "The clinical spectrum of type IV collagen mutations.";
RL   Hum. Mutat. 9:477-499(1997).
RN   [10]
RP   VARIANT AS SER-1564.
RX   MEDLINE=91169492; PubMed=1672282;
RA   Zhou J., Barker D.F., Hostikka S.L., Gregory M.C., Atkin C.L.,
RA   Tryggvason K.;
RT   "Single base mutation in alpha 5(IV) collagen chain gene converting a
RT   conserved cysteine to serine in Alport syndrome.";
RL   Genomics 9:10-18(1991).
RN   [11]
RP   VARIANT AS ARG-325.
RX   MEDLINE=92303559; PubMed=1376965;
RA   Knebelmann B., Deschenes G., Gros F., Hors M.-C., Gruenfeld J.-P.,
RA   Tryggvason K., Gubler M.-C., Antignac C.;
RT   "Substitution of arginine for glycine 325 in the collagen alpha 5
RT   (IV) chain associated with X-linked Alport syndrome: characterization
RT   of the mutation by direct sequencing of PCR-amplified lymphoblast
RT   cDNA fragments.";
RL   Am. J. Hum. Genet. 51:135-142(1992).
RN   [12]
RP   VARIANT AS GLU-325.
RX   MEDLINE=93244772; PubMed=1363780;
RA   Renieri A., Seri M., Myers J.C., Pihlajaniemi T., Massella L.,
RA   Rizzoni G.F., de Marchi M.;
RT   "De novo mutation in the COL4A5 gene converting glycine 325 to
RT   glutamic acid in Alport syndrome.";
RL   Hum. Mol. Genet. 1:127-129(1992).
RN   [13]
RP   VARIANTS AS THR-1517; SER-1538 AND GLN-1563.
RX   MEDLINE=94010948; PubMed=8406498;
RA   Lemmink H.L., Schroeder C.H., Brunner H.G., Nelen M.R., Zhou J.,
RA   Tryggvason K., Haggsma-Schouten W.A.G., Roodvoets A.P., Rascher W.,
RA   van Oost B.A., Smeets H.J.M.;
RT   "Identification of four novel mutations in the COL4A5 gene of
RT   patients with Alport syndrome.";
RL   Genomics 17:485-489(1993).
RN   [14]
RP   VARIANTS AS E-400;V-406;V-638;A-638;R-653;R-796;R-869;R-872 & C-1241.
RX   MEDLINE=95322976; PubMed=7599631;
RA   Boye E., Flinter F., Zhou J., Tryggvason K., Bobrow M., Harris A.;
RT   "Detection of 12 novel mutations in the collagenous domain of the
RT   COL4A5 gene in Alport syndrome patients.";
RL   Hum. Mutat. 5:197-204(1995).
RN   [15]
RP   VARIANT AS ARG-1649.
RX   MEDLINE=96213750; PubMed=8651292;
RA   Barker D.F., Pruchno C.J., Jiang X., Atkin C.L., Stone E.M.,
RA   Denison J.C., Fain P.R., Gregory M.C.;
RT   "A mutation causing Alport syndrome with tardive hearing loss is
RT   common in the western United States.";
RL   Am. J. Hum. Genet. 58:1157-1165(1996).
RN   [16]
RP   VARIANTS AS.
RX   MEDLINE=96213754; PubMed=8651296;
RA   Renieri A., Bruttini M., Galli L., Zanelli P., Neri T.M., Rossetti S.,
RA   Turco A.E., Heiskari N., Zhou J., Gusmano R., Massella L., Banfi G.,
RA   Scolari F., Sessa A., Rizzoni G.F., Tryggvason K., Pignatti P.F.,
RA   Savi M., Ballabio A., de Marchi M.;
RT   "X-linked Alport syndrome: an SSCP-based mutation survey over all 51
RT   exons of the COL4A5 gene.";
RL   Am. J. Hum. Genet. 58:1192-1204(1996).
RN   [17]
RP   VARIANTS AS, AND VARIANTS ASP-430;SER-444;SER-619;ASN-664 & MET-1428.
RX   MEDLINE=97094179; PubMed=8940267;
RA   Knebelmann B., Breillat C., Forestier L., Arrondel C., Jacassier D.,
RA   Giatras I., Drouot L., Deschenes G., Gruenfeld J.-P., Broyer M.,
RA   Gubler M.-C., Antignac C.;
RT   "Spectrum of mutations in the COL4A5 collagen gene in X-linked Alport
RT   syndrome.";
RL   Am. J. Hum. Genet. 59:1221-1232(1996).
RN   [18]
RP   VARIANT AS ASP-1498.
RX   MEDLINE=96233932; PubMed=8829632;
RA   Tverskaya S., Bobrynina V., Tsalykova F., Ignatova M.,
RA   Krasnopolskaya X., Evgrafov O.;
RT   "Substitution of A1498D in noncollagen domain of a5(IV) collagen
RT   chain associated with adult-onset X-linked Alport syndrome.";
RL   Hum. Mutat. 7:149-150(1996).
RN   [19]
RP   VARIANT AS GLN-1677.
RX   MEDLINE=97295089; PubMed=9150741;
RA   Barker D.F., Denison J.C., Atkin C.L., Gregory M.C.;
RT   "Common ancestry of three Ashkenazi-American families with Alport
RT   syndrome and COL4A5 R1677Q.";
RL   Hum. Genet. 99:681-684(1997).
RN   [20]
RP   VARIANTS AS R-174; R-177; R-325; C-1410; W-1421; T-1517 AND D-1596.
RX   MEDLINE=98112435; PubMed=9452056;
RA   Neri T.M., Zanelli P., de Palma G., Savi M., Rossetti S., Turco A.E.,
RA   Pignatti G.F., Galli L., Bruttini M., Renieri A., Mingarelli R.,
RA   Trivelli A., Pinciaroli A.R., Ragaiolo M., Rizzoni G.F., de Marchi M.;
RT   "Missense mutations in the COL4A5 gene in patients with X-linked
RT   Alport syndrome.";
RL   Hum. Mutat. Suppl. 1:S106-S109(1998).
RN   [21]
RP   VARIANTS AS.
**   VARIANTS AS V-420; 456-P-G-P-458 DEL; D-573; D-624; D-635; 802-G--P-
**   807 DEL; R-869; C-941; S-1030; S-1066; D-1143; R-1196; E-1261; S-1357
**   AND R-1649.
RX   MEDLINE=99063529; PubMed=9848783;
RA   Martin P., Heiskari N., Zhou J., Leinonen A., Tumelius T., Hertz J.M.,
RA   Barker D.F., Gregory M.C., Atkin C.L., Styrkarsdottir U., Neumann H.,
RA   Springate J., Shows T.B., Pettersson E., Tryggvason K.;
RT   "High mutation detection rate in the COL4A5 collagen gene in suspected
RT   Alport syndrome using PCR and direct DNA sequencing.";
RL   J. Am. Soc. Nephrol. 9:2291-2301(1998).
RN   [22]
RP   VARIANTS AS GLU-579; LYS-633; ASP-947; VAL-953; ARG-1107; ARG-1158;
RP   SER-1170 AND TRP-1678, AND VARIANTS SER-444 AND ALA-739.
RX   MEDLINE=20030197; PubMed=10561141;
RA   Inoue Y., Nishio H., Shirakawa T., Nakanishi K., Nakamura H.,
RA   Sumino K., Nishiyama K., Iijima K., Yoshikawa N.;
RT   "Detection of mutations in the COL4A5 gene in over 90% of male
RT   patients with X-linked Alport's syndrome by RT-PCR and direct
RT   sequencing.";
RL   Am. J. Kidney Dis. 34:854-862(1999).
RN   [23]
RP   VARIANT AS ARG-822.
RX   MEDLINE=20025011; PubMed=10563487;
RA   Cruz-Robles D., Garcia-Torres R., Antignac C., Forestier L.,
RA   Garcia de la Puente S., Correa-Rotter R., Garcia-Lopez E., Orozco L.;
RT   "Three novel mutations in the COL4A5 gene in Mexican Alport syndrome
RT   patients.";
RL   Clin. Genet. 56:242-243(1999).
RN   [24]
RP   VARIANTS AS, AND VARIANTS.
RX   MEDLINE=99140256; PubMed=10094548;
RA   Plant K.E., Green P.M., Vetrie D., Flinter F.A.;
RT   "Detection of mutations in COL4A5 in patients with Alport syndrome.";
RL   Hum. Mutat. 13:124-132(1999).
RN   [25]
RP   VARIANT AS CYS-177.
RX   MEDLINE=20460632; PubMed=11004279;
RA   Blasi M.A., Rinaldi R., Renieri A., Petrucci R., De Bernardo C.,
RA   Bruttini M., Grammatico P.;
RT   "Dot-and-fleck retinopathy in Alport syndrome caused by a novel
RT   mutation in the COL4A5 gene.";
RL   Am. J. Ophthalmol. 130:130-131(2000).
RN   [26]
RP   VARIANTS AS R-216; R-415; E-1045; D-1086; S-1167 AND 864-S--G-875 DEL.
RX   MEDLINE=20321306; PubMed=10862091;
RA   Martin P., Heiskari N., Pajari H., Groenhagen-Riska C.,
RA   Kaeaeriaeinen H., Koskimies O., Tryggvason K.;
RT   "Spectrum of COL4A5 mutations in Finnish Alport syndrome patients.";
RL   Hum. Mutat. 15:579-579(2000).
RN   [27]
RP   VARIANTS AS ARG-319;SER-739;VAL-902;GLU-911;ASP-1229 AND HIS-1511.
RX   MEDLINE=20148403; PubMed=10684360;
RA   Cheong H.I., Park H.W., Ha I.S., Choi Y.;
RT   "Mutational analysis of COL4A5 gene in Korean Alport syndrome.";
RL   Pediatr. Nephrol. 14:117-121(2000).
RN   [28]
RP   VARIANTS AS R-192; R-292; D-295; R-325; R-558; V-
RP   603; D-624; D-629;
RP   E-722; V-898; A-1006; V-1006; D-1244; R-1649 AND P-1677, AND VARIANT S-444.
RX   MEDLINE=21123908; PubMed=11223851;
RA   Barker D.F., Denison J.C., Atkin C.L., Gregory M.C.;
RT   "Efficient detection of Alport syndrome COL4A5 mutations with
RT   multiplex genomic PCR-SSCP.";
RL   Am. J. Med. Genet. 98:148-160(2001).
CC   -!- FUNCTION: TYPE IV COLLAGEN IS THE MAJOR STRUCTURAL COMPONENT OF
CC       GLOMERULAR BASEMENT MEMBRANES (GBM), FORMING A 'CHICKEN-WIRE'
CC       MESHWORK TOGETHER WITH LAMININS, PROTEOGLYCANS AND ENTACTIN/
CC       NIDOGEN.
CC   -!- SUBUNIT: THERE ARE SIX TYPE IV COLLAGEN ISOFORMS, ALPHA 1(IV)-
CC       ALPHA 6(IV), EACH OF WHICH CAN FORM A TRIPLE HELIX STRUCTURE
CC       WITH 2 OTHER CHAINS TO GENERATE TYPE IV COLLAGEN NETWORK.
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing; Named isoforms=2;
CC       Name=1;
CC         IsoId=P29400-1; Sequence=Displayed;
CC       Name=2;
CC         IsoId=P29400-2; Sequence=VSP_00759;
CC         Note=Longer found in kidney, in which 2 extra G-X-X repeats into
CC         the triple-helix domain are introduced;
CC   -!- DOMAIN: ALPHA CHAINS OF TYPE IV COLLAGEN HAVE A NONCOLLAGENOUS
CC       DOMAIN (NC1) AT THEIR C-TERMINUS, FREQUENT INTERRUPTIONS OF THE
CC       G-X-Y REPEATS IN THE LONG CENTRAL TRIPLE-HELICAL DOMAIN (WHICH MAY
CC       CAUSE FLEXIBILITY IN THE TRIPLE HELIX), AND A SHORT N-TERMINAL
CC       TRIPLE-HELICAL 7S DOMAIN.
CC   -!- PTM: PROLINES AT THE THIRD POSITION OF THE TRIPEPTIDE REPEATING
CC       UNIT (G-X-Y) ARE HYDROXYLATED IN SOME OR ALL OF THE CHAINS.
CC   -!- PTM: TYPE IV COLLAGENS CONTAIN NUMEROUS CYSTEINE RESIDUES WHICH
CC       ARE INVOLVED IN INTER- AND INTRAMOLECULAR DISULFIDE BONDING. 12 OF
CC       THESE, LOCATED IN THE NC1 DOMAIN, ARE CONSERVED IN ALL KNOWN TYPE
CC       IV COLLAGENS.
CC   -!- DISEASE: DEFECTS IN COL4A5 ARE A CAUSE OF X-LINKED ALPORT SYNDROME
CC       (AS). ALPORT SYNDROME IS CHARACTERIZED BY PROGRESSIVE
CC       GLOMERULONEPHRITIS, OFTEN ASSOCIATED WITH HIGH-TONE SENSORINEURAL
CC       DEAFNESS, SPECIFIC EYE ABNORMALITIES (LENTICONOUS AND MACULAR
CC       FLECKS), AND GLOMERULAR BASEMENT MEMBRANE DEFECTS. IN MALES, THE
CC       TYPICAL TIME COURSE FOR THE PROGRESS OF ALPORT SYNDROME IS:
CC       HEMATURIA BY THE AGE OF 5 YEARS, DEAFNESS AND HYPERTENSION IN
CC       EARLY TEENAGE LIFE, DETERIORATION OF RENAL FUNCTION BY AGE 20, AND
CC       END-STAGE RENAL FAILURE SOON THEREAFTER. FEMALES TEND TO FOLLOW A
CC       MUCH MILDER COURSE AND RARELY GO INTO RENAL FAILURE.
DR   EMBL; M58526; AAA99480.1; -.
DR   EMBL; M90464; AAA52046.1; -.
DR   EMBL; U04520; AAC27816.1; -.
DR   EMBL; U04470; AAC27816.1; JOINED.
DR   EMBL; U04471; AAC27816.1; JOINED.
DR   EMBL; U04472; AAC27816.1; JOINED.
DR   EMBL; U04473; AAC27816.1; JOINED.
DR   EMBL; U04474; AAC27816.1; JOINED.
DR   EMBL; U04476; AAC27816.1; JOINED.
DR   EMBL; U04477; AAC27816.1; JOINED.
DR   EMBL; U04478; AAC27816.1; JOINED.
DR   EMBL; U04479; AAC27816.1; JOINED.
DR   EMBL; U04480; AAC27816.1; JOINED.
DR   EMBL; U04483; AAC27816.1; JOINED.
DR   EMBL; U04485; AAC27816.1; JOINED.
DR   EMBL; U04486; AAC27816.1; JOINED.
DR   EMBL; U04487; AAC27816.1; JOINED.
DR   EMBL; U04488; AAC27816.1; JOINED.
DR   EMBL; U04489; AAC27816.1; JOINED.
DR   EMBL; U04490; AAC27816.1; JOINED.
DR   EMBL; U04491; AAC27816.1; JOINED.
DR   EMBL; U04492; AAC27816.1; JOINED.
DR   EMBL; U04493; AAC27816.1; JOINED.
DR   EMBL; U04494; AAC27816.1; JOINED.
DR   EMBL; U04495; AAC27816.1; JOINED.
DR   EMBL; U04496; AAC27816.1; JOINED.
DR   EMBL; U04497; AAC27816.1; JOINED.
DR   EMBL; U04498; AAC27816.1; JOINED.
DR   EMBL; U04499; AAC27816.1; JOINED.
DR   EMBL; U04500; AAC27816.1; JOINED.
DR   EMBL; U04501; AAC27816.1; JOINED.
DR   EMBL; U04502; AAC27816.1; JOINED.
DR   EMBL; U04503; AAC27816.1; JOINED.
DR   EMBL; U04504; AAC27816.1; JOINED.
DR   EMBL; U04505; AAC27816.1; JOINED.
DR   EMBL; U04506; AAC27816.1; JOINED.
DR   EMBL; U04507; AAC27816.1; JOINED.
DR   EMBL; U04508; AAC27816.1; JOINED.
DR   EMBL; U04509; AAC27816.1; JOINED.
DR   EMBL; U04510; AAC27816.1; JOINED.
DR   EMBL; U04511; AAC27816.1; JOINED.
DR   EMBL; U04512; AAC27816.1; JOINED.
DR   EMBL; U04514; AAC27816.1; JOINED.
DR   EMBL; U04515; AAC27816.1; JOINED.
DR   EMBL; U04516; AAC27816.1; JOINED.
DR   EMBL; U04517; AAC27816.1; JOINED.
DR   EMBL; U04518; AAC27816.1; JOINED.
DR   EMBL; U04519; AAC27816.1; JOINED.
DR   EMBL; U04520; AAF66217.2; -.
DR   EMBL; U04470; AAF66217.2; JOINED.
DR   EMBL; U04471; AAF66217.2; JOINED.
DR   EMBL; U04472; AAF66217.2; JOINED.
DR   EMBL; U04473; AAF66217.2; JOINED.
DR   EMBL; U04474; AAF66217.2; JOINED.
DR   EMBL; U04476; AAF66217.2; JOINED.
DR   EMBL; U04477; AAF66217.2; JOINED.
DR   EMBL; U04478; AAF66217.2; JOINED.
DR   EMBL; U04479; AAF66217.2; JOINED.
DR   EMBL; U04480; AAF66217.2; JOINED.
DR   EMBL; U04483; AAF66217.2; JOINED.
DR   EMBL; U04485; AAF66217.2; JOINED.
DR   EMBL; U04486; AAF66217.2; JOINED.
DR   EMBL; U04487; AAF66217.2; JOINED.
DR   EMBL; U04488; AAF66217.2; JOINED.
DR   EMBL; U04489; AAF66217.2; JOINED.
DR   EMBL; U04490; AAF66217.2; JOINED.
DR   EMBL; U04491; AAF66217.2; JOINED.
DR   EMBL; U04492; AAF66217.2; JOINED.
DR   EMBL; U04493; AAF66217.2; JOINED.
DR   EMBL; U04494; AAF66217.2; JOINED.
DR   EMBL; U04495; AAF66217.2; JOINED.
DR   EMBL; U04496; AAF66217.2; JOINED.
DR   EMBL; U04497; AAF66217.2; JOINED.
DR   EMBL; U04498; AAF66217.2; JOINED.
DR   EMBL; U04499; AAF66217.2; JOINED.
DR   EMBL; U04500; AAF66217.2; JOINED.
DR   EMBL; U04501; AAF66217.2; JOINED.
DR   EMBL; U04502; AAF66217.2; JOINED.
DR   EMBL; U04503; AAF66217.2; JOINED.
DR   EMBL; U04504; AAF66217.2; JOINED.
DR   EMBL; U04505; AAF66217.2; JOINED.
DR   EMBL; U04506; AAF66217.2; JOINED.
DR   EMBL; U04507; AAF66217.2; JOINED.
DR   EMBL; U04508; AAF66217.2; JOINED.
DR   EMBL; U04509; AAF66217.2; JOINED.
DR   EMBL; U04510; AAF66217.2; JOINED.
DR   EMBL; AF199451; AAF66217.2; JOINED.
DR   EMBL; AF199452; AAF66217.2; JOINED.
DR   EMBL; U04511; AAF66217.2; JOINED.
DR   EMBL; U04512; AAF66217.2; JOINED.
DR   EMBL; U04514; AAF66217.2; JOINED.
DR   EMBL; U04515; AAF66217.2; JOINED.
DR   EMBL; U04516; AAF66217.2; JOINED.
DR   EMBL; U04517; AAF66217.2; JOINED.
DR   EMBL; U04518; AAF66217.2; JOINED.
DR   EMBL; U04519; AAF66217.2; JOINED.
DR   EMBL; M63473; AAA51558.1; -.
DR   EMBL; M63455; AAA51558.1; JOINED.
DR   EMBL; M63456; AAA51558.1; JOINED.
DR   EMBL; M63457; AAA51558.1; JOINED.
DR   EMBL; M63458; AAA51558.1; JOINED.
DR   EMBL; M63459; AAA51558.1; JOINED.
DR   EMBL; M63460; AAA51558.1; JOINED.
DR   EMBL; M63461; AAA51558.1; JOINED.
DR   EMBL; M63462; AAA51558.1; JOINED.
DR   EMBL; M63463; AAA51558.1; JOINED.
DR   EMBL; M63464; AAA51558.1; JOINED.
DR   EMBL; M63465; AAA51558.1; JOINED.
DR   EMBL; M63466; AAA51558.1; JOINED.
DR   EMBL; M63467; AAA51558.1; JOINED.
DR   EMBL; M63468; AAA51558.1; JOINED.
DR   EMBL; M63470; AAA51558.1; JOINED.
DR   EMBL; M63471; AAA51558.1; JOINED.
DR   EMBL; M63472; AAA51558.1; JOINED.
DR   EMBL; M31115; AAA52045.1; -.
DR   EMBL; Z37153; CAA85512.1; -.
DR   EMBL; S69168; AAC60612.1; -.
DR   EMBL; S59334; AAD13909.1; -.
DR   PIR; S22917; S22917.
DR   MIM; 303630; -.
DR   MIM; 301050; -.
DR   InterPro; IPR001442; C4.
DR   InterPro; IPR000087; Collagen.
DR   Pfam; PF01413; C4; 2.
DR   Pfam; PF01391; Collagen; 21.
DR   ProDom; PD003923; C4; 2.
DR   SMART; SM00111; C4; 2.
KW   Extracellular matrix; Connective tissue; Basement membrane;
KW   Repeat; Hydroxylation; Collagen; Signal; Disease mutation;
KW   Polymorphism; Alternative splicing; Alport syndrome.
FT   SIGNAL        1     26       POTENTIAL.
FT   CHAIN        27   1685       COLLAGEN ALPHA 5(IV) CHAIN.
FT   DOMAIN       27     41       NONHELICAL REGION (NC2).
FT   DOMAIN       42   1456       TRIPLE-HELICAL REGION.
FT   DOMAIN     1457   1685       NONHELICAL REGION (NC1).
FT   DOMAIN     1457   1568       REPEAT NC1-1.
FT   DOMAIN     1569   1685       REPEAT NC1-2.
FT   DISULFID    451    451       INTERCHAIN (POTENTIAL).
FT   DISULFID    481    481       INTERCHAIN (POTENTIAL).
FT   DISULFID    484    484       INTERCHAIN (POTENTIAL).
FT   DISULFID   1476   1567       OR 1564 (BY SIMILARITY).
FT   DISULFID   1509   1564       OR 1567 (BY SIMILARITY).
FT   DISULFID   1521   1527       BY SIMILARITY.
FT   DISULFID   1586   1681       OR 1678 (BY SIMILARITY).
FT   DISULFID   1620   1678       OR 1681 (BY SIMILARITY).
FT   DISULFID   1632   1638       BY SIMILARITY.
FT   CARBOHYD    125    125       N-LINKED (GLCNAC...) (POTENTIAL).
FT   VAR_SEQ    1264   1264       G -> GPTGFQG (in isoform 2).
FT                                /FTId=VSP_00759.
FT   VARIANT      54     54       G -> D (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001914.
FT   VARIANT     114    114       G -> S (IN AS).
FT                                /FTId=VAR_007991.
FT   VARIANT     129    129       G -> E (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001915.
FT   VARIANT     129    129       G -> V (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001916.
FT   VARIANT     174    174       G -> R (IN AS).
FT                                /FTId=VAR_001917.
FT   VARIANT     177    177       G -> C (IN AS WITH DOT-AND-FLECK
FT                                RETINOPATHY).
FT                                /FTId=VAR_011220.
FT   VARIANT     177    177       G -> R (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001918.
FT   VARIANT     192    192       G -> R (IN AS).
FT                                /FTId=VAR_011221.
FT   VARIANT     204    204       G -> V (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011222.
FT   VARIANT     216    216       G -> R (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001919.
FT   VARIANT     219    219       G -> S (IN AS).
FT                                /FTId=VAR_001920.
FT   VARIANT     230    230       G -> R (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011223.
FT   VARIANT     239    239       G -> E (IN AS).
FT                                /FTId=VAR_011224.
FT   VARIANT     264    264       G -> R (IN AS; ADULT TYPE).
FT                                /FTId=VAR_011225.
FT   VARIANT     289    289       G -> V (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001921.
FT   VARIANT     292    292       G -> R (IN AS).
FT                                /FTId=VAR_011226.
FT   VARIANT     292    292       G -> V (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001922.
FT   VARIANT     295    295       G -> D (IN AS).
FT                                /FTId=VAR_011227.
FT   VARIANT     298    298       G -> S (IN AS).
FT                                /FTId=VAR_011228.
FT   VARIANT     319    319       G -> R (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011229.
FT   VARIANT     325    325       G -> E (IN AS).
FT                                /FTId=VAR_001923.
FT   VARIANT     325    325       G -> R (IN AS; JUVENILE AND ADULT TYPES).
FT                                /FTId=VAR_001924.
FT   VARIANT     331    331       G -> V (IN AS).
FT                                /FTId=VAR_007992.
FT   VARIANT     365    365       G -> E (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001925.
FT   VARIANT     365    367       MISSING (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001926.
FT   VARIANT     371    371       G -> E (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001927.
FT   VARIANT     374    374       G -> A (IN AS).
FT                                /FTId=VAR_001928.
FT   VARIANT     383    383       G -> D (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001929.
FT   VARIANT     400    400       G -> E (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001930.
FT   VARIANT     406    406       G -> V (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001931.
FT   VARIANT     409    409       G -> D (IN AS).
FT                                /FTId=VAR_001932.
FT   VARIANT     412    412       G -> V (IN AS; ADULT TYPE).
FT                                /FTId=VAR_011230.
FT   VARIANT     415    415       G -> R (IN AS).
FT                                /FTId=VAR_011231.
FT   VARIANT     420    420       G -> E (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011232.
FT   VARIANT     420    420       G -> V (IN AS).
FT                                /FTId=VAR_011233.
FT   VARIANT     423    423       G -> E (IN AS).
FT                                /FTId=VAR_011234.
FT   VARIANT     430    430       A -> D.
FT                                /FTId=VAR_001933.
FT   VARIANT     444    444       I -> S.
FT                                /FTId=VAR_001934.
FT   VARIANT     456    458       MISSING (IN AS).
FT                                /FTId=VAR_001935.
FT   VARIANT     466    466       G -> E (IN AS).
FT                                /FTId=VAR_001936.
FT   VARIANT     472    472       G -> R (IN AS).
FT                                /FTId=VAR_007993.
FT   VARIANT     491    491       G -> E (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011235.
FT   VARIANT     494    494       G -> D (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001937.
FT   VARIANT     496    507       MISSING (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001938.
FT   VARIANT     497    497       G -> C (IN AS; ADULT TYPE).
FT                                /FTId=VAR_011236.
FT   VARIANT     521    521       G -> C (IN AS).
FT                                /FTId=VAR_001939.
FT   VARIANT     521    521       G -> S (IN AS).
FT                                /FTId=VAR_001940.
FT   VARIANT     524    524       G -> D (IN AS; ADULT TYPE).
FT                                /FTId=VAR_011237.
FT   VARIANT     545    545       G -> R (IN AS).
FT                                /FTId=VAR_007994.
FT   VARIANT     545    545       G -> V (IN AS).
FT                                /FTId=VAR_007995.
FT   VARIANT     558    558       G -> R (IN AS).
FT                                /FTId=VAR_011238.
FT   VARIANT     561    561       G -> R (IN AS).
FT                                /FTId=VAR_007996.
FT   VARIANT     567    567       G -> A (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001941.
FT   VARIANT     573    573       G -> D (IN AS).
FT                                /FTId=VAR_011239.
FT   VARIANT     579    579       G -> E (IN AS).
FT                                /FTId=VAR_011240.
FT   VARIANT     579    579       G -> R (IN AS; ADULT TYPE).
FT                                /FTId=VAR_007997.
FT   VARIANT     603    603       G -> V (IN AS).
FT                                /FTId=VAR_011241.
FT   VARIANT     609    609       G -> R (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011242.
FT   VARIANT     609    609       G -> V (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001942.
FT   VARIANT     619    619       P -> S.
FT                                /FTId=VAR_011243.
FT   VARIANT     621    621       G -> C (IN AS).
FT                                /FTId=VAR_011244.
FT   VARIANT     624    624       G -> D (IN AS).
FT                                /FTId=VAR_011245.
FT   VARIANT     629    629       G -> D (IN AS).
FT                                /FTId=VAR_011246.
FT   VARIANT     632    632       G -> D (IN AS).
FT                                /FTId=VAR_011247.
FT   VARIANT     633    633       E -> K (IN AS).
FT                                /FTId=VAR_011248.
FT   VARIANT     635    635       G -> D (IN AS).
FT                                /FTId=VAR_007998.
FT   VARIANT     638    638       G -> A (IN AS).
FT                                /FTId=VAR_001944.
FT   VARIANT     638    638       G -> S (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_007999.
FT   VARIANT     638    638       G -> V (IN AS).
FT                                /FTId=VAR_001943.
FT   VARIANT     653    653       G -> R (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001945.
FT   VARIANT     664    664       K -> N.
FT                                /FTId=VAR_001946.
FT   VARIANT     669    669       G -> A (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_008000.
FT   VARIANT     681    681       G -> D (IN AS).
FT                                /FTId=VAR_011249.
FT   VARIANT     684    684       G -> V (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001947.
FT   VARIANT     687    687       G -> E (IN AS).
FT                                /FTId=VAR_008001.
FT   VARIANT     722    722       G -> E (IN AS).
FT                                /FTId=VAR_011250.
FT   VARIANT     739    739       P -> A.
FT                                /FTId=VAR_011251.
FT   VARIANT     739    739       P -> S (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011252.
FT   VARIANT     740    740       G -> E (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001948.
FT   VARIANT     743    743       G -> D (IN AS).
FT                                /FTId=VAR_008002.
FT   VARIANT     772    772       G -> D (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001949.
FT   VARIANT     796    796       G -> R (IN AS).
FT                                /FTId=VAR_001950.
FT   VARIANT     802    802       G -> R (IN AS).
FT                                /FTId=VAR_011253.
FT   VARIANT     802    807       MISSING (IN AS).
FT                                /FTId=VAR_011254.
FT   VARIANT     808    808       G -> E (IN AS; ADULT TYPE).
FT                                /FTId=VAR_008003.
FT   VARIANT     811    811       G -> V (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011255.
FT   VARIANT     822    822       G -> R (IN AS).
FT                                /FTId=VAR_011256.
FT   VARIANT     822    824       MISSING (IN AS).
FT                                /FTId=VAR_008004.
FT   VARIANT     852    852       G -> E (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_008005.
FT   VARIANT     852    852       G -> R (IN AS).
FT                                /FTId=VAR_001951.
FT   VARIANT     864    875       MISSING (IN AS).
FT                                /FTId=VAR_011257.
FT   VARIANT     866    866       G -> E (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001952.
FT   VARIANT     869    869       G -> R (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001953.
FT   VARIANT     872    872       G -> R (IN AS).
FT                                /FTId=VAR_001954.
FT   VARIANT     878    878       G -> R (IN AS).
FT                                /FTId=VAR_008006.
FT   VARIANT     898    898       M -> V (IN AS; MILD PHENOTYPE).
FT                                /FTId=VAR_011258.
FT   VARIANT     902    902       G -> V (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011259.
FT   VARIANT     911    911       G -> E (IN AS).
FT                                /FTId=VAR_011260.
FT   VARIANT     941    941       G -> C (IN AS).
FT                                /FTId=VAR_011261.
FT   VARIANT     942    942       MISSING (IN AS).
FT                                /FTId=VAR_001955.
FT   VARIANT     947    947       G -> D (IN AS).
FT                                /FTId=VAR_011262.
FT   VARIANT     953    953       G -> V (IN AS; FOUND ON THE SAME ALLELE
FT                                AS VARIANT E-1211).
FT                                /FTId=VAR_011263.
FT   VARIANT     988    992       MISSING (IN AS; ADULT TYPE).
FT                                /FTId=VAR_008007.
FT   VARIANT    1006   1006       G -> A (IN AS).
FT                                /FTId=VAR_011264.
FT   VARIANT    1006   1006       G -> V (IN AS).
FT                                /FTId=VAR_011265.
FT   VARIANT    1015   1015       G -> E (IN AS).
FT                                /FTId=VAR_011266.
FT   VARIANT    1015   1015       G -> V (IN AS).
FT                                /FTId=VAR_011267.
FT   VARIANT    1030   1030       G -> S (IN AS).
FT                                /FTId=VAR_011268.
FT   VARIANT    1036   1036       G -> V (IN AS).
FT                                /FTId=VAR_011269.
FT   VARIANT    1039   1039       G -> S (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011270.
FT   VARIANT    1045   1045       G -> E (IN AS).
FT                                /FTId=VAR_011271.
FT   VARIANT    1066   1066       G -> R (IN AS).
FT                                /FTId=VAR_011272.
FT   VARIANT    1066   1066       G -> S (IN AS).
FT                                /FTId=VAR_011273.
FT   VARIANT    1086   1086       G -> D (IN AS).
FT                                /FTId=VAR_011274.
FT   VARIANT    1104   1104       G -> V (IN AS).
FT                                /FTId=VAR_001956.
FT   VARIANT    1107   1107       G -> R (IN AS).
FT                                /FTId=VAR_008008.
FT   VARIANT    1143   1143       G -> D (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001957.
FT   VARIANT    1143   1143       G -> S (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001958.
FT   VARIANT    1158   1158       G -> R (IN AS).
FT                                /FTId=VAR_011275.
FT   VARIANT    1161   1161       G -> R (IN AS).
FT                                /FTId=VAR_008009.
FT   VARIANT    1167   1167       G -> S (IN AS).
FT                                /FTId=VAR_011276.
FT   VARIANT    1170   1170       G -> S (IN AS).
FT                                /FTId=VAR_011277.
FT   VARIANT    1182   1182       G -> R (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001959.
FT   VARIANT    1196   1196       G -> R (IN AS).
FT                                /FTId=VAR_011278.
FT   VARIANT    1205   1205       G -> C (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011279.
FT   VARIANT    1211   1211       G -> E (IN AS; FOUND ON THE SAME ALLELE
FT                                AS VARIANT V-953).
FT                                /FTId=VAR_011280.
FT   VARIANT    1211   1211       G -> R (IN AS).
FT                                /FTId=VAR_008010.
FT   VARIANT    1220   1220       G -> D (IN AS).
FT                                /FTId=VAR_008011.
FT   VARIANT    1229   1229       G -> D (IN AS; ADULT TYPE).
FT                                /FTId=VAR_011281.
FT   VARIANT    1241   1241       G -> C (IN AS).
FT                                /FTId=VAR_001960.
FT   VARIANT    1244   1244       G -> D (IN AS).
FT                                /FTId=VAR_011282.
FT   VARIANT    1252   1252       G -> S (IN AS; ADULT TYPE).
FT                                /FTId=VAR_011283.
FT   VARIANT    1261   1261       G -> E (IN AS).
FT                                /FTId=VAR_011284.
FT   VARIANT    1270   1270       G -> S (IN AS).
FT                                /FTId=VAR_001961.
FT   VARIANT    1333   1333       G -> S (IN AS).
FT                                /FTId=VAR_008012.
FT   VARIANT    1357   1357       G -> S (IN AS).
FT                                /FTId=VAR_011285.
FT   VARIANT    1379   1379       G -> V (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001962.
FT   VARIANT    1410   1410       R -> C (IN AS; ADULT AND JUVENILE TYPES).
FT                                /FTId=VAR_001963.
FT   VARIANT    1421   1421       G -> W (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001964.
FT   VARIANT    1422   1422       R -> C (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001965.
FT   VARIANT    1427   1427       G -> V (IN AS; ADULT TYPE).
FT                                /FTId=VAR_008013.
FT   VARIANT    1428   1428       L -> M.
FT                                /FTId=VAR_011286.
FT   VARIANT    1442   1442       G -> D (IN AS).
FT                                /FTId=VAR_008014.
FT   VARIANT    1451   1451       G -> S (IN AS).
FT                                /FTId=VAR_001966.
FT   VARIANT    1486   1486       G -> A (IN AS; ADULT TYPE).
FT                                /FTId=VAR_008015.
FT   VARIANT    1488   1488       S -> F (IN AS).
FT                                /FTId=VAR_011287.
FT   VARIANT    1498   1498       A -> D (IN AS).
FT                                /FTId=VAR_001967.
FT   VARIANT    1511   1511       R -> H (IN AS; JUVENILE TYPE; COULD BE A
FT                                NON PATHOGENIC VARIANT).
FT                                /FTId=VAR_011288.
FT   VARIANT    1517   1517       P -> T (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_001968.
FT   VARIANT    1538   1538       W -> S (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001969.
FT   VARIANT    1559   1559       P -> A.
FT                                /FTId=VAR_008016.
FT   VARIANT    1563   1563       R -> Q (IN AS).
FT                                /FTId=VAR_001970.
FT   VARIANT    1564   1564       C -> S (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001971.
FT   VARIANT    1567   1567       C -> R (IN AS; JUVENILE TYPE).
FT                                /FTId=VAR_011289.
FT   VARIANT    1596   1596       G -> D (IN AS).
FT                                /FTId=VAR_001972.
FT   VARIANT    1649   1649       L -> R (IN AS; ADULT TYPE).
FT                                /FTId=VAR_001973.
FT   VARIANT    1677   1677       R -> P (IN AS).
FT                                /FTId=VAR_011290.
FT   VARIANT    1677   1677       R -> Q (IN AS).
FT                                /FTId=VAR_001974.
FT   VARIANT    1678   1678       C -> W (IN AS).
FT                                /FTId=VAR_011291.
FT   CONFLICT    440    441       AG -> GS (IN REF. 3).
FT   CONFLICT    625    628       FGPP -> LALQ (IN REF. 3).
FT   CONFLICT    667    668       LP -> FR (IN REF. 3).
FT   CONFLICT    888    888       A -> R (IN REF. 3).
**
**   #################    INTERNAL SECTION    ##################
**CL Xq22;
SQ   SEQUENCE   1685 AA;  161044 MW;  4450A6762F12A626 CRC64;
     MKLRGVSLAA GLFLLALSLW GQPAEAAACY GCSPGSKCDC SGIKGEKGER GFPGLEGHPG
     LPGFPGPEGP PGPRGQKGDD GIPGPPGPKG IRGPPGLPGF PGTPGLPGMP GHDGAPGPQG
     IPGCNGTKGE RGFPGSPGFP GLQGPPGPPG IPGMKGEPGS IIMSSLPGPK GNPGYPGPPG
     IQGLPGPTGI PGPIGPPGPP GLMGPPGPPG LPGPKGNMGL NFQGPKGEKG EQGLQGPPGP
     PGQISEQKRP IDVEFQKGDQ GLPGDRGPPG PPGIRGPPGP PGGEKGEKGE QGEPGKRGKP
     GKDGENGQPG IPGLPGDPGY PGEPGRDGEK GQKGDTGPPG PPGLVIPRPG TGITIGEKGN
     IGLPGLPGEK GERGFPGIQG PPGLPGPPGA AVMGPPGPPG FPGERGQKGD EGPPGISIPG
     PPGLDGQPGA PGLPGPPGPA GPHIPPSDEI CEPGPPGPPG SPGDKGLQGE QGVKGDKGDT
     CFNCIGTGIS GPPGQPGLPG LPGPPGSLGF PGQKGEKGQA GATGPKGLPG IPGAPGAPGF
     PGSKGEPGDI LTFPGMKGDK GELGSPGAPG LPGLPGTPGQ DGLPGLPGPK GEPGGITFKG
     ERGPPGNPGL PGLPGNIGPM GPPGFGPPGP VGEKGIQGVA GNPGQPGIPG PKGDPGQTIT
     QPGKPGLPGN PGRDGDVGLP GDPGLPGQPG LPGIPGSKGE PGIPGIGLPG PPGPKGFPGI
     PGPPGAPGTP GRIGLEGPPG PPGFPGPKGE PGFALPGPPG PPGLPGFKGA LGPKGDRGFP
     GPPGPPGRTG LDGLPGPKGD VGPNGQPGPM GPPGLPGIGV QGPPGPPGIP GPIGQPGLHG
     IPGEKGDPGP PGLDVPGPPG ERGSPGIPGA PGPIGPPGSP GLPGKAGASG FPGTKGEMGM
     MGPPGPPGPL GIPGRSGVPG LKGDDGLQGQ PGLPGPTGEK GSKGEPGLPG PPGPMDPNLL
     GSKGEKGEPG LPGIPGVSGP KGYQGLPGDP GQPGLSGQPG LPGPPGPKGN PGLPGQPGLI
     GPPGLKGTIG DMGFPGPQGV EGPPGPSGVP GQPGSPGLPG QKGDKGDPGI SSIGLPGLPG
     PKGEPGLPGY PGNPGIKGSV GDPGLPGLPG TPGAKGQPGL PGFPGTPGPP GPKGISGPPG
     NPGLPGEPGP VGGGGHPGQP GPPGEKGKPG QDGIPGPAGQ KGEPGQPGFG NPGPPGLPGL
     SGQKGDGGLP GIPGNPGLPG PKGEPGFHGF PGVQGPPGPP GSPGPALEGP KGNPGPQGPP
     GRPGLPGPEG PPGLPGNGGI KGEKGNPGQP GLPGLPGLKG DQGPPGLQGN PGRPGLNGMK
     GDPGLPGVPG FPGMKGPSGV PGSAGPEGEP GLIGPPGPPG LPGPSGQSII IKGDAGPPGI
     PGQPGLKGLP GPQGPQGLPG PTGPPGDPGR NGLPGFDGAG GRKGDPGLPG QPGTRGLDGP
     PGPDGLQGPP GPPGTSSVAH GFLITRHSQT TDAPQCPQGT LQVYEGFSLL YVQGNKRAHG
     QDLGTAGSCL RRFSTMPFMF CNINNVCNFA SRNDYSYWLS TPEPMPMSMQ PLKGQSIQPF
     ISRCAVCEAP AVVIAVHSQT IQIPHCPQGW DSLWIGYSFM MHTSAGAEGS GQALASPGSC
     LEEFRSAPFI ECHGRGTCNY YANSYSFWLA TVDVSDMFSK PQSETLKAGD LRTRISRCQV
     CMKRT
//
ID   O21165      PRELIMINARY;   PRT;   262 AA.
AC   O21165;
DT   01-JAN-1998 (TrEMBLrel. 05, Created)
DT   01-JAN-1998 (TrEMBLrel. 05, Last sequence update)
DT   01-JUN-2002 (TrEMBLrel. 21, Last annotation update)
DE   Putative reverse transcriptase (Fragment){EI2}.
GN   RT{EI2}.
OS   Fusarium oxysporum.
OG   Mitochondrion{EI2}.
OG   Plasmid pFOXC1{EI2}.
OC   Eukaryota; Fungi; Ascomycota; Pezizomycotina; Sordariomycetes;
OC   Hypocreales; mitosporic Hypocreales; Fusarium.
OX   NCBI_TaxID=5507{EP3};
RN   [1]{EI2}
RP   SEQUENCE FROM N.A.
RC   TISSUE=Liver{EI2}, and Kidney{EI2};
RX   MEDLINE=97394960; PubMed=9251222;
RA   Kistler H.C., Benny U., Powell W.A.;
RT   "Linear mitochondrial plasmids of Fusarium oxysporum contain genes
RT   with sequence similarity to genes encoding a reverse transcriptase
RT   from Neurospora spp.";
RL   Appl. Environ. Microbiol. 63:3311-3313(1997).
DR   EMBL; AF005240; AAD12231.1; -.{EI2}
DR   InterPro; IPR000477; RVTse.
DR   Pfam; PF00078; rvt; 1.
KW   Plasmid{EP3}; RNA-directed DNA polymerase{EP3,EA1}.
FT   NON_TER       1      1       {EI2}
FT   NON_TER     262    262       {EI2}
**
**   #################     SOURCE SECTION     ##################
**   Fusarium oxysporum plasmid pFOXC1 putative reverse transcriptase (RT) gene,
**   mitochondrial plasmid gene, partial cds.
**   [1]
**   1-785
**   MEDLINE; 97394960.
**   Kistler H.C., Benny U., Powell W.A.;
**   "Linear mitochondrial plasmids of Fusarium oxysporum contain genes with
**   sequence similarity to genes encoding a reverse transcriptase from
**   Neurospora spp";
**   Appl. Environ. Microbiol. 63(8):3311-3313(1997).
**   [2]
**   1-785
**   Kistler H.C., Benny U., Powell W.A.;
**   ;
**   Submitted (23-MAY-1997) to the EMBL/GenBank/DDBJ databases.
**   Plant Pathology, University of Florida, PO Box 110680, Gainesville, FL
**   32611-0680, USA
**   SPTREMBL; O21165; O21165.
**   source          1..785
**                   /db_xref="taxon:5507"
**                   /organelle="mitochondrion"
**                   /organism="Fusarium oxysporum"
**                   /plasmid="pFOXC1"
**   CDS             complement(<1..>785)
**                   /codon_start=1
**                   /evidence=NOT_EXPERIMENTAL
**                   /transl_table=4
**                   /gene="RT"
**                   /product="putative reverse transcriptase"
**                   /protein_id="AAD12231.1"
**   CDS_IN_EMBL_ENTRY 1
**   03-MAR-2000 (Rel. 62, Last updated, Version 3)
**   #################    INTERNAL SECTION    ##################
**EV EP3; TrEMBL; -; AAD12231.1; 30-SEP-2001.
**EV EA1; Rulebase; -; RU000322; 30-JAN-2002.
**EV EI2; EMBL; -; AAD12231.1; 30-SEP-2001.
**ID XXXX_FUSOX
**PM Pfam; PF00078; rvt; 12; 241; T; 18-MAR-2002;
SQ   SEQUENCE   262 AA;  30050 MW;  00F0D8B1DAA6A0F4 CRC64;
     RSELIEIMNQ CRAFRLTMDL KRYYLTKPNG KYRPIGSPTL GSKVISKALT DIWTTIADKR
     RGVMQHAFRP KLGVWSAAFA VCQKLRSRKP SDVIIEFDLK GFFNTIKRNS VQEAANRFSL
     LLGNCVRHII DNTRYVFEEL KPETELHIIN DYTHHKYKRA IPIYRTGVPQ GLPLSPVAAT
     IALENEVNMP EMVMYADDGI LIGGKEKFAE FVKKAIRVGA EVAPEKTREV TKEFKFLGLT
     FNLEKETVSN GDSYRFWNDK DL
//
ID   NUCA_SERMA     STANDARD;      PRT;   266 AA.
AC   P13717;
DT   01-JAN-1990 (Rel. 13, Created)
DT   01-FEB-1996 (Rel. 33, Last sequence update)
DT   16-OCT-2001 (Rel. 40, Last annotation update)
DE   Nuclease precursor (EC 3.1.30.2) (Endonuclease).
GN   NUCA OR NUC.
OS   Serratia marcescens.
OC   Bacteria; Proteobacteria; gamma subdivision; Enterobacteriaceae;
OC   Serratia.
OX   NCBI_TaxID=615;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=W225;
RX   MEDLINE=88084425; PubMed=3319779;
RA   Ball T.K., Saurugger P.N., Benedick M.J.;
RT   "The extracellular nuclease gene of Serratia marcescens and its
RT   secretion from Escherichia coli.";
RL   Gene 57:183-192(1987).
RN   [2]
RP   REVISIONS TO 7-11.
RC   STRAIN=W225;
RA   Benedick M.J.;
RL   Submitted (OCT-1989) to the EMBL/GenBank/DDBJ databases.
RN   [3]
RP   SEQUENCE OF 21-266, AND DISULFIDE BONDS.
RC   STRAIN=B10M1;
RX   MEDLINE=93385170; PubMed=8373817;
RA   Pedersen J., Filimonova M.N., Roepstorff P., Biedermann K.;
RT   "Characterization of Serratia marcescens nuclease isoforms by plasma
RT   desorption mass spectrometry.";
RL   Biochim. Biophys. Acta 1202:13-21(1993).
RN   [4]
RP   PARTIAL SEQUENCE, AND DISULFIDE BONDS.
RX   MEDLINE=89322554; PubMed=2665765;
RA   Biedermann K., Jepsen P.K., Riise E., Svendsen I.;
RT   "Purification and characterization of a Serratia marcescens nuclease
RT   produced by Escherichia coli.";
RL   Carlsberg Res. Commun. 54:17-27(1989).
RN   [5]
RP   ACTIVE SITE.
RX   MEDLINE=94359798; PubMed=8078761;
RA   Friedhoof P., Gimadutdinow O., Pingoud A.;
RT   "Identification of catalytically relevant amino acids of the
RT   extracellular Serratia marcescens endonuclease by alignment-guided
RT   mutagenesis.";
RL   Nucleic Acids Res. 22:3280-3287(1994).
RN   [6]
RP   KINETIC STUDIES.
RX   MEDLINE=95102530; PubMed=7804150;
RA   Filimonova M.N., Krause K.L., Benedik M.J.;
RT   "Kinetic studies of the Serratia marcescens extracellular nuclease
RT   isoforms.";
RL   Biochem. Mol. Biol. Int. 33:1229-1236(1994).
RN   [7]
RP   MUTAGENESIS.
RX   MEDLINE=96313223; PubMed=8758988;
RA   Firedhoff P., Kolmes B., Gimadutdinow O., Wende W., Krause K.L.,
RA   Pingoud A.;
RT   "Analysis of the mechanism of the Serratia nuclease using
RT   site-directed mutagenesis.";
RL   Nucleic Acids Res. 24:2632-2639(1996).
RN   [8]
RP   X-RAY CRYSTALLOGRAPHY (2.1 ANGSTROMS).
RX   MEDLINE=95393180; PubMed=7664065;
RA   Miller M.D., Tanner J., Alpaugh M., Benedik M.J., Krause K.L.;
RT   "2.1-A structure of Serratia endonuclease suggests a mechanism for
RT   binding to double-stranded DNA.";
RL   Nat. Struct. Biol. 1:461-468(1994).
RN   [9]
RP   X-RAY CRYSTALLOGRAPHY (1.7 ANGSTROMS).
RX   MEDLINE=97400229; PubMed=9257723;
RA   Lunin V.Y., Levdikov V.M., Shlyapnikov S.V., Blagova E.V., Lunin V.V.,
RA   Wilson K.S., Mikhailov A.M.;
RT   "Three-dimensional structure of Serratia marcescens nuclease at 1.7-A
RT   resolution and mechanism of its action.";
RL   FEBS Lett. 412:217-220(1997).
CC   -!- FUNCTION: CATALYZES THE HYDROLYSIS OF BOTH DNA AND RNA, DOUBLE-
CC       OR SINGLE-STRANDED, AT THE 3'POSITION OF THE PHOSPHODIESTER BOND
CC       TO PRODUCE 5'-PHOSPHORYLATED MONO-, DI-, TRI- AND
CC       TETRANUCLEOTIDES. DNA IS A SLIGHTLY BETTER SUBSTRATE THAN RNA.
CC   -!- CATALYTIC ACTIVITY: Endonucleolytic cleavage to 5'-
CC       phosphomononucleotide and 5'-phosphooligonucleotide end-products.
CC   -!- COFACTOR: MAGNESIUM IS IMPORTANT FOR ACTIVITY; IN ITS ABSENCE
CC       NUCLEASE ACTIVITY IS SIGNIFICANTLY REDUCED.
CC   -!- SUBUNIT: HOMODIMER.
CC   -!- SUBCELLULAR LOCATION: Secreted.
CC   -!- SIMILARITY: BELONGS TO THE DNA/RNA NON-SPECIFIC ENDONUCLEASES
CC       FAMILY.
DR   EMBL; M19495; AAA26560.1; -.
DR   PIR; A27356; A27356.
DR   PDB; 1SMN; 29-JAN-96.
DR   InterPro; IPR001604; Endonuclease.
DR   Pfam; PF01223; Endonuclease; 1.
DR   SMART; SM00477; NUC; 1.
DR   PROSITE; PS01070; NUCLEASE_NON_SPEC; 1.
KW   Hydrolase; Nuclease; Endonuclease; Magnesium; Signal; 3D-structure.
FT   SIGNAL        1     20
FT   CHAIN        21    266       NUCLEASE, ISOFORM SM2.
FT   CHAIN        22    266       NUCLEASE, ISOFORM SM3.
FT   CHAIN        24    266       NUCLEASE, ISOFORM SM1.
FT   ACT_SITE    110    110
FT   DISULFID     30     34
FT   DISULFID    222    264
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   266 AA;  28944 MW;  A0FF0C1430677B9E CRC64;
     MRFNNKMLAL AALLFAAQAS ADTLESIDNC AVGCPTGGSS NVSIVRHAYT LNNNSTTKFA
     NWVAYHITKD TPASGKTRNW KTDPALNPAD TLAPADYTGA NAALKVDRGH QAPLASLAGV
     SDWESLNYLS NITPQKSDLN QGAWARLEDQ ERKLIDRADI SSVYTVTGPL YERDMGKLPG
     TQKAHTIPSA YWKVIFINNS PAVNHYAAFL FDQNTPKGAD FCQFRVTVDE IEKRTGLIIW
     AGLPDDVQAS LKSKPGVLPE LMGCKN
//
ID   CLP1_AGRT5     STANDARD;      PRT;   202 AA.
AC   Q8UEX6;
DT   15-JUN-2002 (Rel. 41, Created)
DT   15-JUN-2002 (Rel. 41, Last sequence update)
DT   15-JUN-2002 (Rel. 41, Last annotation update)
DE   ATP-dependent Clp protease proteolytic subunit 1 (EC 3.4.21.92)
DE   (Endopeptidase Clp 1).
GN   CLPP1 OR ATU1627 OR AGR_C_3003.
OS   Agrobacterium tumefaciens (strain C58 / ATCC 33970).
OC   Bacteria; Proteobacteria; alpha subdivision; Rhizobiaceae group;
OC   Rhizobiaceae; Rhizobium.
OX   NCBI_TaxID=176299;
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=21608550; PubMed=11743193;
RA   Wood D.W., Setubal J.C., Kaul R., Monks D.E., Kitajima J.P.,
RA   Okura V.K., Zhou Y., Chen L., Wood G.E., Almeida N.F. Jr., Woo L.,
RA   Chen Y., Paulsen I.T., Eisen J.A., Karp P.D., Bovee D. Sr.,
RA   Chapman P., Clendenning J., Deatherage G., Gillet W., Grant C.,
RA   Kutyavin T., Levy R., Li M.-J., McClelland E., Palmieri A.,
RA   Raymond C., Rouse G., Saenphimmachak C., Wu Z., Romero P., Gordon D.,
RA   Zhang S., Yoo H., Tao Y., Biddle P., Jung M., Krespan W., Perry M.,
RA   Gordon-Kamm B., Liao L., Kim S., Hendrick C., Zhao Z.-Y., Dolan M.,
RA   Chumley F., Tingey S.V., Tomb J.-F., Gordon M.P., Olson M.V.,
RA   Nester E.W.;
RT   "The genome of the natural genetic engineer Agrobacterium tumefaciens
RT   C58.";
RL   Science 294:2317-2323(2001).
RN   [2]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=21608551; DOI=10.1002; PubMed=11743194;
RA   Goodner B., Hinkle G., Gattung S., Miller N., Blanchard M.,
RA   Qurollo B., Goldman B.S., Cao Y., Askenazi M., Halling C., Mullin L.,
RA   Houmiel K., Gordon J., Vaudin M., Iartchouk O., Epp A., Liu F.,
RA   Wollam C., Allinger M., Doughty D., Scott C., Lappas C., Markelz B.,
RA   Flanagan C., Crowell C., Gurson J., Lomo C., Sear C., Strub G.,
RA   Cielo C., Slater S.;
RT   "Genome sequence of the plant pathogen and biotechnology agent
RT   Agrobacterium tumefaciens C58.";
RL   Science 294:2323-2328(2001).
CC   -!- CATALYTIC ACTIVITY: Hydrolysis of proteins to small peptides in
CC       the presence of ATP and magnesium. Alpha-casein is the usual test
CC       substrate. In the absence of ATP, only oligopeptides shorter than
CC       five residues are cleaved (such as succinyl-Leu-Tyr-|-NHMEC; and
CC       Leu-|-Tyr-Trp, in which the cleavage of the -Tyr-|-Leu-
CC       bond also occurs).
CC   -!- CATALYTIC ACTIVITY: Cleaves Leu-|-
CC       bonds and disulfide-
CC       bonds.
CC   -!- RNA EDITING: Modified_positions=Not_applicable;
CC       Note=a.
CC   -!- RNA EDITING: Modified_positions=Undetermined.
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative initiation;
CC         Comment=This is just a test;
CC       Name=1;
CC         IsoId=Q8UEX6-1; Sequence=Displayed;
DR   EMBL; AE009120; AAL42629.1; -.
DR   EMBL; AE008085; AAK87406.1; -.
DR   PROSITE; PS00382; CLP_PROTEASE_HIS; 1.
DR   PROSITE; PS00381; CLP_PROTEASE_SER; FALSE_NEG.
KW   Hydrolase; Serine protease; Complete proteome.
FT   ACT_SITE    102    102       BY SIMILARITY.
FT   ACT_SITE    127    127       Performs a certain function (By
FT                                similarity).
**
**   #################    INTERNAL SECTION    ##################
**HA SAM; Annotated by PicoHamap 1.56; MF_00444.2; 21-JUN-2002.
**HF HAMAP; MF_00444; 1.
**NI CLPP1
SQ   SEQUENCE   202 AA;  22834 MW;  754E1B1D1A82C36C CRC64;
     MRETMQLVPM VVEQSSRGER SFDIYSRLLR ERIIFLNGEV DDTVSALVCA QLLFLEAENP
     KKPIHLYINS PGGMVTSGFA MYDTMRYIRA PVHTLCMGTA RSMGSFLLMA GEPGTRAALP
     NASILIHQPS GGFQGQASDM LIHAEEIRQT KHRMTRLYAE HCGRSYEEFE TAMDRDRFMT
     VQEALEWGLI DRILEVREDA AA
//

ID   CPB2_STRPN     STANDARD;      PRT;   243 AA.
AC   Q54518; O08049; O08278; O52232;
DT   28-FEB-2003 (Rel. 41, Created)
DT   28-FEB-2003 (Rel. 41, Last sequence update)
DT   15-SEP-2003 (Rel. 42, Last annotation update)
DE   Protein-tyrosine phosphatase cpsB (EC 3.1.3.48).
GN   CPSB OR CPS19FB OR CAP1B.
OS   Streptococcus pneumoniae.
OC   Bacteria; Firmicutes; Lactobacillales; Streptococcaceae;
OC   Streptococcus.
OX   NCBI_TaxID=1313;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=NCTC 11906 / Serotype 19F, SP-496 / Serotype 19F, SP-VA92 /
RC   Serotype 19F, SP-GA71 / Serotype 19F, PO-329 / Serotype 19F, and
RC   SP-VA96 / Serotype 19F;
RC   TISSUE=Adrenal, Adrenal gland, Carcinoma, Femoral artery, Pancreatic
RC   acinar, and Pituitary;
RX   MEDLINE=98125733; PubMed=9466257;
RA   Coffey T.J., Enright M.C., Daniels M., Morona J.K., Morona R.,
RA   Hryniewicz W., Paton J.C., Spratt B.G.;
RT   "Recombinational exchanges at the capsular polysaccharide
RT   biosynthetic locus lead to frequent serotype changes among natural
RT   isolates of Streptococcus pneumoniae.";
RL   Mol. Microbiol. 27:73-83(1998).
CC   -!- FUNCTION: Dephosphorylates cpsD. Involved in the regulation of
CC       capsular polysaccharide biosynthesis.
CC   -!- CATALYTIC ACTIVITY: Protein tyrosine phosphate + H(2)O = protein
CC       tyrosine + phosphate.
CC   -!- COFACTOR: Manganese.
CC   -!- PATHWAY: Capsular polysaccharide (CPS) biosynthesis.
CC   -!- RNA EDITING: Modified_positions=1306; Note=Partially edited. A
CC       stop codon is created at this position.
CC   -!- RNA EDITING: Modified_positions=; Note=.
CC   -!- SIMILARITY: BELONGS TO THE CPSB/CAPC FAMILY.
DR   EMBL; U09239; AAC44959.1; -.
DR   EMBL; Z83335; CAB05935.1; -.
DR   EMBL; AF030367; AAC38717.1; -.
DR   EMBL; AF030368; AAC38722.1; -.
DR   EMBL; AF030369; AAC38727.1; -.
DR   EMBL; AF030370; AAC38731.1; -.
DR   EMBL; AF030371; AAC38736.1; -.
DR   EMBL; AF030372; AAC38741.1; -.
DR   EMBL; AF106137; AAD17985.1; -.
DR   InterPro; IPR004013; PHP_C.
DR   Pfam; PF02811; PHP_C; 1.
KW   Exopolysaccharide synthesis; Hydrolase; Manganese; Bacterial capsule.
FT   VARIANT     226    226       A -> V (IN STRAIN NCTC 11906).
FT   MUTAGEN     199    199       D->N: LOSS OF ACTIVITY; WHEN ASSOCIATED
FT                                WITH Q-201.
FT   MUTAGEN     201    201       H->Q: LOSS OF ACTIVITY; WHEN ASSOCIATED
FT                                WITH N-199.
**
**   #################    INTERNAL SECTION    ##################
**NI CPSB2
**ZA CAR, 22-JAN-2003;
SQ   SEQUENCE   243 AA;  28353 MW;  810A4C802EC43079 CRC64;
     MIDIHSHIVF DVDDGPKSRE ESKALLAESY RQGVRTIVST SHRRKGMFET PEEKIAENFL
     QVREIAKEVA DDLVIAYGAE IYYTLDALEK LEKKEIPTLN DSRYALIEFS MHTSYRQIHT
     GLSNILMLGI TPVIAHIERY DALENNEKRV RELIDMGCYT QINSYHVSKP KFFGEKYKFM
     KKRARYFLER DLVHVVASDM HNLDSRPPYM QQAYDIIAKK YGAKKAKELF VDNPRKIIMD
     QLI
//
ID   ATL_STAAU      STANDARD;      PRT;  1256 AA.
AC   P52081;
DT   01-OCT-1996 (Rel. 34, Created)
DT   01-OCT-1996 (Rel. 34, Last sequence update)
DT   28-FEB-2003 (Rel. 41, Last annotation update)
DE   Bifunctional autolysin precursor [Includes: N-acetylmuramoyl-L-alanine
DE   amidase (EC 3.5.1.28); Mannosyl-glycoprotein endo-beta-N-
DE   acetylglucosamidase (EC 3.2.1.96)].
GN   ATL.
OS   Staphylococcus aureus.
OC   Bacteria; Firmicutes; Bacillales; Staphylococcus.
OX   NCBI_TaxID=1280;
RN   [1]
RP   SEQUENCE FROM N.A., AND SEQUENCE OF 205-214 AND 776-792.
RC   STRAIN=RN450;
RX   MEDLINE=95116542; PubMed=7816834;
RX   DOI=10.1002/1615-9861(200212)2:12<1682::AID-PROT1682>3.0.COAID-PROT1682>3.0.CO;2-Y; 
RA   Oshida T., Sugai M., Komatsuzawa H., Hong Y.-M., Suginaka H.,
RA   Tomasz A.;
RT   "A Staphylococcus aureus autolysin that has an N-acetylmuramoyl-L-
RT   alanine amidase domain and an endo-beta-N-acetylglucosaminidase
RT   domain: cloning, sequence analysis, and characterization.";
RL   Proc. Natl. Acad. Sci. U.S.A. 92:285-289(1995).
RN   [2]
RP   SEQUENCE FROM N.A.
RC   STRAIN=NCTC 8325-4;
RA   Foster S.J.;
RL   Submitted (APR-1995) to the EMBL/GenBank/DDBJ databases.
RN   [3]
RP   SEQUENCE OF 1-28.
RX   MEDLINE=81090973; PubMed=6108877;
RA   Bennett C.D.;
**   /NO TITLE.
RL   Unpublished results, cited by:
RL   Norton T.R.;
RL   Fed. Proc. 40:21-25(1981).
CC   -!- FUNCTION: ENDOHYDROLYSIS OF THE DI-N-ACETYLCHITOBIOSYL UNIT IN
CC       HIGH-MANNOSE GLYCOPEPTIDES AND GLYCOPROTEINS CONTAINING THE
CC       -[(MAN)5(GLCNAC)2]-ASN STRUCTURE. ONE N-ACETYL-D-GLUCOSAMINE
CC       RESIDUE REMAINS ATTACHED TO THE PROTEIN; THE REST OF THE
CC       OLIGOSACCHARIDE IS RELEASED INTACT.
CC   -!- CATALYTIC ACTIVITY: Hydrolyzes the link between N-acetylmuramoyl
CC       residues and L-amino acid residues in certain bacterial cell-wall
CC       glycopeptides.
CC   -!- CATALYTIC ACTIVITY: Endohydrolysis of the di-N-acetylchitobiosyl
CC       unit in high-mannose glycopeptides and glycoproteins containing
CC       the -[Man(GlcNAc)2]Asn-structure. One N-acetyl-D-glucosamine
CC       residue remains attached to the protein; the rest of the
CC       oligosaccharide is released intact.
CC   -!- SUBCELLULAR LOCATION: Secreted.
CC   -!- PTM: Undergoes proteolytic processing to generate the two
CC       extracellular lytic enzymes.
CC   -!- SIMILARITY: IN THE N-TERMINAL SECTION; BELONGS TO THE N-
CC       ACETYLMURAMOYL-L-ALANINE AMIDASE FAMILY 2.
CC   -!- SIMILARITY: IN THE C-TERMINAL SECTION; BELONGS TO FAMILY 73 OF
CC       GLYCOSYL HYDROLASES.
DR   EMBL; D17366; BAA04185.1; -.
DR   EMBL; L41499; AAA99982.1; -.
DR   InterPro; IPR002502; Amidase_2.
DR   InterPro; IPR002901; Amidase_4.
DR   Pfam; PF01510; Amidase_2; 1.
DR   Pfam; PF01832; Amidase_4; 1.
DR   SMART; SM00644; Ami_2; 1.
DR   SMART; SM00047; LYZ2; 1.
KW   Cell wall; Hydrolase; Signal; Multifunctional enzyme; Repeat.
FT   SIGNAL        1     29       Potential.
FT   CHAIN        30   1256       Bifunctional autolysin.
FT   DOMAIN      199    775       N-acetylmuramoyl-L-alanine amidase.
FT   DOMAIN      776   1256       ENDO-BETA-N-ACETYLGLUCOSAMIDASE.
FT   REPEAT      425    589       1.
FT   REPEAT      596    758       2.
FT   REPEAT      770    932       3.
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   1256 AA;  137384 MW;  2BB76CAA292FDD20 CRC64;
     MAKKFNYKLP SMVALTLVGS AVTAHQVQAA ETTQDQTTNK NVLDSNKVKA TTEQAKAEVK
     NPTQNISGTQ VYQDPAIVQP KTANNKTGNA QVSQKVDTAQ VNGDTRANQS ATTNNTQPVA
     KSTSTTAPKT NTNVTNAGYS LVDDEDDNSE NQINPELIKS AAKPAALETQ YKTAAPKAAT
     TSAPKAKTEA TPKVTTFSAS AQPRSVAATP KTSLPKYKPQ VNSSINDYIC KNNLKAPKIE
     EDYTSYFPKY AYRNGVGRPE GIVVHDTAND RSTINGEISY MKNNYQNAFV HAFVDGDRII
     ETAPTDYLSW GVGAVGNPRF INVEIVHTHD YASFARSMNN YADYAATQLQ YYGLKPDSAE
     YDGNGTVWTH YAVSKYLGGT DHADPHGYLR SHNYSYDQLY DLINEKYLIK MGKVAPWGTQ
     STTTPTTPSK PTTPSKPSTG KLTVAANNGV AQIKPTNSGL YTTVYDKTGK ATNEVQKTFA
     VSKTATLGNQ KFYLVQDYNS GNKFGWVKEG DVVYNTAKSP VNVNQSYSIK PGTKLYTVPW
     GTSKQVAGSV SGSGNQTFKA SKQQQIDKSI YLYGSVNGKS GWVSKAYLVD TAKPTPTPTP
     KPSTPTTNNK LTVSSLNGVA QINAKNNGLF TTVYDKTGKP TKEVQKTFAV TKEASLGGNK
     FYLVKDYNSP TLIGWVKQGD VIYNNAKSPV NVMQTYTVKP GTKLYSVPWG TYKQEAGAVS
     GTGNQTFKAT KQQQIDKSIY LFGTVNGKSG WVSKAYLAVP AAPKKAVAQP KTAVKAYTVT
     KPQTTQTVSK IAQVKPNNTG IRASVYEKTA KNGAKYADRT FYVTKERAHG NETYVLLNNT
     SHNIPLGWFN VKDLNVQNLG KEVKTTQKYT VNKSNNGLSM VPWGTKNQVI LTGNNIAQGT
     FNATKQVSVG KDVYLYGTIN NRTGWVNAKD LTAPTAVKPT TSAAKDYNYT YVIKNGNGYY
     YVTPNSDTAK YSLKAFNEQP FAVVKEQVIN GQTWYYGKLS NGKLAWIKST DLAKELIKYN
     QTGMTLNQVA QIQAGLQYKP QVQRVPGKWT DAKFNDVKHA MDTKRLAQDP ALKYQFLRLD
     QPQNISIDKI NQFLKGKGVL ENQGAAFNKA AQMYGINEVY LISHALLETG NGTSQLAKGA
     DVVNNKVVTN SNTKYHNVFG IAAYDNDPLR EGIKYAKQAG WDTVSKAIVG GAKFIGNSYV
     KAGQNTLYKM RWNPAHPGTH QYATDVDWAN INAKIIKGYY DKIGEVGKYF DIPQYK
//
ID   ALBU_BOVIN     STANDARD;      PRT;   607 AA.
AC   P02769; O02787;
DT   21-JUL-1986 (Rel. 01, Created)
DT   01-FEB-1996 (Rel. 33, Last sequence update)
DT   10-OCT-2003 (Rel. 42, Last annotation update)
DE   Serum albumin precursor (Allergen Bos d 6).
GN   Name=ALB;  Synonyms=ALB1, aal, abl; OrderedLocusNames=, ; ORFNames= ;
OS   Bos taurus (Bovine).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Cetartiodactyla; Ruminantia; Pecora; Bovidae;
OC   Bovinae; Bos.
OX   NCBI_TaxID=9913;
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Holowachuk E.W., Stoltenborg J.K., Reed R.G., Peters T. Jr.;
RL   Submitted (AUG-1991) to the EMBL/GenBank/DDBJ databases.
RN   [2]
RP   SEQUENCE FROM N.A., AND VARIANT THR-214.
RC   TISSUE=Liver;
RA   Barry T., Power S., Gannon F.;
RL   Submitted (JUL-1994) to the EMBL/GenBank/DDBJ databases.
RN   [3]
RP   SEQUENCE FROM N.A.
RC   TISSUE=Liver;
RA   Hilger C., Grigioni F., de Beaufort C., Michel G., Hentges F.;
RL   Submitted (JUL-1998) to the EMBL/GenBank/DDBJ databases.
RN   [4]
RP   SEQUENCE FROM N.A., AND VARIANT THR-214.
RA   Wu H.T., Huang M.C.;
RT   "The complete cDNA sequence of bovine serum albumin.";
RL   Submitted (AUG-2002) to the EMBL/GenBank/DDBJ databases.
RN   [5]
RP   SEQUENCE OF 1-32.
RX   MEDLINE=80024278; PubMed=488109;
RA   McGillivray R.T.A., Chung D.W., Davie E.W.;
RT   "Biosynthesis of bovine plasma proteins in a cell-free system. Amino-
RT   terminal sequence of preproalbumin.";
RL   Eur. J. Biochem. 98:477-485(1979).
RN   [6]
RP   SEQUENCE OF 25-424 AND 429-607, AND VARIANT THR-214.
**   MEDLINE=None; PubMed=None;
RA   Brown J.R.;
RT   "Structure of bovine serum albumin.";
RL   Fed. Proc. 34:591-591(1975).
RN   [7]
RP   REVISIONS TO 190-195.
RA   Brown J.R.;
RL   Submitted (APR-1975) to the PIR data bank.
RN   [8]
RP   SEQUENCE OF 402-433.
RX   MEDLINE=82023364; PubMed=7283978;
RA   Reed R.G., Putnam F.W., Peters T. Jr.;
RT   "Sequence of residues 400-403 of bovine serum albumin.";
RL   Biochem. J. 191:867-868(1980).
RN   [9]
RP   SEQUENCE OF 19-28.
RX   MEDLINE=77134075; PubMed=843354;
RA   Patterson J.E., Geller D.M.;
RT   "Bovine microsomal albumin: amino terminal sequence of bovine
RT   proalbumin.";
RL   Biochem. Biophys. Res. Commun. 74:1220-1226(1977).
RN   [10]
RP   SEQUENCE, AND REVISIONS TO 118-119 AND 180.
RX   MEDLINE=91083649; PubMed=2260975;
RA   Hirayama K., Akashi S., Furuya M., Fukuhara K.-I.;
RT   "Rapid confirmation and revision of the primary structure of bovine
RT   serum albumin by ESIMS and Frit-FAB LC/MS.";
RL   Biochem. Biophys. Res. Commun. 173:639-646(1990).
RN   [11]
RP   SEQUENCE OF 25-41.
RX   MEDLINE=88267456; PubMed=3389500;
RA   Hsieh J.C., Lin F.P., Tam M.F.;
RT   "Electroblotting onto glass-fiber filter from an analytical
RT   isoelectrofocusing gel: a preparative method for isolating proteins
RT   for N-terminal microsequencing.";
RL   Anal. Biochem. 170:1-8(1988).
RN   [12]
RP   SEQUENCE OF 437-451.
RA   Vilbois F.;
RL   Submitted (AUG-1998) to Swiss-Prot.
RN   [13]
RP   DISULFIDE BONDS.
**   MEDLINE=None; PubMed=None;
RA   Brown J.R.;
RT   "Structure of serum albumin: disulfide bridges.";
RL   Fed. Proc. 33:1389-1389(1974).
CC   -!- FUNCTION: Serum albumin, the main protein of plasma, has a good
CC       binding capacity for water, Ca(2+), Na(+), K(+), fatty acids,
CC       hormones, bilirubin and drugs. Its main function is the regulation
CC       of the colloidal osmotic pressure of blood.
CC   -!- SUBCELLULAR LOCATION: Secreted.
CC   -!- TISSUE SPECIFICITY: Plasma.
CC   -!- ALLERGEN: Causes an allergic reaction in human.
CC   -!- SIMILARITY: Belongs to the ALB/AFP/VDB family.
CC   -!- SIMILARITY: Contains 3 albumin domains.
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing; Named isoforms=2;
CC       Name=1;
CC         IsoId=Q92482-1; Sequence=Displayed;
CC       Name=2; Synonyms=delta5;
CC         IsoId=Q92482-2, Q92482-3; Sequence=VSP_003229, VSP_003230, VSP_003229, VSP_003230, VSP_003229, VSP_003230;
CC         Note=Due to a polymorphism at the 5'-splice donor site of intron
CC         5, leading to exon 5 skipping and premature termination of
CC         translation. This is the molecular basis of the GIL blood group;
CC   -!- BIOPHYSICOCHEMICAL PROPERTIES:
CC       Kinetic parameters: KM=62 mM for glucose{EA1} ;
CC         Vmax=0.11 mmol/min/mg enzyme when maltose is used as the
CC         substrate;
CC         KM=90 mM for maltose{EP1}; Vmax=0.20 mmol/min/mg enzyme when glucose is used as
CC         the substrate;
CC         Note=Acetylates glucose, maltose, mannose, galactose, and
CC         fructose with a decreasing relative rate of 1, 0.55, 0.20, 0.07,
CC         0.04;
CC   -!- BIOPHYSICOCHEMICAL PROPERTIES:
CC       Kinetic parameters: KM=   ;
CC         Vmax=;
CC         KM=;
CC         Note=;
CC   -!- TOXIC DOSE: LD(50) of the glycosylated and non-glycosylated forms
CC       are 13.4 +/- 0.7 and 5.8 +/- 0.3 nmol/g by intra-abdominal
CC       injection into the cricket G.assimilis, respectively. LD(50) of
CC       the glycosylated and non-glycosylated forms are 15 +/- 1 and 0.16 +/-
CC       0.01 uM for human HL-60 cells, respectively.
CC   -!- INTERACTION:
CC       P38936:CDKN1A; NbExp=1; IntAct=EBI-374862, EBI-375077;
CC       P42771:CDKN2A; NbExp=1; IntAct=EBI-374862, EBI-375053;
CC       Q9H211:CDT1; NbExp=1; IntAct=EBI-374862, EBI-456953;
CC       Q96H67:cdt1; NbExp=1; IntAct=EBI-374862, EBI-371677;
CC       Q7L590:MCM10; NbExp=1; IntAct=EBI-374862, EBI-374912;
CC       P49736:MCM2; NbExp=1; IntAct=EBI-374862, EBI-374819;
CC       P25205:MCM3; NbExp=1; IntAct=EBI-374862, EBI-355153;
CC       Q13415:ORC1L; NbExp=1; IntAct=EBI-374862, EBI-374847;
CC       Q13416:ORC2L; NbExp=1; IntAct=EBI-374862, EBI-374957;
CC       Q9UBD5:ORC3L; NbExp=1; IntAct=EBI-374862, EBI-374916;
CC       O43913:ORC5L; NbExp=1; IntAct=EBI-374862, EBI-374928;
CC       Q9Y5N6:ORC6L; NbExp=1; IntAct=EBI-374862, EBI-374840;
CC       P62988:RPS27A; NbExp=1; IntAct=EBI-374862, EBI-413034;
DR   EMBL; M73993; AAA51411.1; -.
DR   EMBL; X58989; CAA41735.1; -.
DR   EMBL; Y17769; CAA76847.1; -.
DR   EMBL; AF542068; AAN17824.1; -.
DR   HSSP; P02768; 1HK1.
DR   InterPro; IPR000264; Serum_albumin.
DR   Pfam; PF00273; Serum_albumin; 3.
DR   PRINTS; PR00802; SERUMALBUMIN.
DR   ProDom; PD002486; Serum_albumin; 1.
DR   SMART; SM00103; ALBUMIN; 3.
DR   PROSITE; PS00212; ALBUMIN; 3.
KW   Metal-binding; Lipid-binding; Repeat; Signal; Copper; Allergen;
KW   Polymorphism.
FT   SIGNAL        1     18
FT   PROPEP       19     24
FT   CHAIN        25    607       Serum albumin.
FT   DOMAIN       25    204       Albumin 1.
FT   DOMAIN      211    396       Albumin 2.
FT   DOMAIN      403    594       Albumin 3.
FT   METAL        27     27       Copper (By similarity).
FT   DISULFID     77     86
FT   DISULFID     99    115
FT   DISULFID    114    125
FT   DISULFID    147    192
FT   DISULFID    191    200
FT   DISULFID    223    269
FT   DISULFID    268    276
FT   DISULFID    288    302
FT   DISULFID    301    312
FT   DISULFID    339    384
FT   DISULFID    383    392
FT   DISULFID    415    461
FT   DISULFID    460    471
FT   DISULFID    484    500
FT   DISULFID    499    510
FT   DISULFID    537    582
FT   DISULFID    581    590
FT   MUTAGEN     398    429       KRSRSDRAVTGPSAQQSFEVRVPEQRDALHLPLSWRVKRP
FT                                ->TA: Complete loss of HR activity.
FT   MUTAGEN     399    429       TYEGKHNHHLLLSPPSSSTLPFNSPQLSKQTI->SLQPTRV
FT                                NIIIICS: In ttg2-2; defects in trichome
FT                                development, seed coat color and mucilage
FT                                production.
FT   VARIANT     214    214       A -> T.
FT   CONFLICT    302    302       C -> K (in Ref. 6).
FT   CONFLICT    304    305       KP -> PC (in Ref. 6).
FT   CONFLICT    324    324       N -> D (in Ref. 6).
FT   CONFLICT    394    395       ST -> TS (in Ref. 6).
FT   CONFLICT    437    437       K -> R (in Ref. 12).
FT   CONFLICT    493    494       SE -> ES (in Ref. 6).
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   607 AA;  69293 MW;  39167DFE768585D4 CRC64;
     MKWVTFISLL LLFSSAYSRG VFRRDTHKSE IAHRFKDLGE EHFKGLVLIA FSQYLQQCPF
     DEHVKLVNEL TEFAKTCVAD ESHAGCEKSL HTLFGDELCK VASLRETYGD MADCCEKQEP
     ERNECFLSHK DDSPDLPKLK PDPNTLCDEF KADEKKFWGK YLYEIARRHP YFYAPELLYY
     ANKYNGVFQE CCQAEDKGAC LLPKIETMRE KVLASSARQR LRCASIQKFG ERALKAWSVA
     RLSQKFPKAE FVEVTKLVTD LTKVHKECCH GDLLECADDR ADLAKYICDN QDTISSKLKE
     CCDKPLLEKS HCIAEVEKDA IPENLPPLTA DFAEDKDVCK NYQEAKDAFL GSFLYEYSRR
     HPEYAVSVLL RLAKEYEATL EECCAKDDPH ACYSTVFDKL KHLVDEPQNL IKQNCDQFEK
     LGEYGFQNAL IVRYTRKVPQ VSTPTLVEVS RSLGKVGTRC CTKPESERMP CTEDYLSLIL
     NRLCVLHEKT PVSEKVTKCC TESLVNRRPC FSALTPDETY VPKAFDEKLF TFHADICTLP
     DTEKQIKKQT ALVELLKHKP KATEEQLKTV MENFVAFVDK CCAADDKEAC FAVEGPKLVV
     STQTALA
//
