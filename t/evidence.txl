ID   AATM_BOVIN     STANDARD;      PRT;   430 AA.
AC   P12344;
DT   01-OCT-1989 (Rel. 12, Created)
DT   01-NOV-1995 (Rel. 32, Last sequence update)
DT   15-JUL-1999 (Rel. 38, Last annotation update)
DE   ASPARTATE AMINOTRANSFERASE, MITOCHONDRIAL PRECURSOR (EC 2.6.1.1)
DE   (TRANSAMINASE A) (GLUTAMATE OXALOACETATE TRANSAMINASE-2){EC3}.
GN   GOT2{EI1}.
OG   Plasmid pCP301 {ECO:0000313|EMBL:AAH50488.1, ECO:0000269|PubMed:11285225}, Plasmid pWR100, Plasmid pINV_F6_M1382, and
OG   Plasmid pSF5; Chloroplast.
OS   Bos taurus (Bovine){EI1}.
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Cetartiodactyla; Ruminantia; Pecora; Bovoidea;
OC   Bovidae; Bovinae; Bos.
OX   NCBI_TaxID=126566, 55{EC2};
RN   [1]{EI1}
RP   SEQUENCE FROM N.A.
RC   TISSUE=HEART;
RA   Palmisano A.;
RL   Submitted (OCT-1993) to the EMBL/GenBank/DDBJ databases.
RN   [2]{EI1}
RP   SEQUENCE OF 30-41.
RX   MEDLINE=79191877; PubMed=446759;
RA   Capasso S., Garzillo A.M., Marino G., Mazzarella L., Pucci P.,
RA   Sannia G.;
RT   "Mitochondrial bovine aspartate aminotransferase. Preliminary
RT   sequence and crystallographic data.";
RL   FEBS Lett. 101:351-354(1979).
CC   -!- CATALYTIC ACTIVITY: L-ASPARTATE + 2-OXOGLUTARATE = OXALOACETATE +
CC       L-GLUTAMATE{EC3}.
CC   -!- COFACTOR: PYRIDOXAL PHOSPHATE{EC3}.
CC   -!- SEQUENCE CAUTION:
CC       Sequence=CAI22254{EC1,EI1}; Type=Miscellaneous discrepancy; Positions=Several; Note=Translated as Gln;
CC       Sequence=CAI21743; Type=Erroneous gene model prediction;
CC   -!- RNA EDITING: Modified_positions=1, 4, 18, 33 {ECO:0000269|PubMed:10707984, ECO:0000269|PubMed:11285225}, 34, 72, 80, 86, 95,
CC       121, 123, 154, 155, 156, 163, 169, 193, 196, 233, 295, 346;
CC       Note=The initiator methionine is created by RNA editing. The
CC       nonsense codons at positions 72, 121, 169, 193 and 346 are
CC       modified to sense codons. {ECO:0000269|PubMed:10707984};
DR   EMBL; Z25466; CAA80960.1; -. {ECO:0000303}
DR   PROSITE; PS00105; AA_TRANSFER_CLASS_1; 1.
KW   Transferase{EC2}; Aminotransferase{EC2}.
FT   TRANSIT       1     29       MITOCHONDRION {ECO:0000303}.
FT   CHAIN        30    430       ASPARTATE AMINOTRANSFERASE {ECO:0000303}.
FT   BINDING     279    279       PYRIDOXAL PHOSPHATE (BY SIMILARITY) {ECO:0000305}.
FT   VARIANT       3      3       A -> L {ECO:0000305}.
FT                                /FTId=VAR_000001.
FT   VARIANT       3      3       A -> LFSJFSHLJSDHFLSJLFHSJLLSJKDHFLSKJFHJ.
FT                                /FTId=VAR_000002.
**
**   #################     SOURCE SECTION     ##################
**   #################    INTERNAL SECTION    ##################
**EV EI1; EMBL; CAA80960.1; 03-SEP-1989.
**EV EC2; Curator; MM; 06-SEP-1989.
**EV EC3; Curator; TT; 10-JUN-1999.
**OX 9913;
SQ   SEQUENCE   430 AA;  47513 MW;  16DDF475382035AA CRC64;
     MALLHSGRFL SGVAAAFHPG LAAAASARAS SWWAHVEMGP PDPILGVTEA FKRDTNSKKM
     NLGVGAYRDD NGKPYVLPSV RKAEAQIAAK NLDKEYLPIA GLAEFCKASA ELALGENNEV
     LKSGRYVTVQ TISGTGALRI GASFLQRFFK FSRDVFLPKP TWGNHTPIFR DAGMQLQSYR
     YYDPKTCGFD FTGAIEDISK IPAQSVILLH ACAHNPTGVD PRPEQWKEMA TVVKKNNLFA
     FFDMAYQGFA SGDGNKDAWA VRHFIEQGIN VCLCQSYAKN MGLYGERVGA FTVVCKDAEE
     AKRVESQLKI LIRPMYSNPP INGARIASTI LTSPDLRKQW LHEVKGMADR IISMRTQLVS
     NLKKEGSSHN WQHIIDQIGM FCYTGLKPEQ VERLTKEFSI YMTKDGRISV AGVTSGNVAY
     LAHAIHQVTK
//
