ID   DCTQ_RHOSH              Unreviewed;       227 AA.
AC   Q9LBD9;
DT   01-OCT-2000 (TrEMBLrel. 15, Created)
DT   01-OCT-2000 (TrEMBLrel. 15, Last sequence update)
DT   01-OCT-2000 (TrEMBLrel. 15, Last annotation update)
DE   Putative small C4-dicarboxylate integral membrane transport protein.
GN   DCTQ.
OS   Rhodobacter sphaeroides (Rhodopseudomonas sphaeroides).
OC   Bacteria; Proteobacteria; alpha subdivision; Rhodobacter group;
OC   Rhodobacter.
OX   NCBI_TaxID=1063;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=4P1;
** nothing has been published
RA   Omrani M.D.;
RL   Submitted (MAY-1997) to the EMBL/GenBank/DDBJ databases.
CC   -!- FUNCTION: REQUIRED FOR C4-DICARBOXYLATE TRANSPORT (BY SIMILARITY).
CC   -!- SUBCELLULAR LOCATION: INTEGRAL MEMBRANE PROTEIN (BY SIMILARITY).
**this miscellaneous CC line should be deleted
CC   -!- MISCELLANEOUS: SHOWS POOR SPECIFICITY.
CC   -!- SEQUENCE CAUTION:
CC       Sequence=CAI22254; Type=Miscellaneous discrepancy; Positions=Several; Note=Translated as Gln;
CC       Sequence=CAI22254; Type=Erroneous gene model prediction; Positions=1528, 1529;
CC       Sequence=CAI21743; Type=Miscellaneous discrepancy; Positions=1528;
CC       Sequence=CAI21743; Type=Erroneous gene model prediction;
DR   EMBL; AF005842; AAF72734.1; -.
PE   1; Evidence at protein level;
KW   Transmembrane; Transport.
FT   TOPO_DOM      1     10       CYTOPLASMIC (BY SIMILARITY).
FT   TRANSMEM     11     29       BY SIMILARITY.
FT   TOPO_DOM     30     68       PERIPLASMIC (BY SIMILARITY).
FT   TRANSMEM     69     91       BY SIMILARITY.
FT   TOPO_DOM     92    112       CYTOPLASMIC (BY SIMILARITY).
FT   TRANSMEM    113    133       BY SIMILARITY.
FT   TOPO_DOM    134    152       PERIPLASMIC (BY SIMILARITY).
FT   TRANSMEM    153    173       BY SIMILARITY.
**argh
 FT   TOPO_DOM    174    227       CYTOPLASMIC (BY SIMILARITY).
**
**   #################     SOURCE SECTION     ##################
**   Rhodobacter sphaeroides C4-dicarboxylate binding-protein precursor (dctP),
**   small integral membrane transport protein (dctQ) and large integral
**   membrane transport protein (dctM) genes, complete cds.
**   source          1..3718
**                   /db_xref="taxon:1063"
**                   /organism="Rhodobacter sphaeroides"
**                   /strain="4P1"
**   CDS             1214..1897
**                   /codon_start=1
**                   /note="DctQ; contains four putative transmembrane helices"
**                   /transl_table=11
**                   /gene="dctQ"
**                   /product="small integral membrane transport protein"
**                   /protein_id="AAF72734.1"
**   CDS_IN_EMBL_ENTRY 3
**   02-JUN-2000 (Rel. 63, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_RHOSH
SQ   SEQUENCE   227 AA;  24991 MW;  2208AD920C1230DA CRC64;
     MMRLLDRLEE TLIASLIAAA TGLIFVSVVQ RYSLGLLADG VAFFRGHDMP ELSAMMRSAY
     LGLREFNLVW AQELCIILFV WMAKFGAAYG VRTGIHVGID VLINKLDERK RGFFILLGLG
     AGALFTGIIA TLGGNFVWHM AQTSAISPDL ELPMWLVYLA IPLGSALMCF RFLQVAVIFA
     RTGELAHHDH GHVEGVDTED EGIDVLGSTF LKSPLTPRDL VEKPKDE
//
ID   DCTQ_WOLSU  PRELIMINARY;      PRT;   170 AA.
AC   Q9ZEJ3;
DT   01-MAY-1999 (TrEMBLrel. 10, Created)
DT   01-MAY-1999 (TrEMBLrel. 10, Last sequence update)
DT   01-MAY-1999 (TrEMBLrel. 10, Last annotation update)
 DE   Small C4-dicarboxylate integral membrane transport protein.
 ** unsure gene name,
 ** will be deleted
GN   DCTQ.
 OS   Wolinella succinogenes.
OC   Bacteria; Proteobacteria; epsilon subdivision; Helicobacter group;
OC   Wolinella.
OX   NCBI_TaxID=844;
RN   [1]
RP   SEQUENCE FROM N.A.
 RC   STRAIN=DSMZ 1740;
 RX   MEDLINE=20461222; PubMed=11004174;
 RA   Ullmann R., Gross R., Simon J., Unden G., Kroeger A.;
 RT   "Transport of C(4)-dicarboxylates in Wolinella succinogenes.";
 RL   J. Bacteriol. 182:5757-5764(2000).
 CC   -!- FUNCTION: INVOLVED IN THE ELECTROGENIC UNIPORT OF C4-
 CC       DICARBOXYLATES.
**   or should this be in similarity?, do things in similarity have to have
++   a dr line?
 CC   -!- PATHWAY: BELONGS TO THE TRIPARTITE ATP-INDEPENDENT PERIPLASMIC
 CC       (TRAP) TRANSPORTER FAMILY
DR   EMBL; AJ132740; CAA10757.1; -.
**
**   #################     SOURCE SECTION     ##################
**   Wolinella succinogenes dctP, dctQ and dctM genes, and partial orfN
**   source          1..3309
**                   /organism="Wolinella succinogenes"
**                   /db_xref="taxon:844"
**   CDS             1086..1598
**                   /db_xref="PID:e1375211"
**                   /transl_table=11
**                   /gene="dctQ"
**                   /product="small integral C4-dicarboxylate membrane
**                   transport protein, putative"
**                   /protein_id="CAA10757.1"
**   CDS_IN_EMBL_ENTRY 4
**   04-FEB-1999 (Rel. 58, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_WOLSU
SQ   SEQUENCE   170 AA;  18753 MW;  4CCF9F77E2F85C20 CRC64;
     MSKFFEILDL GIAAMNKSIA VIGISTGVIL AFINVVLRYF FDSGLTWAGE TINYLFIWSA
     LFGAAYGFKK GIHIAVTILV ERFPPLLAKA SLMIASLISL IFLLFIAYFG LHYVLLVKDM
     GFMSVDLGIP QWIPMVVIPV AFFAASYRVG EKIYEISKQP ADTVVKSAGR
//
ID   DCTQ_RHOCA  PRELIMINARY;      PRT;   227 AA.
AC   O07837;
DT   01-JUL-1997 (TrEMBLrel. 04, Created)
DT   01-JUL-1997 (TrEMBLrel. 04, Last sequence update)
DT   01-NOV-1998 (TrEMBLrel. 08, Last annotation update)
 DE   Small C4-dicarboxylate integral membrane transport protein.
GN   DCTQ.
OS   Rhodobacter capsulatus (Rhodopseudomonas capsulata).
OC   Bacteria; Proteobacteria; alpha subdivision; Rhodobacter group;
OC   Rhodobacter.
OX   NCBI_TaxID=1061;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=B11;
RX   MEDLINE=92236423; PubMed=1809844;
RA   Shaw J.G., Hamblin M.J., Kelly D.J.;
RT   "Purification, characterization and nucleotide sequence of the
RT   periplasmic C4-dicarboxylate-binding protein (DctP) from Rhodobacter
RT   capsulatus.";
RL   Mol. Microbiol. 5:3055-3062(1991).
RN   [2]
RP   SEQUENCE FROM N.A.
** I see SB10003 / St Louis as the strain from table 1, not B10. This may be due to an error.
RC   STRAIN=B11;
RX   MEDLINE=97431499; PubMed=9287004;
RA   Forward J.A., Behrendt M.C., Wyborn N.R., Cross R., Kelly D.J.;
RT   "TRAP transporters: a new family of periplasmic solute transport
RT   systems encoded by the dctPQM genes of Rhodobacter capsulatus and by
RT   homologs in diverse gram-negative bacteria.";
RL   J. Bacteriol. 179:5482-5493(1997).
 RN   [3]
 RP   DISCUSSION OF SEQUENCE.
 RX   MEDLINE=20090467; PubMed=10627041;
 RA   Rabus R., Jack D.L., Kelly D.J., Saier M.H. Jr;
 RT   "TRAP transporters: an ancient family of extracytoplasmic
 RT   solute-receptor-dependent secondary active transporters.";
 RL   Microbiology 145:3431-3445(1999).
 RN   [4]
 RP   TOPOLOGY.
 RC   STRAIN=B10;
 RX   PubMed=11150659;
 RA   Wyborn N.R., Alderson J., Andrews S.C., Kelly D.J.;
 RT   "Topological analysis of DctQ, the small integral membrane protein of
 RT   the C4-dicarboxylate TRAP transporter of Rhodobacter capsulatus.";
 RL   FEMS Microbiol. Lett. 194:13-17(2001).
 CC   -!- FUNCTION: REQUIRED FOR C4-DICARBOXYLATE TRANSPORT (table 2,ref2).
**   we could also say that transport is thought to use the membrane
**   potential, in that case we might put it into function. Note that in
**   ref 4 the use of an electrochemical gradient for energy is now sure
**   and is cited in the intro.
 CC   -!- ENZYME REGULATION: TRANSPORT IS SENSITIVE TO MEMBRANE POTENTIAL
 CC       UNCOUPLERS AND VANADATE INSENSITIVE (ref2 figs 6&7).
** inner membrane upon expression in ecoli, but can we extrapolate to Rhoca?
 CC   -!- SUBCELLULAR LOCATION: INTEGRAL MEMBRANE PROTEIN (ref2 fig4).
 DR   EMBL; AE009099; AAL42381.1; -.
DR   EMBL; X63974; CAA45386.1; -.
 DR   Pfam; PF00627; UBA; 1.
DR   PROSITE; PS12345; TEST_DOMAIN; 1.
**   PROSITE; PS12346; FALSE_DOMAIN; FALSE_POS.
 KW   Transmembrane; Transport.
 FT   TOPO_DOM      1     10       CYTOPLASMIC.
 FT   TRANSMEM     11     29
 FT   TOPO_DOM     30     68       PERIPLASMIC.
 FT   TRANSMEM     69     91
 FT   TOPO_DOM     92    112       CYTOPLASMIC.
 FT   TRANSMEM    113    133
 FT   TOPO_DOM    134    152       PERIPLASMIC.
 FT   TRANSMEM    153    173
 FT   TOPO_DOM    174    227       CYTOPLASMIC.
** transmembrane regions are unsure
**
**   #################     SOURCE SECTION     ##################
**   R capsulatus dctP gene for C4-dicarboxylase binding protein
**   source          1..4500
**                   /organism="Rhodobacter capsulatus"
**                   /strain="B10"
**                   /clone_lib="Cosmid, pLAFR1"
**                   /clone="pDCT200, pDCT205"
**                   /chromosome="1"
**   CDS             1084..1767
**                   /gene="dctQ"
**                   /product="small integral membrane transport protein"
**                   /db_xref="PID:e324693"
**   CDS_2_OUT_OF_5
**   26-JUN-1997 (Rel. 52, Last updated, Version 20)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_RHOCA
SQ   SEQUENCE   227 AA;  24763 MW;  0DE9D59DE5450F99 CRC64;
     MLRILDRAEE VLIAALIATA TVLIFVSVTH RFTLGFVADF VGFFRGHGMT GAAAAAKSLY
     TTLRGINLVW AQELCIILFV WMAKFGAAYG VRTGIHVGID VLINRLDAPK RRFFILLGLG
     AGALFTGIIA TLGANFVLHM YHASSTSPDL ELPMWLVYLA IPMGSSLMCF RFLQVAFGFA
     RTGELPHHDH GHVDGVDTEN EGIDAEGDVL LHSPLTPRDL VEKPKDN
//
ID   BL1A_HUMAN      PRELIMINARY;      PRT;   835 AA.
**BCLA_human = O95999
AC   Q9H165; Q86W14; Q8WU92; Q96JL6; Q9H163; Q9H164; Q9H3G9; Q9NWA7;
DT   01-MAR-2001 (TrEMBLrel. 16, Created)
DT   01-MAR-2001 (TrEMBLrel. 16, Last sequence update)
DT   01-MAR-2003 (TrEMBLrel. 23, Last annotation update)
DE   B-cell CLL/lymphoma 11A.
GN   BCL11A OR EVI9 OR KIAA1809.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606;
**   Q9H3G9;
RN   [1]
RP   SEQUENCE FROM N.A. (ISOFORMS 4 AND 5).
RC   TISSUE=Fetal brain;
RX   PubMed=11161790;
RA   Saiki Y., Yamazaki Y., Yoshida M., Katoh O., Nakamura T.;
RT   "Human EVI9, a homologue of the mouse myeloid leukemia gene, is
RT   expressed in the hematopoietic progenitors and down-regulated during
RT   myeloid differentiation of HL60 cells.";
RL   Genomics 70:387-391(2000).
**   Q9H165  Q9H163; Q9H164;
RN   [2]
RP   SEQUENCE FROM N.A. (ISOFORMS 1; 2 AND 3).
RX   PubMed=11719382;
RA   Satterwhite E., Sonoki T., Willis T.G., Harder L., Nowak R.,
RA   Arriola E.L., Liu H., Price H.P., Gesk S., Steinemann D.,
RA   Schlegelberger B., Oscier D.G., Siebert R., Tucker P.W., Dyer M.J.;
RT   "The BCL11 gene family: involvement of BCL11A in lymphoid
RT   malignancies.";
RL   Blood 98:3413-3420(2001).
**Q86W14;
RN   [3]
RP   SEQUENCE FROM N.A. (ISOFORM 6).
RA   Suriyapperuma S.P., Sarfarazi M.;
RT   "Identification of a new isoform for B-cell CLL/lymphoma 11A (BCL11A)
RT   gene.";
RL   Submitted (FEB-2003) to the EMBL/GenBank/DDBJ databases.
**   Q96JL6
RN   [4]
RP   SEQUENCE FROM N.A. (ISOFORM 2).
RC   TISSUE=Brain;
RX   PubMed=11853319;
RA   Nagase T., Kikuno R., Ohara O.;
RT   "Prediction of the coding sequences of unidentified human genes. XXII.
RT   The complete sequences of 50 new cDNA clones which code for large
RT   proteins.";
RL   DNA Res. 8:319-327(2001).
**   Q9NWA7;
RN   [5]
RP   SEQUENCE FROM N.A. (ISOFORM 7).
RC   TISSUE=Embryo;
RX   PubMed=14702039;
RA   Ota T., Suzuki Y., Nishikawa T., Otsuki T., Sugiyama T., Irie R.,
RA   Wakamatsu A., Hayashi K., Sato H., Nagai K., Kimura K., Makita H.,
RA   Sekine M., Obayashi M., Nishi T., Shibahara T., Tanaka T., Ishii S.,
RA   Yamamoto J.-I., Saito K., Kawai Y., Isono Y., Nakamura Y.,
RA   Nagahari K., Murakami K., Yasuda T., Iwayanagi T., Wagatsuma M.,
RA   Shiratori A., Sudo H., Hosoiri T., Kaku Y., Kodaira H., Kondo H.,
RA   Sugawara M., Takahashi M., Kanda K., Yokoi T., Furuya T., Kikkawa E.,
RA   Omura Y., Abe K., Kamihara K., Katsuta N., Sato K., Tanikawa M.,
RA   Yamazaki M., Ninomiya K., Ishibashi T., Yamashita H., Murakawa K.,
RA   Fujimori K., Tanai H., Kimata M., Watanabe M., Hiraoka S., Chiba Y.,
RA   Ishida S., Ono Y., Takiguchi S., Watanabe S., Yosida M., Hotuta T.,
RA   Kusano J., Kanehori K., Takahashi-Fujii A., Hara H., Tanase T.-O.,
RA   Nomura Y., Togiya S., Komai F., Hara R., Takeuchi K., Arita M.,
RA   Imose N., Musashino K., Yuuki H., Oshima A., Sasaki N., Aotsuka S.,
RA   Yoshikawa Y., Matsunawa H., Ichihara T., Shiohata N., Sano S.,
RA   Moriya S., Momiyama H., Satoh N., Takami S., Terashima Y., Suzuki O.,
RA   Nakagawa S., Senoh A., Mizoguchi H., Goto Y., Shimizu F., Wakebe H.,
RA   Hishigaki H., Watanabe T., Sugiyama A., Takemoto M., Kawakami B.,
RA   Yamazaki M., Watanabe K., Kumagai A., Itakura S., Fukuzumi Y.,
RA   Fujimori Y., Komiyama M., Tashiro H., Tanigami A., Fujiwara T.,
RA   Ono T., Yamada K., Fujii Y., Ozaki K., Hirao M., Ohmori Y.,
RA   Kawabata A., Hikiji T., Kobatake N., Inagaki H., Ikema Y., Okamoto S.,
RA   Okitani R., Kawakami T., Noguchi S., Itoh T., Shigeta K., Senba T.,
RA   Matsumura K., Nakajima Y., Mizuno T., Morinaga M., Sasaki M.,
RA   Togashi T., Oyama M., Hata H., Watanabe M., Komatsu T.,
RA   Mizushima-Sugano J., Satoh T., Shirai Y., Takahashi Y., Nakagawa K.,
RA   Okumura K., Nagase T., Nomura N., Kikuchi H., Masuho Y., Yamashita R.,
RA   Nakai K., Yada T., Nakamura Y., Ohara O., Isogai T., Sugano S.;
RT   "Complete sequencing and characterization of 21,243 full-length human
RT   cDNAs.";
RL   Nat. Genet. 36:40-45(2004).
**   Q96JL6
RN   [6]
RP   SEQUENCE FROM N.A. (ISOFORM 2).
RC   TISSUE=Lymph;
RX   MEDLINE=22388257; PubMed=12477932;
RA   Strausberg R.L., Feingold E.A., Grouse L.H., Derge J.G.,
RA   Klausner R.D., Collins F.S., Wagner L., Shenmen C.M., Schuler G.D.,
RA   Altschul S.F., Zeeberg B., Buetow K.H., Schaefer C.F., Bhat N.K.,
RA   Hopkins R.F., Jordan H., Moore T., Max S.I., Wang J., Hsieh F.,
RA   Diatchenko L., Marusina K., Farmer A.A., Rubin G.M., Hong L.,
RA   Stapleton M., Soares M.B., Bonaldo M.F., Casavant T.L., Scheetz T.E.,
RA   Brownstein M.J., Usdin T.B., Toshiyuki S., Carninci P., Prange C.,
RA   Raha S.S., Loquellano N.A., Peters G.J., Abramson R.D., Mullahy S.J.,
RA   Bosak S.A., McEwan P.J., McKernan K.J., Malek J.A., Gunaratne P.H.,
RA   Richards S., Worley K.C., Hale S., Garcia A.M., Gay L.J., Hulyk S.W.,
RA   Villalon D.K., Muzny D.M., Sodergren E.J., Lu X., Gibbs R.A.,
RA   Fahey J., Helton E., Ketteman M., Madan A., Rodrigues S., Sanchez A.,
RA   Whiting M., Madan A., Young A.C., Shevchenko Y., Bouffard G.G.,
RA   Blakesley R.W., Touchman J.W., Green E.D., Dickson M.C.,
RA   Rodriguez A.C., Grimwood J., Schmutz J., Myers R.M.,
RA   Butterfield Y.S.N., Krzywinski M.I., Skalska U., Smailus D.E.,
RA   Schnerch A., Schein J.E., Jones S.J.M., Marra M.A.;
RT   "Generation and initial analysis of more than 15,000 full-length
RT   human and mouse cDNA sequences.";
RL   Proc. Natl. Acad. Sci. U.S.A. 99:16899-16903(2002).
** PG 387 REF1
CC   -!- FUNCTION: An essential factor in lymphopoiesis and is required for
CC       B-cell formation in fetal liver. May play important roles in
CC       leukemogenesis and hematopoiesis. Might act as a dominant proto-
CC       oncogene. Potentiates ARP1-mediated transcriptional repression in
CC       a trichostatin-insensitive manner (By similarity).
**CC   -!- SUBCELLULAR LOCATION: Nuclear.
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing; Named isoforms=7;
CC       Name=1; Synonyms=XL;
CC         IsoId=Q9H165-1; Sequence=Displayed;
CC         Note=;
CC       Name=2; Synonyms=L;
CC         IsoId=Q9H165-2; Sequence=?;
CC         Note=;
CC       Name=3; Synonyms=S;
CC         IsoId=Q9H165-3; Sequence=?;
CC         Note=May be due to an exon skipping;
CC       Name=4;
CC         IsoId=Q9H165-4; Sequence=?;
CC         Note=;
CC       Name=5;
CC         IsoId=Q9H165-5; Sequence=?;
CC         Note=;
CC       Name=6;
CC         IsoId=Q9H165-6; Sequence=?;
CC         Note=;
CC       Name=7;
CC         IsoId=Q9H165-7; Sequence=?;
CC         Note=;
**PG 389 REF1 2 pg3415
CC   -!- TISSUE SPECIFICITY: Expressed at high levels in brain, spleen
CC       thymus, bone marrow and testis. Expressed in CD34-positive myeloid
CC       precursor cells, B cells, monocytes, and megakaryoctes. Expression
CC       is tightly regulated during B-cell development.
CC   -!- DISEASE: May be involved in lymphoid malignancies through either
CC       chromosomal translocation t(2;14)(p13;q32.3), or amplification.
**add standard line about chromosomal translocation, add Ft line for breakpoints if known
**add cc similarity
**   Q9H3G9;
DR   EMBL; AF080216; AAG49025.1; -.
**   Q9H165; Q9H163;Q9H164;
DR   EMBL; AJ404611; CAC17723.1; -.
DR   EMBL; AJ404612; CAC17724.1; -.
DR   EMBL; AJ404613; CAC17725.1; -.
**Q86W14;
DR   EMBL; AY228763; AAO88272.1; -.
**Q96JL6; Q8WU92;
DR   EMBL; AB058712; BAB47438.1; ALT_INIT.
**   Q9NWA7;
DR   EMBL; AK001035; BAA91476.1; -.
**Q96JL6; Q8WU92;
DR   EMBL; BC021098; AAH21098.1; ALT_INIT.
DR   HSSP; P15822; 1BBO.
DR   HGNC; HGNC:13221; BCL11A.
DR   MIM; 606557; -.
DR   InterPro; IPR007087; Znf_C2H2.
DR   Pfam; PF00096; zf-C2H2; 6.
DR   SMART; SM00355; ZnF_C2H2; 6.
DR   PROSITE; PS00028; ZINC_FINGER_C2H2_1; 6.
DR   PROSITE; PS50157; ZINC_FINGER_C2H2_2; 6.
KW   B-cell activation; Metal-binding; Zinc; Zinc-finger;
KW   Chromosomal translocation; Transcription regulation;
KW   Alternative splicing.
FT   ZN_FING     170    193       C2H2-type 1.
FT   ZN_FING     377    399       C2H2-type 2.
FT   ZN_FING     405    429       C2H2-type 3.
FT   ZN_FING     742    764       C2H2-type 4.
FT   ZN_FING     770    792       C2H2-type 5.
FT   ZN_FING     800    823       C2H2-type 6.
FT   COMPBIAS    260    373       Pro-rich.
FT   COMPBIAS    481    509       Glu-rich.
FT   VAR_SEQ       1    648       Missing (in isoform 7).
FT   VAR_SEQ       1      1       M -> MSKGTDEDIFSGVSFFLTRLSRCEPSRRPPAPQPT
FT                                (in isoform 4 and isoform 5).
FT   VAR_SEQ     129    163       DKLLHWRGLSSPRSAHGALIPTPGMSAEYAPQGIC -> G
FT                                (in isoform 6).
FT   VAR_SEQ     211    239       VGIPSGLGAECPSQPPLHGIHIADNNPFN -> CSSHTPIR
FT                                RSTQRAQDVWQFSDGSRALKF (in isoform 5).
FT   VAR_SEQ     212    243       GIPSGLGAECPSQPPLHGIHIADNNPFNLLRI -> LHTPP
FT                                FGVVPRELKMCGSFRMEAREPLSSEKI (in isoform
FT                                3).
FT   VAR_SEQ     240    835       Missing (in isoform 5).
FT   VAR_SEQ     244    835       Missing (in isoform 3).
FT   VAR_SEQ     522    532       Missing (in isoform 4).
FT   VAR_SEQ     745    773       EYCGKVFKNCSNLTVHRRSHTGERPYKCE -> SSHTPIRR
FT                                STQRAQDVWQFSDGSSRALKF (in isoform 2).
FT   VAR_SEQ     745    773       EYCGKVFKNCSNLTVHRRSHTGERPYKCE -> SSHTPIRR
FT                                STQRAQDVWQFSDGSSRALKF (in isoform 4).
FT   VAR_SEQ     774    835       Missing (in isoform 2).
FT   CONFLICT    119    119       G -> R (in Ref. 2).
FT   CONFLICT    316    316       S -> F (in Ref. 1).
FT   CONFLICT    386    386       F -> L (in Ref. 1).
FT   CONFLICT    648    648       A -> T (in Ref. 2; CAC17723/CAC17724).
FT   CONFLICT    653    653       E -> D (in Ref. 1).
FT   CONFLICT    730    730       P -> T (in Ref. 2; CAC17723/CAC17724).
**
**   #################    INTERNAL SECTION    ##################
**CL 2p16.1;
**NI BCLLA
**IS Q9H165-8.
SQ   SEQUENCE   835 AA;  91196 MW;  D36A7D0BE6976DCF CRC64;
     MSRRKQGKPQ HLSKREFSPE PLEAILTDDE PDHGPLGAPE GDHDLLTCGQ CQMNFPLGDI
     LIFIEHKRKQ CNGSLCLEKA VDKPPSPSPI EMKKASNPVE VGIQVTPEDD DCLSTSSRGI
     CPKQEHIADK LLHWRGLSSP RSAHGALIPT PGMSAEYAPQ GICKDEPSSY TCTTCKQPFT
     SAWFLLQHAQ NTHGLRIYLE SEHGSPLTPR VGIPSGLGAE CPSQPPLHGI HIADNNPFNL
     LRIPGSVSRE ASGLAEGRFP PTPPLFSPPP RHHLDPHRIE RLGAEEMALA THHPSAFDRV
     LRLNPMAMEP PAMDFSRRLR ELAGNTSSPP LSPGRPSPMQ RLLQPFQPGS KPPFLATPPL
     PPLQSAPPPS QPPVKSKSCE FCGKTFKFQS NLVVHRRSHT GEKPYKCNLC DHACTQASKL
     KRHMKTHMHK SSPMTVKSDD GLSTASSPEP GTSDLVGSAS SALKSVVAKF KSENDPNLIP
     ENGDEEEEED DEEEEEEEEE EEEELTESER VDYGFGLSLE AARHHENSSR GAVVGVGDES
     RALPDVMQGM VLSSMQHFSE AFHQVLGEKH KRGHLAEAEG HRDTCDEDSV AGESDRIDDG
     TVNGRGCSPG ESASGGLSKK LLLGSPSSLS PFSKRIKLEK EFDLPPAAMP NTENVYSQWL
     AGYAASRQLK DPFLSFGDSR QSPFASSSEH SSENGSLRFS TPPGELDGGI SGRSGTGSGG
     STPHISGPGP GRPSSKEGRR SDTCEYCGKV FKNCSNLTVH RRSHTGERPY KCELCNYACA
     QSSKLTRHMK THGQVGKDVY KCEICKMPFS VYSTLEKHMK KWHSDRVLNN DIKTE
//
ID   NUSG_SULAC     STANDARD;      PRT;   152 AA.
AC   P27341;
DT   01-AUG-1992 (Rel. 23, Created)
DT   01-AUG-1992 (Rel. 23, Last sequence update)
DT   16-OCT-2001 (Rel. 40, Last annotation update)
DE   Putative transcription antitermination protein nusG.
OS   Sulfolobus acidocaldarius.
OC   Archaea; Crenarchaeota; Thermoprotei; Sulfolobales; Sulfolobaceae;
OC   Sulfolobus.
OX   NCBI_TaxID=2285;
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=92048486; PubMed=1658539;
RA   Ramirez C., Matheson A.T.;
RT   "A gene in the archaebacterium Sulfolobus solfataricus that codes for
RT   a protein equivalent to the alpha subunits of the signal recognition
RT   particle receptor in eukaryotes.";
RL   Mol. Microbiol. 5:1687-1693(1991).
RN   [2]
RP   SEQUENCE FROM N.A.
RC   STRAIN=ATCC 33909 / NCIB 11770 / DSM 639;
RX   MEDLINE=95226466; PubMed=7711082;
RA   Moll R., Schmidtke S., Schaefer G.;
RT   "Nucleotide sequence of a gene cluster encoding ribosomal proteins in
RT   the thermoacidophilic crenarchaeon Sulfolobus acidocaldarius.";
RL   Biochim. Biophys. Acta 1261:315-318(1995).
RN   [3]
RP   SIMILARITY.
RX   MEDLINE=95206934; PubMed=7899076;
RA   Ouzounis C., Kyrpides N., Sander C.;
RT   "Novel protein families in archaean genomes.";
RL   Nucleic Acids Res. 23:565-570(1995).
CC   -!- SIMILARITY: TO BACTERIAL PROTEINS NUSG AND ALSO TO THE L24P FAMILY
CC       OF RIBOSOMAL PROTEINS.
CC   -!- CAUTION: Was originally (Ref.1) thought to originate from
CC       S.solfataricus strain P1, but the culture was contaminated with
CC       S.acidocaldarius.
DR   EMBL; X58538; CAA41431.1; -.
DR   EMBL; X77509; CAA54645.1; -.
DR   PIR; S53705; S53705.
DR   InterPro; IPR003257; Bac_NusG.
DR   InterPro; IPR005824; KOW.
DR   InterPro; IPR006646; KOW_sub.
DR   InterPro; IPR006645; NgN.
DR   InterPro; IPR008991; Transl_SH3_like.
DR   Pfam; PF00467; KOW; 1.
DR   ProDom; PD005267; Bac_NusG; 1.
DR   SMART; SM00739; KOW; 1.
DR   SMART; SM00738; NGN; 1.
**   PROSITE; PS01108; RIBOSOMAL_L24; FALSE_POS_1.
**   PROSITE; PS00430; TONB_DEPENDENT_REC_1; FALSE_POS_1.
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   152 AA;  16877 MW;  316F31BBCC22620D CRC64;
     MEDFKYRNYY VLRVTGGQEI NVALILEERI KTNNINEIFS VVVPPNIKGY VILEATGPHV
     VKLISSGIRH VKGVAHGLIQ KEDVTKFVSK SVALPAVKEG DLVEVISGPF RGMQAQVVRV
     ESTKNEVVLN ILESSYPVQV TVPLEQVKPV KR
//
ID   NUSG_SULAC     STANDARD;      PRT;   152 AA.
AC   P27341;
DT   01-AUG-1992 (Rel. 23, Created)
DT   01-AUG-1992 (Rel. 23, Last sequence update)
DT   16-OCT-2001 (Rel. 40, Last annotation update)
DE   Putative transcription antitermination protein nusG.
OS   Sulfolobus acidocaldarius.
OC   Archaea; Crenarchaeota; Thermoprotei; Sulfolobales; Sulfolobaceae;
OC   Sulfolobus.
OX   NCBI_TaxID=2285;
 CC       -!- FUNCTION: Nonselective ADPR-gated cation channel mediating sodium
 CC       and calcium ion influx in response to oxidative stress.
 CC       Extracellular calcium passes through the channel and acts from the
 CC       intracellular side as a positive regulator in channel activation
 CC       by hydrogen superoxide. Also activated by nicotinamide adenine
 CC       dinucleotide (NAD(+)), reactive nitrogen species and arachidonic
 CC       acid. Confers susceptibility to cell death following oxidative
 CC       stress. Isoform 2 does not seem to be regulated by ADPR. Has an
 CC       ADP-ribose pyrophosphatase activity.
 CC       -!- CATALYTIC ACTIVITY: ADP-ribose + H(2)O = AMP + D-ribose 5-
 CC       phosphate.
**no evidence found
 CC       -!- COFACTOR: Binds NAD(+).
 CC       -!- SUBUNIT: Isoform 1 can interact with isoform 3. This interaction
 CC       decreases calcium influx through isoform 1 and suppresses
 CC       susceptibility to oxidative stress-induced cell death.
 CC       -!- SUBCELLULAR LOCATION: Integral membrane protein.
 CC       -!- ALTERNATIVE PRODUCTS:
 CC       Event=Alternative splicing; Named isoforms=3;
 CC       Comment=Additional isoforms seem to exist;
 CC       Name=1; Synonyms=TRPM2-L;
 CC       IsoId=O94759-1; Sequence=Displayed;
 CC       Name=2;
 CC       IsoId=O94759-2; Sequence=VSP_006574, VSP_006575;
 CC       Name=3; Synonyms=TRPM2-S;
 CC       IsoId=O94759-3; Sequence=?;
 CC       -!- TISSUE SPECIFICITY: Highly expressed in brain and peripheral blood
 CC       cells, such as neutrophils. Also detected in bone marrow, spleen,
 CC       heart, liver and lung. Isoform 2 is found in neutrophil
 CC       granulocytes.
CC       -!- SIMILARITY: Belongs to the transient receptor family. LTrpC
CC       subfamily.
 CC       -!- CAUTION: Ref.5 sequence differs from that shown due to frameshifts
 CC       in positions 1227 and 1237.
 CC       -!- BIOPHYSICOCHEMICAL PROPERTIES:
 CC       Kinetic parameters:
 CC         KM=100 uM for ADP-ribose;
 CC         Vmax=0.1 umol/min/mg enzyme;
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   152 AA;  16877 MW;  316F31BBCC22620D CRC64;
     MEDFKYRNYY VLRVTGGQEI NVALILEERI KTNNINEIFS VVVPPNIKGY VILEATGPHV
     VKLISSGIRH VKGVAHGLIQ KEDVTKFVSK SVALPAVKEG DLVEVISGPF RGMQAQVVRV
     ESTKNEVVLN ILESSYPVQV TVPLEQVKPV KR
//
ID   PAX3_HUMAN     STANDARD;      PRT;   479 AA.
AC   P23760; Q16448;
DT   01-NOV-1991 (Rel. 20, Created)
DT   01-NOV-1995 (Rel. 32, Last sequence update)
DT   28-FEB-2003 (Rel. 41, Last annotation update)
DE   Paired box protein Pax-3 (HUP2).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606;
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   479 AA;  52968 MW;  8AFCA674E3ACB4FE CRC64;
     MTTLAGAVPR MMRPGPGQNY PRSGFPLEVS TPLGQGRVNQ LGGVFINGRP LPNHIRHKIV
     EMAHHGIRPC VISRQLRVSH GCVSKILCRY QETGSIRPGA IGGSKPKQVT TPDVEKKIEE
     YKRENPGMFS WEIRDKLLKD AVCDRNTVPS VSSISRILRS KFGKGEEEEA DLERKEAEES
     EKKAKHSIDG ILSERASAPQ SDEGSDIDSE PDLPLKRKQR RSRTTFTAEQ LEELERAFER
     THYPDIYTRE ELAQRAKLTE ARVQVWFSNR RARWRKQAGA NQLMAFNHLI PGGFPPTAMP
     TLPTYQLSET SYQPTSIPQA VSDPSSTVHR PQPLPPSTVH QSTIPSNPDS SSAYCLPSTR
     HGFSSYTDSF VPPSGPSNPM NPTIGNGLSP QVMGLLTNHG GVPHQPQTDY ALSPLTGGLE
     PTTTVSASCS QRLDHMKSLD SLPTSQSYCP PTYSTTGYSM DPVTGYQYGQ YGQSKPWTF
//
