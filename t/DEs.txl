ID   PYR5_DROME     STANDARD;      PRT;   493 AA.
AC   Q01637; Q24221;
DT   01-JUL-1993, integrated into UniProtKB/Swiss-Prot.
DT   01-JUL-1993, sequence version 1.
DT   30-MAY-2000, entry version 1.
DE   URIDINE 5'-MONOPHOSPHATE SYNTHASE (UMP SYNTHASE) (RUDIMENTARY-LIKE-
DE   PROTEIN) [INCLUDES: OROTATE PHOSPHORIBOSYLTRANSFERASE (EC 2.4.2.10)
DE   (OPRTASE); OROTIDINE 5'-PHOSPHATE DECARBOXYLASE (EC 4.1.1.23)
DE   (OMPDECASE)] (Fragments).
GN   R-L.
OS   Drosophila melanogaster (Fruit fly).
OC   Eukaryota; Metazoa; Arthropoda; Tracheata; Hexapoda; Insecta;
OC   Pterygota; Neoptera; Endopterygota; Diptera; Brachycera; Muscomorpha;
OC   Ephydroidea; Drosophilidae; Drosophila.
OX   NCBI_TaxID=7227;
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Eisenberg M.T., Kirkpatrick R., Rawls J.;
RL   Submitted (XXX-1992) to the EMBL/GenBank/DDBJ databases.
SQ   SEQUENCE   493 AA;  53327 MW;  56479CDAB1F6A308 CRC64;
     MVAQNSDKMR ALALKLFEIN AFKFGDFKMK VGINSPVYFD LRVIVSLGLP QQTVSDLLVE
     HIKDKQLSAK HVCGVPYTAL PRATIVSVQQ GTPMLVRRKE AKAYGTKKLV EGIFNAGDTC
     LIVEDVVTSG SSILDTVRDL QGEGIVVTDA VVVVDREQGG VANIAKHGVR MHSLFTLSFL
     LNTLHEAGRI EKSTVEAVAK YIAAVQINSD GTFVGGDKVT FPAANDLQRT KLTYESRANL
     AKSAVAKRLF NLIASKQTNL CLAADLTHAD EILDVADKCG PYICLLKTHV DIVEDFSDKF
     IADLQALAQR HNFLLMEDRK FADIGNTVSL QYGKGIYKIS SWADLVTAHT LPGRSILQGL
     KAGLGEGGAG KERGVFLLAE MSASGNLIDA KYKENSNKIA TEGADVDFVA GVVCQSSDAF
     AFPGLLQLTP GVKIDEGVDQ LGQQYQSPEH VVKERGADIG VVGRGILKAS SPKQAAQTYR
     DRLWAAYQDR VAK
//
ID   25AA_MOUSE     STANDARD;      PRT;   367 AA.
AC   P11928; Q64440;
DT   01-JUL-1993, integrated into UniProtKB/Swiss-Prot.
DT   01-JUL-1993, sequence version 1.
DT   30-MAY-2000, entry version 1.
DE   (2'-5')OLIGOADENYLATE SYNTHETASE 1A (EC 2.7.7.-) ((2-5')OLIGO(A)
DE   SYNTHETASE 1A) (2-5A SYNTHETASE 1A).
GN   OAS1A OR OIAS1.
OS   Mus musculus (Mouse).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Rodentia; Sciurognathi; Muridae; Murinae; Mus.
OX   NCBI_TaxID=10090;
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=87117515; PubMed=3808949;
RT   "Mouse 2-5A synthetase cDNA: nucleotide sequence and comparison to
RT   human 2-5A synthetase.";
RL   Nucleic Acids Res. 14:10117-10117(1986).
RN   [2]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=91021025; PubMed=2171206;
RA   Coccia E.M., Romeo G., Nissim A., Marziali G., Albertini R.,
RA   Affabris E., Battistini A., Fiorucci G., Orsatti R., Rossi G.B.,
RA   Chebath J.;
RT   "A full-length murine 2-5A synthetase cDNA transfected in NIH-3T3
RT   cells impairs EMCV but not VSV replication.";
RL   Virology 179:228-233(1990).
RN   [3]
RP   SEQUENCE OF 14-367 FROM N.A.
RX   MEDLINE=91232962; PubMed=1709495;
RA   Rutherford M.N., Kumar A., Nissim A., Chebath J., Williams B.R.G.;
RT   "The murine 2-5A synthetase locus: three distinct transcripts from
RT   two linked genes.";
RL   Nucleic Acids Res. 19:1917-1924(1991).
CC   -!- CATALYTIC ACTIVITY: BINDS DOUBLE-STRANDED RNA AND POLYMERIZES ATP
CC       INTO PPP(A2'P5'A)N OLIGOMERS, WHICH ACTIVATE THE LATENT RNASE L
CC       THAT, WHEN ACTIVATED, CLEAVES SINGLE-STRANDED RNAS.
CC   -!- INDUCTION: BY INTERFERONS.
CC   -!- SIMILARITY: BELONGS TO THE 2-5A SYNTHETASE FAMILY.
DR   EMBL; X04958; CAA28620.1; -.
DR   EMBL; M33863; AAA37116.1; -.
DR   EMBL; X58077; CAA41105.1; -.
DR   PIR; A24725; SYMSO1.
DR   MGI; MGI:97429; OAS1A.
DR   INTERPRO; IPR001797; -.
DR   PROSITE; PS00832; 25A_SYNTH_1; 1.
DR   PROSITE; PS00833; 25A_SYNTH_2; 1.
KW   RNA-binding; Transferase; Nucleotidyltransferase;
KW   Interferon induction; Multigene family.
SQ   SEQUENCE   367 AA;  42456 MW;  3A4D145FA582A976 CRC64;
     MEHGLRSIPA WTLDKFIEDY LLPDTTFGAD VKSAVNVVCD FLKERCFQGA AHPVRVSKVV
     KGGSSGKGTT LKGRSDADLV VFLNNLTSFE DQLNRRGEFI KEIKKQLYEV QHERRFRVKF
     EVQSSWWPNA RSLSFKLSAP HLHQEVEFDV LPAFDVLGHV NTSSKPDPRI YAILIEECTS
     LGKDGEFSTC FTELQRNFLK QRPTKLKSLI RLVKHWYQLC KEKLGKPLPP QYALELLTVF
     AWEQGNGCYE FNTAQGFRTV LELVINYQHL RIYWTKYYDF QHQEVSKYLH RQLRKARPVI
     LDPADPTGNV AGGNPEGWRR LAEEADVWLW YPCFIKKDGS RVSSWDVPTV VPVPFEQVEE
     NWTCILL
//
ID   ARGJ_BACST     STANDARD;      PRT;   410 AA.
AC   Q07908;
DT   01-OCT-1994 (Rel. 30, Created)
DT   01-OCT-1994 (Rel. 30, Last sequence update)
DT   15-JUN-2002 (Rel. 41, Last annotation update)
DE   Arginine biosynthesis bifunctional protein argJ [Includes: Glutamate-
DE   and N-acetyltransferase (EC 2.3.1.35) (Ornithine acetyltransferase)
DE   (Ornithine transacetylase) (OATASE); Amino-acid acetyltransferase
DE   (EC 2.3.1.1) (N-acetylglutamate synthase) (AGS)] [Contains: Arginine
DE   biosynthesis bifunctional protein alpha chain; Arginine biosynthesis
DE   bifunctional protein beta chain].
GN   ARGJ.
OS   Bacillus stearothermophilus.
OC   Bacteria; Firmicutes; Bacillus/Clostridium group; Bacillales;
OC   Geobacillus.
OX   NCBI_TaxID=1422;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=NCIB 8224;
RX   MEDLINE=93232760; PubMed=8473852;
RA   Sakanyan V., Charlier D.R.M., Legrain C., Kochikyan A., Mett I.,
RA   Pierard P., Glansdorff N.;
RT   "Primary structure, partial purification and regulation of key
RT   enzymes of the acetyl cycle of arginine biosynthesis in Bacillus
RT   stearothermophilus: dual function of ornithine acetyltransferase.";
RL   J. Gen. Microbiol. 139:393-402(1993).
RN   [2]
RP   CHARACTERIZATION, AND MUTAGENESIS OF THR-197.
RX   PubMed=11320085;
RA   Marc F., Weigel P., Legrain C., Glansdorff N., Sakanyan V.;
RT   "An invariant threonine is involved in self-catalyzed cleavage of the
RT   precursor protein for ornithine acetyltransferase.";
RL   J. Biol. Chem. 276:25404-25410(2001).
CC   -!- FUNCTION: Catalyzes two activities which are involved in the
CC       cyclic version of arginine biosynthesis: the synthesis of
CC       acetlyglutamate from glutamate and acetyl-CoA, and of ornithine by
CC       transacetylation between acetylornithine and glutamate.
CC   -!- CATALYTIC ACTIVITY: N(2)-acetyl-L-ornithine + L-glutamate = L-
CC       ornithine + N-acetyl-L-glutamate.
CC   -!- CATALYTIC ACTIVITY: Acetyl-CoA + L-glutamate = CoA + N-acetyl-L-
CC       glutamate.
CC   -!- ENZYME REGULATION: Feedback inhibition by L-arginine.
CC   -!- PATHWAY: Arginine biosynthesis; first and fifth steps.
CC   -!- SUBUNIT: Heterotetramer of two alpha and two beta chains.
CC   -!- MISCELLANEOUS: Independently synthesized alpha and beta subunits
CC       do not reconstitute a functional protein. Self-catalyzed precursor
CC       cleavage is a necessary step to form an active enzyme, probably by
CC       directing appropriate folding and/or topological organization of
CC       the active site.
CC   -!- SUBCELLULAR LOCATION: Cytoplasmic (Probable).
CC   -!- SIMILARITY: BELONGS TO THE ARGJ FAMILY.
DR   EMBL; L06036; AAA22197.1; -.
DR   InterPro; IPR002813; ArgJ.
DR   Pfam; PF01960; ArgJ; 1.
DR   ProDom; PD004193; ArgJ; 1.
DR   TIGRFAMs; TIGR00120; ArgJ; 1.
KW   Arginine biosynthesis; Multifunctional enzyme; Transferase;
KW   Acyltransferase.
FT   CHAIN         1    196       ARGININE BIOSYNTHESIS BIFUNCTIONAL
FT                                PROTEIN ARGJ ALPHA CHAIN.
FT   CHAIN       197    410       ARGININE BIOSYNTHESIS BIFUNCTIONAL
FT                                PROTEIN ARGJ BETA CHAIN.
FT   SITE        196    197       CLEAVAGE (NONHYDROLYTIC).
FT   MUTAGEN     197    197       T->G: NO AUTOPROTEOLYSIS; LOSS OF
FT                                ACTIVITY.
FT   MUTAGEN     197    197       T->S,C: LOW RATE OF INTRAMOLECULAR
FT                                CLEAVAGE; LOSS OF ACTIVITY.
** to be added:
**HF HAMAP; MF_01106; 1.
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   410 AA;  43377 MW;  00E948D64E8A8913 CRC64;
     MTITKQTGQV TAVADGTVVT PEGFQAAGVN AGLRYSKNDL GVILCDVPAS AAAVYTQSHF
     QAAPLKVTQA SLAVEQKLQA VIVNRPCANA CTGAQGLKDA YEMRELCAKQ FGLALHHVAV
     ASTGVIGEYL PMEKIRAGIK QLVPGVTMAD AEAFQTAILT TDTVMKRACY QTTIDGKTVT
     VGGAAKGSGM IHPNMATMLA FITTDANVSS PVLHAALRSI TDVSFNQITV DGDTSTNDMV
     VVMASGLAGN DELTPDHPDW ENFYEALRKT CEDLAKQIAK DGEGATKLIE VRVRGAKTDE
     EAKKIAKQIV GSNLVKTAVY GADANWGRII GAIGYSDAEV NPDNVDVAIG PMVMLKGSEP
     QPFSEEEAAA YLQQETVVIE VDLHIGDGVG VAWGCDLTYD YVKINASYRT
//
ID   ANGT_HUMAN     STANDARD;      PRT;   485 AA.
AC   P01019; Q16358; Q16359; Q96F91;
DT   21-JUL-1986 (Rel. 01, Created)
DT   21-JUL-1986 (Rel. 01, Last sequence update)
DT   28-FEB-2003 (Rel. 41, Last annotation update)
DE   Angiotensinogen precursor [Contains: Angiotensin I (Ang I);
DE   Angiotensin II (Ang II); Angiotensin III (Ang III) (Des-Asp[1]-
DE   angiotensin II)].
GN   AGT OR SERPINA8.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606;
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=89170129; PubMed=2924688;
RA   Gaillard I., Clauser E., Corvol P.;
RT   "Structure of human angiotensinogen gene.";
RL   DNA 8:87-99(1989).
RN   [2]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=85000455; PubMed=6089875;
RA   Kageyama R., Ohkubo H., Nakanishi S.;
RT   "Primary structure of human preangiotensinogen deduced from the
RT   cloned cDNA sequence.";
RL   Biochemistry 23:3603-3609(1984).
RN   [3]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=90237063; PubMed=1692023;
RA   Fukamizu A., Takahashi S., Seo M.S., Tada M., Tanimoto K., Uehara S.,
RA   Murakami K.;
RT   "Structure and expression of the human angiotensinogen gene.
RT   Identification of a unique and highly active promoter.";
RL   J. Biol. Chem. 265:7576-7582(1990).
RN   [4]
RP   SEQUENCE FROM N.A.
RC   TISSUE=Brain;
RA   Strausberg R.;
RL   Submitted (JUL-2001) to the EMBL/GenBank/DDBJ databases.
RN   [5]
RP   SEQUENCE OF 1-338 FROM N.A.
RX   MEDLINE=87244745; PubMed=2885106;
RA   Kunapuli S.P., Kumar A.;
RT   "Molecular cloning of human angiotensinogen cDNA and evidence for the
RT   presence of its mRNA in rat heart.";
RL   Circ. Res. 60:786-790(1987).
RN   [6]
RP   SEQUENCE OF 34-45, AND SUBUNITS.
RC   TISSUE=Serum;
RX   MEDLINE=95293954; PubMed=7539791;
RA   Oxvig C., Haaning J., Kristensen L., Wagner J.M., Rubin I.,
RA   Stigbrand T., Gleich G.J., Sottrup-Jensen L.;
RT   "Identification of angiotensinogen and complement C3dg as novel
RT   proteins binding the proform of eosinophil major basic protein in
RT   human pregnancy serum and plasma.";
RL   J. Biol. Chem. 270:13645-13651(1995).
RN   [7]
RP   SEQUENCE OF 34-43.
RX   MEDLINE=69014170; PubMed=4300938;
RA   Arakawa K., Minohara A., Yamada J., Nakamura M.;
RT   "Enzymatic degradation and electrophoresis of human angiotensin I.";
RL   Biochim. Biophys. Acta 168:106-112(1968).
RN   [8]
RP   CARBOHYDRATE-LINKAGE SITES.
RX   MEDLINE=86056581; PubMed=3934016;
RA   Campbell D.J., Bouhnik J., Coezy E., Menard J., Corvol P.;
RT   "Processing of rat and human angiotensinogen precursors by microsomal
RT   membranes.";
RL   Mol. Cell. Endocrinol. 43:31-40(1985).
RN   [9]
RP   FUNCTION OF ANGIOTENSIN III.
RX   MEDLINE=75166949; PubMed=1132082;
RA   Goodfriend T.L., Peach M.J.;
RT   "Angiotensin III: (DES-Aspartic Acid-1)-Angiotensin II. Evidence and
RT   speculation for its role as an important agonist in the renin -
RT   angiotensin system.";
RL   Circ. Res. 36:38-48(1975).
RN   [10]
RP   STRUCTURE BY NMR OF ANGIOTENSIN II.
RX   MEDLINE=98151281; PubMed=9492317;
RA   Carpenter K.A., Wilkes B.C., Schiller P.W.;
RT   "The octapeptide angiotensin II adopts a well-defined structure in a
RT   phospholipid environment.";
RL   Eur. J. Biochem. 251:448-453(1998).
RN   [11]
RP   VARIANTS MET-207; THR-268 AND CYS-281.
RX   MEDLINE=93008239; PubMed=1394429;
RA   Jeunemaitre X., Soubrier F., Kotelevtsev Y.V., Lifton R.P.,
RA   Williams C.S., Charru A., Hunt S.C., Hopkins P.N., Williams R.R.,
RA   Lalouel J.-M., Corvol P.;
RT   "Molecular basis of human hypertension: role of angiotensinogen.";
RL   Cell 71:169-180(1992).
RN   [12]
RP   VARIANT THR-268.
RX   MEDLINE=93291876; PubMed=8513325;
RA   Ward K., Hata A., Jeunemaitre X., Helin C., Nelson L., Namikawa C.,
RA   Farrington P.F., Ogasawara M., Suzumori K., Tomoda S., Berrebi S.,
RA   Sasaki M., Corvol P., Lifton R.P., Lalouel J.-M.;
RT   "A molecular variant of angiotensinogen associated with
RT   preeclampsia.";
RL   Nat. Genet. 4:59-61(1993).
RN   [13]
RP   VARIANTS ILE-242; ARG-244 AND CYS-281.
RX   MEDLINE=95331754; PubMed=7607642;
RA   Hixson J.E., Powers P.K.;
RT   "Detection and characterization of new mutations in the human
RT   angiotensinogen gene (AGT).";
RL   Hum. Genet. 96:110-112(1995).
RN   [14]
RP   CHARACTERIZATION OF VARIANT CYS-281.
RX   MEDLINE=96199253; PubMed=8621667;
RA   Gimenez-Roqueplo A.P., Leconte I., Cohen P., Simon D., Guyene T.T.,
RA   Celerier J., Pau B., Corvol P., Clauser E., Jeunemaitre X.;
RT   "The natural mutation Y248C of human angiotensinogen leads to abnormal
RT   glycosylation and altered immunological recognition of the protein.";
RL   J. Biol. Chem. 271:9838-9844(1996).
CC   -!- FUNCTION: IN RESPONSE TO LOWERED BLOOD PRESSURE, THE ENZYME RENIN
CC       CLEAVES ANGIOTENSIN I, FROM ANGIOTENSINOGEN. ACE (ANGIOTENSIN
CC       CONVERTING ENZYME) THEN REMOVES A DIPEPTIDE TO YIELD THE
CC       PHYSIOLOGICALLY ACTIVE PEPTIDE ANGIOTENSIN II, THE MOST POTENT
CC       PRESSOR SUBSTANCE KNOWN, WHICH HELPS REGULATE VOLUME AND MINERAL
CC       BALANCE OF BODY FLUIDS.
CC   -!- FUNCTION: Angiotensin III stimulates aldosterone release.
CC   -!- SUBUNIT: During pregnancy, exists as a disulfide-linked 2:2
CC       heterotetramer with the proform of PRG2 and as a complex (probably
CC       a 2:2:2 heterohexamer) with pro-PRG2 and C3dg.
CC   -!- SUBCELLULAR LOCATION: Secreted.
CC   -!- TISSUE SPECIFICITY: Synthesized by the liver and secreted in the
CC       plasma.
CC   -!- DISEASE: AGT SEEMS TO BE ASSOCIATED WITH A PREDISPOSITION TO
CC       ESSENTIAL HYPERTENSION AS WELL AS PREGNANCY-INDUCED HYPERTENSION
CC       (PIH) (PREECLAMPSIA).
CC   -!- SIMILARITY: BELONGS TO THE SERPIN FAMILY.
CC   -!- CAUTION: IT IS UNCERTAIN WHETHER MET-1 OR MET-10 IS THE INITIATOR.
DR   EMBL; K02215; AAA51731.1; -.
DR   EMBL; M24689; AAA51679.1; -.
DR   EMBL; M24686; AAA51679.1; JOINED.
DR   EMBL; M24687; AAA51679.1; JOINED.
DR   EMBL; M24688; AAA51679.1; JOINED.
DR   EMBL; X15324; CAA33385.1; -.
DR   EMBL; X15325; CAA33385.1; JOINED.
DR   EMBL; X15326; CAA33385.1; JOINED.
DR   EMBL; X15327; CAA33385.1; JOINED.
DR   EMBL; M69110; AAA52282.1; -.
DR   EMBL; BC011519; AAH11519.1; -.
DR   EMBL; S78529; AAD14287.1; -.
DR   EMBL; S78530; AAD14288.1; -.
DR   PIR; A01249; ANHU.
DR   PIR; A31362; A31362.
DR   PIR; A35203; A35203.
DR   SWISS-2DPAGE; P01019; HUMAN.
DR   Genew; HGNC:333; AGT.
DR   MIM; 106150; -.
DR   GO; GO:0005625; C:soluble fraction; TAS.
DR   GO; GO:0004867; F:serine protease inhibitor activity; TAS.
DR   GO; GO:0007166; P:cell surface receptor linked signal transdu...; TAS.
DR   GO; GO:0007267; P:cell-cell signaling; TAS.
DR   GO; GO:0007565; P:pregnancy; TAS.
DR   GO; GO:0008217; P:regulation of blood pressure; TAS.
DR   InterPro; IPR000227; Angiotensngn.
DR   InterPro; IPR000215; Serpin.
DR   Pfam; PF00079; serpin; 1.
DR   PRINTS; PR00654; ANGIOTENSNGN.
DR   SMART; SM00093; SERPIN; 1.
DR   PROSITE; PS00284; SERPIN; 1.
KW   Vasoconstrictor; Glycoprotein; Plasma; Serpin; Signal;
KW   Disease mutation; Polymorphism.
FT   SIGNAL        1     33
FT   CHAIN        34    485       ANGIOTENSINOGEN.
FT   PEPTIDE      34     43       ANGIOTENSIN I.
FT   PEPTIDE      34     41       ANGIOTENSIN II.
FT   PEPTIDE      35     41       ANGIOTENSIN III.
FT   CARBOHYD     47     47       N-LINKED (GLCNAC...).
FT   CARBOHYD    170    170       N-LINKED (GLCNAC...).
FT   CARBOHYD    304    304       N-LINKED (GLCNAC...).
FT   CARBOHYD    328    328       N-LINKED (GLCNAC...).
FT   VARIANT     207    207       T -> M (IN dbSNP:4762).
FT                                /FTId=VAR_007093.
FT   VARIANT     242    242       T -> I (IN HYPERTENSION).
FT                                /FTId=VAR_007094.
FT   VARIANT     244    244       L -> R (IN HYPERTENSION).
FT                                /FTId=VAR_007095.
FT   VARIANT     268    268       M -> T (IN HYPERTENSION; dbSNP:699).
FT                                /FTId=VAR_007096.
FT   VARIANT     281    281       Y -> C (IN HYPERTENSION; ALTERS THE
FT                                STRUCTURE, GLYCOSYLATION AND SECRETION OF
FT                                ANGIOTENSINOGEN).
FT                                /FTId=VAR_007097.
FT   VARIANT     392    392       L -> M (IN dbSNP:1805090).
FT                                /FTId=VAR_014573.
FT   CONFLICT    333    333       Q -> E (IN REF. 1).
FT   CONFLICT    335    335       P -> S (IN REF. 4).
**
**   #################    INTERNAL SECTION    ##################
**CL 1q42-q43;
SQ   SEQUENCE   485 AA;  53154 MW;  5026C2DFB2DD236E CRC64;
     MRKRAPQSEM APAGVSLRAT ILCLLAWAGL AAGDRVYIHP FHLVIHNEST CEQLAKANAG
     KPKDPTFIPA PIQAKTSPVD EKALQDQLVL VAAKLDTEDK LRAAMVGMLA NFLGFRIYGM
     HSELWGVVHG ATVLSPTAVF GTLASLYLGA LDHTADRLQA ILGVPWKDKN CTSRLDAHKV
     LSALQAVQGL LVAQGRADSQ AQLLLSTVVG VFTAPGLHLK QPFVQGLALY TPVVLPRSLD
     FTELDVAAEK IDRFMQAVTG WKTGCSLMGA SVDSTLAFNT YVHFQGKMKG FSLLAEPQEF
     WVDNSTSVSV PMLSGMGTFQ HWSDIQDNFS VTQVPFTESA CLLLIQPHYA SDLDKVEGLT
     FQQNSLNWMK KLSPRTIHLT MPQLVLQGSY DLQDLLAQAE LPAILHTELN LQKLSNDRIR
     VGEVLNSIFF ELEADEREPT ESTQQLNKPE VLEVTLNRPF LFAVYDQSAT ALHFLGRVAN
     PLSTA
//
