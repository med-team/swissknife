ID   DCTQ_RHOSH              Unreviewed;       227 AA.
AC   Q9LBD9;
DT   01-OCT-2000 (TrEMBLrel. 15, Created)
DT   01-OCT-2000 (TrEMBLrel. 15, Last sequence update)
DT   01-OCT-2000 (TrEMBLrel. 15, Last annotation update)
DE   Putative small C4-dicarboxylate integral membrane transport protein (Yadda).
GN   DCTQ.
OS   Rhodobacter sphaeroides (Rhodopseudomonas sphaeroides).
OC   Bacteria; Proteobacteria; alpha subdivision; Rhodobacter group;
OC   Rhodobacter.
OX   NCBI_TaxID=1063;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=4P1;
RA   Omrani M.D.;
RL   Submitted (MAY-1997) to the EMBL/GenBank/DDBJ databases.
CC   -!- FUNCTION: REQUIRED FOR C4-DICARBOXYLATE TRANSPORT (BY SIMILARITY).
CC   -!- SUBCELLULAR LOCATION: INTEGRAL MEMBRANE PROTEIN (BY SIMILARITY).
SQ   SEQUENCE   227 AA;  24991 MW;  2208AD920C1230DA CRC64;
     MMRLLDRLEE TLIASLIAAA TGLIFVSVVQ RYSLGLLADG VAFFRGHDMP ELSAMMRSAY
     LGLREFNLVW AQELCIILFV WMAKFGAAYG VRTGIHVGID VLINKLDERK RGFFILLGLG
     AGALFTGIIA TLGGNFVWHM AQTSAISPDL ELPMWLVYLA IPLGSALMCF RFLQVAVIFA
     RTGELAHHDH GHVEGVDTED EGIDVLGSTF LKSPLTPRDL VEKPKDE
//
ID   DCTQ_RHOSH              Unreviewed;       227 AA.
AC   Q9LBD9;
DT   01-OCT-2000 (TrEMBLrel. 15, Created)
DT   01-OCT-2000 (TrEMBLrel. 15, Last sequence update)
DT   01-OCT-2000 (TrEMBLrel. 15, Last annotation update)
DE   Putative small C4-dicarboxylate integral membrane transport protein
DE   with a very very very very very very very very very very very very
DE   very very very very very very very very very very very very very
DE   very very very very very very very very very very very long name (Fragment).
OS   Rhodobacter sphaeroides (Rhodopseudomonas sphaeroides).
OC   Bacteria; Proteobacteria; alpha subdivision; Rhodobacter group;
OC   Rhodobacter.
OX   NCBI_TaxID=1063;
SQ   SEQUENCE   227 AA;  24991 MW;  2208AD920C1230DA CRC64;
     MMRLLDRLEE TLIASLIAAA TGLIFVSVVQ RYSLGLLADG VAFFRGHDMP ELSAMMRSAY
     LGLREFNLVW AQELCIILFV WMAKFGAAYG VRTGIHVGID VLINKLDERK RGFFILLGLG
     AGALFTGIIA TLGGNFVWHM AQTSAISPDL ELPMWLVYLA IPLGSALMCF RFLQVAVIFA
     RTGELAHHDH GHVEGVDTED EGIDVLGSTF LKSPLTPRDL VEKPKDE
//
ID   3MGH_SYNJB              Reviewed;         192 AA.
AC   Q2JI31;
DT   12-DEC-2006, integrated into UniProtKB/Swiss-Prot.
DT   07-MAR-2006, sequence version 1.
DT   08-FEB-2011, entry version 31.
DE   RecName: Full=Putative 3-methyladenine DNA glycosylase;
DE            EC=3.2.2.-;
GN   OrderedLocusNames=CYB_2817;
OS   Synechococcus sp. (strain JA-2-3B'a(2-13)) (Cyanobacteria bacterium
OS   Yellowstone B-Prime).
SQ   SEQUENCE   192 AA;  21330 MW;  71CAF20F5D810045 CRC64;
     MEWLSQPAPL VAPALLGMVL VRQFADGLQV RAQIVETEAY TAGDPACHAY RRKTQRNQVM
     FGPPGHLYIY RIYGLYHCLN IVTEPEGIPA AVLIRAAQLD RLPDWIPANK QNQPARAAAG
     PGLLCQALRI DGSHNGWRLE RAEAGQEGIW LEGSPSWQTQ LSIVQTTRIG ITQGAEIPWR
     WYIGGHPAVS RY
//
