ID   O43499                  Unreviewed;       480 AA.
AC   O43499; O08291; O08202; O08292; O08203; O08293; O08204; O08294;
AC   O08205; O08295; O08206; O08296; O08207; O08297; O08208; O08298;
AC   O08209; O08299; O08210; O08300; O08211; O08301; O08212; O08302;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-1998 (TrEMBLrel. 06, Last annotation update)
DE   HTGN51.
GN   TGN.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=126566, 55{E2};
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Kain R., Angata K., Kerjaschki D., Fukuda M.;
RL   J. Biol. Chem. 273:0-0(1998).
DR   EMBL; AF029316; AAB96908.1; -.
DR   EMBL; AF029313; AAB96908.1; JOINED.
DR   EMBL; AF029314; AAB96908.1; JOINED.
DR   EMBL; AF029315; AAB96908.1; JOINED.
KW   Zinc-finger; Receptor; Transcription regulation; DNA-binding;
KW   Nuclear protein.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens trans-golgi network glycoprotein (TGN) gene, exon 4,
**   and complete cds.
**   [1]
**   1-2563
**   Kain R., Angata K., Kerjaschki D., Fukuda M.;
**   "Molecular Cloning and Expression of a Novel Human Trans-Golgi Network
**   Glycoprotein, TGN51, that Contains Multiple Tyrosine-containing
**   Motifs";
**   J. Biol. Chem. 273:0-0(1998).
**   [2]
**   1-2563
**   Kain R., Angata K., Kerjaschki D., Fukuda M.;
**   ;
**   Submitted (09-OCT-1997) to the EMBL/GenBank/DDBJ databases.
**   Glycobiology Program, The Burnham Institute, 10901 N. Torrey Pines Rd,
**   La Jolla, CA 92037, USA
**   source          1..2563
**                   /organism="Homo sapiens"
**                   /chromosome="2"
**                   /note="sequence from P1 plasmid 10508"
**                   /map="2p11.2"
**   CDS
**   join(AF029313:63..108,AF029314:1..1178,AF029315:278..361,
**                   210..344)
**                   /codon_start=1
**                   /db_xref="PID:g2772928"
**                   /note="trans-golgi network glycoprotein"
**                   /gene="TGN"
**                   /product="hTGN51"
**   CDS_IN_EMBL_ENTRY 3
**   20-JAN-1998 (Rel. 54, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   480 AA;  50993 MW;  8CAE4EE663B7225D CRC64;
     MRFVVALVLV NVAAAGAVPL LATESVKQEE AGVRPSAGNV STHPSLSQRP GGSTKSHPEP
     QTPKDSPSKS SAEAQTPEDT PNKSGGEAKT LKDSSNKSGA EAQTPKGSTS KSGSEAQTTK
     DSTSKSHPEL QTPKDSTGKS GAEAQTPEDS PNRSGAEPKT QKDSPSKSGS EAQTTKDVPN
     KSGADGQTPK DGSSKSGAED QTPKDVPNKS GAEKQTPKDG SNKSGAEEQG PIDGPSKSGA
     EEQTSKDSPN KVVPEQPSRK DHSKPISNPS DNKELPKADT NQLADKGKLS PHAFKTESGE
     ETDLISPPQE EVKSSEPTED VGPKEAEDDD TGPEEGSPPK EEKEKMSGSA SSENREGTLS
     DSTGSEKDDL YPNGSGNGSA ESSHFFAYLV TAAILVAVLY IAHHNKRKII AFVLEGKRSK
     VTRRPKASDY QRLDQKYVLI LNVFPAPPKR SFLPQVLTEW YIPLEKDERH QWIVLLSFQL
//
ID   O43500      PRELIMINARY;      PRT;   437 AA.
AC   O43500;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-1998 (TrEMBLrel. 06, Last annotation update)
DE   HTGN46.
GN   TGN.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Kain R., Angata K., Kerjaschki D., Fukuda M.;
RL   J. Biol. Chem. 273:0-0(1998).
DR   EMBL; AF029316; AAB96906.1; -.
DR   EMBL; AF029313; AAB96906.1; JOINED.
DR   EMBL; AF029314; AAB96906.1; JOINED.
DR   EMBL; AF029315; AAB96906.1; JOINED.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens trans-golgi network glycoprotein (TGN) gene, exon 4,
**   and complete cds.
**   [1]
**   1-2563
**   Kain R., Angata K., Kerjaschki D., Fukuda M.;
**   "Molecular Cloning and Expression of a Novel Human Trans-Golgi Network
**   Glycoprotein, TGN51, that Contains Multiple Tyrosine-containing
**   Motifs";
**   J. Biol. Chem. 273:0-0(1998).
**   [2]
**   1-2563
**   Kain R., Angata K., Kerjaschki D., Fukuda M.;
**   ;
**   Submitted (09-OCT-1997) to the EMBL/GenBank/DDBJ databases.
**   Glycobiology Program, The Burnham Institute, 10901 N. Torrey Pines Rd,
**   La Jolla, CA 92037, USA
**   source          1..2563
**                   /organism="Homo sapiens"
**                   /chromosome="2"
**                   /note="sequence from P1 plasmid 10508"
**                   /map="2p11.2"
**   CDS
**   join(AF029313:63..108,AF029314:1..1178,AF029315:278..361,
**                   268..273)
**                   /codon_start=1
**                   /db_xref="PID:g2772926"
**                   /note="trans-golgi network glycoprotein"
**                   /gene="TGN"
**                   /product="hTGN46"
**   CDS_IN_EMBL_ENTRY 3
**   20-JAN-1998 (Rel. 54, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   437 AA;  45759 MW;  14B21554256E983B CRC64;
     MRFVVALVLV NVAAAGAVPL LATESVKQEE AGVRPSAGNV STHPSLSQRP GGSTKSHPEP
     QTPKDSPSKS SAEAQTPEDT PNKSGGEAKT LKDSSNKSGA EAQTPKGSTS KSGSEAQTTK
     DSTSKSHPEL QTPKDSTGKS GAEAQTPEDS PNRSGAEPKT QKDSPSKSGS EAQTTKDVPN
     KSGADGQTPK DGSSKSGAED QTPKDVPNKS GAEKQTPKDG SNKSGAEEQG PIDGPSKSGA
     EEQTSKDSPN KVVPEQPSRK DHSKPISNPS DNKELPKADT NQLADKGKLS PHAFKTESGE
     ETDLISPPQE EVKSSEPTED VGPKEAEDDD TGPEEGSPPK EEKEKMSGSA SSENREGTLS
     DSTGSEKDDL YPNGSGNGSA ESSHFFAYLV TAAILVAVLY IAHHNKRKII AFVLEGKRSK
     VTRRPKASDY QRLDQKS
//
ID   O43501      PRELIMINARY;      PRT;   453 AA.
AC   O43501;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-1998 (TrEMBLrel. 06, Last annotation update)
DE   HTGN48.
GN   TGN.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Kain R., Angata K., Kerjaschki D., Fukuda M.;
RL   J. Biol. Chem. 273:0-0(1998).
DR   EMBL; AF029316; AAB96907.1; -.
DR   EMBL; AF029313; AAB96907.1; JOINED.
DR   EMBL; AF029314; AAB96907.1; JOINED.
DR   EMBL; AF029315; AAB96907.1; JOINED.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens trans-golgi network glycoprotein (TGN) gene, exon 4,
**   and complete cds.
**   [1]
**   1-2563
**   Kain R., Angata K., Kerjaschki D., Fukuda M.;
**   "Molecular Cloning and Expression of a Novel Human Trans-Golgi Network
**   Glycoprotein, TGN51, that Contains Multiple Tyrosine-containing
**   Motifs";
**   J. Biol. Chem. 273:0-0(1998).
**   [2]
**   1-2563
**   Kain R., Angata K., Kerjaschki D., Fukuda M.;
**   ;
**   Submitted (09-OCT-1997) to the EMBL/GenBank/DDBJ databases.
**   Glycobiology Program, The Burnham Institute, 10901 N. Torrey Pines Rd,
**   La Jolla, CA 92037, USA
**   source          1..2563
**                   /organism="Homo sapiens"
**                   /chromosome="2"
**                   /note="sequence from P1 plasmid 10508"
**                   /map="2p11.2"
**   CDS
**   join(AF029313:63..108,AF029314:1..1178,AF029315:278..361,
**                   251..304)
**                   /codon_start=1
**                   /db_xref="PID:g2772927"
**                   /note="trans-golgi network glycoprotein"
**                   /gene="TGN"
**                   /product="hTGN48"
**   CDS_IN_EMBL_ENTRY 3
**   20-JAN-1998 (Rel. 54, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   453 AA;  47578 MW;  13F70172177BFE82 CRC64;
     MRFVVALVLV NVAAAGAVPL LATESVKQEE AGVRPSAGNV STHPSLSQRP GGSTKSHPEP
     QTPKDSPSKS SAEAQTPEDT PNKSGGEAKT LKDSSNKSGA EAQTPKGSTS KSGSEAQTTK
     DSTSKSHPEL QTPKDSTGKS GAEAQTPEDS PNRSGAEPKT QKDSPSKSGS EAQTTKDVPN
     KSGADGQTPK DGSSKSGAED QTPKDVPNKS GAEKQTPKDG SNKSGAEEQG PIDGPSKSGA
     EEQTSKDSPN KVVPEQPSRK DHSKPISNPS DNKELPKADT NQLADKGKLS PHAFKTESGE
     ETDLISPPQE EVKSSEPTED VGPKEAEDDD TGPEEGSPPK EEKEKMSGSA SSENREGTLS
     DSTGSEKDDL YPNGSGNGSA ESSHFFAYLV TAAILVAVLY IAHHNKRKII AFVLEGKRSK
     VTRRPKASDY QRLDQKIFSP PSPNRMVYSS GKR
//
ID   O43530      PRELIMINARY;      PRT;   456 AA.
AC   O43530;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   NBMPR-INSENSITIVE NUCLEOSIDE TRANSPORTER EI.
GN   ENT2.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Crawford C.R., Patel D.H., Naeve C.W., Belt J.A.;
RL   J. Biol. Chem. 273:0-0(1998).
DR   EMBL; AF034102; AAB97834.1; -.
DR   InterPro; IPR002259; -.
DR   PFAM; PF01733; Nucleoside_tran; 1.
DR   PRINTS; PR01130; DERENTRNSPRT.
DR   PRODOM; PD005103; -; 1.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens NBMPR-insensitive nucleoside transporter ei (ENT2)
**   mRNA, complete cds.
**   [1]
**   1-2522
**   Crawford C.R., Patel D.H., Naeve C.W., Belt J.A.;
**   "Cloning of the Human Equilibrative, Nitrobenzylmercaptopurineriboside
**   (NBMPR)-Insensitive Nucleoside Transporter ei by Functional Expression
**   in a Transport-Deficient Cell Line";
**   J. Biol. Chem. 273:0-0(1998).
**   [2]
**   1-2522
**   Crawford C.R., Naeve C.W., Belt J.A.;
**   ;
**   Submitted (11-NOV-1997) to the EMBL/GenBank/DDBJ databases.
**   Molecular Pharmacology, St. Jude Children's Research Hospital, 332 N.
**   Lauderdale, Memphis, TN 38105, USA
**   for description of the isolation and properties of the cDNA, see
**   Crawford, et al, Proc. Annu. Meet. Am. Assoc. Cancer Res., 38:A406,
**   1997 (abstract).
**   source          1..2522
**                   /organism="Homo sapiens"
**                   /cell_line="HeLa S3; ATCC CCL2.2"
**                   /clone_lib="Clonetech HL1152y"
**   CDS             238..1608
**                   /codon_start=1
**                   /db_xref="PID:g2811137"
**                   /note="plasma membrane transport protein"
**                   /gene="ENT2"
**                   /function="mediates equilibrative transport of purine
**   and
**                   pyrimidine nucleosides, and the purine base
**   hypoxanthine"
**                   /product="NBMPR-insensitive nucleoside transporter ei"
**   CDS_IN_EMBL_ENTRY 1
**   28-JAN-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PRODOM; PD005103; PD005103; 131; 456; T; 19-JUN-2000;
**PM PFAM; PF01733; Nucleoside_tran; 131; 454; T; 19-JUN-2000;
**PM PRINTS; PR01130; DERENTRNSPRT; 137; 159; T; 19-JUN-2000;
**PM PRINTS; PR01130; DERENTRNSPRT; 165; 185; T; 19-JUN-2000;
**PM PRINTS; PR01130; DERENTRNSPRT; 191; 214; T; 19-JUN-2000;
**PM PRINTS; PR01130; DERENTRNSPRT; 301; 318; T; 19-JUN-2000;
**PM PRINTS; PR01130; DERENTRNSPRT; 333; 354; T; 19-JUN-2000;
**PM PRINTS; PR01130; DERENTRNSPRT; 361; 378; T; 19-JUN-2000;
**PM PRINTS; PR01130; DERENTRNSPRT; 393; 409; T; 19-JUN-2000;
**PM PRINTS; PR01130; DERENTRNSPRT; 412; 428; T; 19-JUN-2000;
**PM PRINTS; PR01130; DERENTRNSPRT; 430; 454; T; 19-JUN-2000;
SQ   SEQUENCE   456 AA;  50113 MW;  ABCBD244306708E1 CRC64;
     MARGDAPRDS YHLVGISFFI LGLGTLLPWN FFITAIPYFQ ARLAGAGNST ARILSTNHTG
     PEDAFNFNNW VTLLSQLPLL LFTLLNSFLY QCVPETVRIL GSLLAILLLF ALTAALVKVD
     MSPGPFFSIT MASVCFINSF SAVLQGSLFG QLGTMPSTYS TLFLSGQGLA GIFAALAMLL
     SMASGVDAET SALGYFITPC VGILMSIVCY LSLPHLKFAR YYLANKSSQA QAQELETKAE
     LLQSDENGIP SSPQKVALTL DLDLEKEPES EPDEPQKPGK PSVFTVFQKI WLTALCLVLV
     FTVTLSVFPA ITAMVTSSTS PGKWSQFFNP ICCFLLFNIM DWLGRSLTSY FLWPDEDSRL
     LPLLVCLRFL FVPLFMLCHV PQRSRLPILF PQDAYFITFM LLFAVSNGYL VSLTMCLAPR
     QVLPHEREVA GALMTFFLAL GLSCGASLSF LFKALL
//
ID   O43534      PRELIMINARY;      PRT;   377 AA.
AC   O43534;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   NATURAL KILLER CELL INHIBITORY RECEPTOR.
GN   KIR2DL4.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Valiant N.M., Uhrberg M., Shilling H.G., Lienert-Weidenbach K.,
RA   Arnett K.L., D'Andrea A., Phillips J.H., Lanier L.L., Parham P.;
RL   Immunity 0:0-0(1997).
RN   [2]
RP   SEQUENCE FROM N.A.
RA   Uhrberg M., Valiant N.M., Shum B., Shilling H.G.,
RA   Lienert-Weidenbach K., Corliss B., Tyan D., Lanier L.L., Parham P.;
RL   Immunity 0:0-0(1998).
DR   EMBL; AF034773; AAB95166.1; -.
DR   HSSP; P43626; 1NKR.
DR   InterPro; IPR003006; -.
DR   Pfam; PF00047; ig; 2.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens natural killer cell inhibitory receptor (KIR2DL4)
**   mRNA, variant 3, complete cds.
**   [1]
**   1-1179
**   Valiant N.M., Uhrberg M., Shilling H.G., Lienert-Weidenbach K.,
**   Arnett K.L., D'Andrea A., Phillips J.H., Lanier L.L., Parham P.;
**   "Functionally and Structurally Distinct NK Cell Receptor Repertoires
**   in
**   the Peripheral Blood of Two Human Donors";
**   Immunity 0:0-0(1997).
**   [2]
**   1-1179
**   Uhrberg M., Valiant N.M., Shum B., Shilling H.G., Lienert-Weidenbach
**   K.,
**   Corliss B., Tyan D., Lanier L.L., Parham P.;
**   "Human Diversity in Killer Cell Inhibitory Receptor (KIR) Genes";
**   Immunity 0:0-0(1998).
**   [3]
**   1-1179
**   Shilling H.G.;
**   ;
**   Submitted (17-NOV-1997) to the EMBL/GenBank/DDBJ databases.
**   Structural Biology, Stanford University, Sherman Fairchild Building,
**   Stanford University School of Medicine, Stanford, CA 94305-5400, USA
**   source          1..1179
**                   /organism="Homo sapiens"
**                   /cell_type="peripheral blood"
**   CDS             7..1140
**                   /codon_start=1
**                   /db_xref="PID:g2739182"
**                   /gene="KIR2DL4"
**                   /product="natural killer cell inhibitory receptor"
**   CDS_IN_EMBL_ENTRY 1
**   09-JAN-1998 (Rel. 54, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00047; ig; 44; 99; T; 19-JUN-2000;
**PM PFAM; PF00047; ig; 139; 197; T; 19-JUN-2000;
SQ   SEQUENCE   377 AA;  41426 MW;  969DA8DB9872F4B6 CRC64;
     MSMSPTVIIL ACLGFFLDQS VWAHVGGQDK PFCSAWPSAV VPQGGHVTLR CHCRRGFNIF
     TLYKKDGVPV PELYNRIFWN SFLISPVTPA HAGTYRCRGF HPHSPTEWSA PSNPLVIMVT
     GLYEKPSLTA RPGPTVRAGE NVTLSCSSQS SFDIYHLSRE GEAHELRLPA VPSINGTFQA
     DFPLGPATHG ETYRCFGSFH GSPYEWSDPS DPLPVSVTGN PSSSWPSPTE PSFKTGIARH
     LHAVIRYSVA IILFTILPFF LLHRWCSKKK NAAVMNQEPA GHRTVNREDS DEQDPQEVTY
     AQLDHCIFTQ RKITGPSQRS KRPSTDTSVC IELPNAEPRA LSPAHEHHSQ ALMGSSRETT
     ALSQTQLASS NVPAAGI
//
ID   O43538      PRELIMINARY;      PRT;   653 AA.
AC   O43538;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   AMPHIPHYSIN I.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=BREAST;
RA   Floyd S.R., Butler M.H., Cremona O., David C., Freyberg Z., Zhang X.,
RA   Solimena M., Tokunaga A., Ishizu H., Tsutsui K., De Camilli P.V.;
RL   Mol. Med. (Camb. Mass.) 0:0-0(1998).
DR   EMBL; AF034996; AAC02977.1; -.
DR   HSSP; P29355; 2SEM.
DR   INTERPRO; IPR001452; -.
DR   INTERPRO; IPR003005; -.
DR   INTERPRO; IPR003017; -.
DR   PFAM; PF00018; SH3; 1.
DR   PRINTS; PR00452; SH3DOMAIN.
DR   PRINTS; PR01251; AMPHIPHYSIN.
DR   PRINTS; PR01252; AMPHIPHYSIN1.
DR   PROSITE; PS50002; SH3; 1.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens amphiphysin I mRNA, alternative splice isoform,
**   complete cds.
**   [1]
**   1-3161
**   Floyd S.R., Butler M.H., Cremona O., David C., Freyberg Z., Zhang X.,
**   Solimena M., Tokunaga A., Ishizu H., Tsutsui K., De Camilli P.V.;
**   "Expression of amphiphysin I, an autoantigen of paraneoplastic
**   neurological syndromes, in breast cancer";
**   Mol. Med. (Camb. Mass.) 0:0-0(1998).
**   [2]
**   1-3161
**   Floyd S.R., Butler M.H., Cremona O., David C., Freyberg Z., Zhang X.,
**   Solimena M., Tokunaga A., Ishizu H., Tsutsui K., De Camilli P.V.;
**   ;
**   Submitted (18-NOV-1997) to the EMBL/GenBank/DDBJ databases.
**   Cell Biology, Yale University, 295 Congress Avenue, New Haven, CT
**   06510,
**   USA
**   source          1..3161
**                   /organism="Homo sapiens"
**                   /cell_line="Hs578T"
**                   /tissue_type="breast"
**   CDS             70..2031
**                   /codon_start=1
**                   /db_xref="PID:g2895528"
**                   /note="alternative splice isoform"
**                   /product="amphiphysin I"
**   CDS_IN_EMBL_ENTRY 1
**   20-FEB-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00018; SH3; 583; 652; T; 19-JUN-2000;
**PM PRINTS; PR00452; SH3DOMAIN; 583; 593; T; 19-JUN-2000;
**PM PRINTS; PR00452; SH3DOMAIN; 597; 612; T; 19-JUN-2000;
**PM PRINTS; PR00452; SH3DOMAIN; 619; 628; T; 19-JUN-2000;
**PM PRINTS; PR00452; SH3DOMAIN; 640; 652; T; 19-JUN-2000;
**PM PRINTS; PR01251; AMPHIPHYSIN; 23; 40; T; 19-JUN-2000;
**PM PRINTS; PR01251; AMPHIPHYSIN; 51; 61; T; 19-JUN-2000;
**PM PRINTS; PR01251; AMPHIPHYSIN; 83; 95; T; 19-JUN-2000;
**PM PRINTS; PR01251; AMPHIPHYSIN; 118; 132; T; 19-JUN-2000;
**PM PRINTS; PR01251; AMPHIPHYSIN; 140; 151; T; 19-JUN-2000;
**PM PRINTS; PR01251; AMPHIPHYSIN; 175; 184; T; 19-JUN-2000;
**PM PRINTS; PR01251; AMPHIPHYSIN; 186; 196; T; 19-JUN-2000;
**PM PRINTS; PR01251; AMPHIPHYSIN; 576; 585; T; 19-JUN-2000;
**PM PRINTS; PR01251; AMPHIPHYSIN; 597; 611; T; 19-JUN-2000;
**PM PRINTS; PR01252; AMPHIPHYSIN1; 3; 11; T; 19-JUN-2000;
**PM PRINTS; PR01252; AMPHIPHYSIN1; 93; 104; T; 19-JUN-2000;
**PM PRINTS; PR01252; AMPHIPHYSIN1; 220; 234; T; 19-JUN-2000;
**PM PRINTS; PR01252; AMPHIPHYSIN1; 235; 246; T; 19-JUN-2000;
**PM PRINTS; PR01252; AMPHIPHYSIN1; 247; 258; T; 19-JUN-2000;
**PM PRINTS; PR01252; AMPHIPHYSIN1; 386; 396; T; 19-JUN-2000;
**PM PRINTS; PR01252; AMPHIPHYSIN1; 632; 643; T; 19-JUN-2000;
**PM PROSITE; PS50002; SH3; 580; 653; T; 19-JUN-2000;
SQ   SEQUENCE   653 AA;  71929 MW;  44C1115E3E70B6A9 CRC64;
     MADIKTGIFA KNVQKRLNRA QEKVLQKLGK ADETKDEQFE EYVQNFKRQE AEGTRLQREL
     RGYLAAIKGM QEASMKLTES LHEVYEPDWY GREDVKMVGE KCDVLWEDFH QKLVDGSLLT
     LDTYLGQFPD IKNRIAKRSR KLVDYDSARH HLEALQSSKR KDESRISKAE EEFQKAQKVF
     EEFNVDLQEE LPSLWSRRVG FYVNTFKNVS SLEAKFHKEI AVLCHKLYEV MTKLGDQHAD
     KAFTIQGAPS DSGPLRIAKT PSPPEEPSPL PSPTASPNHT LAPASPAPAR PRSPSQTRKG
     PPVPPLPKVT PTKELQQENI ISFFEDNFVP EISVTTPSQN EVPEVKKEET LLDLDFDPFK
     PEVTPAGSAG VTHSPMSQTL PWDLWTTSTD LVQPASGGSF NGFTQPQDTS LFTMQTDQSM
     ICNLIIPGAD ADAAVGTLVS AAEGAPGEEA EAEKATVPAG EGVSLEEAKI GTETTEGAES
     AQPEAEELEA TVPQEKVIPS VVIEPASNHE EEGENEITIG AEPKETTEDA APPGPTSETP
     ELATEQKPIQ DPQPTPSAPA MGAADQLASA REASQELPPG FLYKVETLHD FEAANSDELT
     LQRGDVVLVV PSDSEADQDA GWLVGVKESD WLQYRDLATY KGLFPENFTR RLD
//
ID   O43541      PRELIMINARY;      PRT;   496 AA.
AC   O43541;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   SMAD6.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Hata A., Lagna G., Massague J., Hemmati-Brivanlou A.;
RL   Genes Dev. 0:0-0(1997).
DR   EMBL; AF035528; AAB94137.1; -.
DR   INTERPRO; IPR001132; -.
DR   PFAM; PF00968; Dwarfin; 1.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens Smad6 mRNA, complete cds.
**   [1]
**   1-2887
**   Hata A., Lagna G., Massague J., Hemmati-Brivanlou A.;
**   "Smad6 inhibits BMP/Smad1 signaling by specifically competing with the
**   Smad4 tumor suppressor";
**   Genes Dev. 0:0-0(1997).
**   [2]
**   1-2887
**   Hata A., Lagna G., Massague J., Hemmati-Brivanlou A.;
**   ;
**   Submitted (21-NOV-1997) to the EMBL/GenBank/DDBJ databases.
**   Laboratory of Molecular Embryology, The Rockefeller University, 1230
**   York Avenue, New York, NY 10021, USA
**   source          1..2887
**                   /organism="Homo sapiens"
**                   /cell_line="Jurkat T-cell"
**   CDS             937..2427
**                   /codon_start=1
**                   /db_xref="PID:g2736316"
**                   /note="SMAD family member"
**                   /function="inhibitor of BMP signaling"
**                   /product="Smad6"
**   CDS_IN_EMBL_ENTRY 1
**   07-JAN-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00968; Dwarfin; 132; 495; T; 19-JUN-2000;
SQ   SEQUENCE   496 AA;  53496 MW;  4D50B634D8911B37 CRC64;
     MFRSKRSGLV RRLWRSRVVP NREEGGSGGG GGGDEDGSLG SRAEPAPRAR EGGGCGRSEV
     RPVAPRRPRD AVGQRGAQGA GRRRRAGGPP RPMSEPGAGA GSSLLDVAEP GGPGWLPESD
     CETVTCCLFS ERDAAGAPRD ASDPLAGAAL EPAGGGRSRE ARSRLLLLEQ ELKTVTYSLL
     KRLKERSLDT LLEAVESRGG VPGGCVLVPR ADLRLGGQPA PPQLLLGRLF RWPDLQHAVE
     LKPLCGCHSF AAAADGPTVC CNPYHFSRLC GPESPPPPYS RLSPRDEYKP LDLSDSTLSY
     TETEATNSLI TAPGEFSDAS MSPDATKPSH WCSVAYWEHR TRVGRLYAVY DQAVSIFYDL
     PQGSGFCLGQ LNLEQRSESV RRTRSKIGFG ILLSKEPDGV WAYNRGEHPI FVNSPTLDAP
     GGRALVVRKV PPGYSIKVFD FERSGLQHAP EPDAADGPYD PNSVRISFAK GWGPCYSRQF
     ITSCPCWLEI LLNNPR
//
ID   O43545      PRELIMINARY;      PRT;   179 AA.
AC   O43545;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   MESODERM-SPECIFIC BASIC-HELIX-LOOP-HELIX PROTEIN.
GN   POD1.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Quaggin S.E., Vanden Heuvel G.B., Igarashi P.;
RL   Mech. Dev. 0:0-0(1997).
DR   EMBL; AF035718; AAC62514.1; -.
DR   HSSP; P10085; 1MDY.
DR   INTERPRO; IPR001092; -.
DR   INTERPRO; IPR003015; -.
DR   PFAM; PF00010; HLH; 1.
DR   PROSITE; PS00038; HELIX_LOOP_HELIX; UNKNOWN_1.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens mesoderm-specific basic-helix-loop-helix protein
**   (POD1) mRNA, complete cds.
**   [1]
**   1-1254
**   Quaggin S.E., Vanden Heuvel G.B., Igarashi P.;
**   "Pod-1, A Mesoderm-Specific Basic-Helix-Loop-Helix Protein Expressed
**   in
**   Mesenchymal and Glomerular Epithelial Cells in the Developing Kidney";
**   Mech. Dev. 0:0-0(1997).
**   [2]
**   1-1254
**   Quaggin S.E., Vanden Heuvel G.B., Igarashi P.;
**   ;
**   Submitted (24-NOV-1997) to the EMBL/GenBank/DDBJ databases.
**   Internal Medicine, Yale University, 333 Cedar Street, New Haven, CT
**   06520-8029, USA
**   source          1..1254
**                   /organism="Homo sapiens"
**                   /chromosome="6"
**   CDS             261..800
**                   /codon_start=1
**                   /db_xref="PID:g2745887"
**                   /note="Pod-1"
**                   /gene="POD1"
**                   /product="mesoderm-specific basic-helix-loop-helix
**   protein"
**   misc_feature    492..653
**                   /note="encodes basic-helix-loop-helix domain"
**                   /gene="POD1"
**                   AA 78 -> 131
**   CDS_IN_EMBL_ENTRY 1
**   08-JAN-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00010; HLH; 80; 132; T; 19-JUN-2000;
**PM PROSITE; PS00038; HELIX_LOOP_HELIX; 116; 131; ?; 19-JUN-2000;
**PM PROSITE; PS50037; HELIX_LOOP_HELIX_2; 89; 129; T; 19-JUN-2000;
SQ   SEQUENCE   179 AA;  19743 MW;  9B6F496C4A6B658A CRC64;
     MSTGSLSDVE DLQEVEMLEC DGLKMDSNKE FVTSNESTEE SSNCENGSPQ KGRGGLGKRR
     RAPTKKSPLS GVSQEGKQVQ RNAANARERA RMRVLSKAFS RLKTTLPWVP PDTKLSKLDT
     LRLASSYIAH LRQILANDKY ENGYIHPVNL TWPFMVAGKP ESDLKEVVTA SRLCGTTAS
//
ID   O43547      PRELIMINARY;      PRT;   232 AA.
AC   O43547;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-1998 (TrEMBLrel. 06, Last annotation update)
DE   VESICLE SOLUBLE NSF ATTACHMENT PROTEIN RECEPTOR.
GN   VTI1.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=98112804; PubMed=9446565;
RA   Fischer von Mollard G., Stevens T.H.;
RT   "A human homolog can functionally replace the yeast vesicle-associated
RT   SNARE Vti1p in two vesicle transport pathways.";
RL   J. Biol. Chem. 273:2624-2630(1998).
DR   EMBL; AF035824; AAC52016.1; -.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens vesicle soluble NSF attachment protein receptor (VTI1)
**   mRNA, complete cds.
**   [1]
**   1-935
**   Fischer von Mollard G., Stevens T.H.;
**   "A human homolog can functionally replace the yeast v-SNARE Vti1p in
**   two
**   vesicle transport pathways";
**   J. Biol. Chem. 273:2624-2630(1998).
**   [2]
**   1-935
**   Fischer von Mollard G., Stevens T.H.;
**   ;
**   Submitted (25-NOV-1997) to the EMBL/GenBank/DDBJ databases.
**   Institute of Molecular Biology, University of Oregon, Eugene, OR
**   97403,
**   USA
**   source          1..935
**                   /organism="Homo sapiens"
**   CDS             72..770
**                   /codon_start=1
**                   /db_xref="PID:g2687400"
**                   /note="Vti1; v-SNARE"
**                   /gene="VTI1"
**                   /product="vesicle soluble NSF attachment protein
**   receptor"
**   CDS_IN_EMBL_ENTRY 1
**   05-FEB-1998 (Rel. 54, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   232 AA;  26687 MW;  E34B62215F5F1EDC CRC64;
     MASSAASSEH FEKLHEIFRG LHENLQGVPE RLLGTAGTEE KKKLIRDFDE KQQEANETLA
     EMEEELRYAP LSFRNPMMSK LRNYRKDLAK LHREVRSTPL TATPGGRGDM KYGIYAVENE
     HMNRLQSQRA MLLQGTESLN RATQSIERSH RIATETDQIG SEIIEELGEQ RDQLERTKSR
     LVNTSENLSK SRKILRSMSR KVTTNKLLLS IIILLELAIL GGLVYYKFFR SH
//
ID   O43557      PRELIMINARY;      PRT;   240 AA.
AC   O43557;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   TUMOR NECROSIS FACTOR SUPERFAMILY MEMBER LIGHT.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=98122340; PubMed=9462508;
RA   Mauri D.N., Ebner R., Montgomery R.I., Kochel K.D., Cheung T.C.,
RA   Yu G.-L., Ruben S., Murphy M., Eisenberg R.J., Cohen G.H., Spear P.G.,
RA   Ware C.F.;
RT   "LIGHT, a new member of the TNF superfamily, and lymphotoxin alpha are
RT   ligands for herpesvirus entry mediator.";
RL   Immunity 8:21-30(1998).
DR   EMBL; AF036581; AAC39563.1; -.
DR   HSSP; P01375; 4TSV.
DR   INTERPRO; IPR000478; -.
DR   PFAM; PF00229; TNF; 1.
DR   PROSITE; PS50049; TNF_2; 1.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens tumor necrosis factor superfamily member LIGHT mRNA,
**   complete cds.
**   [1]
**   1-1169
**   Mauri D.N., Ebner R., Montgomery R.I., Kochel K.D., Cheung T.C.,
**   Yu G.-L., Ruben S., Murphy M., Eisenberg R.J., Cohen G.H., Spear P.G.,
**   Ware C.F.;
**   "LIGHT, a new member of the TNF superfamily, and lymphotoxin (LT)a are
**   ligands for herpesvirus entry mediator (HVEM)";
**   Immunity 8:21-30(1998).
**   [2]
**   1-1169
**   Ebner R., Kochel K.D., Ware C.F.;
**   ;
**   Submitted (02-DEC-1997) to the EMBL/GenBank/DDBJ databases.
**   Division of Molecular Immunology, La Jolla Institute for Allergy and
**   Immunology, 10355 Science Center Drive, San Diego, CA 92121, USA
**   source          1..1169
**                   /organism="Homo sapiens"
**                   /chromosome="16"
**                   /cell_type="peripheral blood mononuclear cells
**   activated
**                   with phorbol ester and phytohemagglutinin for 12 hr"
**   CDS             49..771
**                   /codon_start=1
**                   /db_xref="PID:g2815624"
**                   /function="ligand for herpesvirus entry mediator
**   (HVEM) and
**                   lymphotoxin-beta receptor (LTbR)"
**                   /product="tumor necrosis factor superfamily member
**   LIGHT"
**   CDS_IN_EMBL_ENTRY 1
**   31-JAN-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00229; TNF; 93; 240; T; 19-JUN-2000;
**PM PROSITE; PS50049; TNF_2; 95; 240; T; 19-JUN-2000;
SQ   SEQUENCE   240 AA;  26351 MW;  49D0BF67E1390B39 CRC64;
     MEESVVRPSV FVVDGQTDIP FTRLGRSHRR QSCSVARVGL GLLLLLMGAG LAVQGWFLLQ
     LHWRLGEMVT RLPDGPAGSW EQLIQERRSH EVNPAAHLTG ANSSLTGSGG PLLWETQLGL
     AFLRGLSYHD GALVVTKAGY YYIYSKVQLG GVGCPLGLAS TITHGLYKRT PRYPEELELL
     VSQQSPCGRA TSSSRVWWDS SFLGGVVHLE AGEEVVVRVL DERLVRLRDG TRSYFGAFMV
//
ID   O43561      PRELIMINARY;      PRT;   262 AA.
AC   O43561;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   LINKER FOR ACTIVATION OF T CELLS (LAT).
GN   LAT.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Zhang W., Sloan-Lancaster J., Kitchen J., Trible R.P., Samelson L.E.;
RL   Cell 0:0-0(1997).
DR   EMBL; AF036906; AAC39637.1; -.
DR   INTERPRO; IPR000345; -.
DR   PROSITE; PS00190; CYTOCHROME_C; UNKNOWN_1.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens linker for activation of T cells (LAT) mRNA,
**   alternatively spliced form, complete cds.
**   [1]
**   1-1460
**   Zhang W., Sloan-Lancaster J., Kitchen J., Trible R.P., Samelson L.E.;
**   "LAT: the ZAP-70 tyrosine kinase substrate that links T cell receptor
**   to
**   cellular activation";
**   Cell 0:0-0(1997).
**   [2]
**   1-1460
**   Zhang W., Sloan-Lancaster J., Kitchen J., Trible R.P., Samelson L.E.;
**   ;
**   Submitted (05-DEC-1997) to the EMBL/GenBank/DDBJ databases.
**   Cell Biology and Metabolism Branch, National Institute of Child Health
**   and Development, National Institute of Health, 9000 Rockville Pike,
**   Bethesda, MD 20892, USA
**   LAT is a highly tyrosine phosphorylated protein, previously
**   described as p36-38, and it associates with many signaling
**   molecules, such as Grb2, PLC-gamma1, PI-3 kinase, cbl, Vav, and
**   SLP-76, either directly or indirectly upon T cell activation. It is
**   a potential type III transmembrane protein.
**   source          1..1460
**                   /organism="Homo sapiens"
**                   /cell_line="Jurkat T cells"
**   CDS             79..867
**                   /codon_start=1
**                   /db_xref="PID:g2828026"
**                   /note="tyrosine kinase substrate; This a alternatively
**                   spliced form of LAT"
**                   /gene="LAT"
**                   /product="LAT"
**   CDS_IN_EMBL_ENTRY 1
**   03-FEB-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PROSITE; PS00190; CYTOCHROME_C; 26; 31; ?; 19-JUN-2000;
SQ   SEQUENCE   262 AA;  27930 MW;  BCD80AE7DCA64153 CRC64;
     MEEAILVPCV LGLLLLPILA MLMALCVHCH RLPGSYDSTS SDSLYPRGIQ FKRPHTVAPW
     PPAYPPVTSY PPLSQPDLLP IPRSPQPLGG SHRTPSSRRD SDGANSVASY ENEGASGIRG
     AQAGWGVWGP SWTRLTPVSL PPEPACEDAD EDEDDYHNPG YLVVLPDSTP ATSTAAPSAP
     ALSTPGIRDS AFSMESIDDY VNVPESGESA EASLDGSREY VNVSQELHPG AAKTEPAALS
     SQEAEEVEEE GAPDYENLQE LN
//
ID   O43562      PRELIMINARY;      PRT;   424 AA.
AC   O43562;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-1998 (TrEMBLrel. 06, Last annotation update)
DE   ORGANIC CATION TRANSPORTER-LIKE PROTEIN 2.
GN   ORCTL2.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Cooper P.R., Smilinich N.J., Day C.D., Nowak N.J., Reid L.H.,
RA   Pearsall R.S., Reece M., Prawitt D., Landers J., Housman D.E.,
RA   Winterpacht A., Zabel B.U., Pelletier J., Weissman B.E., Shows T.B.,
RA   Higgins M.J.;
RL   Genomics 0:0-0(1998).
DR   EMBL; AF037064; AAC04787.1; -.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens organic cation transporter-like protein 2 (ORCTL2)
**   mRNA, complete cds.
**   [1]
**   1-1535
**   Cooper P.R., Smilinich N.J., Day C.D., Nowak N.J., Reid L.H.,
**   Pearsall R.S., Reece M., Prawitt D., Landers J., Housman D.E.,
**   Winterpacht A., Zabel B.U., Pelletier J., Weissman B.E., Shows T.B.,
**   Higgins M.J.;
**   "Divergently transcribed overlapping genes expressed in liver and
**   kidney
**   and located in the 11p15.5 imprinted domain";
**   Genomics 0:0-0(1998).
**   [2]
**   1-1535
**   Cooper P.R., Shows T.B., Pelletier J., Landers J., Higgins M.J.;
**   ;
**   Submitted (08-DEC-1997) to the EMBL/GenBank/DDBJ databases.
**   Human Genetics, Roswell Park Cancer Institute, Elm and Carlton
**   Streets,
**   Buffalo, NY 14263, USA
**   source          1..1535
**                   /organism="Homo sapiens"
**                   /chromosome="11"
**                   /map="11p15.5"
**   CDS             203..1477
**                   /codon_start=1
**                   /db_xref="PID:g2921449"
**                   /note="predicted integral membrane protein functioning
**   in
**                   organic cation transport. A second in frame ATG is
**   present
**                   at position 251. In the mouse gene, the corresponding
**   ATG
**                   is contained within a better translational context
**                   suggesting that translation may start at the second
**   ATG in
**                   both cases"
**                   /gene="ORCTL2"
**                   /product="organic cation transporter-like protein 2"
**   CDS_IN_EMBL_ENTRY 1
**   10-MAR-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   424 AA;  44864 MW;  6EFD5F912A7761F5 CRC64;
     MQGARAPRDQ GRSPGRMSAL GRSSVILLTY VLAATELTCL FMQFSIVPYL SRKLGLDSIA
     FGYLQTTFGV LQLLGGPVFG RFADQRGARA ALTLSFLAAL ALYLLLAAAS SPALPGVYLL
     FASRLPGALM HTLPAAQMVI TDLSAPEERP AALGRLGLCF GVGVILGSLL GGTLVSAYGI
     QCPAILAALA TLLGAVLSFT CIPASTKGAK TDAQAPLPGG PRASVFDLKA IASLLRLPDV
     PRIFLVKVAS NCPTGLFMVM FSIISMDFFQ LEAAQAGYLM SFFGLLQMVT QGLVIGQLSS
     HFSEEVMLRA SVLVFIVVGL AMAWMSSVFH FCLLVPGLVF SLCTLNVVTD SMLIKAVSTS
     DTGTMLGLCA SVQPLLRTLG PTVGGLLYRS FGVPVFGHVQ VAINTLVLLV LWRKPMPQRK
     DKVR
//
ID   O43568      PRELIMINARY;      PRT;   346 AA.
AC   O43568;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   XRCC3.
GN   XRCC3.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Liu N., Lamerdin J.E., Tebbs R.S., Schild D., Tucker J.D., Shen R.,
RA   Brookman K.W., Siciliano M.J., Walter C.A., Fan W., Narayana L.S.,
RA   Zhou Z.-Q., Adamson A.W., Sorenson K.J., Chen D.J., Jones N.J.,
RA   Thompson L.H.;
RL   Mol. Cell 0:0-0(1998).
DR   EMBL; AF037222; AAC04805.1; -.
DR   INTERPRO; IPR001553; -.
**
**   #################     SOURCE SECTION     ##################
**   Human DNA from chromosome 14-specific cosmid containing XRCC3 DNA
**   repair gene, genomic sequence, complete sequence.
**   [1]
**   1-36628
**   Liu N., Lamerdin J.E., Tebbs R.S., Schild D., Tucker J.D., Shen R.,
**   Brookman K.W., Siciliano M.J., Walter C.A., Fan W., Narayana L.S.,
**   Zhou Z.-Q., Adamson A.W., Sorenson K.J., Chen D.J., Jones N.J.,
**   Thompson L.H.;
**   "XRCC2 and XRCC3, New Members of the Rad51 Family, Promote Chromosome
**   Stability and Protect Against DNA Damages";
**   Mol. Cell 0:0-0(1998).
**   [2]
**   1-36628
**   Lamerdin J.E.;
**   ;
**   Submitted (08-DEC-1997) to the EMBL/GenBank/DDBJ databases.
**   Human Genome Center, Lawrence Livermore National Laboratory, 7000 East
**   Ave., Livermore, CA 94551, USA
**   source          1..36628
**                   /organism="Homo sapiens"
**                   /chromosome="14"
**                   /note="cosmid from library constructed at LANL from
**                   flow-sorted material containing chromosome 14 as the
**   only
**                   human chromosome"
**                   /clone="hsXRCC3GEN"
**                   /map="14q32.3"
**   CDS             join(6396..6450,8824..8961,10268..10480,14156..14310,
**                   17907..18119,18304..18350,18465..18684)
**                   /codon_start=1
**                   /db_xref="PID:g2921500"
**                   /note="DNA repair protein"
**                   /gene="XRCC3"
**                   /product="XRCC3"
**   CDS_IN_EMBL_ENTRY 1
**   10-MAR-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PROSITE; PS50162; RECA_1; 78; 263; T; 19-JUN-2000;
SQ   SEQUENCE   346 AA;  37880 MW;  C531EAE5F307C0E3 CRC64;
     MDLDLLDLNP RIIAAIKKAK LKSVKEVLHF SGPDLKRLTN LSSPEVWHLL RTASLHLRGS
     SILTALQLHQ QKERFPTQHQ RLSLGCPVLD ALLRGGLPLD GITELAGRSS AGKTQLALQL
     CLAVQFPRQH GGLEAGAVYI CTEDAFPHKR LQQLMAQQPR LRTDVPGELL QKLRFGSQIF
     IEHVADVDTL LECVNKKVPV LLSRGMARLV VIDSVAAPFR CEFDSQASAP RARHLQSLGA
     MLRELSSAFQ SPVLCINQVT EAMEEQGAAH GPLGFWDERV SPALGITWAN QLLVRLLADR
     LREEEAALGC PARTLRVLSA PHLPPSSCSY TISAEGVRGT PGTQSH
//
ID   O43574      PRELIMINARY;      PRT;   777 AA.
AC   O43574;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   BRCA1-ASSOCIATED RING DOMAIN PROTEIN.
GN   BARD1.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Thai T.H., Du F., Tsan J.T., Jin Y., Phung A., Spillman M.A.,
RA   Massa H.F., Muller C.Y., Ashfaq R., Mathis J.M., Miller D.S.,
RA   Trask B.J., Baer R., Bowcock A.M.;
RL   Hum. Mol. Genet. 0:0-0(1998).
CC   -!- SIMILARITY: CONTAINS A C3HC4-CLASS ZINC FINGER.
DR   EMBL; AF038042; AAB99978.1; -.
DR   EMBL; AF038034; AAB99978.1; JOINED.
DR   EMBL; AF038035; AAB99978.1; JOINED.
DR   EMBL; AF038036; AAB99978.1; JOINED.
DR   EMBL; AF038037; AAB99978.1; JOINED.
DR   EMBL; AF038038; AAB99978.1; JOINED.
DR   EMBL; AF038039; AAB99978.1; JOINED.
DR   EMBL; AF038040; AAB99978.1; JOINED.
DR   EMBL; AF038041; AAB99978.1; JOINED.
DR   HSSP; P25963; 1IKN.
DR   INTERPRO; IPR001357; -.
DR   INTERPRO; IPR001841; -.
DR   INTERPRO; IPR002110; -.
DR   PFAM; PF00023; ank; 3.
DR   PROSITE; PS00518; ZINC_FINGER_C3HC4; 1.
KW   Zinc-finger.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens BRCA1-associated RING domain protein (BARD1) gene,
**   exons 10, 11 and complete cds.
**   [1]
**   1-4334
**   Thai T.H., Du F., Tsan J.T., Jin Y., Phung A., Spillman M.A.,
**   Massa H.F., Muller C.Y., Ashfaq R., Mathis J.M., Miller D.S.,
**   Trask B.J., Baer R., Bowcock A.M.;
**   "Mutations in the BRCA1-associated RING domain (BARD1) gene in primary
**   breast, ovarian and uterine cancers";
**   Hum. Mol. Genet. 0:0-0(1998).
**   [2]
**   1-4334
**   Thai T.H., Du F., Tsan J.T., Jin Y., Phung A., Spillman M.A.,
**   Massa H.F., Muller C.Y., Ashfaq R., Mathis J.M., Miller D.S.,
**   Trask B.J., Baer R., Bowcock A.M.;
**   ;
**   Submitted (11-DEC-1997) to the EMBL/GenBank/DDBJ databases.
**   Microbiology, UT Southwestern Medical Center, 6000 Harry Hines Blvd.,
**   Dallas, TX 75235, USA
**   source          1..4334
**                   /organism="Homo sapiens"
**                   /chromosome="2"
**                   /map="2q34-2q35"
**   CDS             join(AF038034:174..331,AF038035:2614..2670,
**   AF038035:7289..7437,AF038036:621..1570,AF038037:451..531,
**                   AF038038:508..680,AF038039:548..656,AF038040:566..698,
**                   AF038041:226..318,519..616,2019..2351)
**                   /codon_start=1
**                   /db_xref="PID:g2828068"
**                   /gene="BARD1"
**                   /product="BRCA1-associated RING domain protein"
**   CDS_IN_EMBL_ENTRY 1
**   03-FEB-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00023; ank; 427; 459; T; 19-JUN-2000;
**PM PFAM; PF00023; ank; 460; 492; T; 19-JUN-2000;
**PM PFAM; PF00023; ank; 493; 525; T; 19-JUN-2000;
**PM PROSITE; PS00518; ZINC_FINGER_C3HC4; 66; 75; T; 19-JUN-2000;
**PM PROSITE; PS50088; ANK_REP; 427; 459; T; 19-JUN-2000;
**PM PROSITE; PS50088; ANK_REP; 460; 492; T; 19-JUN-2000;
**PM PROSITE; PS50088; ANK_REP; 493; 525; T; 19-JUN-2000;
**PM PROSITE; PS50089; ZF_RING; 50; 86; T; 19-JUN-2000;
**PM PROSITE; PS50172; BRCT_DOMAIN; 570; 653; T; 19-JUN-2000;
**PM PROSITE; PS50172; BRCT_DOMAIN; 667; 777; T; 19-JUN-2000;
**PM PROSITE; PS50297; ANK_REP_REGION; 427; 525; T; 19-JUN-2000;
**RU RU000251; 22-JAN-1998.
SQ   SEQUENCE   777 AA;  86579 MW;  51DCB76574015D4D CRC64;
     MPDNRQPRNR QPRIRSGNEP RSAPAMEPDG RGAWAHSRAA LDRLEKLLRC SRCTNILREP
     VCLGGCEHIF CSNCVSDCIG TGCPVCYTPA WIQDLKINRQ LDSMIQLCSK LRNLLHDNEL
     SDLKEDKPRK SLFNDAGNKK NSIKMWFSPR SKKVRYVVSK ASVQTQPAIK KDASAQQDSY
     EFVSPSPPAD VSERAKKASA RSGKKQKKKT LAEINQKWNL EAEKEDGEFD SKEESKQKLV
     SFCSQPSVIS SPQINGEIDL LASGSLTESE CFGSLTEVSL PLAEQIESPD TKSRNEVVTP
     EKVCKNYLTS KKSLPLENNG KRGHHNRLSS PISKRCRTSI LSTSGDFVKQ TVPSENIPLP
     ECSSPPSCKR KVGGTSGSKN SNMSDEFISL SPGTPPSTLS SSSYRRVMSS PSAMKLLPNM
     AVKRNHRGET LLHIASIKGD IPSVEYLLQN GSDPNVKDHA GWTPLHEACN HGHLKVVELL
     LQHKALVNTT GYQNDSPLHD AAKNGHVDIV KLLLSYGASR NAVNIFGLRP VDYTDDESMK
     SLLLLPEKNE SSSASHCSVM NTGQRRDGPL VLIGSGLSSE QQKMLSELAV ILKAKKYTEF
     DSTVTHVVVP GDAVQSTLKC MLGILNGCWI LKFEWVKACL RRKVCEQEEK YEIPEGPRRS
     RLNREQLLPK LFDGCYFYLW GTFKHHPKDN LIKLVTAGGG QILSRKPKPD SDVTQTINTV
     AYHARPDSDQ RFCTQYIIYE DLCNYHPERV RQGKVWKAPS SWFIDCVMSF ELLPLDS
//
ID   O43588      PRELIMINARY;      PRT;   978 AA.
AC   O43588;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-1998 (TrEMBLrel. 06, Last annotation update)
DE   GENERAL TRANSCRIPTION FACTOR 2-I.
GN   GTF2I.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Perez-Jurado L.A., Wang Y.K., Peoples R., Coloma A., Cruces J.,
RA   Francke U.;
RL   Hum. Mol. Genet. 0:0-0(1998).
DR   EMBL; AF038967; AAC08313.1; -.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens general transcription factor 2-I (GTF2I) mRNA,
**   alternatively spliced product, complete cds.
**   [1]
**   1-4423
**   Perez-Jurado L.A., Wang Y.K., Peoples R., Coloma A., Cruces J.,
**   Francke U.;
**   "A duplicated gene in the breakpoint regions of the 7q11.23
**   Williams-Beuren syndrome deletion encodes the initiator binding
**   protein
**   TFII-I and BAP-135, a phosphorylation target of Btk";
**   Hum. Mol. Genet. 0:0-0(1998).
**   [2]
**   1-4423
**   Perez-Jurado L.A., Wang Y.K., Peoples R., Coloma A., Cruces J.,
**   Francke U.;
**   ;
**   Submitted (18-DEC-1997) to the EMBL/GenBank/DDBJ databases.
**   Howard Hughes Medical Institute, Stanford Medical Center, Beckman
**   Center
**   B201, Stanford, CA 94305, USA
**   source          1..4423
**                   /organism="Homo sapiens"
**                   /chromosome="7"
**                   /map="7q11.23"
**   CDS             317..3253
**                   /codon_start=1
**                   /db_xref="PID:g2827203"
**                   /note="alternatively spliced"
**                   /gene="GTF2I"
**                   /product="general transcription factor 2-I"
**   CDS_IN_EMBL_ENTRY 1
**   03-FEB-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   978 AA;  110280 MW;  1F006D480F0BE702 CRC64;
     MAQVAMSTLP VEDEESSESR MVVTFLMSAL ESMCKELAKS KAEVACIAVY ETDVFVVGTE
     RGRAFVNTRK DFQKDFVKYC VEEEEKAAEM HKMKSTTQAN RMSVDAVEIE TLRKTVEDYF
     CFCYGKALGK STVVPVPYEK MLRDQSAVVV QGLPEGVAFK HPENYDLATL KWILENKAGI
     SFIIKRPFLE PKKHVGGRVM VTDADRSILS PGGSCGPIKV KTEPTEDSGI SLEMAAVTVK
     EESEDPDYYQ YNIQGSHHSS EGNEGTEMEV PAEDSTQHVP SETSEDPEVE VTIEDDDYSP
     PSKRPKANEL PQPPVPEPAN AGKRKVREFN FEKWNARITD LRKQVEELFE RKYAQAIKAK
     GPVTIPYPLF QSHVEDLYVE GLPEGIPFRR PSTYGIPRLE RILLAKERIR FVIKKHELLN
     STREDLQLDK PASGVKEEWY ARITKLRKMV DQLFCKKFAE ALGSTEAKAV PYQKFEAHPN
     DLYVEGLPEN IPFRSPSWYG IPRLEKIIQV GNRIKFVIKR PELLTHSTTE VTQPRTNTPV
     KEDWNVRITK LRKQVEEIFN LKFAQALGLT EAVKVPYPVF ESNPEFLYVE GLPEGIPFRS
     PTWFGIPRLE RIVRGSNKIK FVVKKPELVI SYLPPGMASK INTKALQSPK RPRSPGSNSK
     VPEIEVTVEG PNNNNPQTSA VRTPTQTNGS NVPFKPRGRE FSFEAWNAKI TDLKQKVENL
     FNEKCGEALG LKQAVKVPFA LFESFPEDFY VEGLPEGVPF RRPSTFGIPR LEKILRNKAK
     IKFIIKKPEM FETAIKESTS SKSPPRKINS SPNVNTTASG VEDLNIIQVT IPDDDNERLS
     KVEKARQLRE QVNDLFSRKF GEAIGMGFPV KVPYRKITIN PGCVVVDGMP PGVSFKAPSY
     LEISSMRRIL DSAEFIKFTV IRPFPGLVIN NQLVDQSESE GPVIQESAEP SQLEVPATEE
     IKETDGSSQI KQEPDPTW
//
ID   O43589      PRELIMINARY;      PRT;   977 AA.
AC   O43589;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-1998 (TrEMBLrel. 06, Last annotation update)
DE   GENERAL TRANSCRIPTION FACTOR 2-I.
GN   GTF2I.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Perez-Jurado L.A., Wang Y.K., Peoples R., Coloma A., Cruces J.,
RA   Francke U.;
RL   Hum. Mol. Genet. 0:0-0(1998).
DR   EMBL; AF038968; AAC08314.1; -.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens general transcription factor 2-I (GTF2I) mRNA,
**   alternatively spliced product, complete cds.
**   [1]
**   1-4420
**   Perez-Jurado L.A., Wang Y.K., Peoples R., Coloma A., Cruces J.,
**   Francke U.;
**   "A duplicated gene in the breakpoint regions of the 7q11.23
**   Williams-Beuren syndrome deletion encodes the initiator binding
**   protein
**   TFII-I and BAP-135, a phosphorylation target of Btk";
**   Hum. Mol. Genet. 0:0-0(1998).
**   [2]
**   1-4420
**   Perez-Jurado L.A., Wang Y.K., Peoples R., Coloma A., Cruces J.,
**   Francke U.;
**   ;
**   Submitted (18-DEC-1997) to the EMBL/GenBank/DDBJ databases.
**   Howard Hughes Medical Institute, Stanford Medical Center, Beckman
**   Center
**   B201, Stanford, CA 94305, USA
**   source          1..4420
**                   /organism="Homo sapiens"
**                   /chromosome="7"
**                   /map="7q11.23"
**   CDS             317..3250
**                   /codon_start=1
**                   /db_xref="PID:g2827205"
**                   /note="alternatively spliced"
**                   /gene="GTF2I"
**                   /product="general transcription factor 2-I"
**   CDS_IN_EMBL_ENTRY 1
**   03-FEB-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   977 AA;  110106 MW;  418D59EC623E6141 CRC64;
     MAQVAMSTLP VEDEESSESR MVVTFLMSAL ESMCKELAKS KAEVACIAVY ETDVFVVGTE
     RGRAFVNTRK DFQKDFVKYC VEEEEKAAEM HKMKSTTQAN RMSVDAVEIE TLRKTVEDYF
     CFCYGKALGK STVVPVPYEK MLRDQSAVVV QGLPEGVAFK HPENYDLATL KWILENKAGI
     SFIIKRPFLE PKKHVGGRVM VTDADRSILS PGGSCGPIKV KTEPTEDSGI SLEMAAVTVK
     EESEDPDYYQ YNIQAGPSET DDVDEKQPLS KPLQGSHHSS EGNEGTEMEV PAEDDDYSPP
     SKRPKANELP QPPVPEPANA GKRKVREFNF EKWNARITDL RKQVEELFER KYAQAIKAKG
     PVTIPYPLFQ SHVEDLYVEG LPEGIPFRRP STYGIPRLER ILLAKERIRF VIKKHELLNS
     TREDLQLDKP ASGVKEEWYA RITKLRKMVD QLFCKKFAEA LGSTEAKAVP YQKFEAHPND
     LYVEGLPENI PFRSPSWYGI PRLEKIIQVG NRIKFVIKRP ELLTHSTTEV TQPRTNTPVK
     EDWNVRITKL RKQVEEIFNL KFAQALGLTE AVKVPYPVFE SNPEFLYVEG LPEGIPFRSP
     TWFGIPRLER IVRGSNKIKF VVKKPELVIS YLPPGMASKI NTKALQSPKR PRSPGSNSKV
     PEIEVTVEGP NNNNPQTSAV RTPTQTNGSN VPFKPRGREF SFEAWNAKIT DLKQKVENLF
     NEKCGEALGL KQAVKVPFAL FESFPEDFYV EGLPEGVPFR RPSTFGIPRL EKILRNKAKI
     KFIIKKPEMF ETAIKESTSS KSPPRKINSS PNVNTTASGV EDLNIIQVTI PDDDNERLSK
     VEKARQLREQ VNDLFSRKFG EAIGMGFPVK VPYRKITINP GCVVVDGMPP GVSFKAPSYL
     EISSMRRILD SAEFIKFTVI RPFPGLVINN QLVDQSESEG PVIQESAEPS QLEVPATEEI
     KETDGSSQIK QEPDPTW
//
ID   O43592      PRELIMINARY;      PRT;   962 AA.
AC   O43592;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-1998 (TrEMBLrel. 06, Last annotation update)
DE   EXPORTIN T.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Kutay U., Lipowsky G., Izaurralde E., Schwarzmaier P., Hartmann E.,
RA   Goerlich D.;
RL   Mol. Cell 0:0-0(1998).
DR   EMBL; AF039022; AAC39793.1; -.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens exportin t mRNA, complete cds.
**   [1]
**   1-2889
**   Kutay U., Lipowsky G., Izaurralde E., Schwarzmaier P., Hartmann E.,
**   Goerlich D.;
**   "Identification of a t-RNA-specific nuclear export receptor";
**   Mol. Cell 0:0-0(1998).
**   [2]
**   1-2889
**   Goerlich D., Hartmann E.;
**   ;
**   Submitted (17-DEC-1997) to the EMBL/GenBank/DDBJ databases.
**   Zellbiologie, MDC Berlin, Robert-Roessle-Str. 10, Berlin 13125,
**   Deutschland
**   source          1..2889
**                   /organism="Homo sapiens"
**   CDS             1..2889
**                   /codon_start=1
**                   /db_xref="PID:g2873377"
**                   /function="nuclear export factor involved in tRNA
**   export"
**                   /function="binds to Ran-GTP"
**                   /product="exportin t"
**   CDS_IN_EMBL_ENTRY 1
**   16-FEB-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   962 AA;  109992 MW;  317F7FB8186A58F1 CRC64;
     MDEQALLGLN PNADSDFRQR ALAYFEQLKI SPDAWQVCAE ALAQRTYSDD HVKFFCFQVL
     EHQVKYKYSE LTTVQQQLIR ETLISWLQAQ MLNPQPEKTF IRNKAAQVFA LLFVTEYLTK
     WPKFFFDILS VVDLNPRGVD LYLRILMAID SELVDRDVVH TSEEARRNTL IKDTMREQCI
     PNLVESWYQI LQNYQFTNSE VTCQCLEVVG AYVSWIDLSL IANDRFINML LGHMSIEVLR
     EEACDCLFEV VNKGMDPVDK MKLVESLCQV LQSAGFFSID QEEDVDFLAR FSKLVNGMGQ
     SLIVSWSKLI KNGDIKNAQE ALQAIETKVA LMLQLLIHED DDISSNIIGF CYDYLHILKR
     LTVLSDQQKA NVEAIMLAVM KKLTYDEEYN FENEGEDEAM FVEYRKQLKL LLDRLAQVSP
     ELLLASVRRV FSSTLQNWQT TRFMEVEVAI RLLYMLAEAL PVSHGAHFSG DVSKASALQD
     MMRTLVTSGV SSYQHTSVTL EFFETVVRYE KFFTVEPQHI PCVLMAFLDH RGLRHSSAKV
     RSRTAYLFSR FVKSLNKQMN PFIEDILNRI QDLLELSPPE NGHQSLLSSD DQLFIYETAG
     VLIVNSEYPA ERKQALMRNL LTPLMEKFKI LLEKLMLAQD EERQASLADC LNHAVGFASR
     TSKAFSNKQT VKQCGCSEVY LDCLQTFLPA LSCPLQKDIL RSGVRTFLHR MIICLEEEVL
     PFIPSASEHM LKDCEAKDLQ EFIPLINQIT AKFKIQVSPF LQQMFMPLLH AIFEVLLRPA
     EENDQSAALE KQMLRRSYFA FLQTVTGSGM SEVIANQGAE NVERVLVTVI QGAVEYPDPI
     AQKTCFIILS KLVELWGGKD GPVGFADFVY KHIVPACFLA PLKQTFDLAD AQTVLALSEC
     AVTLKTIHLK RGPECVQYLQ QEYLPSLQVA PEIIQEFCQA LQQPDAKVFK NYLKVFFQRA
     KP
//
ID   O43629      PRELIMINARY;      PRT;   194 AA.
AC   O43629;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   OLFACTORY RECEPTOR-LIKE PROTEIN (FRAGMENT).
GN   OLFR42A.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Gallinaro H.;
RL   Immunogenetics 0:0-0(1998).
DR   EMBL; AF042078; AAC00184.1; -.
DR   INTERPRO; IPR000276; -.
DR   PFAM; PF00001; 7tm_1; 1.
DR   PROSITE; PS00237; G_PROTEIN_RECEPTOR; UNKNOWN_1.
FT   NON_TER       1      1
FT   NON_TER     194    194
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens olfactory receptor-like protein (OLFR42A) gene,
**   OLFR42A-9026.2 allele, partial cds.
**   [1]
**   1-583
**   Gallinaro H.;
**   "Olfactory receptor gene cluster in man and mouse major
**   histocompatibility complex";
**   Immunogenetics 0:0-0(1998).
**   [2]
**   1-583
**   Gallinaro H.;
**   ;
**   Submitted (09-JAN-1998) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, CIGH-CNRS, 1 Avenue de Grande Bretagne, Toulouse
**   31300, France
**   source          1..583
**                   /organism="Homo sapiens"
**                   /chromosome="6"
**                   /map="6p21.3"
**                   /cell_line="12th IHW # 9026"
**   CDS             <1..>583
**                   /codon_start=1
**                   /db_xref="PID:g2828682"
**                   /gene="OLFR42A"
**                   /product="olfactory receptor-like protein"
**   CDS_IN_EMBL_ENTRY 1
**   05-FEB-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00001; 7tm_1; 1; 189; T; 19-JUN-2000;
**PM PROSITE; PS00237; G_PROTEIN_RECEPTOR; 51; 67; ?; 19-JUN-2000;
**PM PROSITE; PS50262; G_PROTEIN_RECEPTOR_2; 1; 194; T; 19-JUN-2000;
SQ   SEQUENCE   194 AA;  21527 MW;  6A23FBF2FDCACEAF CRC64;
     YFFLSNLSFL DLCFTTSCVP QMLVNLWGPK KTISFLGCSV QLFIFLSLGT TECILLTVMA
     FDRYVAVCQP LHYATIIHPR LCWQLASVAW VMSLVQSIVQ TPSTLHLPFC PHQQIDDFLC
     EVPSLIRLSC GDTSYNEIQL AVSSVIFVVV PLSLILASYG ATAQAVLRIN SATAWRKAFG
     TCSSHLTVVT LFYS
//
ID   O43636      PRELIMINARY;      PRT;  1245 AA.
AC   O43636;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   ROD PHOTORECEPTOR CNG-CHANNEL BETA SUBUNIT.
GN   RCNC2.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Grunwald M.E., Yu W.P., Yu H.H., Yau K.W.;
RL   J. Biol. Chem. 0:0-0(1998).
DR   EMBL; AF042498; AAC04830.1; -.
DR   INTERPRO; IPR000595; -.
DR   INTERPRO; IPR001622; -.
DR   INTERPRO; IPR002025; -.
DR   PFAM; PF00027; cNMP_binding; 1.
DR   PFAM; PF00914; CNG_membrane; 1.
DR   PROSITE; PS00888; CNMP_BINDING_1; 1.
DR   PROSITE; PS00889; CNMP_BINDING_2; 1.
DR   PROSITE; PS50042; CNMP_BINDING_3; 1.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens rod photoreceptor CNG-channel beta subunit (RCNC2)
**   mRNA, complete cds.
**   [1]
**   1-4382
**   Grunwald M.E., Yu W.P., Yu H.H., Yau K.W.;
**   "Identification of a domain on the beta subunit of the rod cGMP-gated
**   cation channel that mediates inhibition by calcium-calmodulin";
**   J. Biol. Chem. 0:0-0(1998).
**   [2]
**   1-4382
**   Grunwald M.E., Yu W.P., Yu H.H., Yau K.W.;
**   ;
**   Submitted (12-JAN-1998) to the EMBL/GenBank/DDBJ databases.
**   Neuroscience, Johns Hopkins University School of Medicine, 725 N.
**   Wolfe
**   St., Baltimore, MD 21205, USA
**   source          1..4382
**                   /organism="Homo sapiens"
**   CDS             71..3808
**                   /codon_start=1
**                   /db_xref="PID:g2921583"
**                   /note="cyclic nucleotide-gated cation channel beta
**   subunit"
**                   /gene="RCNC2"
**                   /product="rod photoreceptor CNG-channel beta subunit"
**   CDS_IN_EMBL_ENTRY 1
**   10-MAR-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00027; cNMP_binding; 971; 1065; T; 19-JUN-2000;
**PM PFAM; PF00914; CNG_membrane; 726; 942; T; 19-JUN-2000;
**PM PROSITE; PS00888; CNMP_BINDING_1; 983; 999; T; 19-JUN-2000;
**PM PROSITE; PS00889; CNMP_BINDING_2; 1022; 1042; T; 19-JUN-2000;
**PM PROSITE; PS50042; CNMP_BINDING_3; 956; 1060; T; 19-JUN-2000;
**PM PROSITE; PS50265; CHANNEL_PORE_K; 825; 877; T; 19-JUN-2000;
SQ   SEQUENCE   1245 AA;  139160 MW;  40C4860BFCF86126 CRC64;
     MLGWVQRVLP QPPGTPRKTK MQEEEEVEPE PEMEAEVEPE PNPEEAETES ESMPPEESFK
     EEEVAVADPS PQETKEAALT STISLRAQGA EISEMNSPSH RVLTWLMKGV EKVIPQPVHS
     ITEDPAQILG HGSTGDTGCT DEPNEALEAQ DTRPGLRLLL WLEQNLERVL PQPPKSSEVW
     RDEPAVATAP PGRPQEMGPK LQARETPSLP TPIPLQPKEE PKEAPAPEPQ PGSQAQTSSL
     PPTRDPARLV AWVLHRLEMA LPQPVLHGKI GEQEPDSPGI CDVQTISILP GGQVEPDLVL
     EEVEPPWEDA HQDVSTSPQG TEVVPAYEEE NKAVEKMPRE LSRIEEEKED EEEEEEEEEE
     EEEEEVTEVL LDSCVVSQVG VGQSEEDGTR PQSTSDQKLW EEVGEEAKKE AEEKAKEEAE
     EVAEEEAEKE PQDWAETKEE PEAEAEAASS GVPATKQHPE VQVEDTDADS CPLMAEENPP
     STVLPPPSPA KSDTLIVPSS ASGTHRKKLP SEDDEAEELK ALSPAESPVV AWSDPTTPKD
     TDGQDRAAST ASTNSAIIND RLQELVKLFK ERTEKVKEKL IDPDVTSDEE SPKPSPAKKA
     PEPAPDTKPA EAEPVEEEHY CDMLCCKFKH RPWKKYQFPQ SIDPLTNLMY VLWLFFVVMA
     WNWNCWLIPV RWAFPYQTPD NIHHWLLMDY LCDLIYFLDI TVFQTRLQFV RGGDIITDKK
     DMRNNYLKSR RFKMDLLSLL PLDFLYLKVG VNPLLRLPRC LKYMAFFEFN SRLESILSKA
     YVYRVIRTTA YLLYSLHLNS CLYYWASAYQ GLGSTHWVYD GVGNSYIRCY YFAVKTLITI
     GGLPDPKTLF EIVFQLLNYF TGVFAFSVMI GQMRDVVGAA TAGQTYYRSC MDSTVKYMNF
     YKIPKSVQNR VKTWYEYTWH SQGMLDESEL MVQLPDKMRL DLAIDVNYNI VSKVALFQGC
     DRQMIFDMLK RLRSVVYLPN DYVCKKGEIG REMYIIQAGQ VQVLGGPDGK SVLVTLKAGS
     VFGEISLLAV GGGNRRTANV VAHGFTNLFI LDKKDLNEIL VHYPESQKLL RKKARRMLRS
     NNKPKEEKSV LILPPRAGTP KLFNAALAMT GKMGGKGAKG GKLAHLRARL KELAALEAAA
     KQQELVEQAK SSQDVKGEEG SAAPDQHTHP KEAATDPPAP RTPPEPPGSP PSSPPPASLG
     RPEGEEEGPA EPEEHSVRIC MSPGPEPGEQ ILSVKMPEER EEKAE
//
ID   O43707      PRELIMINARY;      PRT;   884 AA.
AC   O43707;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   ALPHA ACTININ 4.
GN   HACTN4.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Honda K., Yamada T., Endo R., Ino Y., Gotoh M., Tsuda H., Yamada Y.,
RA   Chiba H., Hirohashi S.;
RL   J. Cell Biol. 0:0-0(1998).
DR   EMBL; D89980; BAA24447.1; -.
DR   HSSP; Q01082; 1AA2.
DR   INTERPRO; IPR001589; -.
DR   INTERPRO; IPR001715; -.
DR   INTERPRO; IPR002017; -.
DR   INTERPRO; IPR002048; -.
DR   PFAM; PF00036; efhand; 2.
DR   PFAM; PF00307; CH; 2.
DR   PFAM; PF00435; spectrin; 4.
DR   PROSITE; PS00018; EF_HAND; UNKNOWN_1.
DR   PROSITE; PS00019; ACTININ_1; 1.
DR   PROSITE; PS00020; ACTININ_2; 1.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens mRNA for alpha actinin 4, complete cds.
**   [1]
**   1-2873
**   Honda K.;
**   ;
**   Submitted (20-DEC-1996) to the EMBL/GenBank/DDBJ databases.
**   Kazufumi Honda, National Cancer Center Research Institute, Pathology
**   Division; 1-1 Tsukiji 5-chome, Chuo-ku, Tokyo 104, Japan
**   (E-mail:tyamada@gan2.ncc.go.jp, Tel:+81-3-3542-2511,
**   Fax:+81-3-3248-2737)
**   [2]
**   Honda K., Yamada T., Endo R., Ino Y., Gotoh M., Tsuda H., Yamada Y.,
**   Chiba H., Hirohashi S.;
**   "Actinin-4, a novel actin-bundling protein associated with cell
**   motility
**   and cancer invasion";
**   J. Cell Biol. 0:0-0(1998).
**   source          1..2873
**                   /organism="Homo sapiens"
**                   /sequenced_mol="cDNA to mRNA"
**                   /cell_line="NCC-MS-1 CDDP"
**   CDS             89..2743
**                   /codon_start=1
**                   /db_xref="PID:d1025362"
**                   /transl_table=1
**                   /gene="HACTN4"
**                   /product="alpha actinin 4"
**   CDS_IN_EMBL_ENTRY 1
**   26-FEB-1998 (Rel. 54, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00036; efhand; 742; 770; T; 19-JUN-2000;
**PM PFAM; PF00036; efhand; 783; 811; T; 19-JUN-2000;
**PM PFAM; PF00307; CH; 23; 127; T; 19-JUN-2000;
**PM PFAM; PF00307; CH; 136; 242; T; 19-JUN-2000;
**PM PFAM; PF00435; spectrin; 266; 376; T; 19-JUN-2000;
**PM PFAM; PF00435; spectrin; 386; 491; T; 19-JUN-2000;
**PM PFAM; PF00435; spectrin; 501; 612; T; 19-JUN-2000;
**PM PFAM; PF00435; spectrin; 622; 725; T; 19-JUN-2000;
**PM PROSITE; PS00018; EF_HAND; 751; 763; ?; 19-JUN-2000;
**PM PROSITE; PS00019; ACTININ_1; 25; 34; T; 19-JUN-2000;
**PM PROSITE; PS00020; ACTININ_2; 99; 123; T; 19-JUN-2000;
**PM PROSITE; PS50021; CH_DOMAIN; 23; 127; T; 19-JUN-2000;
**PM PROSITE; PS50021; CH_DOMAIN; 136; 239; T; 19-JUN-2000;
**PM PROSITE; PS50083; SPEC_REPEAT; 353; 455; T; 19-JUN-2000;
**PM PROSITE; PS50083; SPEC_REPEAT; 464; 575; T; 19-JUN-2000;
**PM PROSITE; PS50083; SPEC_REPEAT; 592; 691; T; 19-JUN-2000;
**PM PROSITE; PS50222; EF_HAND_2; 734; 808; T; 19-JUN-2000;
SQ   SEQUENCE   884 AA;  102268 MW;  0B5D0C6B614E424C CRC64;
     MGDYMAQEDD WDRDLLLDPA WEKQQRKTFT AWCNSHLRKA GTQIENIDED FRDGLKLMLL
     LEVISGERLP KPERGKMRVH KINNVNKALD FIASKGVKLV SIGAEEIVDG NAKMTLGMIW
     TIILRFAIQD ISVEETSAKE GLLLWCQRKT APYKNVNVQN FHISWKDGLA FNALIHRHRP
     ELIEYDKLRK DDPVTNLNNA FEVAEKYLDI PKMLDAEDIV NTARPDEKAI MTYVSSFYHA
     FSGAQKAETA ANRICKVLAV NQENEHLMED YEKLASDLLE WIRRTIPWLE DRVPQKTIQE
     MQQKLEDFRD YRRVHKPPKV QEKCQLEINF NTLQTKLRLS NRPAFMPSEG KMVSDINNGW
     QHLEQAEKGY EEWLLNEIRR LERLDHLAEK FRQKASIHEA WTDGKEAMLK HRDYETATLS
     DIKALIRKHE AFESDLAAHQ DRVEQIAAIA QELNELDYYD SHNVNTRCQK ICDQWDALGS
     LTHSRREALE KTEKQLEAID QLHLEYAKRA APFNNWMESA MEDLQDMFIV HTIEEIEGLI
     SAHDQFKSTL PDADREREAI LAIHKEAQRI AESNHIKLSG SNPYTTVTPQ IINSKWEKVQ
     QLVPKRDHAL LEEQSKQQSN EHLRRQFASQ ANVVGPWIQT KMEEIGRISI EMNGTLEDQL
     SHLKQYERSI VDYKPNLDLL EQQHQLIQEA LIFDNKHTNY TMEHIRVGWE QLLTTIARTI
     NEVENQILTR DAKGISQEQM QEFRASFNHF DKDHGGALGP EEFKACLISL GYDVENDRQG
     EAEFNRIMSL VDPNHSGLVT FQAFIDFMSR ETTDTDTADQ VIASFKVLAG DKNFITAEEL
     RRELPPDQAE YCIARMAPYQ GPDAVPGALD YKSFSTALYG ESDL
//
ID   O43721      PRELIMINARY;      PRT;   220 AA.
AC   O43721;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-1998 (TrEMBLrel. 06, Last annotation update)
DE   HYPOTHETICAL 24.5 kDa PROTEIN.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=FETAL HEART;
RX   MEDLINE=98153806; PubMed=9480850;
RA   Vidal-Taboada J.M., Bergonon S., Sanchez M., Lopez-Acedo C., Groet J.,
RA   Nizetic D., Egeo A., Scartezzini P., Katsanis N., Fisher E.M.C.,
RA   Delabar J.M., Oliva R.;
RT   "High resolution physical mapping and identification of transcribed
RT   sequences in the Down syndrome region-2.";
RL   Biochem. Biophys. Res. Commun. 243:572-578(1998).
DR   EMBL; AJ222636; CAA10896.1; -.
KW   Hypothetical protein.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens partial human cDNA (660 bp)
**   [1]
**   1-660
**   Scartezzini P.;
**   ;
**   Submitted (27-NOV-1997) to the EMBL/GenBank/DDBJ databases.
**   Scartezzini P., Pediatrics, E/O Ospedali Galliera, Via Mura delle
**   capuccine 14, 16128 Genova, ITALY.
**   [3]
**   Vidal-Taboada J.M., Bergonon S., Sanchez M., Lopez-Acedo C., Groet J.,
**   Nizetic D., Egeo A., Scartezzini P., Katsanis N., Fisher E.M.C.,
**   Delabar J.M., Oliva R.;
**   "High resolution physical mapping and identification of transcribed
**   sequences in the down syndrome region-2";
**   Biochem. Biophys. Res. Commun. 243:572-578(1998).
**   source          1..660
**                   /organism="Homo sapiens"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="fetal heart"
**   CDS             1..660
**                   /codon_start=1
**                   /db_xref="PID:e1254889"
**                   /product="hypothetical protein"
**   Warning: illegal start codon
**   CDS_IN_EMBL_ENTRY 1
**   03-MAR-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   220 AA;  24455 MW;  4B795D89F191ECDF CRC64;
     GTRRSGLSRS SNLRVTRTRA AQRKTGPVSL ANGCGRKATR KRVYLSDSDN NSLETGEILK
     ARAGNNRKVL RKCAAVAANK IKLMSDVEEN SSSESVCSGR KLPHRNASAV ARKKLLHNSE
     DEQSLKSEIE EEELKDENQP LPVSSSHTAQ SNVDESENRD SESESDLRVA RKNWHANGYK
     SHTPAPSKTK FLKIESSEED SKVMIQIMHV QNCWPINVCQ
//
ID   O43919      PRELIMINARY;      PRT;   233 AA.
AC   O43919;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   LINKER FOR ACTIVATION OF T CELLS (LAT).
GN   LAT OR PP36.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Zhang W., Sloan-Lancaster J., Kitchen J., Trible R.P., Samelson L.E.;
RL   Cell 0:0-0(1997).
RN   [2]
RP   SEQUENCE FROM N.A.
RC   TISSUE=THYMUS;
RA   Weber J.R., Orstavik S., Torgersen K.M., Danbolt N.C., Berg S.F.,
RA   Tasken K., Imboden J.B., Vaage J.T.;
RL   Submitted (JAN-1998) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; AF036905; AAC39636.1; -.
DR   EMBL; AJ223280; CAA11218.1; -.
DR   INTERPRO; IPR000345; -.
DR   PROSITE; PS00190; CYTOCHROME_C; UNKNOWN_1.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens linker for activation of T cells (LAT) mRNA, complete
**   cds.
**   [1]
**   1-1060
**   Zhang W., Sloan-Lancaster J., Kitchen J., Trible R.P., Samelson L.E.;
**   "LAT: the ZAP-70 tyrosine kinase substrate that links T cell receptor
**   to
**   cellular activation";
**   Cell 0:0-0(1997).
**   [2]
**   1-1060
**   Zhang W., Sloan-Lancaster J., Kitchen J., Trible R.P., Samelson L.E.;
**   ;
**   Submitted (05-DEC-1997) to the EMBL/GenBank/DDBJ databases.
**   Cell Biology and Metabolism Branch, National Institute of Child Health
**   and Development, National Institute of Health, 9000 Rockville Pike,
**   Bethesda, MD 20892, USA
**   LAT is a highly tyrosine phosphorylated protein, previously
**   described as p36-38, and it associates with many signaling
**   molecules, such as Grb2, PLC-gamma1, PI-3 kinase, cbl, Vav, and
**   SLP-76, either directly or indirectly upon T cell activation. It is
**   a potential type III transmembrane protein.
**   [1]
**   1-1616
**   Orstavik S.;
**   ;
**   Submitted (09-JAN-1998) to the EMBL/GenBank/DDBJ databases.
**   Orstavik S., Institute of Medical Biochemistry, University of Oslo, BP
**   1112, Blindern, N-0317 Oslo, NORWAY.
**   [2]
**   Weber J.R., Orstavik S., Torgersen K.M., Danbolt N.C., Berg S.F.,
**   Tasken K., Imboden J.B., Vaage J.T.;
**   "Cloning of pp36, a tyrosine-phosphorylated adaptor protein
**   specifically
**   expressed in T and NK cells.";
**   Unpublished.
**   source          1..1060
**                   /organism="Homo sapiens"
**                   /cell_line="Jurkat T cells"
**   CDS             58..759
**                   /codon_start=1
**                   /db_xref="PID:g2828024"
**                   /note="tyrosine kinase substrate"
**                   /gene="LAT"
**                   /product="LAT"
**   CDS_IN_EMBL_ENTRY 1
**   03-FEB-1998 (Rel. 54, Last updated, Version 1)
**   source          1..1616
**                   /organism="Homo sapiens"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="thymus"
**   CDS             323..1024
**                   /codon_start=1
**                   /db_xref="PID:e1234817"
**                   /gene="pp36"
**                   /product="36 kDa phosphothyrosine protein"
**   CDS_IN_EMBL_ENTRY 1
**   13-JAN-1998 (Rel. 54, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PROSITE; PS00190; CYTOCHROME_C; 26; 31; ?; 19-JUN-2000;
SQ   SEQUENCE   233 AA;  24985 MW;  0832E2D2B4220BC6 CRC64;
     MEEAILVPCV LGLLLLPILA MLMALCVHCH RLPGSYDSTS SDSLYPRGIQ FKRPHTVAPW
     PPAYPPVTSY PPLSQPDLLP IPRSPQPLGG SHRTPSSRRD SDGANSVASY ENEEPACEDA
     DEDEDDYHNP GYLVVLPDST PATSTAAPSA PALSTPGIRD SAFSMESIDD YVNVPESGES
     AEASLDGSRE YVNVSQELHP GAAKTEPAAL SSQEAEEVEE EGAPDYENLQ ELN
//
ID   O43923      PRELIMINARY;      PRT;   183 AA.
AC   O43923;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   METALLOPROTEINASE.
GN   MMP20.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Bernot A., Heilig R., Clepet C., Smaoui N., Delpech M., da Silva C.,
RA   Devaud C., Petit J.L., Chiannilkulchai N., Fizames C., Samson D.,
RA   Cruaud C., Caloustian C., Gyapay G., Weissenbach J.;
RL   Submitted (JAN-1998) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; AJ003147; CAA05902.1; -.
DR   EMBL; AJ003144; CAA05900.1; -.
DR   HSSP; P09237; 1MMP.
DR   INTERPRO; IPR001818; -.
DR   PFAM; PF00413; Peptidase_M10; 1.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens complete genomic sequence between D16S3070 and
**   D16S3275, containing Familial Mediterranean Fever gene disease
**   [1]
**   Bernot A., Heilig R., Clepet C., Smaoui N., Delpech M., da Silva C.,
**   Devaud C., Petit J.L., Chiannilkulchai N., Fizames C., Samson D.,
**   Cruaud C., Caloustian C., Gyapay G., Weissenbach J.;
**   "A transcriptional map of the FMF region";
**   Unpublished.
**   [2]
**   1-239566
**   Bernot A.;
**   ;
**   Submitted (07-JAN-1998) to the EMBL/GenBank/DDBJ databases.
**   GENOSCOPE - Centre National de Sequencage, 2 rue Gaston Cremieux, EVRY
**   BP191, FRANCE.
**   [2]
**   1-627
**   Bernot A.;
**   ;
**   Submitted (07-JAN-1998) to the EMBL/GenBank/DDBJ databases.
**   GENOSCOPE - Centre National de Sequencage, 2 rue Gaston Cremieux, EVRY
**   BP191, FRANCE.
**   [3]
**   Bernot A., Heilig R., Clepet C., Smaoui N., Delpech M., da Silva C.,
**   Devaud C., Petit J.L., Chiannilkulchai N., Fizames C., Samson D.,
**   Cruaud C., Caloustian C., Gyapay G., Weissenbach J.;
**   "A transcriptional map of the FMF region";
**   Unpublished.
**   source          179596..222837
**                   /organism="Homo sapiens"
**                   /chromosome="16"
**                   /map="p13.3"
**                   /clone="YAC 26fe7"
**                   /sub_clone="30e10"
**   CDS             join(6772..6775,9222..9357,9467..9758,15042..15161)
**                   /db_xref="PID:e1246030"
**                   /gene="mmp20"
**                   /product="metalloproteinase"
**   CDS_IN_EMBL_ENTRY 5
**   22-JAN-1998 (Rel. 54, Last updated, Version 1)
**   source          1..627
**                   /organism="Homo sapiens"
**                   /chromosome="16"
**                   /map="p13.3"
**   CDS             14..565
**                   /codon_start=1
**                   /db_xref="PID:e1245446"
**                   /gene="MMP20"
**                   /product="metalloproteinase"
**   CDS_IN_EMBL_ENTRY 1
**   22-JAN-1998 (Rel. 54, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00413; Peptidase_M10; 1; 140; T; 19-JUN-2000;
SQ   SEQUENCE   183 AA;  20354 MW;  2F844C2B8338787B CRC64;
     MDPGTVATMR KPRCSLPDVL GVAGLVRRRR RYALSGSVWK KRTLTWRVRS FPQSSQLSQE
     TVRVLMSYAL MAWGMESGLT FHEVDSPQGQ EPDILIDFAR AFHQDSYPFD GLGGTLAHAF
     FPGEHPISGD THFDDEETWT FGSKASQQLE QELAGGSPVD EELGFSRGWR VNPLGPGSPE
     RLS
//
ID   C2F_HUMAN   PRELIMINARY;      PRT;   151 AA.
AC   Q92979; O00726; O00675;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-JUN-1998 (TrEMBLrel. 06, Last annotation update)
DE   C2F PROTEIN.
GN   C2F.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Ansari-Lari M.A., Shen Y., Muzny D.M., Lee W., Gibbs R.A.;
RL   Genome Res. 7:268-280(1997).
CC   -!- SIMILARITY: TO YEAST L9470.5 AND SPAC18G6.07C.
DR   EMBL; U72514; AAC51641.1; ALT_INIT.
DR   EMBL; U47924; CAB35662.1; -.
KW   Hypothetical protein.
**
**   #################     SOURCE SECTION     ##################
**   Human C2f mRNA, complete cds.
**   [1]
**   1-886
**   Ansari-Lari M.A., Shen Y., Muzny D.M., Lee W., Gibbs R.A.;
**   "Large scale sequencing in human chromosome 12p13: experimental and
**   computational gene structure determination";
**   Genome Res. 7:268-280(1997).
**   [2]
**   1-886
**   Ansari-Lari M.A., Shen Y., Muzny D.M., Lee W., Gibbs R.A.;
**   ;
**   Submitted (24-SEP-1996) to the EMBL/GenBank/DDBJ databases.
**   Molecular and Human Genetics, Baylor College of Medicine, One Baylor
**   Plaza, Houston, TX 77030, USA
**   [3]
**   1-886
**   Ansari-Lari M.A. PhD, Shen Y., Muzzny D.M., Lee W., Gibbs R.A. PhD.;
**   ;
**   Submitted (24-JUL-1997) to the EMBL/GenBank/DDBJ databases.
**   Department of Moelcular and Human Genetics, Baylor College of
**   Medicine,
**   One Baylor Plaza, Houston, TX 77030, USA
**   source          1..886
**                   /organism="Homo sapiens"
**                   /chromosome="12"
**                   /map="12p13"
**   CDS             <1..721
**                   /codon_start=2
**                   /db_xref="PID:g2276396"
**                   /evidence=EXPERIMENTAL
**                   /note="similar to EST with GenBank Accession Number
**   R64505;
**                   similar to S. cerevisiae hypothetical protein L9470.5
**                   encoded by GenBank Accession Number S51431, and to S.
**   pombe
**                   hypothetical 34.9 KD protein encoded by GenBank
**   Accession
**                   Number Z68198; see corresponding genomic sequence in
**                   GenBank Accession Number U72506"
**                   /gene="C2f"
**                   /product="C2f"
**   CDS_IN_EMBL_ENTRY 1
**   26-JUL-1997 (Rel. 52, Last updated, Version 3)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**ZZ CREATED AND FINISHED BY FIONA.
**ZZ UPDATED BY FIONA.
**ZZ CURATED.
SQ   SEQUENCE   151 AA;  16611 MW;  2D0090A2429296DB CRC64;
     MLMDSPLNRA GLLQVYIHTQ KNVLIEVNPQ TRIPRTFDRF CGLMVQLLHK LSVRAADGPQ
     KLLKVIKNPV SDHFPVGCMK VGTSFSIPVV SDVRELVPSS DPIVFVVGAF AHGKVSVEYT
     EKMVSISNYP LSAALTCAKL TTAFEEVWGV I
//
ID   Q12757      PRELIMINARY;      PRT;   171 AA.
AC   Q12757;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   MUCIN (FRAGMENT).
GN   B1.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=COLON;
RA   Walter A.O., Schwoeble W., Dippold W.;
RL   Submitted (DEC-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X83412; CAA58319.1; -.
FT   VAR_SEQ     358    383       ETNWPRELKDGNGQESLSMSSSSSPA -> DPGSPKKCRAR
FT                                FGLNQQTDWCGPCRRKKKCIRYLPGEGRCPSPVPSDDSALG
FT                                CPGSPAPQDSPSYHLLPRFPTELLTSPAERHLHPQVSPLLS
FT                                ASQPQGPHRPPAAPCRAHRYSNRNLRDRWPSRHRTPGRLQE
FT                                PTP (in isoform 1L and isoform 1S).
FT                                /FTId=VSP_006960.
FT   VAR_SEQ     358    383       ETNWPRELKDGNGQESLSMSSSSSPA -> GGKRNAFGTYP
FT                                EKAAAPAPFLPMTVL (in isoform 2L and
FT                                isoform 2S).
FT                                /FTId=VSP_002191.
FT   VAR_SEQ     358    383       ETNWPRELKDGNGQESLSMSSSSSPA -> DPGSPKKCRAR
FT                                FGLNQQTDWCGPCR (in isoform 3L and isoform
FT                                3S).
FT                                /FTId=VSP_002192.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens B1 mRNA for mucin
**   [1]
**   Walter A.O., Schwoeble W., Dippold W.;
**   "Cloning and expression of the carboxy terminus of a novel mucin";
**   Unpublished.
**   [2]
**   1-798
**   Walter A.O.;
**   ;
**   Submitted (13-DEC-1994) to the EMBL/GenBank/DDBJ databases.
**   A.O. Walter, I. Medical Clinic & Univ. of Mainz,
**   Verfuegungsgebaeude, Obere Zahlbacherstr. 63, 55101 Mainz, FRG
**   source          1..798
**                   /organism="Homo sapiens"
**                   /tissue_type="colon"
**                   /cell_line="T84"
**                   /clone_lib="lambda ZapXR"
**                   /chromosome="7"
**   CDS             <1..516
**                   /partial
**                   /codon_start=1
**                   /gene="B1"
**                   /product="mucin"
**                   /db_xref="PID:g853956"
**   CDS_1_OUT_OF_1
**   01-JUN-1995 (Rel. 44, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   171 AA;  17863 MW;  3F249989A6FA501D CRC64;
     AREKRKPQQP QRRPAGGTGQ RRGSGYSPSA DQQGAQDREE EAAAAPAPTS SGHRTEKRKR
     LQLQCQPAGG TGQRRGSGQG PSARPAAFTG QRRGSRSSPS ADQQRAQDRE EEAARPQRRP
     AAGTGQRRGS AAAPVPTSSG TGQRRGSAAA PAPTSSGTGQ RRGSEEMEEE G
//
ID   Q12814      PRELIMINARY;      PRT;    47 AA.
AC   Q12814;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   CEA FAMILY MEMBER (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Mclenachan P.A., Rutherfurd K.J., Beggs K.T., Sims S., Mansfield B.C.;
RL   Submitted (DEC-1993) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U04433; AAA18341.1; -.
FT   NON_TER       1      1
FT   NON_TER      47     47
**
**   #################     SOURCE SECTION     ##################
**   Human CEA family member gene, BI-like domain, partial cds.
**   [1]
**   1-384
**   McLenachan P.A., Rutherfurd K.J., Beggs K.T., Sims S.,
**   Mansfield B.C.;
**   "Characterization of the PSG11 Gene";
**   Unpublished.
**   [2]
**   1-384
**   Mansfield B.C.;
**   ;
**   Submitted (15-DEC-1993) to the EMBL/GenBank/DDBJ databases.
**   Brian C. Mansfield, Massey University, Microbiology and Genetics,
**   Palmerston North, New Zealand
**   NCBI gi: 436166
**   source          1..384
**                   /clone="C20.5"
**                   /clone_lib="CVOO1K"
**                   /organism="Homo sapiens"
**                   /cell_type="leukocyte"
**   CDS             <244..>384
**                   /standard_name="CEA family member gene, BI-like
**   domain"
**                   /note="NCBI gi: 436167"
**                   /codon_start=2
**                   /db_xref="PID:g436167"
**   CDS_1_OUT_OF_1
**   26-MAY-1994 (Rel. 39, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   47 AA;  5415 MW;  832E3F1A03BC0913 CRC64;
     WLGHPFTPVI SYELGANLRL FIHVASNPPS PYFWRVMETF CNTCKSS
//
ID   Q12833      PRELIMINARY;      PRT;    58 AA.
AC   Q12833;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   FIBROMODULIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=CARTILAGE;
RA   Sztrolovics R., Chen X.N., Grover J., Roughley P.J., Korenberg J.R.;
RL   Submitted (JAN-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U05291; AAA16153.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human fibromodulin mRNA, partial cds.
**   [1]
**   1-1892
**   Sztrolovics R., Chen X.N., Grover J., Roughley P.J.,
**   Korenberg J.R.;
**   "Localization of the human fibromodulin gene to chromosome 1q32
**   and completion of the cDNA sequence";
**   Unpublished.
**   [2]
**   1-1892
**   Sztrolovics R.;
**   ;
**   Submitted (12-JAN-1994) to the EMBL/GenBank/DDBJ databases.
**   Robert Sztrolovics, Department of Surgery, McGill University,
**   Genetics Unit, Shriners Hospital for Crippled Children, 1529 Cedar
**   Avenue, Montreal, Quebec, H3G 1A6, Canada
**   source          1..1892
**                   /isolate="patient A10/03/93"
**                   /clone="pHFM-3'UT"
**                   /clone_lib="PCR product"
**                   /organism="Homo sapiens"
**                   /sex="female"
**                   /cell_type="chondrocyte"
**                   /tissue_type="cartilage"
**                   /dev_stage="neonate"
**                   /map="1q32"
**                   /chromosome="1"
**   CDS             <1..178
**                   /note="Encodes only the most carboxy terminal 58 amino
**                   acids of fibromodulin"
**                   /product="fibromodulin"
**                   /codon_start=2
**                   /db_xref="PID:g450855"
**   CDS_1_OUT_OF_1
**   29-JAN-1994 (Rel. 38, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   58 AA;  6470 MW;  822B24A3ACDD4D80 CRC64;
     YLQGNRINEF SISSFCTVVD VVNFSKLQVL RLDGNEIKRS AMPADAPLCL RLASLIEI
//
ID   Q12914      PRELIMINARY;      PRT;  1692 AA.
AC   Q12914;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   G2 PROTEIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE OF 1-564 FROM N.A.
RC   TISSUE=KIDNEY;
RA   Foord O., Rose E.;
RL   Submitted (JUN-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U10991; AAA21253.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human G2 protein mRNA, partial cds.
**   [1]
**   1-1694
**   Foord O., Rose E.;
**   "Transposon-based mapping and sequencing of a novel transcript
**   within the WAGR region of human chromosome 11p13";
**   Unpublished.
**   [2]
**   1-6868
**   Foord O.;
**   ;
**   Submitted (17-JUN-1994) to the EMBL/GenBank/DDBJ databases.
**   Orit Foord, Advanced Center for Genetic Technology, Applied
**   Biosystems Division, Perkin-Elmer Corporation, 850 Lincoln Centre
**   Drive, Foster City, CA 94404, USA
**   NCBI gi: 533094
**   source          1..6868
**                   /clone="pG2-6.9"
**                   /clone_lib="lambda gt11 cDNA library from embroyonic
**                   kidney"
**                   /chromosome="11"
**                   /organism="Homo sapiens"
**                   /map="11p13"
**                   /tissue_type="kidney"
**                   /dev_stage="embryo"
**   CDS             <3..5081
**                   /note="NCBI gi: 533095"
**                   /codon_start=1
**                   /product="G2"
**                   /db_xref="PID:g533095"
**   CDS_1_OUT_OF_1
**   08-SEP-1994 (Rel. 40, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   1692 AA;  183170 MW;  C5A975AF1485B51A CRC64;
     IPEGRLSAEH TSSLVPSLHI TTLGQEQAIL SGAVPASPST GTADFPSILT FLQPTENHAS
     PSPVPEMPTL PAEGSDGSPP ATRDLLLSSK VPNLLSTSWT FPRWKKDSVT AILGKNEEAN
     VTIPLQGFPR KEVLSLHTVN GFVSDFSTGS VSSPIITAPR TNPLSSGPPL PSILSIQATQ
     TVFPSLLAFS STKPEVYAAA VDHSGLPASA PKQVRASPSS MDVYDSLTIG DMKKPATTDV
     FWSSLSAETG SLSTESIISG LQQQTNYDLN GHTISTTSWE THLAPTAPPN GLTSAADAIK
     SQDFKDTAGH SVTAEGFSIQ DLVLGTSIEQ PVQQSDMTMV GSHIDLWPTS NNNHSRDFQT
     AEVAYYSPTT RHSVSHPQLQ LPNQPAHPLL LTSPGPTSTG SLQEMLSDGT DTGSEISSDI
     NSSPERNAST PFQNILGYHS AAESSISTSV FPRTSSRVLR ASQHPKKWTA DTVSSKVQPT
     AAAAVTLFLR KSSPPALSAA LVAKGTSSSP LAVASGPAKS SSMTTLAKNV TNKAASGPKR
     TPGAVHTAFP FTPTYMYART GHTTSTHTAI ARKHGHCLWP VVYNLPPPGK PQAMHTGLPN
     PTNLEMPRAS TPRPLTVTAA LTSITASVKA TRLPPLRAEN TDAVLPAASA AVVTTGKMAS
     NLECQMSSKL LVKTVLFLTQ RRVQISESLK FSIAKGLTQA LRKAFHQNDV SAHVDILEYS
     HNVTVGYYAT KGKLVYLPAV VIEMLGVYGV SNVTADLKQH TPHLQSVAVL ASPWNPQPAG
     YFQLKTVLQF VSQADNIQSC KFAQTMEQRL QKAFQDAERK VLNTKSNLTI QIVSTSNASQ
     AVTLVYVVGN QSTFLNGTVA SSLLSQLSAE LVGFYLTYPP LTIAEPLEYP NLDISETTRD
     YWVITVLQGV DNSLVGLHNQ SFARVMEQRL AQLFMMSQQQ GRRFKRATTL GSYTVQMVKM
     QRVPGPKDPA ELTYYTLYNG KPLLGTVAAK ILSTIDSQRM ALTLHHVVLL QADPVVKNPP
     NNLWIIAAVL APIAVVTVII IIITAVLCRK NKNDFKPDTM INLPQRAKPV QGFDYAKQHL
     GQQGADEEVI PVTQETVVLP LPIRDAPQER DVAQDGSTIK TAKSTETRKS RSPSENGSVI
     SNESGKPSSG RRSPQNVMAQ QKVTKEEARK RNVPASDEEE GAVLFDNSSK VAAEPFDTSS
     GSVQLIAIKP TALPMVPPTS DRSQESSAVL NGEVNKALKQ KSDIEHYRNK LRLKAKRKGY
     YDFPAVETSK GLTERKKMYE KAPKEMEHVL DPDSELCAPF TESKNRQQMK NSVYRSRQSL
     NSPSPGETEM DLLVTRERPR RGIRNSGYDT EPEIIEETNI DRVPEPRGYS RSRQVKGHSE
     TSTLSSQPSI DEVRQQMHML LEEAFSLASA GHAGQSRHQE AYGSAQHLPY SEVVTSAPGT
     MTRPRAGVQW VPTYRPEMYQ YSLPRPAYRF SQLPEMVMGS PPPPVPPRTG PVAVASLRRS
     TSDIGSKTRM AESTGPEPAQ LHDSASFTQM SRGPVSVTQL DQSALNYSGN TVPAVFAIPA
     ANRPGFTGYF IPTPPSSYRN QAWMSYAGEN ELPSQWADSV PLPGYIEAYP RSRYPQSSPS
     RLPRQYSQPA NLHPSLEQAP APSTAASQQS LAENDPSDAP LTNISTAALV KAIREEVAKL
     AKKQTDMFEF QV
//
ID   Q12915      PRELIMINARY;      PRT;   204 AA.
AC   Q12915;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   IBD1 (FRAGMENT).
GN   IBD1.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=HEMATOPOIETIC;
RA   Appierto V., Pergolizzi R., Spurr N., Biunno I.;
RL   Submitted (JUN-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U11036; AAA67652.1; -.
FT   NON_TER       1      1
FT   NON_TER     204    204
**
**   #################     SOURCE SECTION     ##################
**   Human Ibd1 mRNA, partial cds.
**   [1]
**   1-613
**   Appierto V., Pergolizzi R., Spurr N., Biunno I.;
**   "Identification and chromosomal localization of two new genes";
**   Unpublished.
**   [2]
**   1-613
**   Biunno I.;
**   ;
**   Submitted (20-JUN-1994) to the EMBL/GenBank/DDBJ databases.
**   Ida Biunno, CNR, ITBA, Via Ampere 56, Milano, 20131, Italy
**   NCBI gi: 836882
**   source          1..613
**                   /clone_lib="Clontech T lymphocytes cDNA library"
**                   /organism="Homo sapiens"
**                   /cell_type="T-lymophocyte"
**                   /tissue_type="hematopoietic"
**                   /chromosome="18"
**   CDS             <1..>612
**                   /gene="Ibd1"
**                   /note="NCBI gi: 836883"
**                   /codon_start=1
**                   /function="unknown"
**                   /db_xref="PID:g836883"
**   CDS_1_OUT_OF_1
**   04-JUN-1995 (Rel. 44, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   204 AA;  22095 MW;  5ADAE93D3744E581 CRC64;
     VSGIRGAGSG CTRQTFPMAS VTRAVFGELP SGGGTVEKFQ LQSDLLRVDI ISWGCTITAL
     EVKDRQGRAS DVVLGFAELE GYLQKQPYFG AVIGRVANRI AKGTFKVDGK EYHLAITRNP
     TVCTGRSQQG FDKVLWTLGA VKCVQFSRIS PDGEEGYPGE LKVWVTYTLD GGELHSATTE
     HKPVQATPVN LTTILTSTWQ ATRI
//
ID   Q12925      PRELIMINARY;      PRT;   113 AA.
AC   Q12925;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   HYPOTHETICAL PROTEIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Lamartine J., Wang Q., Kaneko K., Tsuji S., Mattei M., Lenoir G.,
RA   Sylla B.S.;
RL   Submitted (JUL-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U12206; AAA62165.1; -.
KW   Hypothetical protein.
FT   NON_TER     113    113
**
**   #################     SOURCE SECTION     ##################
**   Human clone pL713 hypothetical protein mRNA, partial cds.
**   [1]
**   1-766
**   Lamartine J., Wang Q., Kaneko K., Tsuji S., Mattei M., Lenoir G.,
**   Sylla B.S.;
**   "Cloning, sequencing, and chromosomal assignment of a new cDNA
**   clone to Xq12-q13 and 14q11";
**   Unpublished.
**   [2]
**   1-766
**   Sylla B.S.;
**   ;
**   Submitted (12-JUL-1994) to the EMBL/GenBank/DDBJ databases.
**   Bakary S. Sylla, International Agency for Research on Cancer,
**   MCA/VHC, 150 Cours Albert Thomas, Lyon, 69372 Cedex O8, France
**   NCBI gi: 662789
**   source          1..766
**                   /clone="pL713"
**                   /clone_lib="human placental cDNA library"
**                   /organism="Homo sapiens"
**                   /chromosome="Xq12-q13 and 14q11"
**   CDS             428..>766
**                   /note="ORF1; NCBI gi: 662790"
**                   /codon_start=1
**                   /product="unknown"
**                   /db_xref="PID:g662790"
**   CDS_1_OUT_OF_1
**   04-MAR-1995 (Rel. 42, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   113 AA;  11385 MW;  F93B6A01C558A7B2 CRC64;
     MEPASAHLLL NDMIAGQHCL EVTIFICFST SFCSSFSFSA SSSISLTLDS SASGPQWRVP
     VTSTSPAPPP PLGCRGSRTS PGPGAPGGRG AGAAPLRARA PARAPAARPQ APP
//
ID   Q12928      PRELIMINARY;      PRT;    48 AA.
AC   Q12928;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   THROMBOSPONDIN-P50 (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Dimitry J.M., Sheibani N., Finn M., Boak B.M., Paul L.L.,
RA   Frazier W.A.;
RL   Submitted (JUL-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U12471; AAA21126.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human thrombospondin-1 gene, partial cds.
**   [1]
**   1-1966
**   Dimitry J.M., Sheibani N., Finn M., Boak B.M., Paul L.L.,
**   Frazier W.A.;
**   "Identification of a human thrombospondin-1 isoform generated by
**   alternative splicing";
**   Unpublished.
**   [2]
**   1-1966
**   Frazier W.A.;
**   ;
**   Submitted (20-JUL-1994) to the EMBL/GenBank/DDBJ databases.
**   William A. Frazier, Biochemistry and Molecular Biophysics,
**   Washington University Medical School, 660 South Euclid Avenue, St.
**   Louis, MO 63110, USA
**   NCBI gi: 532687
**   source          1..1966
**                   /chromosome="15"
**                   /map="15q21"
**                   /organism="Homo sapiens"
**                   /cell_type="leukocyte"
**   CDS             join(<1..31,961..1076)
**                   /note="NCBI gi: 532688"
**                   /codon_start=1
**                   /product="thrombospondin-p50"
**                   /db_xref="PID:g532688"
**   CDS_1_OUT_OF_2
**   08-SEP-1994 (Rel. 40, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   48 AA;  5132 MW;  728FC778BD079EDF CRC64;
     PDGECCPRCW PRCDCSRGAW ADGKETCCPL MTICAEKAPN AGVRGTTN
//
ID   Q13058      PRELIMINARY;      PRT;   109 AA.
AC   Q13058;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   HYPOTHETICAL PROTEIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=OVARY;
RA   Montagna M., Serova O., Sylla B.S., Feunten J., Lenoir G.M.;
RL   Submitted (DEC-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U18920; AAA69700.1; -.
KW   Hypothetical protein.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human chromosome 17q12-21 mRNA, clone pOV-3, partial cds.
**   [1]
**   Montagna M., Serova O., Sylla B.S., Feunten J., Lenoir G.M.;
**   "100 kb physical map around the EDH17B2 gene: identification of
**   three new genes and a pseudogene of a human homologue of the rat
**   PRL-1 tyrosine phosphatase";
**   Unpublished.
**   [2]
**   1-861
**   Lenoir G.M.;
**   ;
**   Submitted (20-DEC-1994) to the EMBL/GenBank/DDBJ databases.
**   Gilbert M. Lenoir, International Agency for Research on Cancer,
**   VHC/MCA, 150 Cours Albert Thomas, 69372 Lyon CEDEX 08, France
**   NCBI gi: 894179
**   source          1..861
**                   /clone="pOV-3"
**                   /clone_lib="Human ovarian cDNA library (Stratagene)"
**                   /organism="Homo sapiens"
**                   /sex="female"
**                   /tissue_type="ovary"
**                   /dev_stage="adult"
**                   /map="17q12-21"
**                   /chromosome="17"
**                   /note="similar to ESTs, GenBank Accession Numbers
**   Z38537
**                   and M62002"
**   CDS             <1..331
**                   /note="NCBI gi: 894180"
**                   /codon_start=2
**                   /product="unknown"
**                   /db_xref="PID:g894180"
**   CDS_1_OUT_OF_1
**   13-JUL-1995 (Rel. 44, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   109 AA;  11583 MW;  6440B7B6E992044F CRC64;
     GYLHRLSDPT LHSHPFLSPR SCPALHSTAG MLGTEALAAP QCTGLPSLGV GFFPGLAWAL
     PTSTPSGRGC RLMLFPDETL RSSPPPIMMS SVWDWGPLGS ACMPAWLRP
//
ID   Q13079      PRELIMINARY;      PRT;   344 AA.
AC   Q13079;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   VT4 PROTEIN (VT) (FRAGMENT).
GN   VT.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Tesmer V., Babin J., Rajadhyaksha A., Bina M.;
RL   Submitted (DEC-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U19346; AAA64188.1; -.
FT   NON_TER       1      1
FT   NON_TER     344    344
**
**   #################     SOURCE SECTION     ##################
**   Human VT4 protein (VT) mRNA, partial cds.
**   [1]
**   1-1032
**   Tesmer V., Babin J., Rajadhyaksha A., Bina M.;
**   ;
**   Unpublished.
**   [2]
**   1-1032
**   Bina M.;
**   ;
**   Submitted (30-DEC-1994) to the EMBL/GenBank/DDBJ databases.
**   Minou Bina, Department of Chemistry, Purdue University, 1393 Brown
**   Blg., W. Lafayette, IN 47907, USA
**   NCBI gi: 726043
**   source          1..1032
**                   /organism="Homo sapiens"
**                   /cell_line="Hela"
**   CDS             <1..>1032
**                   /gene="VT"
**                   /note="NCBI gi: 726044"
**                   /codon_start=1
**                   /product="VT4"
**                   /db_xref="PID:g726044"
**   CDS_1_OUT_OF_1
**   13-APR-1995 (Rel. 43, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   344 AA;  38166 MW;  D9BCC309953FB987 CRC64;
     RQPSSLPEGL PAPLEKRVKE LAQAARAAEG ESRQKFFTQD INGILLDIEA QTRELSSQVR
     SGVYAYLASF LPCSKDALLK RARKLHLYEQ GGRLKEPLQK LKEAIGRAMP EQMAKYQDEC
     QAHTQAKVAK MLEEEKDKEQ RDRICSDEEE DEEKGGRRIM GPRKKFQWND EIRELLCQVV
     KIKLESQDLE RNNKAQAWED CVKGFLDAEV KPLWPKGWMQ ARTLFKESRR GHGHLTSILA
     KKKVMAPSKI KVKESSTKPD KKVSVPSGQI GGPIALPSDH QTGGLSIGAS SRELPSQASG
     GLANPPPVNL EDSLDEDLIR NPASSVEAVS KELAALNSRA AGNS
//
ID   Q13116      PRELIMINARY;      PRT;   353 AA.
AC   Q13116;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   MEMBRANE PROTEIN-LIKE PROTEIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Van der Steege G., Draaijers T.G., Grootscholten P.M., Osinga J.,
RA   Anzevino R., Velona I., Brahe C., Scheffer H., Van Ommen G.J.B.,
RA   Buys C.H.C.M.;
RL   Eur. J. Hum. Genet. 0:0-0(0).
DR   EMBL; U21556; AAA69956.1; -.
FT   NON_TER       1      1
FT   NON_TER     353    353
**
**   #################     SOURCE SECTION     ##################
**   Human membrane protein-like protein mRNA, partial cds.
**   [1]
**   der Steege G., Draaijers T.G., Grootscholten P.M., Osinga J.,
**   Anzevino R., VELONA I., Brahe C., Scheffer H., van Ommen G.J.B.,
**   Buys C.H.C.M.;
**   "A provisional transcript map of the spinal muscular atrophy (SMA)
**   critical region";
**   Unpublished.
**   [2]
**   1-1263
**   der Steege G.;
**   ;
**   Submitted (23-FEB-1995) to the EMBL/GenBank/DDBJ databases.
**   Gerrit Van der Steege, University of Groningen, Department of
**   Medical Genetics, Antonius Deusinglaan 4, Groningen, 9713 AW, The
**   Netherlands
**   NCBI gi: 899492
**   source          1..1263
**                   /clone="5G5"
**                   /clone_lib="library of A. Bernards"
**                   /organism="Homo sapiens"
**                   /map="5q13.1 and 5p"
**                   /chromosome="5"
**                   /cell_type="pre-B-cells"
**   CDS             <204..>1263
**                   /note="similar to rat integral membrane glycoprotein,
**   PIR
**                   Accession Number A40670; NCBI gi: 899493"
**                   /codon_start=1
**                   /db_xref="PID:g899493"
**   CDS_1_OUT_OF_1
**   19-JUL-1995 (Rel. 44, Last updated, Version 3)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   353 AA;  36947 MW;  431ADAB38FD2CA6E CRC64;
     SQNGPRGHRG CTVCILALRX NGGLSPFVPR PGPLQTDLHA QSSEIRYNQT SQTSWTSSST
     KRNAISSSYS STGGLPGLKQ RRGPASSRCQ LTLSYSKTVS EDGPQAVSSR HTRCEKADTA
     PGQTLAPRGG SPRSQASRPR RRKIPLLPRR RGEPLMLPPP LELGYRVTAE DLHLEKETAL
     QRINSALHVE DKATSDCRPS RPSHTLSSLA TGASGGPPVS KAPTMDAQPD KLKSQDCLGL
     VAALASATEV SSTAPMSGKK HRPPGPLFSS SDPLPATSSH SQDSAQVTSM IPAPFTAASR
     DASMRRTRPG TSAPAXAAAA PPPSTLNPTS GSLLNAVDEG PSHFLASATA AAR
//
ID   Q13119      PRELIMINARY;      PRT;   293 AA.
AC   Q13119;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   DUPLICATE SPINAL MUSCULAR ATROPHY (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Van der Steege G., Draaijers T.G., Grootscholten P.M., Osinga J.,
RA   Anzevino R., Velona I., Brahe C., Scheffer H., Van Ommen G.J.B.,
RA   Buys C.H.C.M.;
RL   Eur. J. Hum. Genet. 0:0-0(0).
DR   EMBL; U21914; AAA64505.1; -.
DR   INTERPRO; IPR002999; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human duplicate spinal muscular atrophy mRNA, clone 5G7, partial
**   cds.
**   [1]
**   der Steege G., Draaijers T.G., Grootscholten P.M., Osinga J.,
**   Anzevino R., VELONA I., Brahe C., Scheffer H., van Ommen G.J.B.,
**   Buys C.H.C.M.;
**   "A provisional transcript map of the spinal muscular atrophy (SMA)
**   critical region";
**   Unpublished.
**   [2]
**   1-1466
**   der Steege G.;
**   ;
**   Submitted (28-FEB-1995) to the EMBL/GenBank/DDBJ databases.
**   Gerrit Van der Steege, University of Groningen, Department of
**   Medical Genetics, Antonius Deusinglaan 4, Groningen, 9713 AW, The
**   Netherlands
**   NCBI gi: 736410
**   source          1..1466
**                   /clone="5G7"
**                   /clone_lib="library of A. Bernards"
**                   /organism="Homo sapiens"
**                   /map="5q13.1"
**                   /chromosome="5"
**                   /cell_type="pre-B-cells"
**   CDS             <1..882
**                   /note="putative open reading frame; duplicate of the
**                   functional spinal muscular atrophy gene, cDNA clone
**   BCD514,
**                   GenBank Accession Number U18423; it is not known if
**   this
**                   copy of the gene is actually translated; NCBI gi:
**   736411"
**                   /codon_start=1
**                   /db_xref="PID:g736411"
**   CDS_1_OUT_OF_1
**   23-MAY-1995 (Rel. 43, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PROSITE; PS50304; TUDOR; 90; 150; T; 19-JUN-2000;
SQ   SEQUENCE   293 AA;  31718 MW;  FEF4B81E12040F24 CRC64;
     AMSSGGSGGG VPEQEDSVLF RRGTGQSDDS DIWDDTALIK AYDKAVASFK HALKNGDICE
     TSGKPKTTPK RKPAKKNKSQ KKNTAASLQQ WKVGDKCSAI WSEDGCIYPA TIASIDFKRE
     TCVVVYTGYG NREEQNLSDL LSPICEVANN IEQNAQENEN ESQVSTDESE NSRSPGNKSD
     NIKPKSAPWN SFLPPPPPMP GPRLGPGKPG LKFNGPPPPP PPPPPHLLSC WLPPFPSGPP
     IIPPPPPICP DSLDDADALG SMLISWYMSG YHTGYYMGFR QNQKEGRCSH SLN
//
ID   Q13222      PRELIMINARY;      PRT;   203 AA.
AC   Q13222;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1999 (TrEMBLrel. 12, Last annotation update)
DE   GATA-4 (FRAGMENT).
GN   GATA-4.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Wood S., Yaremko M.L., Schertzer M.;
RL   Submitted (JUN-1995) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U28835; AAA70335.1; -.
DR   HSSP; P04002; 1WFA.
FT   NON_TER     203    203
**
**   #################     SOURCE SECTION     ##################
**   Human GATA-4 gene, partial cds.
**   [1]
**   1-855
**   Wood S., Yaremko M.L., Schertzer M.;
**   "Localization of human GATA4 to 8p23 maps chromosomal breakpoints
**   disrupting synteny between human 8p and mouse 14 to the Clu-Gata4
**   interval";
**   Unpublished.
**   [2]
**   1-855
**   Wood S.;
**   ;
**   Submitted (09-JUN-1995) to the EMBL/GenBank/DDBJ databases.
**   Stephen Wood, Medical Genetics, University of British Columbia,
**   6174 University Blvd., Vancouver, B.C. V6T 1Z3, Canada
**   NCBI gi: 903937
**   source          1..855
**                   /organism="Homo sapiens"
**                   /clone_lib="LA08NC01, Wood et al. Cytogenet. Cell
**   Genet.
**                   59:243-247, 1992"
**                   /clone="161F5"
**                   /chromosome="8"
**                   /map="8p"
**   CDS             61..>669
**                   /gene="GATA-4"
**                   /note="similar to human GATA-4 cDNA sequence, GenBank
**                   Accession Number L34357; NCBI gi: 903938"
**                   /codon_start=1
**                   /db_xref="PID:g903938"
**   CDS_1_OUT_OF_1
**   26-JUL-1995 (Rel. 44, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   203 AA;  20157 MW;  7C238D931655F208 CRC64;
     MYQSLPWPPT TGRPPVPTRR AAPAPSCXXX APRPRQSTCP PAGALLRAGP VLPPGRRRGL
     CVRRRLGRSS GGAASGAGPG TQQGSPGWSQ AGADGAAYTP PPVSPRFSFP GPTGSLAAAA
     AAAAAREAAA YSSGGGAAGA GLAGREQYGR AXFAGSYSSX YPAYMADVGA SWAAAAAASA
     GPFDSPVLHS LPGRANPXXH PNL
//
ID   Q13306      PRELIMINARY;      PRT;    71 AA.
AC   Q13306;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   THYROID TRANSCRIPTION FACTOR-1 (FRAGMENT).
GN   TTF-1.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Hamdan H., Liu H., Jones C., Delemos R., Minoo P.;
RL   Submitted (AUG-1995) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U33627; AAA83233.1; -.
FT   NON_TER      71     71
**
**   #################     SOURCE SECTION     ##################
**   Human thyroid transcription factor-1 (TTF-1) gene, partial cds.
**   [1]
**   1-1062
**   Hamdan H., Liu H., Jones C., deLemos R., Minoo P.;
**   "Characterization of TTF-1 transcripts in human lung identifies an
**   additional exon";
**   Unpublished.
**   [2]
**   1-1062
**   Hamdan H.;
**   ;
**   Submitted (10-AUG-1995) to the EMBL/GenBank/DDBJ databases.
**   Hasnah Hamdan, University of Southern California, Pediatrics,
**   Division of Basic Research, 1801 E. Marengo, Rm 1G-1, Los Angeles,
**   CA 90033, USA
**   NCBI gi: 1113816
**   source          1..1062
**                   /clone="pHGX4"
**                   /clone_lib="Lambda EMBLT3 SP6/T7 library of Clontech,
**   Palo
**                   Alto, CA."
**                   /organism="Homo sapiens"
**                   /cell_type="leukocyte"
**   CDS             join(164..240,927..>1062)
**                   /gene="TTF-1"
**                   /codon_start=1
**                   /product="thyroid transcription factor-1"
**                   /db_xref="PID:g1113817"
**   CDS_1_OUT_OF_1
**   13-DEC-1995 (Rel. 46, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   71 AA;  7425 MW;  7ABFD04CD963A711 CRC64;
     MWSGGSGKAR GWEAAAGGRS SPGRLSRRRI MSMSPKHTTP FSVSDILSPL EESYKKVGME
     GGGLGAPLAA Y
//
ID   Q13548      PRELIMINARY;      PRT;    36 AA.
AC   Q13548;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   P38-2G4 (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Greco N.J., Rao P., Modeli R., Jamieson G.A.;
RL   Submitted (FEB-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U50137; AAA91484.1; -.
FT   NON_TER      36     36
**
**   #################     SOURCE SECTION     ##################
**   Human p38-2G4 mRNA, partial cds.
**   [1]
**   1-172
**   Greco N.J., Rao P., Modeli R., Jamieson G.A.;
**   ;
**   Submitted (27-FEB-1996) to the EMBL/GenBank/DDBJ databases.
**   Nicholas J. Greco, Platelet Biology, American Red Cross, 15601
**   Crabbs Branch Way, Rockville, MD 20855, USA
**   NCBI gi: 1216525
**   source          1..172
**                   /organism="Homo sapiens"
**                   /cell_type="platelets and CMK 11-5 cells"
**   CDS             65..>172
**                   /note="similar to Mus musculus p38-2G4 encoded by
**   GenBank
**                   Accession Number X84789; NCBI gi: 1216526"
**                   /codon_start=1
**                   /product="p38-2G4"
**                   /db_xref="PID:g1216526"
**   CDS_1_OUT_OF_1
**   08-MAR-1996 (Rel. 47, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   36 AA;  4117 MW;  7C2D4C3BCE49FC53 CRC64;
     MIMEETGKIF KKEKEMKKGI AFPTSISVNN CVCHFP
//
ID   Q13552      PRELIMINARY;      PRT;    31 AA.
AC   Q13552;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   SUPPRESSOR ELEMENT ISHMAEL UPPER CP1 (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=MAMMARY;
RA   DeBlasio T.R., Moasser M.M., Mendelsohn J.;
RL   Submitted (FEB-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U50277; AAA92144.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human breast cancer suppressor element Ishmael Upper CP1 mRNA,
**   partial cds.
**   [1]
**   1-132
**   DeBlasio T.R., Moasser M.M., Mendelsohn J.;
**   ;
**   Submitted (29-FEB-1996) to the EMBL/GenBank/DDBJ databases.
**   Tony R. DeBlasio, Medicine, Memorial Sloan-Kettering Cancer Center,
**   430 E. 67 St. RRL 727, New York City, N.Y. 10021, USA
**   NCBI gi: 1224126
**   source          1..132
**                   /organism="Homo sapiens"
**                   /clone="Ishmael"
**                   /cell_type="epithelial"
**                   /tissue_type="mammary"
**                   /cell_line="MCF-7"
**   CDS             <1..96
**                   /codon_start=1
**                   /product="suppressor element Ishmael Upper CP1"
**                   /db_xref="PID:g1224127"
**   CDS_1_OUT_OF_1
**   14-MAR-1996 (Rel. 47, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   31 AA;  4015 MW;  45566C06AF4812E1 CRC64;
     YLKEGDKKYV WHRLGRKREL SDHFLKWKIQ R
//
ID   Q13559      PRELIMINARY;      PRT;    34 AA.
AC   Q13559;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   BREAST CANCER SUPPRESSOR ELEMENT ISHMAEL UPPER RP2 (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=MAMMARY;
RA   DeBlasio T.R., Moasser M.M., Mendelsohn J.;
RL   Submitted (MAR-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U50403; AAA92145.1; -.
FT   NON_TER       1      1
FT   NON_TER      34     34
**
**   #################     SOURCE SECTION     ##################
**   Human breast cancer suppressor element Ishmael Upper RP2 mRNA,
**   partial cds.
**   [1]
**   1-103
**   DeBlasio T.R., Moasser M.M., Mendelsohn J.;
**   ;
**   Submitted (01-MAR-1996) to the EMBL/GenBank/DDBJ databases.
**   Tony R. DeBlasio, Medicine, Memorial Sloan-Kettering Cancer Center,
**   430 E. 67 St. RRL 727, New York City, N.Y. 10021, USA
**   NCBI gi: 1224130
**   source          1..103
**                   /organism="Homo sapiens"
**                   /cell_line="MCF-7 human mammary"
**                   /tissue_type="mammary"
**                   /cell_type="epithelial"
**                   /clone="Ishmael"
**   CDS             <1..>103
**                   /codon_start=3
**                   /product="breast cancer suppressor element Ishmael
**   Upper
**                   RP2"
**                   /db_xref="PID:g1224131"
**   CDS_1_OUT_OF_1
**   14-MAR-1996 (Rel. 47, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   34 AA;  3726 MW;  EB286FD9128FBBA8 CRC64;
     LLWSTLVSWG CWLTPVIPTL WEAKAGGSPE PRSS
//
ID   Q13581      PRELIMINARY;      PRT;    84 AA.
AC   Q13581;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   KR-ZNF2 (FRAGMENT).
GN   KR-ZNF2.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Sancho E., Gonzalez-Lamuno D., Thomson T.;
RL   Submitted (MAR-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U52097; AAA97913.1; -.
FT   NON_TER       1      1
FT   NON_TER      84     84
**
**   #################     SOURCE SECTION     ##################
**   Human zinc finger protein (kr-znf2) mRNA, partial cds.
**   [1]
**   1-253
**   Sancho E., Gonzalez-Lamuno D., Thomson T.;
**   "Three novel Zn finger proteins";
**   Unpublished.
**   [2]
**   1-253
**   Sancho E.;
**   ;
**   Submitted (22-MAR-1996) to the EMBL/GenBank/DDBJ databases.
**   Elena Sancho, Biologia Celular y Molecular, Instituto Municipal de
**   Investigacion Medica, c/ Dr Aiguader 80, Barcelona 08003, Spain
**   NCBI gi: 1277184
**   source          1..253
**                   /organism="Homo sapiens"
**                   /cell_type="HT-29 colon cancer cells"
**   CDS             <1..>253
**                   /gene="kr-znf2"
**                   /note="zinc finger protein; NCBI gi: 1277185"
**                   /codon_start=1
**                   /product="KR-ZNF2"
**                   /db_xref="PID:g1277185"
**   CDS_1_OUT_OF_1
**   26-APR-1996 (Rel. 47, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   84 AA;  9733 MW;  FAD9F3A6EEE965F6 CRC64;
     PVLSTSPPNL FKKIIYTGEK FYKCEEYGKA FNQSSTLTRI KNSYCTETLT RVKNMAKPLT
     SPQFLTDITQ YTLERNTRQT TKDL
//
ID   Q13599      PRELIMINARY;      PRT;    58 AA.
AC   Q13599;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   TRANSCRIPTION ELONGATION INHIBITOR PVHL (FRAGMENT).
GN   VHL.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=CLEAR-CELL RENAL CELL CARCINOMA;
RA   Wenzel M.;
RL   Submitted (APR-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U54612; AAA98614.1; -.
DR   INTERPRO; IPR002714; -.
DR   PFAM; PF01847; VHL; 1.
FT   NON_TER       1      1
FT   NON_TER      58     58
**
**   #################     SOURCE SECTION     ##################
**   Human von Hippel-Lindau tumor suppressor (vhl) gene, exon 3,
**   partial cds.
**   [1]
**   1-176
**   Wenzel M.;
**   ;
**   Submitted (10-APR-1996) to the EMBL/GenBank/DDBJ databases.
**   Michael Wenzel, KMS-registry, University of Duesseldorf, Moorenstr.
**   5, Duesseldorf 40225, FRG
**   NCBI gi: 1293146
**   source          1..176
**                   /organism="Homo sapiens"
**                   /tissue_type="clear-cell renal cell carcinoma"
**                   /chromosome="3"
**                   /map="3p25-26"
**   CDS             <1..>176
**                   /gene="vhl"
**                   /note="Description: von Hippel-Lindau tumor
**   suppressor;
**                   transcription elongation inhibitor; NCBI gi: 1293147"
**                   /codon_start=3
**                   /product="pVHL"
**                   /db_xref="PID:g1293147"
**   CDS_1_OUT_OF_1
**   04-MAY-1996 (Rel. 47, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF01847; VHL; 1; 58; T; 19-JUN-2000;
SQ   SEQUENCE   58 AA;  7051 MW;  2AA043F064D06238 CRC64;
     YTLKERCPQV VRSLVKPENY RRLDIVRSLY EDLEDHPNVQ KDLERLTQER IAHQRMGD
//
ID   Q13600      PRELIMINARY;      PRT;   350 AA.
AC   Q13600;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   TOPOISOMERASE IIB (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Yuwen H.;
RL   Submitted (APR-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U54831; AAB01982.1; -.
KW   Isomerase.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human topoisomerase IIb mRNA, partial cds.
**   [1]
**   1-1388
**   Yuwen H.;
**   "Binding of wild-type p53 by topoisomerase II and overexpression
**   of topoisomerase II in human hepatocellular carcinoma";
**   Unpublished.
**   [2]
**   1-1388
**   Yuwen H.;
**   ;
**   Submitted (12-APR-1996) to the EMBL/GenBank/DDBJ databases.
**   H. Yuwen, CBER/DTTD/LH HFM-310, FDA, 1401 Rockville Pike,
**   Rockville, MD 20852-1448, USA
**   source          1..1388
**                   /organism="Homo sapiens"
**   CDS             <1..1053
**                   /note="p53 binding protein"
**                   /codon_start=1
**                   /product="topoisomerase IIb"
**                   /db_xref="PID:g1354507"
**   CDS_1_OUT_OF_1
**   08-JUN-1996 (Rel. 48, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   350 AA;  38567 MW;  55E6A5693B1C16CC CRC64;
     EFSGAPVEGA GEEALTPSVP INKGPKPKRE KKEPGTRVRK TPTSSGKPSA KKVKKRNPWS
     DDESKSESDL EETEPVVIPR DSLLRRAAAE RPKYTFDFSE EEDDDADDDD DDNNDLEELK
     VKASPITNDG EDEFVPSDGL DKDEYTFSPG KSKATPEKSL HDKKSQDFGN LFSFPSYSQK
     SEDDSAKFDS NEEDSASVFS PSFGLKQTDK VPSKTVAAKK GKPSSDTVPK PKRAPKQKKV
     VEAVNSDSDS EFGIPKKTTT PKGKGRGAKK RKASGSENEG DYNPGRKTSK TTSKKPKKTS
     FDQDSDVDIF PSDFPTEPPS LPRTGRARKE VKYFAESDEE EDDVDFAMFN
//
ID   Q13681      PRELIMINARY;      PRT;   146 AA.
AC   Q13681;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   ALPHA 3 TYPE IX COLLAGEN (FRAGMENT).
GN   HUMCOL9A3.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=HYALINE CARTILAGE;
RX   MEDLINE=97293217; PubMed=9164858;
RA   Peraelae M., Savontaus M., Metsaeranta M., Vuorio E.;
RT   "Developmental regulation of mRNA species for types II, IX and XI
RT   collagens during mouse embryogenesis.";
RL   Biochem. J. 324:209-216(1997).
DR   EMBL; X91013; CAA62495.1; -.
DR   INTERPRO; IPR000087; -.
DR   PFAM; PF01391; Collagen; 2.
KW   Extracellular matrix.
FT   NON_TER       1      1
FT   NON_TER     146    146
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for alpha 3 type IX collagen
**   [1]
**   Peraelae M.;
**   ;
**   Unpublished.
**   [2]
**   1-438
**   Peraelae M.P.;
**   ;
**   Submitted (21-AUG-1995) to the EMBL/GenBank/DDBJ databases.
**   M.P. Peraelae, Dept. of Medical Biochemistry, Univ. of Turku,
**   Kiinamyllynkatu 10, 20520 Turku, FINLAND
**   source          1..438
**                   /organism="Homo sapiens"
**                   /dev_stage="fetal"
**                   /tissue_type="hyaline cartilage"
**   CDS             <1..>438
**                   /partial
**                   /codon_start=1
**                   /gene="humcol9a3"
**                   /product="alpha 3 type IX collagen"
**                   /db_xref="PID:g975657"
**   misc_feature    1..87
**                   /note="NC2 domain"
**                   AA 1 -> 29
**   misc_feature    88..423
**                   /note="COL1 domain"
**                   AA 30 -> 141
**   misc_feature    424..438
**                   /note="NC1 domain"
**                   AA 142 -> 146
**   CDS_1_OUT_OF_1
**   05-SEP-1995 (Rel. 45, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF01391; Collagen; 28; 86; T; 19-JUN-2000;
**PM PFAM; PF01391; Collagen; 87; 144; T; 19-JUN-2000;
**PM PROSITE; PS50288; COLLAGEN_REP; 29; 87; T; 19-JUN-2000;
**PM PROSITE; PS50288; COLLAGEN_REP; 112; 139; T; 19-JUN-2000;
SQ   SEQUENCE   146 AA;  13913 MW;  99F402AF2683D517 CRC64;
     EQHIRELCGG MISEQIAQLA AHLRKPLAPG SIGRPGPAGP PGPPGPPGSI GHPGTRGPPG
     YRGPTGELGD PGPRGNQGDR GDKGAAGAGL DGPEGDQGPQ GPQGVPGTSK DGQDGAPGEP
     GPPGDPGLPG AIGAQGTPGI CDTSAC
//
ID   Q13727      PRELIMINARY;      PRT;   534 AA.
AC   Q13727;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   AHNAK-RELATED PROTEIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=CERVIX;
RA   Kilwinski J.;
RL   Submitted (AUG-1993) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X74818; CAA52817.1; -.
FT   NON_TER       1      1
FT   NON_TER     534    534
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA of AHNAK-related sequence
**   [1]
**   1-1601
**   Kilwinski J.;
**   ;
**   Submitted (11-AUG-1993) to the EMBL/GenBank/DDBJ databases.
**   J. Kilwinski, Univ. Konstanz, Fakultaet fuer Biologie, D-78434
**   Konstanz, FRG
**   [2]
**   Kilwinski J.;
**   ;
**   Unpublished.
**   source          1..1601
**                   /organism="Homo sapiens"
**                   /cell_line="HeLa S3"
**                   /clone_lib="lambda gt11"
**                   /clone="MB 41"
**                   /dev_stage="carcinoma"
**                   /haplotype="aneuploid"
**                   /tissue_type="cervix"
**                   /cell_type="epithelial"
**   CDS             <1..>1601
**                   /codon_start=1
**                   /product="AHNAK-related protein"
**                   /db_xref="PID:g535177"
**   CDS_1_OUT_OF_1
**   03-SEP-1994 (Rel. 40, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   534 AA;  57740 MW;  D9E5251D1AF639D7 CRC64;
     KGPKFKMPDL HLKAPKISMP EVDLNLKGPK MKGDVDVSLP KVEGDLKGPE VDVKGPKVDI
     DVPDVDVQGP DWHLKMPKVK MPKFSMPGFK GEGPDVDVNL PKADLDVSGP KVDIDVPDVN
     IEGPDAKLKG PKFKMPEMNI KAPKISMPDF DLHLKGPKVK GDVDVSLPKV EGDLKGPEVD
     IKGPKVDIDA PDVDVHGPDW HLKMPKVKMP KFSMPGFKGE GPDVDVTLPK ADIEISGPKV
     DIDAPDVSIE GPDAKLKGPK FKMPEMNIKA PKISMPDIDF NLKGPKVKGD VDVSLPKVEG
     DLKGPEIDIK GPSLDIDTPD VNIEGPEGKL KGPKFKMPEM NIKAPKISMP DFDLHLKGPK
     VKGDVDVSLP KVESDLKGPE VDIEGPEGKL KGPKFKMPDV HFKSPQISMS DIDLNLKGPK
     IKGDMDISVP KLEGDLKGPK VDVKGPKVGI DTPDIDIHGP EGKLKGPKFK MPDLHLKAPK
     ISMPEVDLNL KGPKVKGDMD ISLPKVEGDL KGPEVDIRDP KVDIDVPDVD VQGP
//
ID   Q13791      PRELIMINARY;      PRT;    40 AA.
AC   Q13791;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1999 (TrEMBLrel. 12, Last annotation update)
DE   APOLIPOPROTEIN E3 (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Bohnet K.;
RL   Submitted (OCT-1995) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X92000; CAA63051.1; -.
DR   HSSP; P02649; 1LE2.
KW   Lipoprotein.
FT   VARIANT      20     20       C -> G.
FT   NON_TER       1      1
FT   NON_TER      40     40
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens apolipoprotein E3 gene
**   [1]
**   Bohnet K.;
**   ;
**   Unpublished.
**   [2]
**   1-121
**   Bohnet K.;
**   ;
**   Submitted (05-OCT-1995) to the EMBL/GenBank/DDBJ databases.
**   K. Bohnet, Centre de Medecine Preventive, 2 Ave. Doyen Jacques
**   Parisot, F-54501, Vandoeuvre les Nancy, CEDEX, FRANCE
**   Related sequences: M10065,
**   SC.Rall Jr, J. Biol. Chem., 257, 4171-4178, 1982
**   P.de Knijff et al, Hum. Mutat.,4, 178-194, 1994
**   source          1..121
**                   /organism="Homo sapiens"
**                   /cell_type="leucocytes"
**                   /chromosome="19"
**                   /map="q13.2"
**   CDS             <1..>121
**                   /partial
**                   /codon_start=1
**                   /product="apolipoprotein E3"
**                   /db_xref="PID:e205282"
**   variation       replace(58,"g")
**                   /note="point mutation"
**   CDS_1_OUT_OF_1
**   01-AUG-1996 (Rel. 48, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   40 AA;  4697 MW;  B7F2E3E935CDA52C CRC64;
     QYRGEVQAML GQSTEELRVC LASHLRKLRK RLLRDADDLQ
//
ID   Q13830      PRELIMINARY;      PRT;   144 AA.
AC   Q13830;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   PREGNANCY-SPECIFIC BETA-1 GLYCOPROTEIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Bocco J.;
RL   Submitted (NOV-1991) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X63203; CAA44885.1; -.
KW   Pregnancy.
FT   NON_TER     144    144
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens gene for pregnancy specific beta-1 glycoprotein
**   [1]
**   1-3036
**   Bocco J.;
**   ;
**   Submitted (05-NOV-1991) to the EMBL/GenBank/DDBJ databases.
**   J. Bocco, LGME du CNRS - INSERM U184, 11 Rue Humann, Strasbourg,
**   67085 Cedex, FRANCE
**   [2]
**   1-3036
**   Bocco J.;
**   "Nucleotide sequence of a pregnancy-specific beta-1 glycopotein
**   gene family member";
**   Unpublished.
**   CPGISLE; HSB1GP; Release 3.0.
**   source          1..3036
**                   /organism="Homo sapiens"
**                   /dev_stage="embryo"
**                   /tissue_type="placenta"
**                   /cell_type="trophoblast"
**                   /sex="Female"
**   CDS             join(1191..1254,2492..>2858)
**                   /product="pregnancy-specific beta-1 glycoprotein"
**                   /partial
**                   /db_xref="PID:g29196"
**   CDS_1_OUT_OF_1
**   07-MAY-1992 (Rel. 31, Last updated, Version 3)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   144 AA;  16193 MW;  6871647785F0C63C CRC64;
     MGPLSAPPCT QHITWKGVLL TASLLNFWNL PITAQVTIEA LPPKVSEGKD VLLLVHNLPQ
     NLAGYIWYKG QLMDLYHYIT SYVVDGQINI YGPAYTGRET VYSNASLLIQ NVTREDAGSY
     TLHIIKRGDR TRGVTGYFTF NLYR
//
ID   Q13831      PRELIMINARY;      PRT;    93 AA.
AC   Q13831;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   B1 MUCIN-LIKE ANTIGEN (FRAGMENT).
GN   B1.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=KIDNEY;
RA   Walter A.O., Dippold W.;
RL   Submitted (FEB-1995) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X84838; CAA59277.1; -.
FT   NON_TER      93     93
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens B1 mRNA for mucin-like antigen
**   [1]
**   Walter A.O., Dippold W.;
**   "Cloning and expression of a novel mucin-like antigen";
**   Unpublished.
**   [2]
**   1-854
**   Walter A.O.;
**   ;
**   Submitted (15-FEB-1995) to the EMBL/GenBank/DDBJ databases.
**   A.O. Walter, Medical Clinic, University of Mainz, Obere
**   Zahlbacherstr. 63, 55101 Mainz, FRG
**   Related sequence: X83412 (C-terminus)
**   source          1..854
**                   /organism="Homo sapiens"
**                   /tissue_type="kidney"
**                   /clone_lib="lambda MAX1"
**                   /chromosome="7"
**   CDS             577..>854
**                   /partial
**                   /gene="B1"
**                   /db_xref="PID:g974822"
**   CDS_1_OUT_OF_1
**   01-SEP-1995 (Rel. 45, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   93 AA;  10471 MW;  85A604F02DE1BBCE CRC64;
     MKPRQKEQDT RLRKLRESSE GDQWLENEKT KPLRPQQQPQ RRPAGGTGQR RGSGSSPSAD
     QQRAQDREEE AAAAPVPNQQ RAQDREEEAA AAP
//
ID   Q13981      PRELIMINARY;      PRT;    86 AA.
AC   Q13981;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   CARCINOEMBRYONIC ANTIGEN SUBDOMAIN B (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTAL;
RA   Oikawa S.;
RL   Submitted (SEP-1989) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X16454; CAA34473.1; -.
KW   Cell adhesion.
FT   NON_TER      86     86
**
**   #################     SOURCE SECTION     ##################
**   Human gene for carcinoembryonic antigen subdomains A and B.
**   [1]
**   1-1092
**   Oikawa S.;
**   ;
**   Submitted (02-SEP-1989) to the EMBL/GenBank/DDBJ databases.
**   Oikawa S., Suntory Institute for Biomedical Research, 1-1-1
**   Wakayamadai, Shimamoto-cho, Mishima-gun Osaka 618, Japan.
**   source          1..1092
**                   /organism="Homo sapiens"
**                   /clone="36"
**                   /tissue_type="placental"
**                   /clone_lib="genomic"
**   CDS             715..973
**                   /product="carcinoembryonic antigen subdomain B"
**                   /partial
**                   /db_xref="PID:g296641"
**   CDS_1_OUT_OF_1
**   24-APR-1993 (Rel. 35, Last updated, Version 5)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   86 AA;  9321 MW;  5C76C55FA3E00840 CRC64;
     MDRAFPPFPP QTPITIQGQI STSPATWPLT HLHSTPGLSV ECSSNTHKSS LSPTSQWIRV
     DPMLASPITQ PLASVQSQSR QSQSLL
//
ID   Q14083      PRELIMINARY;      PRT;   136 AA.
AC   Q14083;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   C219-REACTIVE PEPTIDE (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=LEUKEMIA CELL;
RA   Norris M.D., Gilbert J., Madafiglio J., Haber M.;
RL   Gene 0:0-0(0).
DR   EMBL; L34688; AAB00324.1; -.
FT   NON_TER       1      1
FT   NON_TER     136    136
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone D320) C219-reactive peptide mRNA, partial cds.
**   [1]
**   1-409
**   Norris M.D., Gilbert J., Madafiglio J., Haber M.;
**   "Analysis of a novel cDNA encoding a C219-reactive peptide
**   isolated from methotrexate-selected multidrug-resistant human
**   leukemic cells";
**   Unpublished.
**   source          1..409
**                   /organism="Homo sapiens"
**                   /cell_line="CCRF-CEM"
**                   /cell_type="T-cell"
**                   /tissue_type="leukemia cell"
**                   /tissue_lib="lambda gt11"
**   CDS             <1..>409
**                   /codon_start=3
**                   /product="C219-reactive peptide"
**                   /db_xref="PID:g511639"
**   CDS_1_OUT_OF_1
**   21-MAY-1996 (Rel. 47, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   136 AA;  15343 MW;  98DAFBC8F58C28E1 CRC64;
     ENKKSIEKLK DVISMNASEF SEVQIALNEA KLSEEKVKSE CHRVQEENAR LKKKKEQLQQ
     EIEDWSKLHA ELSEQIKSFE KSQKDLEVAL THKDDNINAL TNCITQLNLL ECESESEGQN
     KGGNDSDELA NGEVGG
//
ID   Q14402      PRELIMINARY;      PRT;    25 AA.
AC   Q14402;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   GAMMA-G GLOBIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=LIVER;
RA   Vladimir V., Kavsan V.M.;
RL   Submitted (SEP-1990) to the EMBL/GenBank/DDBJ databases.
RN   [2]
RP   SEQUENCE FROM N.A.
RC   TISSUE=LIVER;
RA   Dmitrenko V.V., Kavsan V.M.;
RL   Submitted (MAY-1992) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X55657; CAA39190.1; -.
DR   INTERPRO; IPR000971; -.
DR   PFAM; PF00042; globin; 1.
DR   PROSITE; PS01033; GLOBIN; 1.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for gamma-G globin (clone pHG18)
**   [1]
**   1-166
**   Vladimir V., Kavsan V.M.;
**   ;
**   Submitted (18-SEP-1990) to the EMBL/GenBank/DDBJ databases.
**   Vladimir V., Kavsan V.M., Institute of Molecular Biology and
**   Genetics Ukr.SSR Acad. of Sci., Zabolotnogo str. 150, Kiev 252627,
**   USSR.
**   [2]
**   Dmitrenko V.V., Kavsan V.M.;
**   "Nucleotide sequence of mitochondrial cytochrome C oxydase II from
**   human fetal liver";
**   Unpublished.
**   source          1..166
**                   /organism="Homo sapiens"
**                   /dev_stage="embryonal"
**                   /tissue_type="liver"
**                   /cell_type="hepatocytes"
**   CDS             <1..80
**                   /product="gamma-G globin"
**                   /codon_start=3
**                   /db_xref="PID:g31723"
**   CDS_1_OUT_OF_1
**   11-MAY-1992 (Rel. 31, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00042; globin; 1; 25; T; 19-JUN-2000;
**PM PROSITE; PS01033; GLOBIN; 1; 25; T; 19-JUN-2000;
SQ   SEQUENCE   25 AA;  2781 MW;  C7BE9A334CD66D91 CRC64;
     FTPEVQASWQ KMVTGVASAL SSRYH
//
ID   Q14441      PRELIMINARY;      PRT;    52 AA.
AC   Q14441;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   GLYCOPROTEIN IB ALPHA (FRAGMENT).
GN   GPIB.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Ishida F.;
RL   Submitted (JUL-1995) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; L39103; AAA69491.1; -.
FT   NON_TER       1      1
FT   NON_TER      52     52
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens glycoprotein Ib alpha (GPIb) gene, partial cds.
**   [1]
**   1-424
**   Ishida F.;
**   "Submission";
**   Unpublished.
**   NCBI gi: 886281
**   source          1..424
**                   /organism="Homo sapiens"
**                   /sequenced_mol="DNA"
**   CDS             <105..>260
**                   /gene="GPIb"
**                   /note="isoform A; putative; NCBI gi: 886282"
**                   /codon_start=1
**                   /product="glycoprotein Ib alpha"
**                   /db_xref="PID:g886282"
**   CDS_1_OUT_OF_1
**   05-JUL-1995 (Rel. 44, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   52 AA;  5187 MW;  829FBEB4792EA30F CRC64;
     SEPAPSPTTP EPTSEPAPSP TTPEPTSEPA PSPTTPEPTS EPAPSPTTPE PT
//
ID   Q14489      PRELIMINARY;      PRT;    58 AA.
AC   Q14489;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   RIBOSOMAL PROTEIN S10 (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=BRAIN;
RA   Dmitrenko V.V., Garifulin O.M., Shostak K.A., Smikodub A.I.,
RA   Kavsan V.M.;
RL   Submitted (APR-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; Z70761; CAA94807.1; -.
KW   Ribosomal protein.
FT   NON_TER       1      1
FT   NON_TER      58     58
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for ribosomal protein S10
**   [1]
**   1-174
**   Dmitrenko V.V., Garifulin O.M., Shostak K.A., Smikodub A.I.,
**   Kavsan V.M.;
**   "Characterization of different mRNA types expressed in human
**   brain";
**   Unpublished.
**   [2]
**   1-174
**   Dmitrenko V.V.;
**   ;
**   Submitted (12-APR-1996) to the EMBL/GenBank/DDBJ databases.
**   Dmitrenko V. V., Institute of Molecular Biology and Genetics,
**   Biosynthesis of Nucleic Acids, Zabolotnogo 150, Kiev, Ukraine,
**   252627
**   source          1..174
**                   /organism="Homo sapiens"
**                   /clone="ICRFp507K114"
**                   /dev_stage="fetus"
**                   /tissue_type="brain"
**                   /clone_lib="S. Meier-Ewert's cDNA library no. 507"
**   CDS             <1..>174
**                   /codon_start=1
**                   /product="ribosomal protein S10"
**                   /db_xref="PID:e236293"
**   CDS_1_OUT_OF_1
**   15-APR-1996 (Rel. 47, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   58 AA;  6281 MW;  CB8CCC9198F69FDB CRC64;
     KGLEGERPAR LTRGEADRDT YRRSAVPPGA DKKAEAGLGQ QPEFQFRGGF GRGRGQPP
//
ID   Q14497      PRELIMINARY;      PRT;    25 AA.
AC   Q14497;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   CCAAT BINDING FACTOR SUBUNIT C (FRAGMENT).
GN   HCBF-C.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=BRAIN;
RA   Dmitrenko V.V., Garifulin O.M., Shostak K.A., Smikodub A.I.,
RA   Kavsan V.M.;
RL   Submitted (MAR-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; Z70024; CAA93846.1; -.
FT   NON_TER      25     25
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for CCAAT binding factor subunit gamma
**   [1]
**   Dmitrenko V.V., Garifulin O.M., Shostak K.A., Smikodub A.I.,
**   Kavsan V.M.;
**   "Characterization of different mRNA types expressed in human
**   brain";
**   Unpublished.
**   [2]
**   1-270
**   Dmitrenko V.V.;
**   ;
**   Submitted (07-MAR-1996) to the EMBL/GenBank/DDBJ databases.
**   Dmitrenko V. V., Institute of Molecular Biology and Genetics,
**   Biosynthesis of Nucleic Acids, Zabolotnogo 150, Kiev, Ukraine,
**   252627
**   source          1..270
**                   /organism="Homo sapiens"
**                   /clone="ICRFp507N0593"
**                   /dev_stage="fetus"
**                   /tissue_type="brain"
**                   /clone_lib="S. Meier-Ewert's cDNA library no.507"
**   CDS             196..>270
**                   /product="CCAAT binding factor subunit C"
**                   /function="transcription factor"
**                   /gene="hCBF-C"
**                   /note="homologous to rat CCAAT binding factor subunit
**   C
**                   (rCBF-C)"
**                   /db_xref="PID:e226027"
**   CDS_1_OUT_OF_1
**   08-MAR-1996 (Rel. 47, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   25 AA;  2649 MW;  12DE150C48C31875 CRC64;
     MSTEGGFGGT SSSDAQQSLQ SFWPR
//
ID   Q14551      PRELIMINARY;      PRT;    39 AA.
AC   Q14551;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   ALBUMIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=LIVER;
RA   Dmitrenko V.V., Kavsan V.M.;
RL   Submitted (JAN-1990) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X51365; CAA35749.1; -.
FT   NON_TER       1      1
FT   NON_TER      39     39
**
**   #################     SOURCE SECTION     ##################
**   Human mRNA for albumin (clone pHA19)
**   [1]
**   1-124
**   Dmitrenko V.V., Kavasan V.M.;
**   ;
**   Submitted (04-JAN-1990) to the EMBL/GenBank/DDBJ databases.
**   Dmitrenko V.V., Kavasan V.M., Institute of Molecular Biology and
**   Genetics, Acad. Sci. Ukr. SSR, 252627 Kiev, Zabolotnogo Str. 150,
**   USSR.
**   [2]
**   1-124
**   Dmitrenko V.V., Kavsan V.M.;
**   ;
**   Unpublished.
**   For clones pHA1,pHA12 and pHA8,25 see <X51363> and <X51364>.
**   Data kindly reviewed (18-SEP-1990) by Dmitrenko V.V.
**   source          1..117
**                   /organism="Homo sapiens"
**                   /dev_stage="embryo"
**                   /tissue_type="liver"
**                   /cell_type="hepatocyte"
**   CDS             <1..>117
**                   /codon_start=1
**                   /product="albumin"
**                   /db_xref="PID:g930070"
**   CDS_1_OUT_OF_1
**   05-AUG-1995 (Rel. 44, Last updated, Version 4)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   39 AA;  4619 MW;  ED477E0AD309370B CRC64;
     QLIHLFFFFV GVKPTPCLKN INFFNHFASF LCASINKKW
//
ID   Q14579      PRELIMINARY;      PRT;    30 AA.
AC   Q14579;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   HUMER (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Oberbaeumer I.;
RL   Submitted (MAR-1995) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X85129; CAA59443.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for humer
**   [1]
**   Oberbaeumer I.;
**   ;
**   Submitted (06-MAR-1995) to the EMBL/GenBank/DDBJ databases.
**   I. Oberbaeumer, Max-Planck-Institut fuer Biochemie, Am Klopferspitz
**   18a, D-82152 Martinsried, FRG
**   [2]
**   1-643
**   Oberbaeumer I.;
**   "A new member of the highly conserved multigene family of
**   thiol-specific antioxidant proteins (TSA) of mouse with
**   wide-spread occurrence in adherent cell lines: defining
**   orthologous mRNAs by their 3' untranslated regions.";
**   Unpublished.
**   Related sequence: T10952; T10244
**   source          1..643
**                   /organism="Homo sapiens"
**                   /cell_line="HT 1080 (fibrosarcoma, ATCC CCL 1212)"
**                   /clone_lib="RT-PCR"
**                   /clone="14"
**   CDS             <1..93
**                   /partial
**                   /codon_start=1
**                   /product="humer"
**                   /note="equivalent of mouse mer5 gene"
**                   /db_xref="PID:g854126"
**   CDS_1_OUT_OF_1
**   31-MAY-1995 (Rel. 43, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   30 AA;  3334 MW;  5713AF472692E46E CRC64;
     EVCPANWTPD SPTIKPSPAA SKEYFQKVNQ
//
ID   Q14759      PRELIMINARY;      PRT;    11 AA.
AC   Q14759;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   LYMPHOCYTE CYTOSOLIC PROTEIN 2 (FRAGMENT).
GN   LCP2.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Sunden S.L.F., Carr L.L., Clements J.L., Motto D.G., Koretzky G.A.;
RL   Genomics 0:0-0(0).
DR   EMBL; U44065; AAA93308.1; -.
FT   NON_TER       1      1
FT   NON_TER      11     11
**
**   #################     SOURCE SECTION     ##################
**   Human lymphocyte cytosolic protein 2 (LCP2) gene, partial cds,
**   partial intron.
**   [1]
**   1-500
**   Sunden S.L.F., Carr L.L., Clements J.L., Motto D.G., Koretzky G.A.;
**   "Polymorphism in and localization of the gene encoding the 76 kDa
**   SH2 domain-containing Leukocyte Protein (SLP-76) to chromosome
**   5q33.1-qter";
**   Unpublished.
**   [2]
**   1-500
**   Clements J.L., Sunden S.L.F., Motto D.G., Koretzky G.A.;
**   ;
**   Submitted (29-DEC-1995) to the EMBL/GenBank/DDBJ databases.
**   J.L. Clements, Pediatrics, University of Iowa, 440G EMRB, Iowa
**   City, IA 52242, USA
**   NCBI gi: 1203823
**   source          1..500
**                   /organism="Homo sapiens"
**                   /chromosome="5"
**                   /map="5q33.1-qter"
**   CDS             <1..>34
**                   /gene="LCP2"
**                   /note="SLP-76; 76 kDa SH2 domain-containing leukocyte
**                   protein; NCBI gi: 1203827"
**                   /codon_start=2
**                   /product="lymphocyte cytosolic protein 2"
**                   /db_xref="PID:g1203827"
**   CDS_1_OUT_OF_1
**   08-APR-1996 (Rel. 47, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   11 AA;  1242 MW;  D695104224072DDD CRC64;
     EAEAALRKIN Q
//
ID   Q14760      PRELIMINARY;      PRT;    25 AA.
AC   Q14760;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   LYMPHOCYTE CYTOSOLIC PROTEIN 2 (FRAGMENT).
GN   LCP2.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Sunden S.L.F., Carr L.L., Clements J.L., Motto D.G., Koretzky G.A.;
RL   Genomics 0:0-0(0).
DR   EMBL; U44067; AAA93309.1; -.
DR   EMBL; U44066; AAA93309.1; JOINED.
FT   NON_TER       1      1
FT   NON_TER      25     25
**
**   #################     SOURCE SECTION     ##################
**   Human lymphocyte cytosolic protein 2 (LCP2) gene, partial cds,
**   partial intron.
**   [1]
**   1-361
**   Sunden S.L.F., Carr L.L., Clements J.L., Motto D.G., Koretzky G.A.;
**   "Polymorphism in and localization of the gene encoding the 76 kDa
**   SH2 domain-containing Leukocyte Protein (SLP-76) to chromosome
**   5q33.1-qter";
**   Unpublished.
**   [2]
**   1-361
**   Clements J.L., Sunden S.L.F., Motto D.G., Koretzky G.A.;
**   ;
**   Submitted (29-DEC-1995) to the EMBL/GenBank/DDBJ databases.
**   J.L. Clements, Pediatrics, University of Iowa, 440G EMRB, Iowa
**   City, IA 52242, USA
**   NCBI gi: 1203825
**   source          1..361
**                   /organism="Homo sapiens"
**                   /chromosome="5"
**                   /map="5q33.1-qter"
**   CDS             join(U44066:<1..36,322..>361)
**                   /gene="LCP2"
**                   /note="SLP-76; 76 kDa SH2 domain-containing leukocyte
**                   protein; NCBI gi: 1203828"
**                   /codon_start=1
**                   /product="lymphocyte cytosolic protein 2"
**                   /db_xref="PID:g1203828"
**   CDS_1_OUT_OF_1
**   08-APR-1996 (Rel. 47, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   25 AA;  2868 MW;  8B7E8429F398865E CRC64;
     FYXXGTGLRG KEDXLSVXXI XDXFX
//
ID   Q14864      PRELIMINARY;      PRT;   109 AA.
AC   Q14864;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   MPS1 PROTEIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTAL;
RA   Spilsbury K., O'Mara M.A., Wu W., Rowe P.B., Symonds G., Takayama Y.;
RL   Submitted (DEC-1993) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; L20314; AAA36324.1; -.
FT   NON_TER       1      1
FT   NON_TER     109    109
**
**   #################     SOURCE SECTION     ##################
**   Human MPS1 gene, partial cds.
**   [1]
**   1-328
**   Spilsbury K., O'Mara M.A., Wu W., Rowe P.B., Symonds G.,
**   Takayama Y.;
**   "MPS1,a novel macrophage expressed gene isolated by differential
**   cDNA analysis";
**   Unpublished.
**   source          1..328
**                   /organism="Homo sapiens"
**                   /sequenced_mol="DNA"
**                   /tissue_type="placental"
**   CDS             <1..>328
**                   /note="partial protein"
**                   /product="MPS1 protein"
**                   /codon_start=1
**                   /db_xref="PID:g553591"
**   CDS_1_OUT_OF_1
**   05-DEC-1993 (Rel. 38, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   109 AA;  12462 MW;  A5205A8C4E70E893 CRC64;
     PVLEVLPGGG WDNLRNVDMG RVMELTYSNC RTTEDGQYII PDEIFTIPQK QSNLEMNSEI
     LESWANYQSS TSYSINTELS LFSKVNGKFS TDFQRMKTLQ VKDQAITTR
//
ID   Q14865      PRELIMINARY;      PRT;   246 AA.
AC   Q14865;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   MODULATOR RECOGNITION FACTOR 2 (FRAGMENT).
GN   MRF-2.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=FORESKIN;
RA   Merrills B.W., Huang T.H., Oka T., LeBon T.R., Gertson P.N.,
RA   Itakura K.;
RL   Submitted (FEB-1992) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; M73837; AAA59870.1; -.
DR   INTERPRO; IPR001606; -.
DR   PFAM; PF01388; ARID; 1.
FT   NON_TER       1      1
FT   NON_TER     246    246
**
**   #################     SOURCE SECTION     ##################
**   Human modulator recognition factor 2 (MRF-2) mRNA, complete cds.
**   [1]
**   1-740
**   Merrills B.W., Huang T.H., Oka T., LeBon T.R., Gertson P.N.,
**   Itakura K.;
**   "A new family of DNA binding factors contain a member responsive
**   to retinoic acid";
**   Unpublished.
**   NCBI gi: 188685
**   source          1..740
**                   /organism="Homo sapiens"
**                   /cell_type="fibroblast (not transformed)"
**                   /haplotype="NA"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="foreskin"
**                   /tissue_lib="lambda gt11"
**   CDS             <1..>740
**                   /gene="MRF-2"
**                   /note="NCBI gi: 553592"
**                   /codon_start=1
**                   /product="modulator recognition factor 2"
**                   /db_xref="PID:g553592"
**   CDS_1_OUT_OF_1
**   18-JAN-1995 (Rel. 42, Last updated, Version 3)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF01388; ARID; 14; 125; T; 19-JUN-2000;
SQ   SEQUENCE   246 AA;  27862 MW;  2AD44181EAFD476C CRC64;
     KVSNEEKPKV AIGEECRADE QAFLVALYKY MKERKTPIER IPYLGFKQIN LWTMFQAAQK
     LGGYETITAR RQWKHIYDEL GGNPGSTSAA TCTRRHYERL ILPYERFIKG EEDKPLPPIK
     PRKQENSSQE NENKTKVSGT KRIKHEIPKS KKEKENAPKP QDAAEVSSEQ EKEQETLISQ
     KSIPEPLPAA DMKKKIEGYQ EFSAKPLASR VDPEKDNETD QGSNSEKVAE EAGEKGPTPP
     LPSAPL
//
ID   Q14880      PRELIMINARY;      PRT;   187 AA.
AC   Q14880;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   MUCIN (FRAGMENT).
GN   MUC5B.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=TRACHEOBRONCHIAL MUCOSA;
RA   Aubert J.;
RL   Submitted (SEP-1993) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X74954; CAA52909.1; -.
FT   NON_TER       1      1
FT   NON_TER     187    187
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens MUC5B mRNA (clone JER28) for mucin (partial)
**   [1]
**   1-561
**   Aubert J.;
**   ;
**   Submitted (07-SEP-1993) to the EMBL/GenBank/DDBJ databases.
**   J. Aubert, Unite Inserm 16, Place de Verdun, 59045 Lille cedex,
**   FRANCE
**   source          1..561
**                   /organism="Homo sapiens"
**                   /tissue_type="tracheobronchial mucosa"
**                   /chromosome="11"
**                   /map="11p15"
**                   /clone="JER28"
**   CDS             <1..>561
**                   /gene="MUC5B"
**                   /product="mucin"
**                   /partial
**                   /codon_start=1
**                   /db_xref="PID:g407070"
**   CDS_1_OUT_OF_1
**   08-OCT-1993 (Rel. 37, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   187 AA;  18993 MW;  8F89B7AA6C2F890E CRC64;
     SSTPGTTWIL TEPSTTATVT GPTGSTATAS STQATAGTPH VSTTATTPTV TSSKATPFSS
     PGTATALPAL RSTATTPTAT SFTAIPLSWA PTGPLSQTTT PRPPCPQPHP PPLQRLSTPP
     PVLTTTPPPT GHRLCGHPLL HPSNSSHYQS ADYHNHGFTA TPSSSPGTAR TLPVWISTTT
     TPTTRGS
//
ID   Q14881      PRELIMINARY;      PRT;   622 AA.
AC   Q14881;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   MUCIN (FRAGMENT).
GN   MUC5B.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=TRACHEOBRONCHIAL MUCOSA;
RA   Desseyn J.L., Guyonnet-Duperat V., Porchet N., Aubert J.P., Laine A.;
RL   Submitted (JUN-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X74955; CAA52910.1; -.
FT   NON_TER       1      1
FT   NON_TER     622    622
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens MUC5B mRNA (clone JER57) for mucin (partial)
**   [1]
**   Aubert J.;
**   ;
**   Submitted (07-SEP-1993) to the EMBL/GenBank/DDBJ databases.
**   J. Aubert, Unite Inserm 16, Place de Verdun, 59045 Lille cedex,
**   FRANCE
**   [2]
**   1-1866
**   Desseyn J.L., Guyonnet-Duperat V., Porchet N., Aubert J.P.,
**   Laine A.;
**   "Human mucin gene MUC5B: the 10.7 kb large central exon encodes
**   various alternate subdomains resulting in a super repeat";
**   Unpublished.
**   [3]
**   1-1866
**   Laine A.;
**   ;
**   Submitted (12-JUN-1996) to the EMBL/GenBank/DDBJ databases.
**   A. Laine, Inserm U377, Place de Verdun, 59045 Lille cedex, FRANCE
**   source          1..1866
**                   /organism="Homo sapiens"
**                   /tissue_type="tracheobronchial mucosa"
**                   /chromosome="11"
**                   /map="11p15"
**                   /clone="JER57"
**                   /germline
**   CDS             <1..>1866
**                   /gene="MUC5B"
**                   /product="mucin"
**                   /partial
**                   /codon_start=1
**                   /db_xref="PID:e248524"
**   CDS_1_OUT_OF_1
**   12-JUN-1996 (Rel. 48, Last updated, Version 11)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   622 AA;  61787 MW;  4FC85A52F50D57E5 CRC64;
     LGLECRAQAQ PGVPLGELGQ VVECSLDFGL VCRNREQVGK FKMCFNYEIR VFCCNYGHCP
     STPATSSTAM PSSTPGTTWI LTELTTTATT TASTGSTATP SSTPGTAPPP KVLTSPATTP
     TATSSKATSS SSPRTATTLP VLTTTATKST ATSVTPIPSS TLGTTGTLPE QTTTPVATMS
     TIHPSSTPET THTSTVLTTK ATTTRATSST STPSSTPGTT WILTELTTAA TTTAGTGPTA
     TPSSTPGTTW ILTELTTTAT TTASTGSTAT LSSTPGTTWI LTEPSTTATV TVPTGSTATA
     SSTQATAGTP HVSTTATTPT VTSSKATPSS SPGTATALPA LRSTATTPTA TSFTAIPSSS
     LGTTWTRLSQ TTTPTATMST ATPSSTPETV HTSTVLTATA TTTGATGSVA TPSSTPGTAH
     TTKVPTTTTT GFTATPSSSP GTALTPPVWI STTTTPTTTT PTTSGSTVTP SSIPGTTHTA
     RVLTTTTTTV ATGSMATPSS STQTSGTPPS LTTTATTITA TGSTTNPSST PGTTPIPPVL
     TSTATTPAAT SSKATSSSSP RTATTLPVLT STATKSTATS FTPIPSSTLW TTWTVPAQTT
     TPMSTMSTIH TSSTPETTHT ST
//
ID   Q14882      PRELIMINARY;      PRT;   330 AA.
AC   Q14882;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   MUCIN (FRAGMENT).
GN   MUC5B.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=TRACHEOBRONCHIAL MUCOSA;
RA   Aubert J.;
RL   Submitted (SEP-1993) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X74956; CAA52911.1; -.
FT   NON_TER       1      1
FT   NON_TER     330    330
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens MUC5B mRNA (clone JUL10) for mucin (partial)
**   [1]
**   1-991
**   Aubert J.;
**   ;
**   Submitted (07-SEP-1993) to the EMBL/GenBank/DDBJ databases.
**   J. Aubert, Unite Inserm 16, Place de Verdun, 59045 Lille cedex,
**   FRANCE
**   source          1..991
**                   /organism="Homo sapiens"
**                   /tissue_type="tracheobronchial mucosa"
**                   /chromosome="11"
**                   /map="11p15"
**                   /clone="JUL10"
**   CDS             <1..>991
**                   /gene="MUC5B"
**                   /product="mucin"
**                   /partial
**                   /codon_start=1
**                   /db_xref="PID:g407053"
**   CDS_1_OUT_OF_1
**   08-OCT-1993 (Rel. 37, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   330 AA;  35556 MW;  97892E19FFC4FA50 CRC64;
     PTKATTTRAT SSMSTPSSTP GMTWILTELT TAATTTAATA PHCDPVLHPR DHLDPHRAQH
     YSTVTVPTGS QPTASSTRGT AGTLKVLTSD HHTHSHQLQS HSLLQSRDCN RPSSTEKHSH
     HTHSYQRYSH PLFLPGTAWT RLSQTTTPTA TMSTATPSST PETVHTSTVL TTTATTTRAT
     ALWPPPPPPQ EQLTLPKCRL PQPRLHSYPL LQPRDGTHAS SVDQHNHHTH NQRLHGDPLL
     HPGTTHTATV LTTTTTTVPL VLWQHPPLAH RPVVLPPSLT TTATTITATG STTNPSSTPG
     TTPIPPVLTT TANHTCSHQQ HSDSLLCPRE
//
ID   Q14913      PRELIMINARY;      PRT;    40 AA.
AC   Q14913;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   NA+/CA2+ EXCHANGER (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=AIRWAY SMOOTH MUSCLE;
RA   Pitt A., Knox A.J.;
RL   Submitted (SEP-1995) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X91815; CAA62923.1; -.
FT   NON_TER       1      1
FT   NON_TER      40     40
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for Na+/Ca2+ exchanger isoform
**   [1]
**   Pitt A., Knox A.J.;
**   "Molecular characterization of thr Na+/Ca2+ exchanger isoform
**   expressed in human airway smooth muscle.";
**   Unpublished.
**   [2]
**   1-122
**   Pitt A.;
**   ;
**   Submitted (07-SEP-1995) to the EMBL/GenBank/DDBJ databases.
**   A. Pitt, Nottingham University, Respiratory Medicine, City
**   Hospital, Hucknall Road, Nottingham, NG5 1PB, UK
**   source          1..122
**                   /organism="Homo sapiens"
**                   /dev_stage="adult"
**                   /tissue_type="airway smooth muscle"
**   CDS             join(<1..104,105..>122)
**                   /partial
**                   /codon_start=2
**                   /product="Na+/Ca2+ exchanger"
**                   /note="alternatively spliced isoform variable region"
**                   /db_xref="PID:g1008973"
**   CDS_1_OUT_OF_1
**   04-OCT-1995 (Rel. 45, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   40 AA;  4787 MW;  C9126B9105B01AC2 CRC64;
     KIITIRIFDR EEYEKECSLS LVLEEPKWIR RGMKGGFTIT
//
ID   Q14928      PRELIMINARY;      PRT;    41 AA.
AC   Q14928;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   KRUEPPEL-TYPE ZINC FINGER PROTEIN (FRAGMENT).
GN   ZNF169.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Dean M., Chidambaram A., Gerrard B.;
RL   Submitted (JUN-1995) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U28322; AAA70187.1; -.
DR   INTERPRO; IPR001909; -.
DR   PFAM; PF01352; KRAB; 1.
FT   NON_TER       1      1
FT   NON_TER      41     41
**
**   #################     SOURCE SECTION     ##################
**   Human Krueppel-type zinc finger protein (ZNF169) gene, partial cds.
**   [1]
**   1-444
**   Dean M., Chidambaram A., Gerrard B.;
**   ;
**   Submitted (02-JUN-1995) to the EMBL/GenBank/DDBJ databases.
**   Michael Dean, LVC, NCI-FCRDC, P.O. Box B, Frederick, MD 21702, USA
**   NCBI gi: 903595
**   source          1..444
**                   /organism="Homo sapiens"
**                   /map="9q22-31"
**                   /chromosome="9"
**   CDS             <59..>184
**                   /gene="ZNF169"
**                   /note="NCBI gi: 903598"
**                   /codon_start=3
**                   /product="Krueppel-type zinc finger protein"
**                   /db_xref="PID:g903598"
**   CDS_1_OUT_OF_1
**   26-JUL-1995 (Rel. 44, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF01352; KRAB; 4; 41; T; 19-JUN-2000;
SQ   SEQUENCE   41 AA;  4816 MW;  653EF5ADE02B70AF CRC64;
     IDGFRDVAVA FTQKEWKLLS SAQRTLYREV MLENYSHLVS L
//
ID   Q15175      PRELIMINARY;      PRT;   535 AA.
AC   Q15175;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   PARANEOPLASTIC ANTIGEN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Fathallah-Shaykh H.M., Finizio J., Ho A., Rosenblum M., Posner J.;
RL   Submitted (MAR-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; L02867; AAA91850.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens 62 kDa paraneoplastic antigen mRNA, 3' end.
**   [1]
**   1-2750
**   Fathallah-Shaykh H.M., Finizio J., Ho A., Rosenblum M., Posner J.;
**   "Cloning and characterization of a second leucine zipper protein
**   recognized by the sera of the patients with antibody associated
**   paraneoplastic cerebellar degeneration";
**   Unpublished.
**   NCBI gi: 1220352
**   source          1..2750
**                   /organism="Homo sapiens"
**                   /cell_line="HeLa"
**                   /tissue_lib="1-ZapII"
**   CDS             <1..1608
**                   /note="62 kDa; NCBI gi: 1220353"
**                   /codon_start=1
**                   /product="paraneoplastic antigen"
**                   /db_xref="PID:g1220353"
**   CDS_1_OUT_OF_1
**   12-MAR-1996 (Rel. 47, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   535 AA;  58014 MW;  6C80A459E888A1DA CRC64;
     YRIPGGGTWQ SARPRVGSRR AVDGEGARRG LCSPSSRRWR PGPPQPHCPG PRAPALSCAA
     AAPARRPRGH AESRRDGGLG SAEEEESWYD QQDLEQDLHL AAELGKTLLE RNKELEGSLQ
     QMYSTNEEQV QEIEYLTKQL DTLRHVNEQH AKVYEQLDLT ARDLELTNHR LVLESKAAQQ
     KIHGLTETIE RLQAQVEELQ AQVEQLRGLE QLRVLREKRE RRRTIHTFPC LKELCTSPRC
     KDAFRLHSSS LELPAAPGAG ERAAADPGGG AALPGEPGAA AQGAGGARVH RGAAGVLGAG
     APAVRDGGLS PACAGAGGRA AGAAADEAGQ DLPTGSGTTT WPRPCSHPSR RPLRPTIPSP
     AAGTTWAPRT GSPHRQPLQA TWCARAAATL RSTPSWPKTQ PAGTRATSHC TPTALRKRGM
     SILREVDEQY HALLEKYEEL LSKCRQHGAG VRDAGVQTSR PISRDSSWRD LRGGEEGQGE
     VKAGEKSLSQ HVEAVDKRLE QSQPEYKALF KEIFSRIQKT KADINATKVK THSSK
//
ID   Q15371      PRELIMINARY;      PRT;   150 AA.
AC   Q15371;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   RIBOSOMAL PROTEIN L18A HOMOLOGUE (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Zenz K.I.;
RL   Submitted (AUG-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X80821; CAA56787.1; -.
KW   Ribosomal protein.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for ribosomal protein L18a homologue
**   [1]
**   Zenz K.I.;
**   ;
**   Unpublished.
**   [2]
**   1-660
**   Zenz K.I.;
**   ;
**   Submitted (02-AUG-1994) to the EMBL/GenBank/DDBJ databases.
**   K.I. Zenz, Institute of Immunology, Christian-Albrechts University,
**   Kiel, Michaelisstr. 5, 24105 Kiel, FRG
**   source          1..660
**                   /organism="Homo sapiens"
**                   /cell_type="lymphocyte"
**                   /cell_line="human periferal lymphocytes"
**   CDS             <200..652
**                   /partial
**                   /codon_start=1
**                   /product="ribosomal protein L18a homologue"
**                   /db_xref="PID:g527580"
**   CDS_1_OUT_OF_1
**   05-AUG-1994 (Rel. 40, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   150 AA;  18050 MW;  4E5E933C97FA075F CRC64;
     TEDLFLNMEH ESLTLEKKSK LEKNIKDDKS TKEKHVSKER NFKEERDKIK KESENLLLWG
     MSAIEESIGL HLVEKEIDIE KQEKHIKESK EKPEKRSQIK EKDIEKMERK TFDKGQAYIR
     ITQTKWPYKK GEKEQKYCCL LKKPDGQREK
//
ID   Q15489      PRELIMINARY;      PRT;    55 AA.
AC   Q15489;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   MYELIN ASSOCIATED GLYCOPROTEIN (FRAGMENT).
GN   S-MAG.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=BRAIN;
RX   MEDLINE=98154940; PubMed=9495552;
RA   Miescher G., Luetzelschwab R., Erne B., Ferracin F., Huber S.,
RA   Stack A.J.;
RT   "Reciprocal expression of myelin-associated glycoprotein splice
RT   variants in the adult human peripheral and central nervous systems.";
RL   Brain Res. Mol. Brain Res. 52:308-315(1997).
DR   EMBL; X98405; CAA67055.1; -.
KW   Myelin.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for myelin associated glycoprotein, S-MAG
**   [1]
**   Miescher G., Luetzelschwab R., Huber S., Ferracin F., Erne B.,
**   Stack A.J.;
**   "Differential expression of human MAG isoforms in brain and
**   nerve";
**   Unpublished.
**   [2]
**   1-446
**   Miescher G.C.;
**   ;
**   Submitted (15-MAY-1996) to the EMBL/GenBank/DDBJ databases.
**   G.C. Miescher, University Hospitals Basel, Neurobiology Lab.
**   Department of Research, Hebelstrasse 20, Kantonsspital Basel,
**   Basel, 4031, Switzerland
**   source          1..446
**                   /organism="Homo sapiens"
**                   /chromosome="19"
**                   /map="q13.1"
**                   /dev_stage="adult"
**                   /lab_host="E.coli"
**                   /isolate="pMAG-PCRII#11"
**                   /tissue_type="brain"
**   CDS             <1..169
**                   /codon_start=2
**                   /gene="S-MAG"
**                   /product="myelin associated glycoprotein"
**                   /db_xref="PID:e248458"
**   misc_feature    137..181
**                   /note="alternative splice from exon 12"
**   CDS_1_OUT_OF_1
**   11-JUN-1996 (Rel. 48, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   55 AA;  6106 MW;  34D851778133F87F CRC64;
     AIVCYITQTR RKKNVTESPS FSAGDNPPVL FSSDFRISGA PEKYESKEVS TLESH
//
ID   Q15559      PRELIMINARY;      PRT;   102 AA.
AC   Q15559;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   (CLONE TEC14) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Piek E., Mosselman S.;
RL   Submitted (MAY-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; L32558; AAA36727.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone tec14) mRNA, partial cds.
**   [1]
**   1-320
**   Piek E., Mosselman S.;
**   ;
**   Unpublished.
**   NCBI gi: 483354
**   source          1..320
**                   /organism="Homo sapiens"
**                   /cell_line="Tera-2 (EC)"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_lib="lambda GEM2"
**   CDS             <1..311
**                   /note="(embryonal carcinoma) cells. The sequence may
**                   contain mismatches (one strand sequenced only once).
**   97%
**                   identical in 320 bp overlap with human 54 kDA prot;
**   ORF.
**                   sequence is expressed in human Tera-2 clone 13; NCBI
**   gi:
**                   483355."
**                   /codon_start=3
**                   /db_xref="PID:g483355"
**   CDS_1_OUT_OF_1
**   19-MAY-1994 (Rel. 39, Last updated, Version 3)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   102 AA;  11659 MW;  B447711590819F74 CRC64;
     IQANVHKGHR QRTYGSVIPH ILPLHVLKKT FSLRDFHFSV SLKKNLVLTC LDLFLGVRTP
     RNDPFVSMML LFTRFLDRPS TILGTGLLYT EGLTVALRLR LS
//
ID   Q15570      PRELIMINARY;      PRT;   130 AA.
AC   Q15570;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   BTF2P44 (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Van der Steege G., Draaijers T.G., Grootscholten P.M., Osinga J.,
RA   Anzevino R., Velona I., Brahe C., Scheffer H., Van Ommen G.J.B.,
RA   Buys C.H.C.M.;
RL   Eur. J. Hum. Genet. 0:0-0(0).
DR   EMBL; U21911; AAA64502.1; -.
FT   NON_TER     130    130
**
**   #################     SOURCE SECTION     ##################
**   Human basic transcription factor BTF2p44 mRNA, 5' end, partial cds.
**   [1]
**   der Steege G., Draaijers T.G., Grootscholten P.M., Osinga J.,
**   Anzevino R., VELONA I., Brahe C., Scheffer H., van Ommen G.J.B.,
**   Buys C.H.C.M.;
**   "A provisional transcript map of the spinal muscular atrophy (SMA)
**   critical region";
**   Unpublished.
**   [2]
**   1-548
**   der Steege G.;
**   ;
**   Submitted (28-FEB-1995) to the EMBL/GenBank/DDBJ databases.
**   Gerrit Van der Steege, University of Groningen, Department of
**   Medical Genetics, Antonius Deusinglaan 4, Groningen, 9713 AW, The
**   Netherlands
**   NCBI gi: 736401
**   source          1..548
**                   /clone="5G3 5'-end"
**                   /clone_lib="library of A. Bernards"
**                   /organism="Homo sapiens"
**                   /chromosome="5"
**                   /map="5q13.1"
**                   /cell_type="pre-B-cells"
**   CDS             157..>548
**                   /note="basic transcription factor 2, 44 kDa subunit;
**   NCBI
**                   gi: 736404"
**                   /codon_start=1
**                   /product="BTF2p44"
**                   /db_xref="PID:g736404"
**   CDS_1_OUT_OF_1
**   23-MAY-1995 (Rel. 43, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   130 AA;  15293 MW;  C681910BD2069D13 CRC64;
     MDEEPERTKR WEGGYERTWE ILKEDESGSL KATIEDILFK AKRKRVFEHH GQVRLGMMRH
     LYVVVDGSRT MEDQDLKPNR LTCTLKLLEY FVEEYFDQNP ISQIGIIVTK SKRAEKLTEL
     SGNPRKXISS
//
ID   Q15597      PRELIMINARY;      PRT;   699 AA.
AC   Q15597;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   TRANSLATION INITIATIONFACTOR EIF-4GAMMA (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=OVARY;
RA   Klaudiny J.J., von der Kammer H.H., Scheit K.K.;
RL   Submitted (JUN-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; Z34918; CAA84397.1; -.
DR   PFAM; PF02020; IF5_eIF4_eIF2; 1.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for translation initiation factor eIF-4gamma
**   (partial)
**   [1]
**   1-2124
**   Klaudiny J.J., von der Kammer H.H., Scheit K.K.;
**   "Characterization of secretory proteins of human ovarian follicle
**   cells by cDNA cloning";
**   Unpublished.
**   [2]
**   1-2124
**   von der Kammer H.;
**   ;
**   Submitted (30-JUN-1994) to the EMBL/GenBank/DDBJ databases.
**   Heinz von der Kammer, Abteilung fuer Molekulare Biologie,
**   Max-Planck-Institut fuer Biophysikalische Chemie, Am Fassberg 11,
**   Goettingen, D-37077, Germany
**   source          1..2124
**                   /organism="Homo sapiens"
**                   /dev_stage="adult"
**                   /tissue_type="ovary"
**   CDS             <1..2100
**                   /note="putative"
**                   /codon_start=1
**                   /note="homologue"
**                   /product="translation initiationfactor eIF-4gamma"
**                   /db_xref="PID:g510307"
**   CDS_1_OUT_OF_1
**   04-JUL-1994 (Rel. 40, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF02020; IF5_eIF4_eIF2; 618; 699; T; 19-JUN-2000;
SQ   SEQUENCE   699 AA;  79421 MW;  DC65F87073258175 CRC64;
     RRSIGNIKFI GELFKLKMLT EAIMHDCVVK LLKNHDEESL ECLCRLLTTI GKDLDFEKAK
     PRMDQYFNQM EKIVKERKTS SRIRFMLQDV IDLRLCNWVS RRADQGPKTI EQIHKEAKIE
     EQEEQRKVQQ LMTKEKRRPG VQRVDEGGWN TVQGAKNSRV LDPSKFLKIT KPTIDEKIQL
     VPKAQLGSWG KGSSGGAKAS ETDALRSSAS SLNRFSALQP PAPSGSTPST PVEFDSRRTL
     TSRGSMGREK NDKPLPSATA RPNTFMRGGS SKDLLDNQSQ EEQRREMLET VKQLTGGVDV
     ERNSTEAERN KTRESAKPEI SAMSAHDKAA LSEEELERKS KSIIDEFLHI NDFKEAMQCV
     EELNAQGLLH VFVRVGVEST LERSQITRDH MGQLLYQLVQ SEKLSKQDFF KGFSETLELA
     DDMAIDIPHI WLYLAELVTP MLKEGGISMR ELTIEFSKPL LPVGRAGVLL SEILHLLCKQ
     MSHKKVGALW READLSWKDF LPEGEDVHNF LLEQKLDFIE SDSPCSSEAL SKKELSAEEL
     YKRLEKLIIE DKANDEQIFD WVEANLDEIQ MSSPTFLRAL MTAVCKAAII ADSSTFRVDT
     AVIKQRVPIL LKYLDSDTEK ELQALYALQA SIVKLDQPAN LLRMFFDCLY DEEVISEDAF
     YKWESSKDPA EQNGKGVALK SVTAFFTWLR EAEEESEDN
//
ID   Q15636      PRELIMINARY;      PRT;   450 AA.
AC   Q15636;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   TRANSCRIPTION FACTOR (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Lania L.;
RL   Submitted (MAY-1994) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; L32162; AAA36767.1; -.
DR   INTERPRO; IPR001909; -.
DR   PFAM; PF01352; KRAB; 1.
DR   PFAM; PF02023; SCAN; 1.
FT   NON_TER     450    450
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens transcription factor mRNA, 5' end.
**   [1]
**   1-1520
**   Lania L.;
**   "Positional cloning of cDNAs from the human chromosome 3p21-22
**   region identifies a clustered organization of ZNF genes";
**   Unpublished.
**   NCBI gi: 487835
**   source          1..1520
**                   /organism="Homo sapiens"
**                   /sequenced_mol="cDNA to mRNA"
**   CDS             171..>1520
**                   /map="3p21-22"
**                   /note="NCBI gi: 487836"
**                   /product="transcription factor"
**                   /codon_start=1
**                   /db_xref="PID:g487836"
**   CDS_1_OUT_OF_1
**   18-MAY-1994 (Rel. 39, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF01352; KRAB; 232; 293; T; 19-JUN-2000;
**PM PFAM; PF02023; SCAN; 48; 153; T; 19-JUN-2000;
SQ   SEQUENCE   450 AA;  50359 MW;  A2CC962BAD05C5C2 CRC64;
     MPPGRWHAAI SSGPVFEGAR ALQTVKKEEE DESYTPVQAR RPQTLNRPGQ ELFRQLFRQL
     RYHESSGPLE TLSRLRELCR WWLRPDVLSK AQILELLVLE QFLSILPGEL RVWVQLHNPE
     SGEELWPCWR SCRGTLMGHP GGTRALPEPR CALDGYRSLR SAQIWSLASP LRSSSALGDH
     LEPPYEIEAR DFLAGQSDTP AAQMPALFPR EGCPGDQVTP TRSLTAQLQE TMTFKDVEVT
     FSQDEWGWLD SAQRNLYRDV MLENYRNMAS LVGPFTKPAL ISWLEAREPW GLNMQAAQPK
     GNPVAAPTGD DLQSKTNKFI LNQEPLEEAE TLAVSSGCPA TSVSEGIDRS ILRESFQQNQ
     SRDKMRDLRE GQMEPPKSEL IGWGGGETSR WVRGGASPPP ALSPLFRITW SGHKDLKDLK
     VRGLRGLEAP RVNVWETEAN QAASTPGPPA
//
ID   Q15662      PRELIMINARY;      PRT;   368 AA.
AC   Q15662;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   TRANSFORMATION-RELATED PROTEIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=EPITHELIUM;
RA   Shen H., Steinberg M.L.;
RL   Submitted (SEP-1993) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; L24521; AAA36776.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human transformation-related protein mRNA, 3' end.
**   [1]
**   1-1240
**   Shen H., Steinberg M.L.;
**   ;
**   Unpublished.
**   source          1..1240
**                   /organism="Homo sapiens"
**                   /cell_type="SV40-transformed keratinocyte"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="epithelium"
**   CDS             <1..1109
**                   /product="transformation-related protein"
**                   /codon_start=3
**                   /db_xref="PID:g403460"
**   repeat_region   1..283
**                   /rpt_family="Alu"
**                   /note="Alu-like repeat"
**                   AA 1 -> 95
**   CDS_1_OUT_OF_1
**   28-SEP-1993 (Rel. 37, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   368 AA;  42029 MW;  A8B79E59EBBBA2B0 CRC64;
     DRLSLLSPRL ECNGMILAHC KLRLPGFKRF SCLSLPSSWD YRHVPPRQVH FVFSVETGFH
     RAGQAGLELL TSSVPPTSAF PKCWDYRRDD QAWPTLSSFR GLNKFAFLPK FFAHPISQFQ
     RVECNVGCPI LLAMKYLAYS SLPGADTMLY FYFYEQEASL AVCNICRQKF HWVLYQISHL
     YRGVIVDNFL LHPDGRFTWT IFFLSWVKQN SLVDFFFGTE SRSVALLPRL ECSGAMSTLH
     TVLRPAYSHI YHPDVKEKTH FLGNVFNKRK LQKKILKTPN PLCALHSAPS PSLPPFLRCT
     GRLPFYLGLD DFLFVAGALM FLPVSFLNPH TLTWPPQCCT RSDCNPLRGQ REISALSHSL
     PTGLSMPL
//
ID   Q15706      PRELIMINARY;      PRT;   112 AA.
AC   Q15706;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   13 kDa DIFFERENTIATION-ASSOCIATED PROTEIN (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=LUNG ADENOCARCINOMA;
RA   Wu M., Li B., Wang Z., Cai Y.;
RL   Submitted (AUG-1995) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U34343; AAB03380.1; -.
FT   NON_TER     112    112
**
**   #################     SOURCE SECTION     ##################
**   Human 13kD differentiation-associated protein mRNA, partial cds.
**   [1]
**   1-681
**   Wu M., Li B., Wang Z., Cai Y.;
**   "Molecular cloning of differentiation associated gene from human
**   lung adenocarcinoma cell line treated with all-trans retinoic
**   acid";
**   Unpublished.
**   [2]
**   1-681
**   Wu M.;
**   ;
**   Submitted (20-AUG-1995) to the EMBL/GenBank/DDBJ databases.
**   Min Wu, Department of Cell Biology, Cancer Institute, Chinese
**   Academy of Medical Sciences, Panjiayuan, Chaoyang District,
**   Beijing, 100021, China
**   source          1..681
**                   /organism="Homo sapiens"
**                   /clone="RA42"
**                   /sex="female"
**                   /cell_line="GLC-82"
**                   /tissue_type="lung adenocarcinoma"
**   CDS             345..>681
**                   /codon_start=1
**                   /product="13kD differentiation-associated protein"
**                   /db_xref="PID:g995939"
**   CDS_1_OUT_OF_1
**   08-JUL-1996 (Rel. 48, Last updated, Version 3)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   112 AA;  13217 MW;  7751BF462C419080 CRC64;
     MGKNTFWDVE GSMVPPEWHR WLHSMTDDPP TTKPLTARKF IWTNHNFNVT GPQNNMYLIL
     PLERRFRSGS HLQHLTSKDN EEQLKHAKYG AFHVITLLLF TIHYNSQLKL CD
//
ID   Q15734      PRELIMINARY;      PRT;   232 AA.
AC   Q15734;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   PHOSPHATIDYLINOSITOL (4,5) BISPHOSPHATE 5-PHOSPHATASE HOMOLOG
DE   (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Nussbaum R.L.;
RL   Submitted (JAN-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U45974; AAB03215.1; -.
DR   INTERPRO; IPR000300; -.
DR   PFAM; PF00783; IPPc; 1.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human phosphatidylinositol (4,5) bisphosphate 5-phosphatase homolog
**   mRNA, partial cds.
**   [1]
**   1-1616
**   Nussbaum R.L.;
**   ;
**   Submitted (11-JAN-1996) to the EMBL/GenBank/DDBJ databases.
**   Robert L. Nussbaum, NCHGR, NIH, 49 Convent Drive, Bethesda, MD
**   20892, USA
**   source          1..1616
**                   /organism="Homo sapiens"
**                   /note="derived using EST HSC39F111, GenBank Accession
**                   Number F12413, and 5'RACE procedure"
**   CDS             <1..699
**                   /note="phosphatidylinositol (4,5) bisphosphate
**                   5-phosphatase homolog"
**                   /codon_start=1
**                   /db_xref="PID:g1399103"
**   CDS_1_OUT_OF_1
**   08-JUL-1996 (Rel. 48, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00783; IPPc; 2; 138; T; 19-JUN-2000;
SQ   SEQUENCE   232 AA;  26172 MW;  6C7B868147F97E75 CRC64;
     RQAWHEGFDE VFWFGDFNFR LSGGRTVVDA LLCQGLVVDV PALLQHDQLI REMRKGSIFK
     GFQEPDIHFL PSYKFDIGKD TYDSTSKQRT PSYTDRVLYR SRHKGDICPV SYSSCPGIKT
     SDHRPVYGLF RVKVRPGRDN IPLAAGKFDR ELYLLGIKRR ISKEIQRQQA LQSQNSSTIC
     SVFAERGLTA ATWGDCIDQN PLGRTKSLPP FGDPRDCGDR ASVPASQEGS QP
//
ID   Q15735      PRELIMINARY;      PRT;   397 AA.
AC   Q15735;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   PHOSPHATIDYLINOSITOL (4,5)BISPHOSPHATE 5-PHOSPHATASE HOMOLOG
DE   (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=BRAIN;
RA   Nussbaum R.L.;
RL   Submitted (JAN-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U45975; AAB03216.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human phosphatidylinositol (4,5)bisphosphate 5-phosphatase homolog
**   mRNA, partial cds.
**   [1]
**   1-1496
**   Nussbaum R.L.;
**   ;
**   Submitted (11-JAN-1996) to the EMBL/GenBank/DDBJ databases.
**   Robert L. Nussbaum, NCHGR, NIH, 49 Convent Drive, Bethesda, MD
**   20892, USA
**   source          1..1496
**                   /organism="Homo sapiens"
**                   /note="derived using ESTs, GenBank Accession Number
**   R13943
**                   and R15390"
**                   /tissue_type="brain"
**                   /dev_stage="neonatal infant"
**   CDS             <1..1194
**                   /note="phosphatidylinositol (4,5)bisphosphate
**   5-phosphatase
**                   homolog"
**                   /codon_start=1
**                   /db_xref="PID:g1399105"
**   CDS_1_OUT_OF_1
**   08-JUL-1996 (Rel. 48, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   397 AA;  43893 MW;  71418E311E24FBFA CRC64;
     ARGLHFVKFA IDSDQLHQLW EKDQLNMAKN TWPILKGFQE GPLNFAPTFK FDVGTNKYDT
     SAKKRKPAWT DRILWKVKAP GGGPSPSGRK SHRLQVTQHS YRSHMEYTVS DHKPVAAQFL
     LQFAFRDDMP LVRLEVADEW VRPEQAVVRY RMETVFARSS WDWIGLYRVG FRHCKDYVAY
     VWAKHEDVDG NTYQVTFSEE SLPKGHGDFI LGYYSHNHSI LIGITEPFQI SLPSSELASS
     STDSSGTSSE GEDDSTLELL APKSRSPSPG KSKRHRSRSP GLARFPGLAL RPSSRERRGA
     SRSPSPQSRR LSRVAPDRSS NGSSRGSSEE GPSGLPGPWA FPPAVPRSLG LLPALRLETV
     DPGGGGSWGP DREALAPNSL SPSPQGHRGL EEGGLGP
//
ID   Q15752      PRELIMINARY;      PRT;    73 AA.
AC   Q15752;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   RETINOBLASTOMA BINDING PROTEIN 3 (FRAGMENT).
GN   RBBP3.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Vogt T., Welsh J., Stolz W., Kullmann F., Zamudio J., McClelland M.;
RL   Submitted (MAR-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U50848; AAB86738.1; -.
FT   NON_TER       1      1
FT   NON_TER      73     73
**
**   #################     SOURCE SECTION     ##################
**   Human retinoblastoma binding protein 3 mRNA, partial cds.
**   [1]
**   1-220
**   Vogt T., Welsh J., Stolz W., Kullmann F., McClelland M.;
**   ;
**   Submitted (06-MAR-1996) to the EMBL/GenBank/DDBJ databases.
**   Thomas Vogt, Molecular Science, Sidney Kimmel Cancer Center, 11099
**   North Torrey Pines Road, San Diego, CA 92037, USA
**   source          1..220
**                   /organism="Homo sapiens"
**                   /dev_stage="newborn"
**                   /cell_type="melanocytes"
**   CDS             <1..>220
**                   /gene="RBBP3"
**                   /note="mRNA is differentially regulated in TPA treated
**                   cells, i.e., upregulated; sequence has potential to
**   form a
**                   zinc finger motif; new member of the RBBP familiy"
**                   /codon_start=3
**                   /product="retinoblastoma binding protein 3"
**                   /db_xref="PID:g1480479"
**   CDS_1_OUT_OF_1
**   10-AUG-1996 (Rel. 48, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   73 AA;  8378 MW;  DC60BF32D7943E3A CRC64;
     GDSYHTFXXI PPLHDVPKGD WRCPKCLAQE CSKPQEAFGF EQAARDYTLR TFGEMADAFK
     SDYFNMPVHM VPL
//
ID   Q15810      PRELIMINARY;      PRT;    77 AA.
AC   Q15810;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   CLONE 137308 ORF1 (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Sampson M., McClendon D., Barlow C., Wiley K.;
RL   Submitted (JUL-1996) to the EMBL/GenBank/DDBJ databases.
RN   [2]
RP   SEQUENCE FROM N.A.
RA   Hamilton R.;
RL   Submitted (JUN-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U60873; AAB05597.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human clone 137308 mRNA, partial cds.
**   [1]
**   1-601
**   Sampson M., McClendon D., Barlow C., Wiley K.;
**   "Human cDNA clone 137308";
**   Unpublished.
**   [2]
**   1-601
**   Hamilton R.;
**   ;
**   Submitted (14-JUN-1996) to the EMBL/GenBank/DDBJ databases.
**   Biological Sciences, Mississippi College, 200 South Capitol Street,
**   Clinton, MS 39058, USA
**   source          1..601
**                   /organism="Homo sapiens"
**                   /clone="137308"
**   CDS             <1..235
**                   /note="orf1 protein."
**                   /codon_start=2
**                   /db_xref="PID:g1478282"
**   CDS_1_OUT_OF_1
**   03-AUG-1996 (Rel. 48, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   77 AA;  8689 MW;  0B3EBD4DF30BA6C2 CRC64;
     ARVQEGRPWR REPASIDACR LNFQRLRRRK FSNVLFPGLA QEALYSGGYH LKFADELMGG
     NLKKSTADAS GSRGHQL
//
ID   Q15888      PRELIMINARY;      PRT;     8 AA.
AC   Q15888;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP15H8A) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32069; AAA73878.1; -.
FT   NON_TER       1      1
FT   NON_TER       8      8
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP15H8A) mRNA, partial EST.
**   [1]
**   1-24
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557141
**   source          1..24
**                   /organism="Homo sapiens"
**                   /clone="XP15H8A"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>24
**                   /map="chromosome X"
**                   /note="ORF; NCBI gi: 558107"
**                   /codon_start=2
**                   /db_xref="PID:g558107"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   8 AA;  1068 MW;  0315A37EAB5B0763 CRC64;
     KPEYCWSR
//
ID   Q15889      PRELIMINARY;      PRT;     8 AA.
AC   Q15889;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP15H8B) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32070; AAA73879.1; -.
FT   NON_TER       1      1
FT   NON_TER       8      8
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP15H8B) mRNA, partial EST.
**   [1]
**   1-24
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557142
**   source          1..24
**                   /organism="Homo sapiens"
**                   /clone="XP15H8B"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>24
**                   /map="chromosome X"
**                   /note="ORF; NCBI gi: 558108"
**                   /codon_start=1
**                   /db_xref="PID:g558108"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   8 AA;  865 MW;  0474472325A761E7 CRC64;
     LHPSKLNG
//
ID   Q15890      PRELIMINARY;      PRT;     8 AA.
AC   Q15890;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP19G12A) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32083; AAA73880.1; -.
FT   NON_TER       1      1
FT   NON_TER       8      8
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP19G12A) mRNA, partial cds.
**   [1]
**   1-24
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557146
**   source          1..24
**                   /organism="Homo sapiens"
**                   /clone="XP19G12A"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>24
**                   /map="Xp11.1-q11"
**                   /note="ORF; NCBI gi: 558109"
**                   /codon_start=1
**                   /db_xref="PID:g558109"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   8 AA;  975 MW;  605EA6C5BEA5A2D3 CRC64;
     WVSCSQCY
//
ID   Q15891      PRELIMINARY;      PRT;     9 AA.
AC   Q15891;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP2E8B) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32131; AAA73881.1; -.
FT   NON_TER       1      1
FT   NON_TER       9      9
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP2E8B) mRNA, partial cds.
**   [1]
**   1-26
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557154
**   source          1..26
**                   /organism="Homo sapiens"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>26
**                   /map="chromosome 17"
**                   /note="ORF; NCBI gi: 557735"
**                   /codon_start=1
**                   /db_xref="PID:g557735"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 7)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   9 AA;  1030 MW;  E56635A1A33686D1 CRC64;
     EHQMKTSLG
//
ID   Q15892      PRELIMINARY;      PRT;     9 AA.
AC   Q15892;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP3B4A) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32071; AAA73882.1; -.
FT   NON_TER       1      1
FT   NON_TER       9      9
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP3B4A) mRNA, partial EST.
**   [1]
**   1-26
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557159
**   source          1..26
**                   /organism="Homo sapiens"
**                   /clone="XP3B4A"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>26
**                   /map="Xq24"
**                   /note="ORF; NCBI gi: 558110"
**                   /codon_start=1
**                   /db_xref="PID:g558110"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   9 AA;  971 MW;  49B22732CDC40B17 CRC64;
     ALERAVLLS
//
ID   Q15893      PRELIMINARY;      PRT;     8 AA.
AC   Q15893;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP587A) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32073; AAA73883.1; -.
FT   NON_TER       1      1
FT   NON_TER       8      8
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP587A) mRNA, partial EST.
**   [1]
**   1-24
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557169
**   source          1..24
**                   /organism="Homo sapiens"
**                   /clone="XP587A"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>24
**                   /map="Xq22"
**                   /note="ORF; NCBI gi: 558111"
**                   /codon_start=1
**                   /db_xref="PID:g558111"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   8 AA;  874 MW;  DAA1B6D7376456C5 CRC64;
     SQNPLQTS
//
ID   Q15894      PRELIMINARY;      PRT;     8 AA.
AC   Q15894;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP587B) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32074; AAA73884.1; -.
FT   NON_TER       1      1
FT   NON_TER       8      8
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP587B) mRNA, partial EST.
**   [1]
**   1-24
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557170
**   source          1..24
**                   /organism="Homo sapiens"
**                   /clone="XP587B"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>24
**                   /map="Xq22"
**                   /note="ORF; NCBI gi: 558112"
**                   /codon_start=1
**                   /db_xref="PID:g558112"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   8 AA;  952 MW;  EBC735B1E1F1B6D6 CRC64;
     MQTHHSLV
//
ID   Q15895      PRELIMINARY;      PRT;     8 AA.
AC   Q15895;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP6A10A) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32075; AAA73885.1; -.
FT   NON_TER       1      1
FT   NON_TER       8      8
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP6A10A) mRNA, partial EST.
**   [1]
**   1-25
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557173
**   source          1..25
**                   /organism="Homo sapiens"
**                   /clone="XP6A10A"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>25
**                   /map="Xq21.3-q22"
**                   /note="ORF; NCBI gi: 558113"
**                   /codon_start=1
**                   /db_xref="PID:g558113"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   8 AA;  921 MW;  C6C735B33686C1AA CRC64;
     DTQMKSLV
//
ID   Q15896      PRELIMINARY;      PRT;     9 AA.
AC   Q15896;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP6A10B) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32076; AAA73886.1; -.
FT   NON_TER       1      1
FT   NON_TER       9      9
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP6A10B) mRNA, partial EST.
**   [1]
**   1-28
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557174
**   source          1..28
**                   /organism="Homo sapiens"
**                   /clone="XP6A10B"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>28
**                   /map="Xq21.3-q22"
**                   /note="ORF; NCBI gi: 558114"
**                   /codon_start=1
**                   /db_xref="PID:g558114"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   9 AA;  1047 MW;  11D15731B2C9C054 CRC64;
     ENIFVTLIV
//
ID   Q15897      PRELIMINARY;      PRT;     7 AA.
AC   Q15897;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP6A11A) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32077; AAA73887.1; -.
FT   NON_TER       1      1
FT   NON_TER       7      7
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP6A11A) mRNA, partial EST.
**   [1]
**   1-24
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557175
**   source          1..24
**                   /organism="Homo sapiens"
**                   /clone="XP6A11A"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>24
**                   /map="Xq22"
**                   /note="ORF; NCBI gi: 558115"
**                   /codon_start=3
**                   /db_xref="PID:g558115"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   7 AA;  814 MW;  672B1DD3372046B0 CRC64;
     QILKAEL
//
ID   Q15898      PRELIMINARY;      PRT;     8 AA.
AC   Q15898;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP6A11B) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32078; AAA73888.1; -.
FT   NON_TER       1      1
FT   NON_TER       8      8
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP6A11B) mRNA, partial EST.
**   [1]
**   1-24
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557176
**   source          1..24
**                   /organism="Homo sapiens"
**                   /clone="XP6A11B"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>24
**                   /map="Xq22"
**                   /note="ORF; NCBI gi: 558116"
**                   /codon_start=1
**                   /db_xref="PID:g558116"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   8 AA;  938 MW;  34A415B0477B45BB CRC64;
     ESYPISRS
//
ID   Q15900      PRELIMINARY;      PRT;     8 AA.
AC   Q15900;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP7B11A) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32079; AAA73890.1; -.
FT   NON_TER       1      1
FT   NON_TER       8      8
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP7B11A) mRNA, partial EST.
**   [1]
**   1-25
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557179
**   source          1..25
**                   /organism="Homo sapiens"
**                   /clone="XP7B11A"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>25
**                   /map="Xq27.3-q28,2,3,16"
**                   /note="ORF; NCBI gi: 558117"
**                   /codon_start=2
**                   /db_xref="PID:g558117"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   8 AA;  931 MW;  B5DDC403369AAEB1 CRC64;
     HCDMKRAA
//
ID   Q15901      PRELIMINARY;      PRT;     8 AA.
AC   Q15901;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP7B11B) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32080; AAA73891.1; -.
FT   NON_TER       1      1
FT   NON_TER       8      8
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP7B11B) mRNA, partial EST.
**   [1]
**   1-25
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557180
**   source          1..25
**                   /organism="Homo sapiens"
**                   /clone="XP7B11B"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>25
**                   /map="Xq27.3-q28, 2, 3, 16"
**                   /note="ORF; NCBI gi: 558118"
**                   /codon_start=1
**                   /db_xref="PID:g558118"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   8 AA;  860 MW;  37D72878676729CB CRC64;
     EFLPGGLQ
//
ID   Q15902      PRELIMINARY;      PRT;     8 AA.
AC   Q15902;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP7E7A) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32081; AAA73892.1; -.
FT   NON_TER       1      1
FT   NON_TER       8      8
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP7E7A) mRNA, partial EST.
**   [1]
**   1-24
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557183
**   source          1..24
**                   /organism="Homo sapiens"
**                   /clone="XP7E7A"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>24
**                   /map="Xq24"
**                   /note="ORF; NCBI gi: 558119"
**                   /codon_start=2
**                   /db_xref="PID:g558119"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   8 AA;  931 MW;  83D699CAB1B1B2C9 CRC64;
     FVTTDFMA
//
ID   Q15903      PRELIMINARY;      PRT;     7 AA.
AC   Q15903;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   (CLONE XP7E7B) (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=PLACENTA;
RA   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
RA   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A., Zhao Z.Y.,
RA   Caskey C.T.H.;
RL   Hum. Mol. Genet. 0:0-0(0).
DR   EMBL; L32082; AAA73893.1; -.
FT   NON_TER       1      1
FT   NON_TER       7      7
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens (clone XP7E7B) mRNA, partial cds.
**   [1]
**   1-24
**   Lee C.C., Yazdani A., Wehnert M., Bailey J., Couch L., Xiong M.,
**   Coolbaugh M.I., Chinault C.A., Baldini A., Lindsay E.A.,
**   Zhao Z.Y., Caskey C.T.H.;
**   "Isolation of chromosome-specific genes by reciprocal probing of
**   arrayed cDNAs and cosmid libraries";
**   Unpublished.
**   NCBI gi: 557184
**   source          1..24
**                   /organism="Homo sapiens"
**                   /clone="XP7E7B"
**                   /haplotype="diploid"
**                   /sequenced_mol="cDNA to mRNA"
**                   /tissue_type="placenta"
**   CDS             <1..>24
**                   /map="Xq24"
**                   /note="ORF; NCBI gi: 558120"
**                   /codon_start=3
**                   /db_xref="PID:g558120"
**   CDS_1_OUT_OF_1
**   10-AUG-1995 (Rel. 44, Last updated, Version 6)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   7 AA;  849 MW;  6B040339CDD33DB0 CRC64;
     AKAFKRE
//
ID   Q16467      PRELIMINARY;      PRT;    43 AA.
AC   Q16467;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-NOV-1996 (TrEMBLrel. 01, Last annotation update)
DE   PROTON ATPASE HOMOLOGUE (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Bhat K.S.;
RL   Submitted (NOV-1992) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; L05089; AAC15853.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human proton ATPase homologue mRNA, 3' end.
**   [1]
**   1-373
**   Bhat K.S.;
**   "Expressed sequence tags from a human cell line";
**   Unpublished.
**   source          1..373
**                   /organism="Homo sapiens"
**   CDS             1..132
**                   /note="Expressed Sequence Tag; amino acid sequence
**   shows
**                   homology with carboxy end of yeast proton ATPase
**                   proteolipid chain (PIR:A34633)"
**                   /note="putative"
**                   /codon_start=1
**                   /citation=[1]
**                   /partial
**                   /db_xref="PID:g190378"
**   CDS_1_OUT_OF_1
**   08-DEC-1992 (Rel. 34, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   43 AA;  4393 MW;  6E7292577E46BDB3 CRC64;
     VGSGAALADA QNPSLFVKIL IVEIFGSALA SLGSSSQFFR PPE
//
ID   Q16779      PRELIMINARY;      PRT;    51 AA.
AC   Q16779;
DT   01-NOV-1996 (TrEMBLrel. 01, Created)
DT   01-NOV-1996 (TrEMBLrel. 01, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   HEXOKINASE III (EC 2.7.1.1) (GLUCOKINASE) (HEXOKINASE TYPE IV)
DE   (FRAGMENT).
GN   HK3.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Colosimo A., Calabrese G., Gennarelli M., Ruzzo A., Sangiuolo F.,
RA   Magnani M., Palka G., Novelli G., DallaPiccola B.;
RL   Cytogenet. Cell Genet. 0:0-0(0).
CC   -!- CATALYTIC ACTIVITY: ATP + D-HEXOSE = ADP + D-HEXOSE 6-PHOSPHATE.
DR   EMBL; L37749; AAB03512.1; -.
KW   Transferase.
FT   NON_TER       1      1
FT   NON_TER      51     51
**
**   #################     SOURCE SECTION     ##################
**   Human hexokinase III (HK3) gene, partial cds.
**   [1]
**   1-245
**   Colosimo A., Calabrese G., Gennarelli M., Ruzzo A., Sangiuolo F.,
**   Magnani M., Palka G., Novelli G., Dallapiccola B.;
**   "Assignment of the hexokinase type 3 (HK3) gene to human
**   chromosome band 5q35.3 by somatic cell hybrids and in situ
**   hybridization";
**   Unpublished.
**   [2]
**   1-245
**   Colosimo A.;
**   ;
**   Submitted (11-NOV-1994) to the EMBL/GenBank/DDBJ databases.
**   Sanita' Pubblica e Biologia Cellulare, Cattedra di Genetica Umana
**   Universita, Via di Tor Vergata, 135, Rome 00133, Italy
**   source          1..245
**                   /organism="Homo sapiens"
**                   /clone="pHKIII"
**                   /map="5q35.3"
**                   /chromosome="5"
**   CDS             join(<1..103,197..>245)
**                   /gene="HK3"
**                   /EC_number="2.7.1.1"
**                   /codon_start=1
**                   /product="hexokinase III"
**                   /db_xref="PID:g1402644"
**   CDS_1_OUT_OF_1
**   09-JUL-1996 (Rel. 48, Last updated, Version 4)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   51 AA;  5505 MW;  52380713EE23C165 CRC64;
     TWSGGPLGTM ALWPCSAPAL MQVWTRRPST PASRGLKRWS AACTWVKSSA T
//
ID   Q92484      PRELIMINARY;      PRT;   177 AA.
AC   Q92484;
DT   01-FEB-1997 (TrEMBLrel. 02, Created)
DT   01-FEB-1997 (TrEMBLrel. 02, Last sequence update)
DT   01-FEB-1997 (TrEMBLrel. 02, Last annotation update)
DE   ACID SPHINGOMYELINASE-LIKE PHOSPHODIESTERASE (FRAGMENT).
GN   ASML3A.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Hofmann K.;
RL   Submitted (SEP-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; Y08136; CAA69330.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for ASM-like phosphodiesterase 3a
**   [1]
**   Hofmann K.;
**   "Acid Sphingomyelinase is a member of a multi-gene family and
**   shares motifs with a large family of metallo-phosphoesterases";
**   Unpublished.
**   [2]
**   1-863
**   Hofmann K.;
**   ;
**   Submitted (17-SEP-1996) to the EMBL/GenBank/DDBJ databases.
**   K. Hofmann, Isrec (Swiss Inst. F. Exp. Canc. Res.), Bioinformatics
**   Group, Chemin Des Boveresses 155, Ch/1066 Epalinges S/Lausanne,
**   SWITZERLAND
**   source          1..863
**                   /organism="Homo sapiens"
**   CDS             <1..536
**                   /codon_start=3
**                   /gene="ASML3a"
**                   /product="acid sphingomyelinase-like
**   phosphodiesterase"
**                   /note="putative"
**                   /db_xref="PID:e266650"
**   CDS_1_OUT_OF_1
**   19-SEP-1996 (Rel. 49, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   177 AA;  20634 MW;  CA38DDAE817B87EC CRC64;
     DIFQKYSDVI AGQFYGHTHR DSIMVLSDKK GSPVNSLFVA PAVTPVKSVL EKQTNNPGIR
     LFQYDPRDYK LLDMLQYYLN LTEANLKGES IWKLEYILTQ TYDIEDLQPE SLYGLAKQFT
     ILDSKQFIKY YNYFFVSYDS SVTCDKTCKA FQICAIMNLD NISYADCLKQ LYIKHKY
//
ID   Q92485      PRELIMINARY;      PRT;   465 AA.
AC   Q92485;
DT   01-FEB-1997 (TrEMBLrel. 02, Created)
DT   01-FEB-1997 (TrEMBLrel. 02, Last sequence update)
DT   01-JUN-2000 (TrEMBLrel. 14, Last annotation update)
DE   ACID SPHINGOMYELINASE-LIKE PHOSPHODIESTERASE.
GN   ASML3B.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Hofmann K.;
RL   Submitted (SEP-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; Y08134; CAA69328.1; -.
DR   INTERPRO; IPR000934; -.
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for ASM-like phosphodiesterase 3b
**   [1]
**   Hofmann K.;
**   "Acid Sphingomyelinase is a member of a multi-gene family and
**   shares motifs with a large family of metallo-phosphoesterases";
**   Unpublished.
**   [2]
**   1-1610
**   Hofmann K.;
**   ;
**   Submitted (17-SEP-1996) to the EMBL/GenBank/DDBJ databases.
**   K. Hofmann, Isrec (Swiss Inst. F. Exp. Canc. Res.), Bioinformatics
**   Group, Chemin Des Boveresses 155, Ch/1066 Epalinges S/Lausanne,
**   SWITZERLAND
**   source          1..1610
**                   /organism="Homo sapiens"
**   CDS             122..1519
**                   /gene="ASML3b"
**                   /product="acid sphingomyelinase-like
**   phosphodiesterase"
**                   /note="putative"
**                   /db_xref="PID:e266651"
**   CDS_1_OUT_OF_1
**   19-SEP-1996 (Rel. 49, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PROSITE; PS50185; PHOSPHO_ESTER; 21; 284; T; 19-JUN-2000;
SQ   SEQUENCE   465 AA;  51910 MW;  AE9FBF92A2C1ED32 CRC64;
     MRLLAWLIFL ANWGGARAEP GKFWHIADLH LDPDYKVSKD PFQVCPSAGS QPVPDAGPWG
     DYLCDSPWAL INSSIYAMKE IEPEPDFILW TGDDTPHVPD EKLGEAAVLE IVERLTKLIR
     EVFPDTKVYA ALGNHDFHPK NQFPAGSNNI YNQIAELWKP WLSNESIALF KKGAFYCEKL
     PGPSGAGRIV VLNTNLYYTS NALTADMADP GQQFQWLEDV LTDASKAGDM VYIVGHVPPG
     FFEKTQNKAW FREGFNEKYL KVVRKHHRVI AGQFFGHHHT DSFRMLYDDA GVPISAMFIT
     PGVTPWKTTL PGVVNGANNP AIRVFEYDRA TLSLXDMVTY FMNLSQANAQ GTPRWELEYQ
     LTEAYGVPDA SAHSIDTVLD RIAGDQSTLQ RYYVYNSVSY SAGVCDEACS MQHVCAMRQV
     DIDAYTTCLY ASGTTPVPQL PXLLMALLGL CTTRAVTCQA HHSSW
//
ID   Q92661      PRELIMINARY;      PRT;    55 AA.
AC   Q92661;
DT   01-FEB-1997 (TrEMBLrel. 02, Created)
DT   01-FEB-1997 (TrEMBLrel. 02, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   UV-B REPRESSED SEQUENCE, HUR 7 (FRAGMENT).
GN   HUR 7.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Abts H.F., Breuhahn K., Michel G., Esser P., Ruzicka T.;
RL   Submitted (MAY-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; X98307; CAA66951.1; -.
DR   HSSP; P05619; 1HLE.
DR   INTERPRO; IPR000215; -.
DR   PFAM; PF00079; serpin; 1.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   H.sapiens mRNA for UV-B repressed sequence, HUR 7
**   [1]
**   Abts H.F., Breuhahn K., Michel G., Esser P., Ruzicka T.;
**   "Analysis of UV-B modulated gene expression in human keratinocytes
**   by mRNA differential display PCR (DD-PCR)";
**   Unpublished.
**   [2]
**   1-405
**   Abts H.F.;
**   ;
**   Submitted (22-MAY-1996) to the EMBL/GenBank/DDBJ databases.
**   H.F. Abts, Heinrich-Heine-Universitaet Duesseldorf, Dermatologie,
**   Cytokinlabor, Geb.11.80, Moorenstrasse 5, 40225 Duesseldorf, FRG
**   source          1..405
**                   /organism="Homo sapiens"
**                   /cell_line="HaCaT"
**                   /cell_type="keratinocyte"
**   CDS             <1..170
**                   /codon_start=3
**                   /gene="HUR 7"
**                   /db_xref="PID:e260052"
**   CDS_1_OUT_OF_1
**   01-SEP-1996 (Rel. 49, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
**PM PFAM; PF00079; serpin; 2; 34; T; 19-JUN-2000;
SQ   SEQUENCE   55 AA;  6224 MW;  107A694FEA43F7E6 CRC64;
     LEDLQAKILG IPYKNNDLSM FVLLPNDIDG LEKVNAYTSL FFLSFPKAFC LRASE
//
ID   Q92771      PRELIMINARY;      PRT;   734 AA.
AC   Q92771;
DT   01-FEB-1997 (TrEMBLrel. 02, Created)
DT   01-FEB-1997 (TrEMBLrel. 02, Last sequence update)
DT   01-AUG-1998 (TrEMBLrel. 07, Last annotation update)
DE   HELICASE (FRAGMENT).
GN   CHLR2.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RA   Amann J.M., Kidd V.J., Lahti J.M.;
RL   Submitted (AUG-1995) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U33834; AAB06963.1; -.
KW   Helicase.
FT   NON_TER       1      1
FT   NON_TER     734    734
**
**   #################     SOURCE SECTION     ##################
**   Human CHL1-related helicase (CHLR2) mRNA, partial cds.
**   [1]
**   1-2202
**   Amann J.M., Kidd V.J., Lahti J.M.;
**   "Isolation and characterization of a human gene related to the
**   yeast chromosome transmission fidelity gene, CHL1";
**   Unpublished.
**   [2]
**   1-2202
**   Lahti J.M.;
**   ;
**   Submitted (11-AUG-1995) to the EMBL/GenBank/DDBJ databases.
**   Jill M. Lahti, St. Jude Children's Research Hospital, Tumor Cell
**   Biology, 332 N. Lauderdale St., Memphis, TN 38105, USA, 38105
**   source          1..2202
**                   /organism="Homo sapiens"
**                   /clone="human CHL-Related 2"
**                   /clone_lib="HeLa cDNA, K562 cDNA, human fetal liver
**   cDNA"
**   CDS             <1..>2202
**                   /gene="CHLR2"
**                   /codon_start=1
**                   /product="helicase"
**                   /db_xref="PID:g1517818"
**   CDS_1_OUT_OF_1
**   01-SEP-1996 (Rel. 49, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   734 AA;  82439 MW;  FBAC66E4801D5F73 CRC64;
     HRVQLKYAAK RLRQEEEERE NLLRLSREML ETGPEAEWLE QLESGEEELV LAEYESDEEK
     KVASGVDEDE DDLEEEHITK IYYCSRTHSQ LAQFVHEVKK SPFGKDVRLV SLGSQQNLCV
     NEDVRSLGSV QLINDRCVDM QRSRHEKKKG AEEEKPKRRR QEKQAACPFY NHEQMGLLRD
     EALAEVKDME QLLALGKEAR ACPYYRSRLA IPAAQLVVLS YQMLLHAATR QAAGIRLQDQ
     VVIIDEAHNL IDTITGMHSV EVSGSQLCQA HSQLLQYMER YGKRLKAKNL MYLKQILYLL
     EKFVAVLGGN IKQNPNTQSL SQTGMELKTI NDFLFQSQID NINLFKVQRY CEKSMISRKL
     FGFTERYGAV FSSREQPKLA GFQQFLQSLQ PRTTEALAAP ADESQASVPQ PASPLMHIEG
     FLAALTTANQ DGRVILSRQG SLSQSTLKFL LLNPAVHFAQ VVKECRAVVI AGGTMQPVSD
     FRQQLLACAG VEAERVVEFS CGHVIPPDNI LPLVICIGVS NQPLEFTFQK RDLPQMMDEV
     GRILCNLCSV VSGGVVCFFP SYEYLRQVHA HWEKGGLLGH LAARKKIFQE PKSAHQVEQV
     LLAYSRCIQA CGQERGPVTG ALLLSVVGGK MSEGINFSDN LGRCVVMVGM PFPNIRSAEL
     QEKMAYLDQT LPRAPGQAPP GKALVENLCM KAVNQSIGRA IRHQKDFASI VLLDQRYARP
     PVLAKLPAWI RARV
//
ID   Q92792      PRELIMINARY;      PRT;   177 AA.
AC   Q92792;
DT   01-FEB-1997 (TrEMBLrel. 02, Created)
DT   01-FEB-1997 (TrEMBLrel. 02, Last sequence update)
DT   01-FEB-1997 (TrEMBLrel. 02, Last annotation update)
DE   D13S824E LOCUS (FRAGMENT).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=BONE MARROW;
RA   Still I.;
RL   Submitted (JAN-1996) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; U47635; AAB18856.1; -.
FT   NON_TER       1      1
**
**   #################     SOURCE SECTION     ##################
**   Human D13S824E locus mRNA, complete cds.
**   [1]
**   1-2486
**   Still I.;
**   ;
**   Submitted (29-JAN-1996) to the EMBL/GenBank/DDBJ databases.
**   Ivan Still, Neurosciences, Cleveland Clinic Foundation, 9500 Euclid
**   Avenue, Cleveland, Ohio 44195, USA
**   Auffray, C. et al (1995). IMAGE: Integration molecular analysis of
**   the human genome and its expression. C.R. Acad. Sci. Paris 318:
**   263-272.
**   source          1..2486
**                   /organism="Homo sapiens"
**                   /chromosome="13"
**                   /tissue_type="bone marrow"
**                   /cell_type="HeLa"
**                   /clone_lib="Clontech catolog HL5005a; Stratagene
**   catalog
**                   936201"
**   CDS             <1..534
**                   /note="DSEG number: D13S824E; orf"
**                   /codon_start=1
**                   /db_xref="PID:g1669391"
**   CDS_1_OUT_OF_1
**   15-NOV-1996 (Rel. 49, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_HUMAN
**OX 9606;
SQ   SEQUENCE   177 AA;  20647 MW;  9005F0FE031F92B5 CRC64;
     KRRAQVEGED LFPVAISFGR PKEYFPPLYS SESHRFTVLE PNTVSFNFKF WRNMYHQFDR
     TLHPRQSVFN IIMNMNEQNK QLEKDIKDLE SKIKQRKNKQ TDGILTKELL HSVHPESPNL
     KTSLCFKEQT LLPVNDALRT IEGSSPADNR YSEYAEEFSK SEPAVVSLEY GVARMTC
//
ID   O54521      PRELIMINARY;      PRT;   144 AA.
AC   O54521;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-MAY-2000 (TrEMBLrel. 13, Last annotation update)
DE   SLYA.
GN   SLYA.
OS   Salmonella enterica serovar Typhi (made up common name to get long OS
OS   line){EI3}.
OC   Bacteria; Proteobacteria; gamma subdivision; Enterobacteriaceae;
OC   Salmonella.
OX   NCBI_TaxID=90370, 119912;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=TY2, RF-1;
RA   Kawakami T., Kaneko A., Sekiya K., Okada N., Imajho-Ohmi S.,
RA   Nonaka T., Matsui H., Kawahara K., Danbara H.;
RL   Submitted (FEB-1998) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; AB010777; BAA24582.1; -.
DR   EMBL; AB010776; BAA24581.1; -.
DR   InterPro; IPR000835; B_TESTDOMAIN.
DR   InterPro; IPR001835; A_TESTDOMAIN.
DR   Pfam; PF01047; MarR; 1.
DR   PRINTS; PR00598; HTHMARR.
DR   PROSITE; PS01117; HTH_MARR_FAMILY; 1.
DR   SMART; SM1234; Smarty.
**
**   #################     SOURCE SECTION     ##################
**   Salmonella choleraesuis serovar Typhi gene for SlyA, complete cds.
**   [1]
**   1-636
**   Okada N.;
**   ;
**   Submitted (27-JAN-1998) to the EMBL/GenBank/DDBJ databases.
**   Nobuhiko Okada, Kitasato University, School of Pharmaceutical
**   Sciences,
**   Department of Microbiology; 5-9-1 Shirokane, Minato, Tokyo 108-8641,
**   Japan (E-mail:okadan@platinum.pharm.kitasato-u.ac.j p,
**   Tel:03-3444-6161,
**   Fax:03-3444-4831)
**   [2]
**   Kawakami T., Kaneko A., Sekiya K., Okada N., Imajho-Ohmi S., Nonaka
**   T.,
**   Matsui H., Kawahara K., Danbara H.;
**   "Identification of TTG initiation codon in slyA of Salmonella, a gene
**   required for survival within macrophages";
**   Unpublished.
**   [1]
**   1-636
**   Okada N.;
**   ;
**   Submitted (27-JAN-1998) to the EMBL/GenBank/DDBJ databases.
**   Nobuhiko Okada, Kitasato University, School of Pharmaceutical
**   Sciences,
**   Department of Microbiology; 5-9-1 Shirokane, Minato, Tokyo 108-8641,
**   Japan (E-mail:okadan@platinum.pharm.kitasato-u.ac.j p,
**   Tel:03-3444-6161,
**   Fax:03-3444-4831)
**   [2]
**   Kawakami T., Kaneko A., Sekiya K., Okada N., Imajho-Ohmi S., Nonaka
**   T.,
**   Matsui H., Kawahara K., Danbara H.;
**   "Identification of TTG initiation codon in slyA of Salmonella, a gene
**   required for survival within macrophages";
**   Unpublished.
**   source          1..636
**                   /organism="Salmonella choleraesuis serovar Typhi"
**                   /sequenced_mol="DNA"
**                   /strain="Ty2"
**   CDS             154..588
**                   /codon_start=1
**                   /db_xref="PID:d1025502"
**                   /transl_table=11
**                   /gene="slyA"
**                   /product="SlyA"
**   CDS_IN_EMBL_ENTRY 1
**   ORGANISM DOESN'T EXIST IN SP
**   11-FEB-1998 (Rel. 54, Last updated, Version 2)
**   source          1..636
**                   /organism="Salmonella choleraesuis choleraesuis"
**                   /sequenced_mol="DNA"
**                   /strain="RF-1"
**   CDS             154..588
**                   /codon_start=1
**                   /db_xref="PID:d1025501"
**                   /transl_table=11
**                   /gene="slyA"
**                   /product="SlyA"
**   CDS_IN_EMBL_ENTRY 1
**   ORGANISM DOESN'T EXIST IN SP
**   11-FEB-1998 (Rel. 54, Last updated, Version 2)
**   #################    INTERNAL SECTION    ##################
**PM PFAM; PF01047; MarR; 29; 133; T; 19-JUN-2000;
**PM PRINTS; PR00598; HTHMARR; 47; 63; T; 19-JUN-2000;
**PM PRINTS; PR00598; HTHMARR; 64; 79; T; 19-JUN-2000;
**PM PRINTS; PR00598; HTHMARR; 83; 99; T; 19-JUN-2000;
**PM PRINTS; PR00598; HTHMARR; 113; 133; T; 19-JUN-2000;
**PM PROSITE; PS01117; HTH_MARR_FAMILY; 62; 96; T; 19-JUN-2000;
SQ   SEQUENCE   144 AA;  16448 MW;  4647F7704F2D78DE CRC64;
     MESPLGSDLA RLVRIWRALI DHRLKPLELT QTHWVTLHNI HQLPPDQSQI QLAKAIGIEQ
     PSLVRTLDQL EDKGLISRQT CASDRRAKRI KLTEKAEPLI AEMEEVIHKT RGEILAGISS
     EEIELLIKLV AKLEHNIMEL HSHD
//
ID   TRA9_MYCTU     STANDARD;      PRT;   278 AA.
AC   P19774;
DT   01-FEB-1991 (Rel. 17, Created)
DT   01-FEB-1991 (Rel. 17, Last sequence update)
DT   30-MAY-2000 (Rel. 39, Last annotation update)
DE   PUTATIVE TRANSPOSASE FOR INSERTION SEQUENCE ELEMENT IS986/IS6110
DE   (ORFB).
GN   (RV0796 OR MTV042.06) AND (RV2106 OR MTCY261.02) AND
GN   (RV2279 OR MTCY339.31C) AND (RV2355 OR MTCY98.24) AND
GN   (RV2814C OR MTCY16B7.29) AND (RV3185 OR MTV014.29) AND
GN   (RV3187 OR MTV014.31) AND (RV3326 OR MTV016.26).
OS   Mycobacterium tuberculosis.
OC   Bacteria; Firmicutes; Actinobacteria; Actinobacteridae;
OC   Actinomycetales; Corynebacterineae; Mycobacteriaceae; Mycobacterium.
OX   NCBI_TaxID=1773;
KW   Transposable element; Transposition; DNA-binding; DNA recombination.
SQ   SEQUENCE   278 AA;  31369 MW;  E4D33328228D7676 CRC64;
     MPIAPSTYYD HINREPSRRE LRDGELKEHI SRVHAANYGV YGARKVWLTL NREGIEVARC
     TVERLMTKLG LSGTTRGKAR RTTIADPATA RPADLVQRRF GPPAPNRLWV ADLTYVSTWA
     GFAYVAFVTD AYARRILGWR VASTMATSMV LDAIEQAIWT RQQEGVLDLK DVIHHTDRGS
     QYTSIRFSER LAEAGIQPSV GAVGSSYDNA LAETINGLYK TELIKPGKPW RSIEDVELAT
     ARWVDWFNHR RLYQYCGDVP PVELEAAYYA QRQRPAAG
//
ID   Q9ZQ91      PRELIMINARY;      PRT;   312 AA.
AC   Q9ZQ91;
DT   01-MAY-1999 (TrEMBLrel. 10, Created)
DT   01-MAY-1999 (TrEMBLrel. 10, Last sequence update)
DT   01-JUN-2001 (TrEMBLrel. 17, Last annotation update)
DE   PUTATIVE HYDROLASE (CONTAINS AN ESTERASE/LIPASE/THIOESTERASE ACTIVE
DE   SITE SERINE DOMAIN (PROSITE: PS50187).
GN   T4M8.1.
OS   Arabidopsis thaliana (Mouse-ear cress).
OC   Eukaryota; Viridiplantae; Embryophyta; Tracheophyta; Spermatophyta;
OC   Magnoliophyta; eudicotyledons; core eudicots; Rosidae; eurosids II;
OC   Brassicales; Brassicaceae; Arabidopsis.
OX   NCBI_TaxID=3702;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=CV. COLUMBIA;
RA   Lin X., Kaul S., Shea T.P., Fujii C.Y., Shen M., VanAken S.E.,
RA   Barnstead M.E., Mason T.M., Bowman C.L., Ronning C.M., Benito M.,
RA   Carrera A.J., Creasy T.H., Buell C.R., Town C.D., Nierman W.C.,
RA   Fraser C.M., Venter J.C.;
RT   "Arabidopsis thaliana chromosome II BAC T4M8 genomic sequence.";
RL   Submitted (MAR-1999) to the EMBL/GenBank/DDBJ databases.
DR   EMBL; AC006284; AAD17422.1; -.
DR   InterPro; IPR000379; Est_lip_thioest_actsite.
KW   Hydrolase.
**
**   #################     SOURCE SECTION     ##################
**   Arabidopsis thaliana chromosome II BAC T4M8 genomic sequence,
**   complete sequence.
**   [1]
**   1-89137
**   Lin X., Kaul S., Shea T.P., Fujii C.Y., Shen M., VanAken S.E.,
**   Barnstead M.E., Mason T.M., Bowman C.L., Ronning C.M., Benito M.,
**   Carrera A.J., Creasy T.H., Buell C.R., Town C.D., Nierman W.C.,
**   Fraser C.M., Venter J.C.;
**   "Arabidopsis thaliana chromosome II BAC T4M8 genomic sequence";
**   Unpublished.
**   [2]
**   1-89137
**   Lin X., Kaul S.;
**   ;
**   Submitted (05-JAN-1999) to the EMBL/GenBank/DDBJ databases.
**   The Institute for Genomic Research, 9712 Medical Center Dr, Rockville,
**   MD 20850, USA, xlin@tigr.org
**   [3]
**   1-89137
**   Lin X.;
**   ;
**   Submitted (04-MAR-1999) to the EMBL/GenBank/DDBJ databases.
**   The Institute for Genomic Research, 9712 Medical Center Dr., Rockville,
**   MD 20850, USA
**   On Mar 4, 1999 this sequence version replaced gi:4156124.
**   Address all correspondence to:
**   Xiaoying Lin
**   The Institute for Genomic Research
**   9712 Medical Center Dr.
**   Rockville, MD 20850, USA
**   e-mail: xlin@tigr.org
**   BAC clone T4M8 is from Arabidopsis chromosome II and is contained
**   in the YAC clone CIC11A04.
**   The orientation of the sequence is from SP6 to T7 end of the BAC
**   clone.
**   Genes were identified by a combination of three methods: Gene
**   prediction programs including GRAIL (available by anonymous ftp
**   from arthur.epm.ornl.gov), Genefinder (Phil Green, University of
**   Washington), Genscan (Chris Burge,
**   http://gnomic.stanford.edu/~chris/GENSCANW.html), and NetPlantGene
**   (http://www.cbs.dtu.dk/netpgene/cbsnetpgene.html), searches of the
**   complete sequence against a peptide database and the Arabidopsis
**   EST database at TIGR (http://www.tigr.org/tdb/at/at.html).
**   Annotated genes are named to indicate the level of evidence for
**   their annotation. Genes with similarity to other proteins are named
**   after the database hits. Genes without significant peptide
**   similarity but with EST similarity are named as 'unknown' proteins.
**   Genes without protein or EST similarity, that are predicted by more
**   than two gene prediction programs over most of their length are
**   annotated as 'hypothetical' proteins. Genes encoding tRNAs are
**   predicted by tRNAscan-SE (Sean Eddy,
**   http://genome.wustl.edu/eddy/tRNAscan-SE/). Simple repeats are
**   identified by repeatmasker (Arian Smit,
**   http://ftp.genome.washington.edu/RM/RepeatMasker.html). Regions of
**   genomic sequence that are not annotated as genes but have predicted
**   exons by GRAIL are annotated as misc features.
**   source          1..89137
**                   /organism="Arabidopsis thaliana"
**                   /chromosome="II"
**                   /db_xref="taxon:3702"
**                   /cultivar="Columbia"
**                   /map="CIC11A04"
**                   /clone="T4M8"
**   CDS             complement(1731..2669)
**                   /codon_start=1
**                   /db_xref="PID:g4335745"
**                   /gene="T4M8.1"
**                   /product="putative hydrolase (contains an
**                   esterase/lipase/thioesterase active site serine domain
**                   (prosite: PS50187)"
**                   /protein_id="AAD17422.1"
**   CDS_IN_EMBL_ENTRY 30
**   12-MAR-1999 (Rel. 59, Last updated, Version 4)
**   #################    INTERNAL SECTION    ##################
**ID XXXX_ARATH
**PM PROSITE; PS50187; ESTERASE; 69; 174; T; 28-JAN-2000;
SQ   SEQUENCE   312 AA;  34750 MW;  1D9B933F2BE9DD78 CRC64;
     MDSVIAFDRS PMFRVYKSGR IERLLGETTV PPSLTPQNGV VSKDIIHSPE KNLSLRIYLP
     EKVTVKKLPI LIYFHGGGFI IETAFSPPYH TFLTSAVAAA NCLAISVNYR RAPEFPVPIP
     YEDSWDSLKW VLTHITGTGP ETWINKHGDF GKVFLAGDSA GGNISHHLTM RAKKEKLCDS
     LISGIILIHP YFWSKTPIDE FEVRDVGKTK GVEGSWRVAS PNSKQGVDDP WLNVVGSDPS
     GLGCGRVLVM VAGDDLFVRQ GWCYAEKLKK SGWEGEVEVM ETKNEGHVFH LKNPNSDNAR
     QVVKKLEEFI NK
//
ID   Q9JSZ7      PRELIMINARY;      PRT;   355 AA.
AC   Q9JSZ7;
DT   01-OCT-2000 (TrEMBLrel. 15, Created)
DT   01-OCT-2000 (TrEMBLrel. 15, Last sequence update)
DT   01-OCT-2000 (TrEMBLrel. 15, Last annotation update)
DE   UDP-N-acetylglucosamine--N-acetylmuramyl-(pentape pyrophosphoryl-
DE   undecaprenol N-acetylglucosamine transferase (EC 2.4.1.){EI1}.
GN   MURG OR NMA2062{EP4}.
OS   Neisseria meningitidis (serogroup A){EP3}.
OC   Bacteria; Proteobacteria; beta subdivision; Neisseriaceae; Neisseria.
OX   NCBI_TaxID=65699;
RN   [1]{EP3}
RP   SEQUENCE FROM N.A.
RC   STRAIN=Z2491 / SEROGROUP A / SEROTYPE 4A;
RX   MEDLINE=20222556; PubMed=10761919;
RA   Parkhill J., Achtman M., James K.D., Bentley S.D., Churcher C.,
RA   Klee S.R., Morelli G., Basham D., Brown D., Chillingworth T.,
RA   Davies R.M., Davis P., Devlin K., Feltwell T., Hamlin N., Holroyd S.,
RA   Jagels K., Leather S., Moule S., Mungall K., Quail M.A.,
RA   Rajandream M.A., Rutherford K.M., Simmonds M., Skelton J.,
RA   Whitehead S., Spratt B.G., Barrell B.G.;
RT   "Complete DNA sequence of a serogroup A strain of Neisseria
RT   menigitidis Z2491.";
RL   Nature 404:502-506(2000).
DR   EMBL; AL162758; CAB85280.1; -.{EI1}
KW   Transferase{EP2}; Glycosyltransferase{EP2}; Complete proteome{EP5}.
**
**   #################     SOURCE SECTION     ##################
**   Neisseria meningitidis serogroup A strain Z2491 complete genome; segment
**   7/7
**   [1]
**   1-195767
**   Parkhill J., Achtman M., James K.D., Bentley S.D., Churcher C., Klee S.R.,
**   Morelli G., Basham D., Brown D., Chillingworth T., Davies R.M., Davis P.,
**   Devlin K., Feltwell T., Hamlin N., Holroyd S., Jagels K., Leather S.,
**   Moule S., Mungall K., Quail M.A., Rajandream M.A., Rutherford K.M.,
**   Simmonds M., Skelton J., Whitehead S., Spratt B.G., Barrell B.G.;
**   "Complete DNA sequence of a serogroup A strain of Neisseria menigitidis
**   Z2491";
**   Nature 404:502-506(2000).
**   [2]
**   1-195767
**   Parkhill J.;
**   ;
**   Submitted (30-MAR-2000) to the EMBL/GenBank/DDBJ databases.
**   Submitted on behalf of the Neisseria sequencing team, Sanger Centre,
**   Wellcome Trust Genome Campus, Hinxton, Cambridge CB10 1SA E-mail:
**   parkhill@sanger.ac.uk
**   Notes:
**
**   Details of N. meningitidis sequencing at the Sanger Centre
**   are available on the World Wide Web.
**   (URL, http://www.sanger.ac.uk/Projects/N_meningitidis/)
**
**   source          1..195767
**                   /db_xref="taxon:487"
**                   /note="serogroup: A"
**                   /organism="Neisseria meningitidis"
**                   /strain="Z2491"
**   CDS             complement(21643..22710)
**                   /note="NMA2062, murG,
**                   UDP-N-acetylglucosamine--N-acetylmuramyl-(pentapeptide)
**                   pyrophosphoryl-undecaprenol N-acetylglucosamine
**                   transferase, len: 355aa; similar to many eg. SW:P17443
**                   (MURG_ECOLI) murG,
**                   UDP-N-acetylglucosamine--N-acetylmuramyl-(pentapeptide)
**                   pyrophosphoryl-undecaprenol N-acetylglucosamine transferase
**                   from Escherichia coli (354 aa) fasta scores; E(): 0, 46.2%
**                   identity in 346 aa overlap."
**                   /transl_table=11
**                   /gene="murG"
**                   /product="UDP-N-acetylglucosamine--N-acetylmuramyl-(pentape
**                   pyrophosphoryl-undecaprenol N-acetylglucosamine
**                   transferase"
**                   /EC_number="2.4.1.-"
**                   /protein_id="CAB85280.1"
**   misc_feature    complement(22163..22172)
**                   /label=DUS
**                   /note="Core DNA uptake sequence: gccgtctgaa"
**                   AA 181 -> 183
**   CDS_IN_EMBL_ENTRY 179
**   30-MAR-2000 (Rel. 63, Last updated, Version 1)
**   #################    INTERNAL SECTION    ##################
**CP 65699; Chromosome; 2011349; -1065; ATG; ; AL157959.1.
**EV EI1; EMBL; -; CAB85280.1; 21-AUG-2000.
**EV EP2; TREMBL; -; CAB85280.1; 21-AUG-2000.
**EV EP3; RefFix; -; -; 20-SEP-2000.
**EV EP4; GenFix; -; v1.2; 20-SEP-2000.
**EV EP5; ProtChange; -; addKW; 01-MAY-2001.
**ID XXXX_NEIME
SQ   SEQUENCE   355 AA;  38056 MW;  DD93836BB897C401 CRC64;
     MGGKTFMLMA GGTGGHIFPA LAVADSLRAR GHHVIWLGSK DSMEERIVPQ YDILLETLAI
     KGVRGNGIKR KLMLPFTLYQ TVREAQQIIR KHRVECVIGF GGFVTFPGGL AAKLLGVPIV
     IHEQNAVAGL SNRHLSRWAK RVLYAFPKAF SHEGGLVGNP VRADISNLPV PAERFQGREG
     RLKILVVGGS LGADVLNKTV PQALALLPDN ARPQMYHQSG RGKLGSLQAD YDALGVQAEC
     VEFITDMVSA YRDADLVICR AGALTIAELT AAGLGALLVP YPHAVDDHQT ANARFMVQAE
     AGLLLPQTQL TAEKLAEILG GLNREKCLKW AENARTLALP HSADDVAEAA IACAA
//
ID   ALYS_MYCPH     STANDARD;      PRT;    17 AA.
AC   P81528;
DT   15-JUL-1999 (Rel. 38, Created)
DT   15-JUL-1999 (Rel. 38, Last sequence update)
DT   30-MAY-2000 (Rel. 39, Last annotation update)
DE   Autolysin (EC 3.5.1.28) (N-acetylmuramoyl-L-alanine amidase)
DE   (Peptidoglycan hydrolase) (Fragment).
GN   LYTA.
OS   Mycobacterium phlei.
OC   Bacteria; Firmicutes; Actinobacteria; Actinobacteridae;
OC   Actinomycetales; Corynebacterineae; Mycobacteriaceae; Mycobacterium.
OX   NCBI_TaxID=1771;
RN   [1]
RP   SEQUENCE.
RC   STRAIN=425;
RX   MEDLINE=99140149; PubMed=10206696;
RA   Li Z.S., Beveridge T.J., Betts J., Clarke A.J.;
RT   "Partial characterization of a major autolysin from Mycobacterium
RT   phlei.";
RL   Microbiology 145:169-176(1999).
CC   -!- CATALYTIC ACTIVITY: HYDROLYZES THE LINK BETWEEN N-ACETYLMURAMOYL
CC       RESIDUES AND L-AMINO ACID RESIDUES IN CERTAIN BACTERIAL CELL-WALL
CC       GLYCOPEPTIDES.
CC   -!- MISCELLANEOUS: THE OPTIMUM PH OF THIS ENZYME IS 7.5.
KW   Hydrolase; Cell wall.
FT   VARIANT       1      1       V -> I OR L.
FT   VARIANT       2      2       A -> G.
FT   VARIANT      14     14       I -> L.
FT   NON_TER       1      1
FT   NON_TER      17     17
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   17 AA;  1817 MW;  1FACA3240F5C4EC5 CRC64;
     VAVKATTTEE ETEIPAK
//
ID   GAG_HV1MN      STANDARD;      PRT;   506 AA.
AC   P05888;
DT   01-NOV-1988 (Rel. 09, Created)
DT   01-FEB-1994 (Rel. 28, Last sequence update)
DT   15-JUL-1998 (Rel. 36, Last annotation update)
DE   GAG POLYPROTEIN [Contains: CORE PROTEINS P17, P24, P2, P7, P1, P6].
GN   GAG.
OS   Human immunodeficiency virus type 1 (MN isolate) (HIV-1).
OC   Viruses; Retroid viruses; Retroviridae; Lentivirus.
OX   NCBI_TaxID=11696;
RN   [1]
RP   SEQUENCE, AND POST-TRANSLATIONAL MODIFICATIONS.
RX   MEDLINE=92194415; PubMed=1548743;
RA   Henderson L.E., Bowers M.A., Sowder R.C. II, Serabyn S.A.,
RA   Johnson D.G., Bess J.W. Jr., Arthur L.O., Bryant D.K., Fenselau C.;
RT   "Gag proteins of the highly replicative MN strain of human
RT   immunodeficiency virus type 1: posttranslational modifications,
RT   proteolytic processings, and complete amino acid sequences.";
RL   J. Virol. 66:1856-1865(1992).
RN   [2]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=88219542; PubMed=3369091;
RA   Gurgo C., Guo H.-G., Franchini G., Aldovini A., Collalti E.,
RA   Farrell K., Wong-Staal F., Gallo R.C., Reitz M.S. Jr.;
RT   "Envelope sequences of two new United States HIV-1 isolates.";
RL   Virology 164:531-536(1988).
RN   [3]
RP   STRUCTURE BY NMR OF 380-434.
RX   MEDLINE=93278285; PubMed=1304355;
RA   Summers M.F., Henderson L.E., Chance M.R., Bess J.W. Jr., South T.L.,
RA   Blake P.R., Sagi I., Perez-Alvarado G., Sowder R.C. III, Hare D.R.,
RA   Arthur L.O.;
RT   "Nucleocapsid zinc fingers detected in retroviruses: EXAFS studies of
RT   intact viruses and the solution-state structure of the nucleocapsid
RT   protein from HIV-1.";
RL   Protein Sci. 1:563-574(1992).
CC   -!- FUNCTION: PERFORMS HIGHLY COMPLEX ORCHESTRATED TASKS DURING THE
CC       ASSEMBLY, BUDDING, MATURATION, AND INFECTION STAGES OF THE VIRAL
CC       REPLICATION CYCLE. DURING VIRAL ASSEMBLY, THE PROTEINS FORM
CC       MEMBRANE ASSOCIATIONS AND SELF-ASSOCIATIONS THAT ULTIMATELY RESULT
CC       IN BUDDING OF AN IMMATURE VIRION FROM THE INFECTED CELL. GAG
CC       PRECURSORS ALSO FUNCTION DURING VIRAL ASSEMBLY TO SELECTIVELY BIND
CC       AND PACKAGE TWO PLUS STRANDS OF GENOMIC RNA.
CC   -!- PTM: THE P24 PROTEIN IS PHOSPHORYLATED.
CC   -!- MISCELLANEOUS: THE MN ISOLATE WAS TAKEN FROM A PEDIATRIC AIDS
CC       PATIENT IN 1984.
CC   --------------------------------------------------------------------------
CC   Copyrighted by the UniProt Consortium, see http://www.uniprot.org/terms
CC   Distributed under the Creative Commons Attribution-NoDerivs License.
CC   --------------------------------------------------------------------------
DR   EMBL; M17449; AAA44853.1; -.
DR   PIR; A38068; A38068.
DR   PDB; 1AAF; 31-JAN-94.
DR   HIV; M17449; GAG$MN.
DR   InterPro; IPR000721; Gag_p24.
DR   InterPro; IPR000071; Retroviral_gag_p17.
DR   InterPro; IPR001878; Znf_CCHC.
DR   Pfam; PF00540; gag_p17; 1.
DR   Pfam; PF00607; gag_p24; 1.
DR   Pfam; PF00098; zf-CCHC; 2.
DR   PRINTS; PR00234; HIV1MATRIX.
DR   PRINTS; PR00939; C2HCZNFINGER.
DR   SMART; SM00343; ZnF_C2HC; 2.
KW   AIDS; Core protein; Polyprotein; Myristate; Phosphorylation;
KW   Zinc-finger; 3D-structure.
FT   INIT_MET      0      0
FT   CHAIN         1    134       CORE PROTEIN P17 (MATRIX ANTIGEN).
FT   CHAIN       135    365       CORE PROTEIN P24 (CORE ANTIGEN).
FT   CHAIN       366    379       CORE PROTEIN P2.
FT   CHAIN       380    434       CORE PROTEIN P7 (NUCLEOCAPSID PROTEIN).
FT   CHAIN       435    450       CORE PROTEIN P1.
FT   CHAIN       451    506       CORE PROTEIN P6.
FT   ZN_FING     394    407       C2HC-TYPE.
FT   LIPID         1      1       MYRISTATE.
FT   VARIANT      34     34       V -> I.
FT   VARIANT      45     45       I -> V.
FT   VARIANT      74     74       R -> L OR S OR N.
FT   VARIANT      92     92       K -> E.
FT   CONFLICT     17     17       K -> N (IN REF. 2).
FT   CONFLICT    141    141       Q -> E (IN REF. 2).
FT   CONFLICT    220    220       A -> V (IN REF. 2).
FT   CONFLICT    226    226       A -> T (IN REF. 2).
FT   CONFLICT    318    319       WM -> RT (IN REF. 2).
FT   CONFLICT    447    448       PG -> R (IN REF. 2).
SQ   SEQUENCE   506 AA;  56630 MW;  AC6F3CEB691C4726 CRC64;
     GARASVLSGG ELDRWEKIRL RPGGKKKYKL KHVVWASREL ERFAINPGLL ETSEGCRQIL
     GQLQPSLQTG SEERKSLYNT VATLYCVHQK IKIKDTKEAL EKIEEEQNKS KKKAQQAAAD
     TGNRGNSSQV SQNYPIVQNI QGQMVHQAIS PRTLNAWVKV VEEKAFSPEV IPMFSALSEG
     ATPQDLNTML NTVGGHQAAM QMLKETINEE AAEWDRLHPA HAGPIAPGQM REPRGSDIAG
     TTSTLQEQIG WMTNNPPIPV GEIYKRWIIL GLNKIVRMYS PSSILDIRQG PKEPFRDYVD
     RFYKTLRAEQ ASQEVKNWMT ETLLVQNANP DCKTILKALG PAATLEEMMT ACQGVGGPGH
     KARVLAEAMS QVTNSATIMM QRGNFRNQRK IIKCFNCGKE GHIAKNCRAP RKRGCWKCGK
     EGHQMKDCTE RQANFLGKIW PSCKGRPGNF PQSRTEPTAP PEESFRFGEE TTTPYQKQEK
     KQETIDKDLY PLASLKSLFG NDPLSQ
//
ID   LDLR_HUMAN     STANDARD;      PRT;   860 AA.
AC   P01130;
DT   21-JUL-1986 (Rel. 01, Created)
DT   21-JUL-1986 (Rel. 01, Last sequence update)
DT   01-OCT-2000 (Rel. 40, Last annotation update)
DE   Low-density lipoprotein receptor precursor (LDL receptor).
GN   LDLR.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606;
FT   VARIANT     667    667       C -> Y (IN FRENCH CANADIAN-2; 5% OF
FT                                FRENCH CANADIANS).
FT                                /FTId=VAR_005407.
FT   VARIANT     685    685       P -> L (IN GUJERAT/ZAMBIA/BELGIAN/DUTCH/
FT                                SWEDEN/JAPAN).
**
**   #################    INTERNAL SECTION    ##################
**CL 19p13.3;
SQ   SEQUENCE   860 AA;  95376 MW;  A4C28E9B8BADAD5E CRC64;
     MGPWGWKLRW TVALLLAAAG TAVGDRCERN EFQCQDGKCI SYKWVCDGSA ECQDGSDESQ
     ETCLSVTCKS GDFSCGGRVN RCIPQFWRCD GQVDCDNGSD EQGCPPKTCS QDEFRCHDGK
     CISRQFVCDS DRDCLDGSDE ASCPVLTCGP ASFQCNSSTC IPQLWACDND PDCEDGSDEW
     PQRCRGLYVF QGDSSPCSAF EFHCLSGECI HSSWRCDGGP DCKDKSDEEN CAVATCRPDE
     FQCSDGNCIH GSRQCDREYD CKDMSDEVGC VNVTLCEGPN KFKCHSGECI TLDKVCNMAR
     DCRDWSDEPI KECGTNECLD NNGGCSHVCN DLKIGYECLC PDGFQLVAQR RCEDIDECQD
     PDTCSQLCVN LEGGYKCQCE EGFQLDPHTK ACKAVGSIAY LFFTNRHEVR KMTLDRSEYT
     SLIPNLRNVV ALDTEVASNR IYWSDLSQRM ICSTQLDRAH GVSSYDTVIS RDIQAPDGLA
     VDWIHSNIYW TDSVLGTVSV ADTKGVKRKT LFRENGSKPR AIVVDPVHGF MYWTDWGTPA
     KIKKGGLNGV DIYSLVTENI QWPNGITLDL LSGRLYWVDS KLHSISSIDV NGGNRKTILE
     DEKRLAHPFS LAVFEDKVFW TDIINEAIFS ANRLTGSDVN LLAENLLSPE DMVLFHNLTQ
     PRGVNWCERT TLSNGGCQYL CLPAPQINPH SPKFTCACPD GMLLARDMRS CLTEAEAAVA
     TQETSTVRLK VSSTAVRTQH TTTRPVPDTS RLPGATPGLT TVEIVTMSHQ ALGDVAGRGN
     EKKPSSVRAL SIVLPIVLLV FLCLGVFLLW KNWRLKNINS INFDNPVYQK TTEDEVHICH
     NQDGYSYPSR QMVSLEDDVA
//
ID   H13_RABIT      STANDARD;      PRT;   213 AA.
AC   P02251;
DT   21-JUL-1986 (Rel. 01, Created)
DT   21-JUL-1986 (Rel. 01, Last sequence update)
DT   15-JUL-1999 (Rel. 38, Last annotation update)
DE   Histone H1.3.
OS   Oryctolagus cuniculus (Rabbit).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Lagomorpha; Leporidae; Oryctolagus.
OX   NCBI_TaxID=9986;
RN   [1]
RP   SEQUENCE.
RA   Hsiang M., Largman C.R., Cole R.D.;
**   /NO TITLE.
RL   Unpublished results, cited by:
RL   Cole R.D.;
RL   (In) Ts'o P.O.P. (eds.);
RL   The molecular biology of the mammalian genetic apparatus, pp.1:93-104,
RL   Elsevier, Amsterdam (1977).
RN   [2]
RP   SEQUENCE OF 1-72.
RX   MEDLINE=72068710; PubMed=5167020;
RA   Rall S.C., Cole R.D.;
RT   "Amino acid sequence and sequence variability of the amino-terminal
RT   regions of lysine-rich histones.";
RL   J. Biol. Chem. 246:7175-7190(1971).
RN   [3]
RP   SEQUENCE OF 73-107.
RX   MEDLINE=74143498; PubMed=4822503;
RA   Jones G.M.T., Rall S.C., Cole R.D.;
RT   "Extension of the amino acid sequence of a lysine-rich histone.";
RL   J. Biol. Chem. 249:2548-2553(1974).
CC   -!- FUNCTION: HISTONES H1 ARE NECESSARY FOR THE CONDENSATION OF
CC       NUCLEOSOME CHAINS INTO HIGHER ORDER STRUCTURES.
CC   -!- SUBCELLULAR LOCATION: NUCLEAR.
CC   -!- SIMILARITY: BELONGS TO THE HISTONE H1/H5 FAMILY.
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing, Alternative initiation; Named isoforms=1;
CC         Comment=This is just a test;
CC       Name=VI;
CC         IsoId=P02251-1; Sequence=Displayed;
DR   PIR; A02578; HSRB13.
DR   HSSP; P08287; 1GHC.
DR   InterPro; IPR001386; Linker_histone.
DR   Pfam; PF00538; linker_histone; 1.
DR   SMART; SM00526; H15; 1.
KW   Chromosomal protein; Nuclear protein; DNA-binding; Multigene family;
KW   Acetylation.
FT   DOMAIN       37    110       GLOBULAR.
FT   MOD_RES       1      1       ACETYLATION.
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   213 AA;  21423 MW;  21DE34BBD10D894E CRC64;
     SEAPAETAAP APAEKSPAKK KKAAKKPGAG AAKRKAAGPP VSELITKAVA ASKERNGLSL
     AALKKALAAG GYDVEKNNSR IKLGLKSLVS KGTLVETKGT GASGSFKLDK KAASGEAKPK
     PKKAGAAKPK KPAGATPKKP KKAAGAKKAV KKTPKKAPKP KAAAKPKVAK PKSPAKVAKS
     PKKAKAVKPK AAKPKAPKPK AAKAKKTAAK KKK
//
ID   ANGT_HUMAN     STANDARD;      PRT;   485 AA.
AC   P01019; Q16358; Q16359; Q96F91;
DT   21-JUL-1986 (Rel. 01, Created)
DT   21-JUL-1986 (Rel. 01, Last sequence update)
DT   28-FEB-2003 (Rel. 41, Last annotation update)
DE   Angiotensinogen precursor [Contains: Angiotensin I (Ang I);
DE   Angiotensin II (Ang II); Angiotensin III (Ang III) (Des-Asp[1]-
DE   angiotensin II)].
GN   AGT OR SERPINA8.
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606;
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=89170129; PubMed=2924688;
RA   Gaillard I., Clauser E., Corvol P.;
RT   "Structure of human angiotensinogen gene.";
RL   DNA 8:87-99(1989).
RN   [2]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=85000455; PubMed=6089875;
RA   Kageyama R., Ohkubo H., Nakanishi S.;
RT   "Primary structure of human preangiotensinogen deduced from the cloned
RT   cDNA sequence.";
RL   Biochemistry 23:3603-3609(1984).
RN   [3]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=90237063; PubMed=1692023;
RA   Fukamizu A., Takahashi S., Seo M.S., Tada M., Tanimoto K., Uehara S.,
RA   Murakami K.;
RT   "Structure and expression of the human angiotensinogen gene.
RT   Identification of a unique and highly active promoter.";
RL   J. Biol. Chem. 265:7576-7582(1990).
RN   [4]
RP   SEQUENCE FROM N.A.
RC   TISSUE=Brain;
RA   Strausberg R.;
RL   Submitted (JUL-2001) to the EMBL/GenBank/DDBJ databases.
RN   [5]
RP   SEQUENCE OF 1-338 FROM N.A.
RX   MEDLINE=87244745; PubMed=2885106;
RA   Kunapuli S.P., Kumar A.;
RT   "Molecular cloning of human angiotensinogen cDNA and evidence for the
RT   presence of its mRNA in rat heart.";
RL   Circ. Res. 60:786-790(1987).
RN   [6]
RP   SEQUENCE OF 34-45, AND SUBUNITS.
RC   TISSUE=Serum;
RX   MEDLINE=95293954; PubMed=7539791;
RA   Oxvig C., Haaning J., Kristensen L., Wagner J.M., Rubin I.,
RA   Stigbrand T., Gleich G.J., Sottrup-Jensen L.;
RT   "Identification of angiotensinogen and complement C3dg as novel
RT   proteins binding the proform of eosinophil major basic protein in
RT   human pregnancy serum and plasma.";
RL   J. Biol. Chem. 270:13645-13651(1995).
RN   [7]
RP   SEQUENCE OF 34-43.
RX   MEDLINE=69014170; PubMed=4300938;
RA   Arakawa K., Minohara A., Yamada J., Nakamura M.;
RT   "Enzymatic degradation and electrophoresis of human angiotensin I.";
RL   Biochim. Biophys. Acta 168:106-112(1968).
RN   [8]
RP   CARBOHYDRATE-LINKAGE SITES.
RX   MEDLINE=86056581; PubMed=3934016;
RA   Campbell D.J., Bouhnik J., Coezy E., Menard J., Corvol P.;
RT   "Processing of rat and human angiotensinogen precursors by microsomal
RT   membranes.";
RL   Mol. Cell. Endocrinol. 43:31-40(1985).
RN   [9]
RP   FUNCTION OF ANGIOTENSIN III.
RX   MEDLINE=75166949; PubMed=1132082;
RA   Goodfriend T.L., Peach M.J.;
RT   "Angiotensin III: (DES-Aspartic Acid-1)-Angiotensin II. Evidence and
RT   speculation for its role as an important agonist in the renin -
RT   angiotensin system.";
RL   Circ. Res. 36:38-48(1975).
RN   [10]
RP   STRUCTURE BY NMR OF ANGIOTENSIN II.
RX   MEDLINE=98151281; PubMed=9492317;
RA   Carpenter K.A., Wilkes B.C., Schiller P.W.;
RT   "The octapeptide angiotensin II adopts a well-defined structure in a
RT   phospholipid environment.";
RL   Eur. J. Biochem. 251:448-453(1998).
RN   [11]
RP   VARIANTS MET-207; THR-268 AND CYS-281.
RX   MEDLINE=93008239; PubMed=1394429;
RA   Jeunemaitre X., Soubrier F., Kotelevtsev Y.V., Lifton R.P.,
RA   Williams C.S., Charru A., Hunt S.C., Hopkins P.N., Williams R.R.,
RA   Lalouel J.-M., Corvol P.;
RT   "Molecular basis of human hypertension: role of angiotensinogen.";
RL   Cell 71:169-180(1992).
RN   [12]
RP   VARIANT THR-268.
RX   MEDLINE=93291876; PubMed=8513325;
RA   Ward K., Hata A., Jeunemaitre X., Helin C., Nelson L., Namikawa C.,
RA   Farrington P.F., Ogasawara M., Suzumori K., Tomoda S., Berrebi S.,
RA   Sasaki M., Corvol P., Lifton R.P., Lalouel J.-M.;
RT   "A molecular variant of angiotensinogen associated with
RT   preeclampsia.";
RL   Nat. Genet. 4:59-61(1993).
RN   [13]
RP   VARIANTS ILE-242; ARG-244 AND CYS-281.
RX   MEDLINE=95331754; PubMed=7607642;
RA   Hixson J.E., Powers P.K.;
RT   "Detection and characterization of new mutations in the human
RT   angiotensinogen gene (AGT).";
RL   Hum. Genet. 96:110-112(1995).
RN   [14]
RP   CHARACTERIZATION OF VARIANT CYS-281.
RX   MEDLINE=96199253; PubMed=8621667;
RA   Gimenez-Roqueplo A.P., Leconte I., Cohen P., Simon D., Guyene T.T.,
RA   Celerier J., Pau B., Corvol P., Clauser E., Jeunemaitre X.;
RT   "The natural mutation Y248C of human angiotensinogen leads to abnormal
RT   glycosylation and altered immunological recognition of the protein.";
RL   J. Biol. Chem. 271:9838-9844(1996).
CC   -!- FUNCTION: IN RESPONSE TO LOWERED BLOOD PRESSURE, THE ENZYME RENIN
CC       CLEAVES ANGIOTENSIN I, FROM ANGIOTENSINOGEN. ACE (ANGIOTENSIN
CC       CONVERTING ENZYME) THEN REMOVES A DIPEPTIDE TO YIELD THE
CC       PHYSIOLOGICALLY ACTIVE PEPTIDE ANGIOTENSIN II, THE MOST POTENT
CC       PRESSOR SUBSTANCE KNOWN, WHICH HELPS REGULATE VOLUME AND MINERAL
CC       BALANCE OF BODY FLUIDS.
CC   -!- FUNCTION: Angiotensin III stimulates aldosterone release.
CC   -!- SUBUNIT: During pregnancy, exists as a disulfide-linked 2:2
CC       heterotetramer with the proform of PRG2 and as a complex (probably
CC       a 2:2:2 heterohexamer) with pro-PRG2 and C3dg.
CC   -!- SUBCELLULAR LOCATION: Secreted.
CC   -!- TISSUE SPECIFICITY: Synthesized by the liver and secreted in the
CC       plasma.
CC   -!- DISEASE: AGT SEEMS TO BE ASSOCIATED WITH A PREDISPOSITION TO
CC       ESSENTIAL HYPERTENSION AS WELL AS PREGNANCY-INDUCED HYPERTENSION
CC       (PIH) (PREECLAMPSIA).
CC   -!- DISEASE: [Asprosin]: Marfanoid-progeroid-lipodystrophy syndrome (MFLS)
CC       [MIM:616914]: An autosomal dominant syndrome characterized by
CC       congenital lipodystrophy, a progeroid facial appearance due to lack of
CC       subcutaneous fat, and variable signs of Marfan syndrome. Clinical
CC       features include premature birth with an accelerated linear growth
CC       disproportionate to the weight gain, ectopia lentis, aortic dilatation,
CC       dural ectasia, and arachnodactyly. Mental and motor development are
CC       within normal limits. {ECO:0000269|PubMed:20979188,
CC       ECO:0000269|PubMed:21594992, ECO:0000269|PubMed:21594993,
CC       ECO:0000269|PubMed:24039054, ECO:0000269|PubMed:24613577,
CC       ECO:0000269|PubMed:24665001, ECO:0000269|PubMed:26860060,
CC       ECO:0000269|PubMed:27087445}. Note=The disease is caused by mutations
CC       affecting the gene represented in this entry.
CC       {ECO:0000269|PubMed:27087445}.
CC   -!- SIMILARITY: BELONGS TO THE SERPIN FAMILY.
CC   -!- CAUTION: IT IS UNCERTAIN WHETHER MET-1 OR MET-10 IS THE INITIATOR.
DR   EMBL; K02215; AAA51731.1; -.
DR   EMBL; M24689; AAA51679.1; -.
DR   EMBL; M24686; AAA51679.1; JOINED.
DR   EMBL; M24687; AAA51679.1; JOINED.
DR   EMBL; M24688; AAA51679.1; JOINED.
DR   EMBL; X15324; CAA33385.1; -.
DR   EMBL; X15325; CAA33385.1; JOINED.
DR   EMBL; X15326; CAA33385.1; JOINED.
DR   EMBL; X15327; CAA33385.1; JOINED.
DR   EMBL; M69110; AAA52282.1; -.
DR   EMBL; BC011519; AAH11519.1; -.
DR   EMBL; S78529; AAD14287.1; -.
DR   EMBL; S78530; AAD14288.1; -.
DR   PIR; A01249; ANHU.
DR   PIR; A31362; A31362.
DR   PIR; A35203; A35203.
DR   SWISS-2DPAGE; P01019; HUMAN.
DR   HGNC; HGNC:333; AGT.
DR   MIM; 106150; -.
DR   GO; GO:0005625; C:soluble fraction; TAS.
DR   GO; GO:0004867; F:serine protease inhibitor activity; TAS.
DR   GO; GO:0007166; P:cell surface receptor linked signal transdu...; TAS.
DR   GO; GO:0007267; P:cell-cell signaling; TAS.
DR   GO; GO:0007565; P:pregnancy; TAS.
DR   GO; GO:0008217; P:regulation of blood pressure; TAS.
DR   InterPro; IPR000227; Angiotensngn.
DR   InterPro; IPR000215; Serpin.
DR   Pfam; PF00079; serpin; 1.
DR   PRINTS; PR00654; ANGIOTENSNGN.
DR   SMART; SM00093; SERPIN; 1.
DR   PROSITE; PS00284; SERPIN; 1.
KW   Vasoconstrictor; Glycoprotein; Plasma; Serpin; Signal;
KW   Disease mutation; Polymorphism.
FT   SIGNAL        1     33
FT   CHAIN        34    485       ANGIOTENSINOGEN.
FT   PEPTIDE      34     43       ANGIOTENSIN I.
FT   PEPTIDE      34     41       ANGIOTENSIN II.
FT   PEPTIDE      35     41       ANGIOTENSIN III.
FT   CARBOHYD     47     47       N-LINKED (GLCNAC...).
FT   CARBOHYD    170    170       N-LINKED (GLCNAC...).
FT   CARBOHYD    304    304       N-LINKED (GLCNAC...).
FT   CARBOHYD    328    328       N-LINKED (GLCNAC...).
FT   VARIANT     207    207       T -> M (IN dbSNP:4762).
FT                                /FTId=VAR_007093.
FT   VARIANT     242    242       T -> I (IN HYPERTENSION).
FT                                /FTId=VAR_007094.
FT   VARIANT     244    244       L -> R (IN HYPERTENSION).
FT                                /FTId=VAR_007095.
FT   VARIANT     268    268       M -> T (IN HYPERTENSION; dbSNP:699).
FT                                /FTId=VAR_007096.
FT   VARIANT     281    281       Y -> C (IN HYPERTENSION; ALTERS THE
FT                                STRUCTURE, GLYCOSYLATION AND SECRETION OF
FT                                ANGIOTENSINOGEN).
FT                                /FTId=VAR_007097.
FT   VARIANT     392    392       L -> M (IN dbSNP:1805090).
FT                                /FTId=VAR_014573.
FT   CONFLICT    333    333       Q -> E (IN REF. 1).
FT   CONFLICT    335    335       P -> S (IN REF. 4).
**
**   #################    INTERNAL SECTION    ##################
**CL 1q42-q43;
SQ   SEQUENCE   485 AA;  53154 MW;  5026C2DFB2DD236E CRC64;
     MRKRAPQSEM APAGVSLRAT ILCLLAWAGL AAGDRVYIHP FHLVIHNEST CEQLAKANAG
     KPKDPTFIPA PIQAKTSPVD EKALQDQLVL VAAKLDTEDK LRAAMVGMLA NFLGFRIYGM
     HSELWGVVHG ATVLSPTAVF GTLASLYLGA LDHTADRLQA ILGVPWKDKN CTSRLDAHKV
     LSALQAVQGL LVAQGRADSQ AQLLLSTVVG VFTAPGLHLK QPFVQGLALY TPVVLPRSLD
     FTELDVAAEK IDRFMQAVTG WKTGCSLMGA SVDSTLAFNT YVHFQGKMKG FSLLAEPQEF
     WVDNSTSVSV PMLSGMGTFQ HWSDIQDNFS VTQVPFTESA CLLLIQPHYA SDLDKVEGLT
     FQQNSLNWMK KLSPRTIHLT MPQLVLQGSY DLQDLLAQAE LPAILHTELN LQKLSNDRIR
     VGEVLNSIFF ELEADEREPT ESTQQLNKPE VLEVTLNRPF LFAVYDQSAT ALHFLGRVAN
     PLSTA
//
ID   NRTC_SYNY3     STANDARD;      PRT;   670 AA.
AC   P73450;
DT   01-NOV-1997 (Rel. 35, Created)
DT   01-NOV-1997 (Rel. 35, Last sequence update)
DT   16-OCT-2001 (Rel. 40, Last annotation update)
DE   Nitrate transport ATP-binding protein nrtC.
GN   NRTC OR SLL1452.
OS   Synechocystis sp. (strain PCC 6803).
OC   Bacteria; Cyanobacteria; Chroococcales; Synechocystis.
OX   NCBI_TaxID=1148;
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=97061201; PubMed=8905231;
RA   Kaneko T., Sato S., Kotani H., Tanaka A., Asamizu E., Nakamura Y.,
RA   Miyajima N., Hirosawa M., Sugiura M., Sasamoto S., Kimura T.,
RA   Hosouchi T., Matsuno A., Muraki A., Nakazaki N., Naruo K., Okumura S.,
RA   Shimpo S., Takeuchi C., Wada T., Watanabe A., Yamada M., Yasuda M.,
RA   Tabata S.;
RT   "Sequence analysis of the genome of the unicellular cyanobacterium
RT   Synechocystis sp. strain PCC6803. II. Sequence determination of the
RT   entire genome and assignment of potential protein-coding regions.";
RL   DNA Res. 3:109-136(1996).
CC   -!- FUNCTION: PROBABLY PART OF A HIGH-AFFINITY BINDING-PROTEIN-
CC       DEPENDENT TRANSPORT SYSTEM FOR NITRATE. PROBABLY RESPONSIBLE FOR
CC       ENERGY COUPLING TO THE TRANSPORT SYSTEM.
CC   -!- SUBCELLULAR LOCATION: Membrane-associated (Potential).
CC   -!- SIMILARITY: BELONGS TO THE ABC TRANSPORTER FAMILY.
CC   -!- SIMILARITY: SOME, IN THE C-TERMINAL DOMAIN TO NRTA.
DR   EMBL; D90906; BAA17490.1; -.
DR   InterPro; IPR003593; AAA_ATPase.
DR   InterPro; IPR003439; ABC_transporter.
DR   InterPro; IPR005890; NtrCD.
DR   Pfam; PF00005; ABC_tran; 1.
DR   ProDom; PD000006; ABC_transporter; 1.
DR   SMART; SM00382; AAA; 1.
DR   TIGRFAMs; TIGR01184; ntrCD; 1.
DR   PROSITE; PS00211; ABC_TRANSPORTER_1; 1.
DR   PROSITE; PS50893; ABC_TRANSPORTER_2; 1.
KW   Transport; ATP-binding; Membrane; Nitrate assimilation;
KW   Complete proteome.
FT   DOMAIN        1    254       ABC TRANSPORTER.
FT   DOMAIN      240    250       INTERNAL.
FT   DOMAIN      255    278       LINKER.
FT   DOMAIN      279    670       NTRA-LIKE.
FT   NP_BIND      42     49       ATP (POTENTIAL).
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   670 AA;  75101 MW;  03B47E6C7918AD14 CRC64;
     MMPFIEIDHV DRIFPLPDGG RYIALKNIEL KISQGEFISL IGHSGCGKST LLNMISGLDK
     PTFGGVIMEG KEITEPGPER MVVFQNYSLL PWLTVRQNIA LAVNRVLRDL PKPEQEKIID
     DNIALVGLQR AAHKRPGELS GGMKQRVAIA RALSTRPKVL LLDEPFGALD ALTRGNLQER
     LMEIVQESGV TCIMVTHDVD EALLLSDRVV MLTTGPEAHI GQILEVPIPR PRHRLEVVNH
     PSYYALRGEM VYFLNQQKRA KKVGAVSQFA EAMGGNGLEK INLDLGFIPL TDCAPLVVAK
     EKGFFQKHGL EQVNLVKEPS WQAIADGIRE RRLDGAQMVA GMPLALTLGM GGKTPLPMVT
     AMVMSRNGNA ITLSKKFAEA GVKTLEDLRL KLAETPDQVS TLGMVHPASM QNLLLRYWLA
     SGSIDPDQDI NLMRLPPPQM VSNLEAGNID GFCVGEPWNS YAVKQNLGYV IATDLDIWNG
     HPEKVLGMRE EWVNKYPATH LALVKALLEA CEYCDDRRHR QEILDYLALP QYVGTSTEYI
     SPGFLTEYDQ GNDAEAEMLL DFNQFYVKQS NYPSRSEGLW ILTQLARWGY IDFPKNWVEI
     IERVRRPDLF GEACRHLGWP DLEGDHHNVS LFDGMVFTPN DPLGYIKRFT IHRDIQVTEI
     LIDQIDQVNQ
//
ID   FAS2_PENPA     STANDARD;      PRT;  1857 AA.
AC   P15368;
DT   01-APR-1990 (Rel. 14, Created)
DT   01-APR-1990 (Rel. 14, Last sequence update)
DT   28-FEB-2003 (Rel. 41, Last annotation update)
DE   Fatty acid synthase subunit alpha (EC 2.3.1.86) [Includes: Acyl
DE   carrier; 3-oxoacyl-[acyl-carrier protein] reductase (EC 1.1.1.100)
DE   (Beta-ketoacyl reductase); 3-oxoacyl-[acyl-carrier protein] synthase
DE   (EC 2.3.1.41) (Beta-ketoacyl synthase)].
GN   FAS2.
OS   Penicillium patulum (Penicillium griseofulvum).
OC   Eukaryota; Fungi; Ascomycota; Pezizomycotina; Eurotiomycetes;
OC   Eurotiales; Trichocomaceae; mitosporic Trichocomaceae; Penicillium.
OX   NCBI_TaxID=5078;
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=89030697; PubMed=3053172;
RA   Wiesner P., Beck J., Beck K.-F., Ripka S., Mueller G., Luecke S.,
RA   Schweizer E.;
RT   "Isolation and sequence analysis of the fatty acid synthetase FAS2
RT   gene from Penicillium patulum.";
RL   Eur. J. Biochem. 177:69-79(1988).
CC   -!- FUNCTION: FATTY ACID SYNTHETASE CATALYZES THE FORMATION OF LONG-
CC       CHAIN FATTY ACIDS FROM ACETYL-COA, MALONYL-COA AND NADPH. THE
CC       ALPHA SUBUNIT CONTAINS DOMAINS FOR: ACYL CARRIER PROTEIN, 3-
CC       OXOACYL-[ACYL-CARRIER PROTEIN] REDUCTASE, AND 3-OXOACYL-[ACYL-
CC       CARRIER-PROTEIN] SYNTHASE.
CC   -!- CATALYTIC ACTIVITY: Acetyl-CoA + N malonyl-CoA + 2N NADPH = a
CC       long-chain acyl-CoA + N CoA + N CO(2) + 2N NADP(+).
CC   -!- CATALYTIC ACTIVITY: Acyl-[acyl-carrier protein] + malonyl-[acyl-
CC       carrier protein] = 3-oxoacyl-[acyl-carrier protein] + CO(2) +
CC       [acyl-carrier protein].
CC   -!- CATALYTIC ACTIVITY: (3R)-3-hydroxyacyl-[acyl-carrier protein] +
CC       NADP(+) = 3-oxoacyl-[acyl-carrier protein] + NADPH.
CC   -!- SUBUNIT: [Alpha(6)beta(6)] hexamers of two multifunctional
CC       subunits (alpha and beta).
CC   -!- SIMILARITY: TO THE FATTY ACID SYNTHETASE, SUBUNIT ALPHA FROM OTHER
CC       FUNGI.
DR   EMBL; M37461; AAA33695.1; -.
DR   PIR; S01787; S01787.
DR   InterPro; IPR000794; Ketoacyl-synt.
DR   InterPro; IPR004568; Pantethn_trn.
DR   InterPro; IPR006162; Ppantne_attach.
DR   Pfam; PF01648; ACPS; 1.
DR   Pfam; PF00109; ketoacyl-synt; 1.
DR   Pfam; PF02801; ketoacyl-synt_C; 1.
DR   ProDom; PD004282; ACPS; 1.
DR   TIGRFAMs; TIGR00556; pantethn_trn; 1.
DR   PROSITE; PS00606; B_KETOACYL_SYNTHASE; 1.
DR   PROSITE; PS00012; PHOSPHOPANTETHEINE; 1.
KW   Fatty acid biosynthesis; Multifunctional enzyme; Oxidoreductase;
KW   Transferase; NADP; Phosphopantetheine.
FT   DOMAIN        1      ?       ACYL CARRIER (ACP).
FT   DOMAIN      648    845       BETA-KETOACYL REDUCTASE.
FT   DOMAIN        ?   1857       BETA-KETOACYL SYNTHASE.
FT   ACT_SITE   1275   1275       BETA-KETOACYL SYNTHASE (BY SIMILARITY).
FT   BINDING     174    174       PHOSPHOPANTETHEINE (BY SIMILARITY).
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   1857 AA;  204466 MW;  34BAFD547D93FEE6 CRC64;
     MRPEVEQELA HTLLVELLAY QFASPVRWIE TQDVILAEQR TERIVEIGPA DTLGGMARRT
     LASKYEAYDA ATSVQRQILC YNKDAKEIYY DVDPVEEEPE ATEPAPSATP AAPAAAPAAG
     APPPPPSAGP AASVEDIPVT AVDILRTLVA QKLKKSLADV PLSKAIKDLV GGKSTLQNEI
     LGDLGKEFGS TPEKPEDVPL DELGASMQAT FNGQLGKQSS SLIARMVSSK MPGGFNITSV
     RKYLETRWGL GSGRQDGVLL LALTMEPAAR LGSEVDAKAY LDDVTNKYAA SAGVNLSAPV
     AGGDSGGAGG GMVMDPAAID ALTKDQRALF KQQLEIIARY LKMDLRGGEK AHVISQETQK
     ALQAQLDLWQ AEHGDFYASG IEPSFDQLKA RVYDSSWNWA RQDALSMYYD IIFGRLQVVD
     REIVSQCIRI MNRSNPLLLD FMQYHIDNCP TERGETYQLA KELGQQLIEN CREVLEVAPV
     YKDVAVPTGP QTTIDARGNI SYKETPRTSA RKLEHYVKHM AEGGPISEYS NRTKVQNDLK
     SVYKLIRKQH RLSKSSQLQF DALYKDVVHA LGMNESQIIP QENGHSKKGG RSAAKRNTPT
     RPGKVETIPF LHLKKKTEHG WDYNKKLTGI YLNVTESAAK DGLSFQGKNV LMTGAGAGSI
     GAEVLQGLIS GGAQVIVTTS RFSREVTEYY QAMYARYGAR GSQLVVVPFN QGSKQDVEAL
     VEYIYDTKKG LGWDLDFVVP FAAIPENGRE IDSIDSKSEL AHRIMLTNLL RLLGSVKTQK
     QAHGFETRPA QVILPLSPNH GTFGNDGLYS ESKLALETLF NRWYSENWGH YLTICGAVIG
     WTRGTGLMSG NNMVAEGVEK LGVRTFSQQE MAFNLLGLMS PAIVNLCQLD PVFADLNGGL
     QFIPDLKGLM TKLRTDIMET SDVRQAVMKE TAIEHNIVNG EDSGVLYKKV IAEPRANIKF
     EFPNLPDWEK EVKPLNENLK GMVNLDKVVV VTGFSEVGPW GNSRTRWEME SKGKFSLEGC
     VEMAWIMGLI KHHNGPLKGQ AYSGWVDAKT GEPVDDKDVK PKYEKHILEH TGIRLIEPEL
     FKGYDPKKKQ LLQEIVIQED LEPFEASKET AEEFKREHGD KVEIFEIPES GEYTVRLCKG
     ATMLIPKALQ FDRLVAGQVP TGWDASRYGI PDDIISQVDP VTLFVLVCTA EAMLSAGVTD
     PYEFYKYVHL SEVGNCIGSG IGGTHRLRGM YKDRFLDKPL QKDILQESFI NTMSAWVNML
     LLSSTGPIKT PVGCCATAVE SVDIGYETIV EGKARVCFVG GFDDFQEEGS YEFANMKATS
     NAEDEFAHGR TPQEMSRPTT TTRAGFMESQ GCGMQLIMTA QLALDMGVPI HGIIALTTTA
     TDKIGRSVRS VPAPGQGVLT TARENPGKFP SPLLDIKYRR RQLDLRKKQI NEWQEAELLY
     LQEEAEAMKA QSDETFNEAE YMQERAQHIE REAIRQEKDA QYSLGNNFWK QDSRIAPLRG
     AMATWGLTVD DIDVASFHGT STVANDKNES DVICQQMKHL GRSKGNAVMG IFQKYLTGHP
     KGAAGAWMFN GCLQVLDSGL VPGNRNADNV DKVMEKFDYI VYPSRSIQTD GVKAFSVTSF
     GFGQKGAQVI GIHPKYLYAT LDQAQYEAYK TKVEARQKKA YRYFHNGLIN NSIFVAKSKA
     PYEDEQQSKV FLNPDYRVSV DKKTSELKFS TTAPEAKQSE STRQTLESLA KANATENSKI
     GVDVEHIDSV NIENETFVER NFTQSEQDYC RKAASPQSSF AGRWSAKEAV FKSLGVSSKG
     AGAALKDIEI GVDANGAPVV NLHGAAAAAA KQAGVKQVSV SISHSDSQAV AVAVSQF
//
ID   UKA1_HUMAN     STANDARD;      PRT;    19 AA.
AC   P31940;
DT   01-JUL-1993 (Rel. 26, Created)
DT   01-JUL-1993 (Rel. 26, Last sequence update)
DT   16-OCT-2001 (Rel. 40, Last annotation update)
DE   Unknown protein from 2D-page of epidermal keratinocytes (Spot 1118)
DE   (Fragments).
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606;
RN   [1]
RP   SEQUENCE.
RC   TISSUE=Keratinocytes;
RX   MEDLINE=93162043; PubMed=1286667;
RA   Rasmussen H.H., van Damme J., Puype M., Gesser B., Celis J.E.,
RA   Vandekerckhove J.;
RT   "Microsequences of 145 proteins recorded in the two-dimensional gel
RT   protein database of normal human epidermal keratinocytes.";
RL   Electrophoresis 13:960-969(1992).
CC   -!- MISCELLANEOUS: ON THE 2D-GEL THE DETERMINED PI OF THIS UNKNOWN
CC       PROTEIN IS: 7.24, ITS MW IS: 23.5 kDa.
DR   Aarhus/Ghent-2DPAGE; 1118; IEF.
FT   UNSURE        6      6
FT   UNSURE       17     17
FT   NON_CONS      6      7
FT   NON_CONS     12     13
FT   NON_TER       1      1
FT   NON_TER      19     19
**
**   #################    INTERNAL SECTION    ##################
**CL ?;
SQ   SEQUENCE   19 AA;  2087 MW;  EF7515F79D50DE12 CRC64;
     HIGLVRLTPT EVQEPIITA
//
ID   A4_MOUSE       STANDARD;      PRT;   770 AA.
AC   P12023;
DT   01-OCT-1989 (Rel. 12, Created)
DT   01-DEC-1992 (Rel. 24, Last sequence update)
DT   28-FEB-2003 (Rel. 41, Last annotation update)
DE   Alzheimer's disease amyloid A4 protein homolog precursor
DE   (Amyloidogenic glycoprotein) (AG).
GN   APP.
OS   Mus musculus (Mouse).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Rodentia; Sciurognathi; Muridae; Murinae; Mus.
OX   NCBI_TaxID=10090;
RN   [1]
RP   SEQUENCE OF 1-289 AND 365-770 FROM N.A.
RC   STRAIN=BALB/c; TISSUE=Brain;
RX   MEDLINE=92096458; PubMed=1756177;
RA   de Strooper B., van Leuven F., van den Berghe H.;
RT   "The amyloid beta protein precursor or proteinase nexin II from mouse
RT   is closer related to its human homolog than previously reported.";
RL   Biochim. Biophys. Acta 1129:141-143(1991).
RN   [2]
RP   SEQUENCE OF 1-289 AND 365-770 FROM N.A.
RC   TISSUE=Brain;
RX   MEDLINE=88106489; PubMed=3322280;
RA   Yamada T., Sasaki H., Furuya H., Miyata T., Goto I., Sakaki Y.;
RT   "Complementary DNA for the mouse homolog of the human amyloid beta
RT   protein precursor.";
RL   Biochem. Biophys. Res. Commun. 149:665-671(1987).
RN   [3]
RP   REVISIONS.
RA   Yamada T.;
RL   Submitted (MAR-1988) to the EMBL/GenBank/DDBJ databases.
RN   [4]
RP   SEQUENCE OF 289-364 FROM N.A.
RC   STRAIN=CD-1; TISSUE=Placenta;
RX   MEDLINE=89345111; PubMed=2569710;
RA   Fukuchi K., Martin G.M., Deeb S.S.;
RT   "Sequence of the protease inhibitor domain of the A4 amyloid protein
RT   precursor of Mus domesticus.";
RL   Nucleic Acids Res. 17:5396-5396(1989).
RN   [5]
RP   SEQUENCE OF 1-19 FROM N.A.
RX   MEDLINE=92209998; PubMed=1555768;
RA   Izumi R., Yamada T., Yoshikai S.I., Sasaki H., Hattori M., Sakai Y.;
RT   "Positive and negative regulatory elements for the expression of the
RT   Alzheimer's disease amyloid precursor-encoding gene in mouse.";
RL   Gene 112:189-195(1992).
RN   [6]
RP   SEQUENCE OF 281-380 FROM N.A., AND ALTERNATIVE SPLICING.
RC   TISSUE=Brain, and Kidney;
RX   MEDLINE=89149813; PubMed=2493250;
RA   Yamada T., Sasaki H., Dohura K., Goto I., Sakaki Y.;
RT   "Structure and expression of the alternatively-spliced forms of mRNA
RT   for the mouse homolog of Alzheimer's disease amyloid beta protein
RT   precursor.";
RL   Biochem. Biophys. Res. Commun. 158:906-912(1989).
RN   [7]
RP   PHOSPHORYLATION.
RX   MEDLINE=22028091; PubMed=11912189;
RA   Taru H., Iijima K.-I., Hase M., Kirino Y., Yagi Y., Suzuki T.;
RT   "Interaction of Alzheimer's beta-amyloid precursor family proteins
RT   with scaffold proteins of the JNK signaling cascade.";
RL   J. Biol. Chem. 277:20070-20078(2002).
CC   -!- SUBCELLULAR LOCATION: Type I membrane protein. Mature,
CC       phosphorylated APP is largely located on the plasma membrane of
CC       cell bodies and the growth cones of neurites of mature neurons (By
CC       similarity).
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing; Named isoforms=5;
CC       Name=APP(770);
CC         IsoId=P12023-1; Sequence=Displayed;
CC       Name=APP(395);
CC         IsoId=P12023-4; Sequence=Not described;
CC       Name=APP(563);
CC         IsoId=P12023-5; Sequence=Not described;
CC       Name=APP(695);
CC         IsoId=P12023-2; Sequence=VSP_000012, VSP_000013;
CC       Name=APP(751);
CC         IsoId=P12023-3; Sequence=VSP_000014;
CC   -!- TISSUE SPECIFICITY: Isoform APP(770) is expressed in kidney.
CC       Isoform APP(751) is widely expressed. Isoform APP(695) is
CC       expressed in brain, kidney and liver.
CC   -!- DOMAIN: The clathrin-binding site is essential for its association
CC       with X11-alpha, -beta, and -gamma. The sequence specific
CC       recognition extends to peptide residues that are C-terminal to the
CC       NPXY motif. This interaction appears to be independent of
CC       phosphorylation. Binds to Jip1 which may result in the
CC       phosphorylation of App by members of the JNK-signaling cascade.
CC       Cytoplasmic domain binds Apbb1, phosphorylation within this domain
CC       results in a conformational change which prevents protein binding
CC       (By similarity).
CC   -!- SIMILARITY: BELONGS TO THE APP FAMILY.
CC   -!- SIMILARITY: Contains 1 BPTI/Kunitz inhibitor domain.
DR   EMBL; X59379; -; NOT_ANNOTATED_CDS.
DR   EMBL; M18373; AAA37139.1; -.
DR   EMBL; X15210; CAA33280.1; -.
DR   EMBL; D10603; BAA01456.1; -.
DR   EMBL; M24397; AAA39929.1; -.
DR   PIR; A27485; A27485.
DR   PIR; S04855; S04855.
DR   PIR; S19727; S19727.
DR   HSSP; P05067; 1AAP.
DR   MGI; MGI:88059; App.
DR   InterPro; IPR001868; A4_APP.
DR   InterPro; IPR001255; Beta-APP.
DR   InterPro; IPR002223; Kunitz_BPTI.
DR   Pfam; PF02177; A4_EXTRA; 1.
DR   Pfam; PF03494; Beta-APP; 1.
DR   Pfam; PF00014; Kunitz_BPTI; 1.
DR   PRINTS; PR00203; AMYLOIDA4.
DR   PRINTS; PR00759; BASICPTASE.
DR   ProDom; PD000222; Kunitz_BPTI; 1.
DR   SMART; SM00006; A4_EXTRA; 1.
DR   SMART; SM00131; KU; 1.
DR   PROSITE; PS00319; A4_EXTRA; 1.
DR   PROSITE; PS00320; A4_INTRA; 1.
DR   PROSITE; PS00280; BPTI_KUNITZ_1; 1.
DR   PROSITE; PS50279; BPTI_KUNITZ_2; 1.
KW   Glycoprotein; Amyloid; Neurone; Transmembrane; Signal;
KW   Alternative splicing; Serine protease inhibitor; Phosphorylation.
FT   SIGNAL        1     17       BY SIMILARITY.
FT   CHAIN        18    770       ALZHEIMER'S DISEASE AMYLOID A4 PROTEIN
FT                                HOMOLOG.
FT   TOPO_DOM     18    699       EXTRACELLULAR (POTENTIAL).
FT   TRANSMEM    700    723       POTENTIAL.
FT   TOPO_DOM    724    770       CYTOPLASMIC (POTENTIAL).
FT   DOMAIN      287    345       BPTI/KUNITZ INHIBITOR.
FT   DOMAIN      673    715       EQUIVALENT OF BETA-AMYLOID PROTEIN.
FT   SITE        759    762       CLATHRIN-BINDING (BY SIMILARITY).
FT   MOD_RES     743    743       PHOSPHORYLATION.
FT   CARBOHYD    542    542       N-LINKED (GLCNAC...) (POTENTIAL).
FT   CARBOHYD    571    571       N-LINKED (GLCNAC...) (POTENTIAL).
FT   DISULFID    291    341       BY SIMILARITY.
FT   DISULFID    300    324       BY SIMILARITY.
FT   DISULFID    316    337       BY SIMILARITY.
FT   VAR_SEQ     289    289       E -> V (in isoform APP(695)).
FT                                /FTId=VSP_000012.
FT   VAR_SEQ     290    364       Missing (in isoform APP(695)).
FT                                /FTId=VSP_000013.
FT   VAR_SEQ     346    380       Missing (in isoform APP(751)).
FT                                /FTId=VSP_000014.
**
**   #################    INTERNAL SECTION    ##################
**IS P12023-6
**ZB SAO, 16-SEP-2002;
SQ   SEQUENCE   770 AA;  86752 MW;  26C50DE0890CAF7A CRC64;
     MLPSLALLLL AAWTVRALEV PTDGNAGLLA EPQIAMFCGK LNMHMNVQNG KWESDPSGTK
     TCIGTKEGIL QYCQEVYPEL QITNVVEANQ PVTIQNWCKR GRKQCKTHTH IVIPYRCLVG
     EFVSDALLVP DKCKFLHQER MDVCETHLHW HTVAKETCSE KSTNLHDYGM LLPCGIDKFR
     GVEFVCCPLA EESDSVDSAD AEEDDSDVWW GGADTDYADG SEDKVVEVAE EEEVADVEEE
     EADDDEDVED GDEVEEEAEE PYEEATERTT STATTTTTTT ESVEEVVREV CSEQAETGPC
     RAMISRWYFD VTEGKCVPFF YGGCGGNRNN FDTEEYCMAV CGSVSTQSLL KTTSEPLPQD
     PDKLPTTAAS TPDAVDKYLE TPGDENEHAH FQKAKERLEA KHRERMSQVM REWEEAERQA
     KNLPKADKKA VIQHFQEKVE SLEQEAANER QQLVETHMAR VEAMLNDRRR LALENYITAL
     QAVPPRPHHV FNMLKKYVRA EQKDRQHTLK HFEHVRMVDP KKAAQIRSQV MTHLRVIYER
     MNQSLSLLYN VPAVAEEIQD EVDELLQKEQ NYSDDVLANM ISEPRISYGN DALMPSLTET
     KTTVELLPVN GEFSLDDLQP WHPFGVDSVP ANTENEVEPV DARPAADRGL TTRPGSGLTN
     IKTEEISEVK MDAEFGHDSG FEVRHQKLVF FAEDVGSNKG AIIGLMVGGV VIATVIVITL
     VMLKKKQYTS IHHGVVEVDA AVTPEERHLS KMQQNGYENP TYKFFEQMQN
//
ID   WASL_RAT       STANDARD;      PRT;   501 AA.
AC   O08816;
DT   16-OCT-2001 (Rel. 40, Created)
DT   16-OCT-2001 (Rel. 40, Last sequence update)
DT   28-FEB-2003 (Rel. 41, Last annotation update)
DE   Neural Wiskott-Aldrich syndrome protein (N-WASP).
GN   WASL.
OS   Rattus norvegicus (Rat).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Rodentia; Sciurognathi; Muridae; Murinae; Rattus.
OX   NCBI_TaxID=10116;
RN   [1]
RP   SEQUENCE FROM N.A.
RX   MEDLINE=97464048; PubMed=9322739;
RA   Fukuoka M., Miki H., Takenawa T.;
RT   "Identification of N-WASP homologs in human and rat brain.";
RL   Gene 196:43-48(1997).
CC   -!- FUNCTION: REGULATES ACTIN POLYMERIZATION BY STIMULATING THE ACTIN-
CC       NUCLEATING ACTIVITY OF THE ACTIN-RELATED PROTEIN 2/3 (ARP2/3)
CC       COMPLEX (BY SIMILARITY).
CC   -!- SUBUNIT: BINDS ACTIN AND ARP2/3 COMPLEX; INTERACTS WITH CDC42
CC       BINDS TO SH3 DOMAINS OF ASH/GRB2 (BY SIMILARITY).
CC   -!- SIMILARITY: Contains 1 CRIB domain.
CC   -!- SIMILARITY: Contains 1 WH1 domain.
CC   -!- SIMILARITY: Contains 2 WH2 domains.
DR   EMBL; D88461; BAA21534.1; -.
DR   InterPro; IPR000697; EVH1.
DR   InterPro; IPR000095; PAKbox/Rhobndng.
DR   InterPro; IPR001960; WH1.
DR   InterPro; IPR003124; WH2.
DR   Pfam; PF00786; PBD; 1.
DR   Pfam; PF00568; WH1; 1.
DR   Pfam; PF02205; WH2; 2.
**   PRINTS; PR01217; PRICHEXTENSN; FALSE_POS_1.
DR   SMART; SM00285; PBD; 1.
DR   SMART; SM00461; WH1; 1.
DR   SMART; SM00246; WH2; 2.
DR   PROSITE; PS50108; CRIB; 1.
KW   Actin-binding; Repeat.
FT   DOMAIN       31    135       WH1.
FT   DOMAIN      200    213       CRIB.
FT   DOMAIN      401    418       WH2 1.
FT   DOMAIN      429    446       WH2 2.
FT   COMPBIAS    274    385       PRO-RICH.
FT   COMPBIAS    482    501       ASP-RICH.
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   501 AA;  54325 MW;  480E21F26F7FC77E CRC64;
     MSSGQQPPRR VTNVGSLLLT PQENESLFSF LGKKCVTMSS AVVQLYAADR NCMWSKKCSG
     VACLVKDNPQ RSYFLRIFDI KDGKLLWEQE LYNNFVYNSP RGYFHTFAGD TCQVALNFAN
     EEEAKKFRKA VTDLLGRRQR KSEKRRDAPN GPNLPMATVD IKNPEITTNR FYSSQVNNIS
     HTKEKKKGKA KKKRLTKADI GTPSNFQHIG HVGWDPNTGF DLNNLDPELK NLFDMCGISE
     AQLKDRETSK VIYDFIEKTG GVEAVKNELR RQAPPPPPPS RGGPPPPPPP PHSSGPPPPP
     ARGRGAPPPP PSRAPTAAPP PPPPSRPGVV VPPPPPNRMY PPPPPALPSS APSGPPPPPP
     LSMAGSTAPP PPPPPPPPPG PPPPPGLPSD GDHQVPASSG NKAALLDQIR EGAQLKKVEQ
     NSRPVSCSGR DALLDQIRQG IQLKSVSDGQ ESTPPTPAPT SGIVGALMEV MQKRSKAIHS
     SDEDEDDDDE EDFQDDDEWE D
//
ID   Q9B1S6      PRELIMINARY;      PRT;   260 AA.
AC   Q9B1S6;
DT   01-JUN-2001 (TrEMBLrel. 17, Created)
DT   01-JUN-2001 (TrEMBLrel. 17, Last sequence update)
DT   01-MAR-2004 (TrEMBLrel. 26, Last annotation update)
DE   Cytochrome c oxidase subunit III (EC 1.9.3.1) (Cytochrome co oxidase
DE   subunit III) (Cytochrome c oxidase polypeptide III) (Cytochrome
DE   oxidase subunit III){EP249}.
GN   COX3{EI2}.
OS   Homo sapiens (Human).
OG   Mitochondrion{EI3}.
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606{EP248};
RN   [1]{EI3}
RP   SEQUENCE FROM N.A.
RX   MEDLINE=21012010; PubMed=11130070;
RA   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
RT   "Mitochondrial genome variation and the origin of modern humans.";
RL   Nature 408:708-713(2000).
RN   [2]{EI3}
RP   SEQUENCE FROM N.A.
RX   MEDLINE=21176314; PubMed=11279504;
RA   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
RT   "correction: Mitochondrial genome variation and the origin of modern
RT   humans.";
RL   Nature 410:611-611(2001).
RN   [3]{EI3}
RP   SEQUENCE FROM N.A.
RA   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
RL   Submitted (FEB-2001) to the EMBL/GenBank/DDBJ databases.
RN   [4]{EI2}
RP   SEQUENCE FROM N.A.
RX   PubMed=11553319;
RA   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
RT   "Major genomic mitochondrial lineages delineate early human
RT   expansions.";
RL   BMC Genet. 2:13-13(2001).
RN   [5]{EI2}
RP   SEQUENCE FROM N.A.
RA   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
RL   Submitted (MAY-2001) to the EMBL/GenBank/DDBJ databases.
RN   [6]{EI79}
RP   SEQUENCE FROM N.A.
RX   MEDLINE=22062553; PubMed=12022039;
RA   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
RA   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L.,
RA   Barbosa M., Paco-Larson M.L., Petzl-Erler M.L., Valente V.,
RA   Santos S.E., Zago M.A.;
RT   "Mitochondrial genome diversity of Native Americans supports a single
RT   early entry of founder populations into America.";
RL   Am. J. Hum. Genet. 71:187-192(2002).
RN   [7]{EI118}
RP   SEQUENCE FROM N.A.
RX   MEDLINE=22406325; PubMed=12509511;
RA   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G.,
RA   Hosseini S., Brandon M., Easley K., Chen E., Brown M.D.,
RA   Sukernik R.I., Olckers A., Wallace D.C.;
RT   "Natural selection shaped regional mtDNA variation in humans.";
RL   Proc. Natl. Acad. Sci. U.S.A. 100:171-176(2003).
RN   [8]{EI157}
RP   SEQUENCE FROM N.A.
RC   STRAIN=GD7812, LN7550, LN7589, SD10313, XJ8426, EWK28, QD8141, GD7834,
RC   Miao271, DW48, WH6954, WH6967, Mg246, LN7595, GD7817, WH6958, GD7829,
RC   SD10352, XJ8420, SD10334, WH6979, SD10324, XJ8416, LN7711, QD8166,
RC   GD7837n, QD8168, GD7811, GD7830, WH6980, XJ8451, GD7809, YN289,
RC   GD7813, SD10362, GD7825, XJ8435, GD7824, LN7710, QD8167, YN163,
RC   WH6973, and QD8147;
RA   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
RT   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from
RT   Complete Sequences.";
RL   Am. J. Hum. Genet. 0:0-0(2003).
RN   [9]{EI200}
RP   SEQUENCE FROM N.A.
RC   STRAIN=Aus14, Aus15, Aus16, Aus17, Aus20, Aus21, Aus22, Aus23, B2, B4,
RC   B6, E4, E9, F5, Y6, Y7, C1112, C1190, CAM, T1331, K11b, M306, 961,
RC   100, CP8, GP4, WE16, WE18, WE23, WE4, WE7, 36, NG12, NG29, SH10, SH17,
RC   SH19, SH23, SH29, SH33, S1216, S1220, 496, 513, DCH002, Sb13, and
RC   Sb29;
RX   MEDLINE=22723755; PubMed=12840039;
RA   Ingman M., Gyllensten U.;
RT   "Mitochondrial genome variation and evolutionary history of Australian
RT   and new guinean aborigines.";
RL   Genome Res. 13:1600-1606(2003).
CC   -!- FUNCTION: Subunits I, II and III form the functional core of the
CC       enzyme complex (By similarity){EA1}.
CC   -!- CATALYTIC ACTIVITY: 4 ferrocytochrome c + O(2) = 4 ferricytochrome
CC       c + 2 H(2)O{EA1}.
CC   -!- SIMILARITY: BELONGS TO THE CYTOCHROME C OXIDASE SUBUNIT 3
CC       FAMILY{EA1}.
DR   EMBL; AF347015; AAK17889.2; -.{EI3}
DR   EMBL; AF346963; AAK17213.1; -.{EI4}
DR   EMBL; AF346964; AAK17226.2; -.{EI5}
DR   EMBL; AF346966; AAK17252.1; -.{EI6}
DR   EMBL; AF346967; AAK17265.2; -.{EI7}
DR   EMBL; AF346968; AAK17278.2; -.{EI8}
DR   EMBL; AF346969; AAK17291.2; -.{EI9}
DR   EMBL; AF346970; AAK17304.2; -.{EI10}
DR   EMBL; AF346971; AAK17317.2; -.{EI11}
DR   EMBL; AF346972; AAK17330.2; -.{EI12}
DR   EMBL; AF346973; AAK17343.2; -.{EI13}
DR   EMBL; AF346974; AAK17356.2; -.{EI14}
DR   EMBL; AF346975; AAK17369.2; -.{EI15}
DR   EMBL; AF346976; AAK17382.1; -.{EI16}
DR   EMBL; AF346977; AAK17395.1; -.{EI17}
DR   EMBL; AF346978; AAK17408.1; -.{EI18}
DR   EMBL; AF346979; AAK17421.1; -.{EI19}
DR   EMBL; AF346980; AAK17434.2; -.{EI20}
DR   EMBL; AF346981; AAK17447.2; -.{EI21}
DR   EMBL; AF346982; AAK17460.1; -.{EI22}
DR   EMBL; AF346983; AAK17473.1; -.{EI23}
DR   EMBL; AF346984; AAK17486.2; -.{EI24}
DR   EMBL; AF346985; AAK17499.2; -.{EI25}
DR   EMBL; AF346986; AAK17512.2; -.{EI26}
DR   EMBL; AF346987; AAK17525.2; -.{EI27}
DR   EMBL; AF346990; AAK17564.1; -.{EI28}
DR   EMBL; AF346991; AAK17577.2; -.{EI29}
DR   EMBL; AF346992; AAK17590.2; -.{EI30}
DR   EMBL; AF346993; AAK17603.2; -.{EI31}
DR   EMBL; AF346994; AAK17616.2; -.{EI32}
DR   EMBL; AF346995; AAK17629.2; -.{EI33}
DR   EMBL; AF346996; AAK17642.2; -.{EI34}
DR   EMBL; AF346997; AAK17655.2; -.{EI35}
DR   EMBL; AF346998; AAK17668.2; -.{EI36}
DR   EMBL; AF346999; AAK17681.2; -.{EI37}
DR   EMBL; AF347000; AAK17694.1; -.{EI38}
DR   EMBL; AF347001; AAK17707.2; -.{EI39}
DR   EMBL; AF347002; AAK17720.2; -.{EI40}
DR   EMBL; AF347003; AAK17733.2; -.{EI41}
DR   EMBL; AF347004; AAK17746.2; -.{EI42}
DR   EMBL; AF347005; AAK17759.2; -.{EI43}
DR   EMBL; AF347006; AAK17772.2; -.{EI44}
DR   EMBL; AF347007; AAK17785.2; -.{EI45}
DR   EMBL; AF347008; AAK17798.2; -.{EI46}
DR   EMBL; AF347009; AAK17811.2; -.{EI47}
DR   EMBL; AF347011; AAK17837.2; -.{EI48}
DR   EMBL; AF347014; AAK17876.2; -.{EI49}
DR   EMBL; AF382013; AAL54806.1; -.{EI2}
DR   EMBL; AF381981; AAL54393.1; -.{EI50}
DR   EMBL; AF381982; AAL54403.1; -.{EI51}
DR   EMBL; AF381983; AAL54416.1; -.{EI52}
DR   EMBL; AF381984; AAL54429.1; -.{EI53}
DR   EMBL; AF381985; AAL54442.1; -.{EI54}
DR   EMBL; AF381986; AAL54455.1; -.{EI55}
DR   EMBL; AF381987; AAL54468.1; -.{EI56}
DR   EMBL; AF381988; AAL54481.1; -.{EI57}
DR   EMBL; AF381990; AAL54507.1; -.{EI58}
DR   EMBL; AF381991; AAL54520.1; -.{EI59}
DR   EMBL; AF381993; AAL54546.1; -.{EI60}
DR   EMBL; AF381994; AAL54559.1; -.{EI61}
DR   EMBL; AF381995; AAL54572.1; -.{EI62}
DR   EMBL; AF381996; AAL54585.1; -.{EI63}
DR   EMBL; AF381998; AAL54611.1; -.{EI64}
DR   EMBL; AF381999; AAL54624.1; -.{EI65}
DR   EMBL; AF382000; AAL54637.1; -.{EI66}
DR   EMBL; AF382001; AAL54650.1; -.{EI67}
DR   EMBL; AF382002; AAL54663.1; -.{EI68}
DR   EMBL; AF382003; AAL54676.1; -.{EI69}
DR   EMBL; AF382004; AAL54689.1; -.{EI70}
DR   EMBL; AF382005; AAL54702.1; -.{EI71}
DR   EMBL; AF382006; AAL54715.1; -.{EI72}
DR   EMBL; AF382007; AAL54728.1; -.{EI73}
DR   EMBL; AF382008; AAL54741.1; -.{EI74}
DR   EMBL; AF382009; AAL54754.1; -.{EI75}
DR   EMBL; AF382010; AAL54767.1; -.{EI76}
DR   EMBL; AF382011; AAL54780.1; -.{EI77}
DR   EMBL; AF382012; AAL54793.1; -.{EI78}
DR   EMBL; AF465941; AAN14542.1; -.{EI79}
DR   EMBL; AF465942; AAN14553.1; -.{EI80}
DR   EMBL; AF465943; AAN14564.1; -.{EI81}
DR   EMBL; AF465944; AAN14575.1; -.{EI82}
DR   EMBL; AF465945; AAN14586.1; -.{EI83}
DR   EMBL; AF465946; AAN14597.1; -.{EI84}
DR   EMBL; AF465947; AAN14608.1; -.{EI85}
DR   EMBL; AF465948; AAN14619.1; -.{EI86}
DR   EMBL; AF465949; AAN14630.1; -.{EI87}
DR   EMBL; AF465950; AAN14641.1; -.{EI88}
DR   EMBL; AF465951; AAN14652.1; -.{EI89}
DR   EMBL; AF465952; AAN14663.1; -.{EI90}
DR   EMBL; AF465953; AAN14674.1; -.{EI91}
DR   EMBL; AF465954; AAN14685.1; -.{EI92}
DR   EMBL; AF465955; AAN14696.1; -.{EI93}
DR   EMBL; AF465956; AAN14707.1; -.{EI94}
DR   EMBL; AF465958; AAN14729.1; -.{EI95}
DR   EMBL; AF465959; AAN14740.1; -.{EI96}
DR   EMBL; AF465960; AAN14751.1; -.{EI97}
DR   EMBL; AF465961; AAN14762.1; -.{EI98}
DR   EMBL; AF465962; AAN14773.1; -.{EI99}
DR   EMBL; AF465963; AAN14784.1; -.{EI100}
DR   EMBL; AF465964; AAN14795.1; -.{EI101}
DR   EMBL; AF465965; AAN14806.1; -.{EI102}
DR   EMBL; AF465966; AAN14817.1; -.{EI103}
DR   EMBL; AF465967; AAN14828.1; -.{EI104}
DR   EMBL; AF465968; AAN14839.1; -.{EI105}
DR   EMBL; AF465969; AAN14850.1; -.{EI106}
DR   EMBL; AF465970; AAN14861.1; -.{EI107}
DR   EMBL; AF465971; AAN14872.1; -.{EI108}
DR   EMBL; AF465972; AAN14883.1; -.{EI109}
DR   EMBL; AF465973; AAN14894.1; -.{EI110}
DR   EMBL; AF465974; AAN14905.1; -.{EI111}
DR   EMBL; AF465975; AAN14916.1; -.{EI112}
DR   EMBL; AF465976; AAN14927.1; -.{EI113}
DR   EMBL; AF465977; AAN14938.1; -.{EI114}
DR   EMBL; AF465978; AAN14949.1; -.{EI115}
DR   EMBL; AF465979; AAN14960.1; -.{EI116}
DR   EMBL; AF465980; AAN14971.1; -.{EI117}
DR   EMBL; AY195745; AAO88286.1; -.{EI118}
DR   EMBL; AY195747; AAO88312.1; -.{EI119}
DR   EMBL; AY195750; AAO88351.1; -.{EI120}
DR   EMBL; AY195751; AAO88364.1; -.{EI121}
DR   EMBL; AY195752; AAO88377.1; -.{EI122}
DR   EMBL; AY195754; AAO88403.1; -.{EI123}
DR   EMBL; AY195755; AAO88416.1; -.{EI124}
DR   EMBL; AY195757; AAO88442.1; -.{EI125}
DR   EMBL; AY195758; AAO88455.1; -.{EI126}
DR   EMBL; AY195759; AAO88468.1; -.{EI127}
DR   EMBL; AY195760; AAO88481.1; -.{EI128}
DR   EMBL; AY195761; AAO88494.1; -.{EI129}
DR   EMBL; AY195762; AAO88507.1; -.{EI130}
DR   EMBL; AY195763; AAO88520.1; -.{EI131}
DR   EMBL; AY195764; AAO88533.1; -.{EI132}
DR   EMBL; AY195765; AAO88546.1; -.{EI133}
DR   EMBL; AY195766; AAO88559.1; -.{EI134}
DR   EMBL; AY195767; AAO88572.1; -.{EI135}
DR   EMBL; AY195768; AAO88585.1; -.{EI136}
DR   EMBL; AY195771; AAO88624.1; -.{EI137}
DR   EMBL; AY195772; AAO88637.1; -.{EI138}
DR   EMBL; AY195773; AAO88650.1; -.{EI139}
DR   EMBL; AY195774; AAO88663.1; -.{EI140}
DR   EMBL; AY195775; AAO88676.1; -.{EI141}
DR   EMBL; AY195776; AAO88689.1; -.{EI142}
DR   EMBL; AY195777; AAO88702.1; -.{EI143}
DR   EMBL; AY195778; AAO88715.1; -.{EI144}
DR   EMBL; AY195779; AAO88728.1; -.{EI145}
DR   EMBL; AY195780; AAO88741.1; -.{EI146}
DR   EMBL; AY195781; AAO88754.1; -.{EI147}
DR   EMBL; AY195782; AAO88767.1; -.{EI148}
DR   EMBL; AY195783; AAO88780.1; -.{EI149}
DR   EMBL; AY195784; AAO88793.1; -.{EI150}
DR   EMBL; AY195786; AAO88819.1; -.{EI151}
DR   EMBL; AY195787; AAO88832.1; -.{EI152}
DR   EMBL; AY195788; AAO88845.1; -.{EI153}
DR   EMBL; AY195790; AAO88871.1; -.{EI154}
DR   EMBL; AY195791; AAO88884.1; -.{EI155}
DR   EMBL; AY195792; AAO88897.1; -.{EI156}
DR   EMBL; AY255133; AAO66624.1; -.{EI157}
DR   EMBL; AY255134; AAO66637.1; -.{EI158}
DR   EMBL; AY255135; AAO66650.1; -.{EI159}
DR   EMBL; AY255136; AAO66663.1; -.{EI160}
DR   EMBL; AY255138; AAO66689.1; -.{EI161}
DR   EMBL; AY255139; AAO66702.1; -.{EI162}
DR   EMBL; AY255140; AAO66715.1; -.{EI163}
DR   EMBL; AY255141; AAO66728.1; -.{EI164}
DR   EMBL; AY255142; AAO66741.1; -.{EI165}
DR   EMBL; AY255143; AAO66754.1; -.{EI166}
DR   EMBL; AY255144; AAO66767.1; -.{EI167}
DR   EMBL; AY255145; AAO66780.1; -.{EI168}
DR   EMBL; AY255146; AAO66793.1; -.{EI169}
DR   EMBL; AY255147; AAO66806.1; -.{EI170}
DR   EMBL; AY255148; AAO66819.1; -.{EI171}
DR   EMBL; AY255150; AAO66845.1; -.{EI172}
DR   EMBL; AY255151; AAO66858.1; -.{EI173}
DR   EMBL; AY255152; AAO66871.1; -.{EI174}
DR   EMBL; AY255153; AAO66884.1; -.{EI175}
DR   EMBL; AY255154; AAO66897.1; -.{EI176}
DR   EMBL; AY255155; AAO66910.1; -.{EI177}
DR   EMBL; AY255156; AAO66923.1; -.{EI178}
DR   EMBL; AY255157; AAO66936.1; -.{EI179}
DR   EMBL; AY255158; AAO66949.1; -.{EI180}
DR   EMBL; AY255160; AAO66975.1; -.{EI181}
DR   EMBL; AY255162; AAO67001.1; -.{EI182}
DR   EMBL; AY255163; AAO67014.1; -.{EI183}
DR   EMBL; AY255164; AAO67027.1; -.{EI184}
DR   EMBL; AY255165; AAO67040.1; -.{EI185}
DR   EMBL; AY255166; AAO67053.1; -.{EI186}
DR   EMBL; AY255167; AAO67066.1; -.{EI187}
DR   EMBL; AY255168; AAO67079.1; -.{EI188}
DR   EMBL; AY255169; AAO67091.1; -.{EI189}
DR   EMBL; AY255170; AAO67104.1; -.{EI190}
DR   EMBL; AY255171; AAO67117.1; -.{EI191}
DR   EMBL; AY255172; AAO67130.1; -.{EI192}
DR   EMBL; AY255174; AAO67156.1; -.{EI193}
DR   EMBL; AY255175; AAO67169.1; -.{EI194}
DR   EMBL; AY255176; AAO67182.1; -.{EI195}
DR   EMBL; AY255177; AAO67195.1; -.{EI196}
DR   EMBL; AY255178; AAO67208.1; -.{EI197}
DR   EMBL; AY255179; AAO67221.1; -.{EI198}
DR   EMBL; AY255180; AAO67234.1; -.{EI199}
DR   EMBL; AY289052; AAP47899.1; -.{EI200}
DR   EMBL; AY289053; AAP47912.1; -.{EI201}
DR   EMBL; AY289054; AAP47925.1; -.{EI202}
DR   EMBL; AY289055; AAP47938.1; -.{EI203}
DR   EMBL; AY289056; AAP47951.1; -.{EI204}
DR   EMBL; AY289057; AAP47964.1; -.{EI205}
DR   EMBL; AY289058; AAP47977.1; -.{EI206}
DR   EMBL; AY289059; AAP47990.1; -.{EI207}
DR   EMBL; AY289060; AAP48003.1; -.{EI208}
DR   EMBL; AY289061; AAP48016.1; -.{EI209}
DR   EMBL; AY289062; AAP48029.1; -.{EI210}
DR   EMBL; AY289063; AAP48042.1; -.{EI211}
DR   EMBL; AY289064; AAP48055.1; -.{EI212}
DR   EMBL; AY289065; AAP48068.1; -.{EI213}
DR   EMBL; AY289066; AAP48081.1; -.{EI214}
DR   EMBL; AY289067; AAP48094.1; -.{EI215}
DR   EMBL; AY289068; AAP48107.1; -.{EI216}
DR   EMBL; AY289069; AAP48120.1; -.{EI217}
DR   EMBL; AY289070; AAP48133.1; -.{EI218}
DR   EMBL; AY289071; AAP48146.1; -.{EI219}
DR   EMBL; AY289072; AAP48159.1; -.{EI220}
DR   EMBL; AY289074; AAP48185.1; -.{EI221}
DR   EMBL; AY289075; AAP48198.1; -.{EI222}
DR   EMBL; AY289076; AAP48211.1; -.{EI223}
DR   EMBL; AY289077; AAP48224.1; -.{EI224}
DR   EMBL; AY289078; AAP48237.1; -.{EI225}
DR   EMBL; AY289079; AAP48250.1; -.{EI226}
DR   EMBL; AY289080; AAP48263.1; -.{EI227}
DR   EMBL; AY289081; AAP48276.1; -.{EI228}
DR   EMBL; AY289082; AAP48289.1; -.{EI229}
DR   EMBL; AY289083; AAP48302.1; -.{EI230}
DR   EMBL; AY289084; AAP48315.1; -.{EI231}
DR   EMBL; AY289085; AAP48328.1; -.{EI232}
DR   EMBL; AY289086; AAP48341.1; -.{EI233}
DR   EMBL; AY289087; AAP48354.1; -.{EI234}
DR   EMBL; AY289088; AAP48367.1; -.{EI235}
DR   EMBL; AY289089; AAP48380.1; -.{EI236}
DR   EMBL; AY289090; AAP48393.1; -.{EI237}
DR   EMBL; AY289091; AAP48406.1; -.{EI238}
DR   EMBL; AY289092; AAP48419.1; -.{EI239}
DR   EMBL; AY289093; AAP48431.1; -.{EI240}
DR   EMBL; AY289094; AAP48444.1; -.{EI241}
DR   EMBL; AY289095; AAP48457.1; -.{EI242}
DR   EMBL; AY289096; AAP48470.1; -.{EI243}
DR   EMBL; AY289099; AAP48509.1; -.{EI244}
DR   EMBL; AY289100; AAP48522.1; -.{EI245}
DR   EMBL; AY289101; AAP48535.1; -.{EI246}
DR   EMBL; AY289102; AAP48548.1; -.{EI247}
DR   GO; GO:0016021; C:integral to membrane; IEA.
DR   GO; GO:0005739; C:mitochondrion; IEA.
DR   GO; GO:0004129; F:cytochrome-c oxidase activity; IEA.
DR   GO; GO:0016491; F:oxidoreductase activity; IEA.
DR   GO; GO:0006118; P:electron transport; IEA.
DR   InterPro; IPR000298; CytC_oxdse_III.
DR   Pfam; PF00510; COX3; 1.
DR   ProDom; PD000382; CytC_oxdse_III; 1.
DR   TIGRFAMs; TIGR01732; tiny_TM_bacill; 1.
DR   PROSITE; PS50253; COX3; 1.
KW   Oxidoreductase{EA1}; Transmembrane{EA1}; Mitochondrion{EA1,EP248}.
**
**   #################     SOURCE SECTION     ##################
**   Homo sapiens mitochondrion, complete genome.
**   [1]
**   1-16571
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16571
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16571
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16569
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16569
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16574
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16574
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16574
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16569
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16569
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16567
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16567
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16567
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16567
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16567
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16567
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16570
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16570
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16567
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16567
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16570
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16570
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16570
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16570
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16571
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16571
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16571
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16567
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16567
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16570
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16570
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16569
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16569
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16569
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16569
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16569
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16569
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16570
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16570
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16570
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16570
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16569
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16569
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16569
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16569
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16570
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16570
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16567
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16567
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16567
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16567
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16566
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16566
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16566
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16569
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16569
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16570
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16570
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16571
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16571
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16571
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16558
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16558
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16558
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16569
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16569
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16568
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16568
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16568
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16566
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16566
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16566
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16567
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16567
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16561
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16561
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16561
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16562
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16562
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16562
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16569
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16569
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16561
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16561
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16561
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16572
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16572
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16572
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16570
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16570
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16572
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16572
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16572
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16572
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16572
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16572
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16568
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16568
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16568
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16560
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16560
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16560
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16569
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16569
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16567
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16567
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16562
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16562
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16562
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   MEDLINE; 21012010.
**   PUBMED; 11130070.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "Mitochondrial genome variation and the origin of modern humans";
**   Nature 408(6813):708-713(2000).
**   [2]
**   1-16567
**   MEDLINE; 21176314.
**   PUBMED; 11279504.
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   "correction: Mitochondrial genome variation and the origin of modern
**   humans";
**   Nature 410(6828):611-611(2001).
**   [3]
**   1-16567
**   Ingman M., Kaessmann H., Paabo S., Gyllensten U.;
**   ;
**   Submitted (09-FEB-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16567
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16568
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16568
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16569
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16569
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16567
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16567
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16570
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16570
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16570
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16570
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16567
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16567
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16569
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16569
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16567
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16567
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16569
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16569
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16567
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16567
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16569
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16569
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16567
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16567
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16569
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16569
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16571
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16571
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16567
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16567
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16567
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16567
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16569
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16569
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16568
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16568
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16570
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16570
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16569
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16569
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16569
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16569
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16572
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16572
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16570
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16570
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16560
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16560
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16569
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16569
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16564
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16564
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16568
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16568
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16568
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16568
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-16569
**   PUBMED; 11553319.
**   Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C., Cabrera V.M.;
**   "Major genomic mitochondrial lineages delineate early human expansions";
**   BMC Genet. 2(1):13-13(2001).
**   [2]
**   1-16569
**   Cabrera V.M., Maca-Meyer N., Gonzalez A.M., Larruga J.M., Flores C.;
**   ;
**   Submitted (18-MAY-2001) to the EMBL/GenBank/DDBJ databases.
**   Genetics, University of La Laguna, Biology, La Laguna, Tenerife 38271,
**   Spain
**   [1]
**   1-8828
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8828
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8827
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8827
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8828
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8828
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8820
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8820
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8820
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8820
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-8829
**   MEDLINE; 22062553.
**   PUBMED; 12022039.
**   Silva W.A. Jr., Bonatto S.L., Holanda A.J., Ribeiro-Dos-Santos A.K.,
**   Paixao B.M., Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larson M.L., Petzl-Erler M.L., Valente V., Santos S.E., Zago M.A.;
**   "Mitochondrial genome diversity of Native Americans supports a single early
**   entry of founder populations into America";
**   Am. J. Hum. Genet. 71(1):187-192(2002).
**   [2]
**   1-8829
**   Silva W.A. Jr., Bonatto S.L., Ribeiro-Dos-Santos A.K., Paixao M.,
**   Goldman G.H., Abe-Sandes K., Rodriguez-Delfin L., Barbosa M.,
**   Paco-Larcon M.L., Petzl-Erles M.L., Valente V., Santos S.E., Zago M.A.;
**   ;
**   Submitted (06-JAN-2002) to the EMBL/GenBank/DDBJ databases.
**   Molecular Biology, Center for Cell-Based Therapy, Rua Tenente Catao Roxo,
**   2501, Ribeirao Preto, Sao Paulo 14051-140, Brazil
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16568
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16568
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16566
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16566
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16568
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16568
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16565
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16565
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16569
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16569
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16569
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16569
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16572
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16572
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16568
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16568
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16567
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16567
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16568
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16568
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16569
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16569
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16569
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16569
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16568
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16568
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16569
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16569
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16557
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16557
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16569
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16569
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16567
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16567
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16568
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16568
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16565
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16565
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16566
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16566
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16566
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16566
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16570
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16570
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16567
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16567
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16569
**   MEDLINE; 22406325.
**   PUBMED; 12509511.
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   "Natural selection shaped regional mtDNA variation in humans";
**   Proc. Natl. Acad. Sci. U.S.A. 100(1):171-176(2003).
**   [2]
**   1-16569
**   Mishmar D., Ruiz-Pesini E., Golik P., Macaulay V., Clark A.G., Hosseini S.,
**   Brandon M., Easley K., Chen E., Brown M.D., Sukernik R.I., Olckers A.,
**   Wallace D.C.;
**   ;
**   Submitted (11-DEC-2002) to the EMBL/GenBank/DDBJ databases.
**   MAMMAG, University of California, Irvine, 2nd Floor Hewitt Hall, Irvine,
**   California 92697-3940, USA
**   [1]
**   1-16556
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16556
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16492
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16492
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16562
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16562
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16486
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16486
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16571
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16571
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16561
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16561
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16558
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16558
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16574
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16574
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16579
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16579
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16567
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16567
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16576
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16576
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16567
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16567
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16567
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16567
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16575
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16575
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16566
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16566
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16558
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16558
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16566
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16566
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16569
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16562
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16562
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16567
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16567
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16570
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16576
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16576
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16559
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16559
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   "Phylogeny of East Asian Mitochondrial DNA Lineages Inferred from Complete
**   Sequences";
**   Am. J. Hum. Genet. 0:0-0(2003).
**   [2]
**   1-16568
**   Kong Q.-P., Yao Y.-G., Sun C., Bandelt H.-J., Zhu C.-L., Zhang Y.-P.;
**   ;
**   Submitted (12-MAR-2003) to the EMBL/GenBank/DDBJ databases.
**   Chinese Academy of Sciences, Laboratory of Molecular Evolution and Genome
**   Diversity, Kunming Institute of Zoology, Kunming, Yunnan 650223, China
**   [1]
**   1-16572
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16572
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16571
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16571
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16570
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16570
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16570
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16570
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16570
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16575
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16575
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16570
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16570
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16572
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16572
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16573
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16573
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16572
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16572
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16559
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16559
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16560
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16560
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16567
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16567
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16568
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16568
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16571
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16571
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16560
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16560
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16561
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16561
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16570
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16570
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16561
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16561
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16568
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16568
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16570
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16561
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16561
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16572
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16572
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16570
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16570
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16571
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16571
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16561
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16561
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16561
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16561
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16574
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16574
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16572
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16572
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16569
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16569
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16561
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16561
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16560
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16560
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   [1]
**   1-16560
**   MEDLINE; 22723755.
**   PUBMED; 12840039.
**   Ingman M., Gyllensten U.;
**   "Mitochondrial genome variation and evolutionary history of Australian and
**   new guinean aborigines";
**   Genome Res. 13(7):1600-1606(2003).
**   [2]
**   1-16560
**   Ingman M., Gyllensten U.;
**   ;
**   Submitted (02-MAY-2003) to the EMBL/GenBank/DDBJ databases.
**   Genetics and Pathology, Uppsala University, Rudbeck Laboratory, Dag
**   Hammarsjolds vag 20, Uppsala 751 85, Sweden
**   source          1..16571
**                   /db_xref="taxon:9606"
**                   /note="from African (Yoruba) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17889.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16569
**                   /country="Australia"
**                   /db_xref="taxon:9606"
**                   /note="from Aborigine"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17213.1"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 2)
**   source          1..16574
**                   /country="Australia"
**                   /db_xref="taxon:9606"
**                   /note="from Aborigine"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9212..9992
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17226.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /note="from Asian Indian"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17252.1"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 2)
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /note="from African (Bamileke)"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17265.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 4)
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /note="from African (Biaka)"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17278.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /note="from African (Biaka)"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17291.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /note="from Buriat individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17304.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /note="from Chukchi individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17317.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16570
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17330.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16570
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17343.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16571
**                   /db_xref="taxon:9606"
**                   /note="from Crimean Tatar individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9209..9989
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17356.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /note="from Dutch individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17369.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /note="from African (Effik) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17382.1"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 2)
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /note="from African (Effik) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17395.1"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 2)
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /note="from English individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17408.1"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /note="from Evenki individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17421.1"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 2)
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /note="from African (Ewondo) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17434.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /note="from French individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17447.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /note="from Georgian individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17460.1"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /note="from German individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17473.1"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /note="from South American Indian (Guarani) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17486.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /note="from African (Hausa) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17499.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /note="from African (Ibo) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17512.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16566
**                   /db_xref="taxon:9606"
**                   /note="from African (Ibo) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17525.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /note="from Japanese individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17564.1"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /note="from Khirgiz individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17577.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16571
**                   /db_xref="taxon:9606"
**                   /note="from African (Kikuyu) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9209..9989
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17590.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16558
**                   /db_xref="taxon:9606"
**                   /note="from Korean individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9196..9976
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17603.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /note="from African (Lisongo) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17616.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16568
**                   /db_xref="taxon:9606"
**                   /note="from African (Mandenka) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17629.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16566
**                   /db_xref="taxon:9606"
**                   /note="from African (Mbenzele) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9204..9984
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17642.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /note="from African (Mbenzele) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17655.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16561
**                   /db_xref="taxon:9606"
**                   /note="from African (Mbuti) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9199..9979
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17668.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16562
**                   /db_xref="taxon:9606"
**                   /note="from African (Mbuti) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9200..9980
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17681.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /note="from African (Mkamba) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17694.1"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 2)
**   source          1..16561
**                   /db_xref="taxon:9606"
**                   /note="from North American Indian (Piman) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9199..9979
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17707.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16572
**                   /db_xref="taxon:9606"
**                   /note="from PNG (Coast) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9210..9990
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17720.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /note="from PNG (Coast) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17733.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16572
**                   /db_xref="taxon:9606"
**                   /note="from PNG (Highland) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9210..9990
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17746.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16572
**                   /db_xref="taxon:9606"
**                   /note="from PNG (Highland) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9209..9989
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17759.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16568
**                   /db_xref="taxon:9606"
**                   /note="from Saami individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17772.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16560
**                   /db_xref="taxon:9606"
**                   /note="from Samoan individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9198..9978
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17785.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /note="from African (San) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17798.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /note="from African (San) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17811.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16562
**                   /db_xref="taxon:9606"
**                   /note="from Uzbek individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9200..9980
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17837.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /note="from African (Yoruba) individual"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAK17876.2"
**   CDS_IN_EMBL_ENTRY 13
**   12-APR-2001 (Rel. 67, Last updated, Version 3)
**   source          1..16567
**                   /country="India"
**                   /db_xref="taxon:9606"
**                   /haplotype="M*2"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="2619"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54806.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16568
**                   /country="Mauritania"
**                   /db_xref="taxon:9606"
**                   /haplotype="L2"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="441"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome co oxidase subunit III"
**                   /protein_id="AAL54393.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16569
**                   /country="Spain: Canary Islands, Tenerife"
**                   /db_xref="taxon:9606"
**                   /haplotype="U31"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="117"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54403.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16567
**                   /country="Morocco"
**                   /db_xref="taxon:9606"
**                   /haplotype="U32"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="249"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54416.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16570
**                   /country="Morocco"
**                   /db_xref="taxon:9606"
**                   /haplotype="M11"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="250"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54429.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16570
**                   /country="Morocco"
**                   /db_xref="taxon:9606"
**                   /haplotype="T5"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="252"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54442.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16567
**                   /country="Morocco"
**                   /db_xref="taxon:9606"
**                   /haplotype="X"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="255"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54455.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16569
**                   /country="Morocco"
**                   /db_xref="taxon:9606"
**                   /haplotype="J1b"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="268"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54468.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16567
**                   /country="Morocco"
**                   /db_xref="taxon:9606"
**                   /haplotype="L1a"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="271"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54481.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16569
**                   /country="Morocco: Berber"
**                   /db_xref="taxon:9606"
**                   /haplotype="V"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="364"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54507.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16567
**                   /country="Mauritania"
**                   /db_xref="taxon:9606"
**                   /haplotype="L3b"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="430"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54520.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16569
**                   /country="Mauritania"
**                   /db_xref="taxon:9606"
**                   /haplotype="H1"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="446"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54546.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16567
**                   /country="Mauritania"
**                   /db_xref="taxon:9606"
**                   /haplotype="L1b"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="451"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54559.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16569
**                   /country="Jordan"
**                   /db_xref="taxon:9606"
**                   /haplotype="U21"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="766"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54572.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16571
**                   /country="Jordan"
**                   /db_xref="taxon:9606"
**                   /haplotype="M12"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="771"
**   CDS             9209..9989
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54585.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16567
**                   /country="Jordan"
**                   /db_xref="taxon:9606"
**                   /haplotype="L3d"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="800"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54611.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16567
**                   /country="Jordan"
**                   /db_xref="taxon:9606"
**                   /haplotype="N1b"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="832"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54624.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16569
**                   /country="Jordan"
**                   /db_xref="taxon:9606"
**                   /haplotype="U2"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="842"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54637.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16568
**                   /country="Spain: Maragato"
**                   /db_xref="taxon:9606"
**                   /haplotype="J2"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="M26"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54650.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16570
**                   /country="Spain: Maragato"
**                   /db_xref="taxon:9606"
**                   /haplotype="H2"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="M27"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54663.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16569
**                   /country="Spain: Maragato"
**                   /db_xref="taxon:9606"
**                   /haplotype="W"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="M47"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54676.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16569
**                   /country="Spain: Leon"
**                   /db_xref="taxon:9606"
**                   /haplotype="U22"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="M68"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54689.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16572
**                   /country="Spain: Leon"
**                   /db_xref="taxon:9606"
**                   /haplotype="K"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="M72"
**   CDS             9210..9990
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54702.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16570
**                   /country="Spain: Leon"
**                   /db_xref="taxon:9606"
**                   /haplotype="T1"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="M78"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54715.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16560
**                   /country="Spain: Leon"
**                   /db_xref="taxon:9606"
**                   /haplotype="I"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="M90"
**   CDS             9198..9978
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54728.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16569
**                   /country="Morocco"
**                   /db_xref="taxon:9606"
**                   /haplotype="U6"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="279"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54741.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16564
**                   /country="Spain: Canary Islands, Tenerife"
**                   /db_xref="taxon:9606"
**                   /haplotype="C"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="4"
**   CDS             9202..9982
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54754.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16568
**                   /country="Spain: Canary Islands, Hierro"
**                   /db_xref="taxon:9606"
**                   /haplotype="A"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="248"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54767.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16568
**                   /country="Spain: Andalusia"
**                   /db_xref="taxon:9606"
**                   /haplotype="U7"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="1646"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54780.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..16569
**                   /country="Philippines"
**                   /db_xref="taxon:9606"
**                   /haplotype="M*1"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="2601"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAL54793.1"
**   CDS_IN_EMBL_ENTRY 13
**   02-JAN-2002 (Rel. 70, Last updated, Version 1)
**   source          1..8828
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="African"
**                   /clone="NGR0524"
**   CDS             2059..2839
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14542.1"
**   CDS_IN_EMBL_ENTRY 10
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8827
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="African"
**                   /clone="NGR0522"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14553.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="African"
**                   /clone="NGR0475"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14564.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="African"
**                   /clone="NGR0510"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14575.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="ARL0058"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14586.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Japanese"
**                   /clone="JAP1043"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14597.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Japanese"
**                   /clone="JAP1045"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14608.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8828
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Japanese"
**                   /clone="JAP1044"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14619.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="GRC0149"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14630.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="KCR0029"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14641.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="KRC0033"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14652.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="GRC0131"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14663.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="GRC0169"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14674.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="KTN0018"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14685.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="KTN0209"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14696.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="KTN0130"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14707.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8820
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="KPO0001"
**   CDS             2051..2831
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14729.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8820
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="KPO0039"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14740.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="KPO0023"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14751.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="PTJ0068"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14762.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="PTJ0003"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14773.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="PTJ0001"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14784.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Peru"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="QUE1876"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14795.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Peru"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="QUE1881"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14806.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Peru"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="QUE1875"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14817.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Peru"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="QUE1878"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14828.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="QUE1880"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14839.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="TYR0004"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14850.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="TYR0016"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14861.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Caucasian"
**                   /clone="WTE1145"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14872.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Caucasian"
**                   /clone="WTE1182"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14883.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Caucasian"
**                   /clone="WTE1150"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14894.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="WPI0167"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14905.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="YAN0623"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14916.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="YAN0665"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14927.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="YAN0669"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14938.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="YAN0591"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14949.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="YAN0650"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14960.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..8829
**                   /country="Brazil"
**                   /db_xref="taxon:9606"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Amerindian"
**                   /clone="YAN0637"
**   CDS             2060..2840
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_table=2
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAN14971.1"
**   CDS_IN_EMBL_ENTRY 11
**   20-SEP-2002 (Rel. 73, Last updated, Version 1)
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="E12T"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="T haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88286.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16568
**                   /db_xref="taxon:9606"
**                   /haplotype="E4H"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="H haplogroup"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88312.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="E17V"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="V haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88351.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16566
**                   /db_xref="taxon:9606"
**                   /haplotype="E2H"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="H haplogroup"
**   CDS             9204..9984
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9984,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88364.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="E7H"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="H haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88377.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="E9J"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="J haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88403.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16568
**                   /db_xref="taxon:9606"
**                   /haplotype="As11G"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="G haplogroup"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88416.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="E6H"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="H haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88442.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="E5H"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="H haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88455.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16565
**                   /db_xref="taxon:9606"
**                   /haplotype="Na4C"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Native American"
**                   /isolation_source="C haplogroup Native American"
**   CDS             9203..9983
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9983,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88468.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="As1A"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="A haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88481.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /haplotype="As12Z"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Z Haplogroup"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88494.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /haplotype="As7G"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="G Halogroup"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88507.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16572
**                   /db_xref="taxon:9606"
**                   /haplotype="As4C"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Asian"
**                   /isolation_source="C Haplogroup Asian"
**   CDS             9210..9990
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9990,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88520.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="E19U"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="U haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88533.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16568
**                   /db_xref="taxon:9606"
**                   /haplotype="E13K"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="K haplogroup"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88546.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /haplotype="A11L2b"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="L2b haplogroup"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9985,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88559.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="E11T"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="T haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88572.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="E15W"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="W haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88585.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16568
**                   /db_xref="taxon:9606"
**                   /haplotype="As2A"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Asian"
**                   /isolation_source="A haplogroup Asian"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88624.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="As5C"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Asian"
**                   /isolation_source="C haplogroup Asian"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88637.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /haplotype="E18X"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="European"
**                   /isolation_source="X haplogroup European"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88650.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /haplotype="E10J"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="J haplogroup"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88663.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16568
**                   /db_xref="taxon:9606"
**                   /haplotype="E1H"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="H haplogroup"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88676.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /haplotype="A9L2a"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="L2a haplogroup"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88689.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16557
**                   /db_xref="taxon:9606"
**                   /haplotype="A10L1A2"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="L1A2 haplogroup"
**   CDS             9195..9975
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9975,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88702.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /haplotype="E8J"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="J haplogroup"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88715.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="E14W"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="W haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88728.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /haplotype="A2L1"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="L1a haplogroup"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9985,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88741.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="E16V"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="V haplogroup"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88754.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16568
**                   /db_xref="taxon:9606"
**                   /haplotype="A7NL"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="L3 haplogroup"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88767.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16565
**                   /db_xref="taxon:9606"
**                   /haplotype="A4L1B2"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="L1B2 haplogroup"
**   CDS             9203..9983
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9983,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88780.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16566
**                   /db_xref="taxon:9606"
**                   /haplotype="A8NL"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="L3 haplogroup"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9985,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88793.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16566
**                   /db_xref="taxon:9606"
**                   /haplotype="Na5A"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Native American"
**                   /isolation_source="A haplogroup Native American"
**   CDS             9204..9984
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9984,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88819.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="Na3X"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Native American"
**                   /isolation_source="X haplogroup Native American"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88832.1"
**   11-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="A5L2A1"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="L2A1 haplogroup"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88845.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /haplotype="As8D"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Asian"
**                   /isolation_source="D haplogroup Asian"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88871.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /haplotype="As10F"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="F haplogroup"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9985,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88884.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /haplotype="As9Y"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Y haplogroup"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome oxidase subunit III"
**                   /protein_id="AAO88897.1"
**   10-APR-2003 Last updated, EMBL entry
**   source          1..16556
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GD7812"
**                   /isolation_source="Han from Zhanjiang, Guangdong"
**   CDS             9195..9975
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9975,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66624.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16492
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="LN7550"
**                   /isolation_source="Han from Fengcheng, Liaoning"
**   CDS             9130..9910
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9910,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66637.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16562
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="LN7589"
**                   /isolation_source="Han from Fengcheng, Liaoning"
**   CDS             9200..9980
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9980,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66650.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16486
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="SD10313"
**                   /isolation_source="Han from Tai'an, Shandong"
**   CDS             9124..9904
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9904,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66663.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16571
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="XJ8426"
**                   /isolation_source="Han from Yili, Xinjiang"
**   CDS             9209..9989
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9989,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66689.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="EWK28"
**                   /isolation_source="Ewenki from Inner Mongolia"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66702.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16561
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="QD8141"
**                   /isolation_source="Han from Qingdao, Shandong"
**   CDS             9199..9979
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9979,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66715.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GD7834"
**                   /isolation_source="Han from Zhanjiang, Guangdong"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66728.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Miao271"
**                   /isolation_source="Miao from Fenghuang, Hunan"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66741.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="DW48"
**                   /isolation_source="Daur from Inner Mongolia"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66754.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16568
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="WH6954"
**                   /isolation_source="Han from Wuhan, Hubei"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66767.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16558
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="WH6967"
**                   /isolation_source="Han from Wuhan, Hubei"
**   CDS             9196..9976
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9976,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66780.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Mg246"
**                   /isolation_source="Mongolian from Inner Mongolia"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66793.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16574
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="LN7595"
**                   /isolation_source="Han from Fengcheng, Liaoning"
**   CDS             9212..9992
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9992,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66806.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GD7817"
**                   /isolation_source="Han from Zhanjiang, Guangdong"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66819.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16579
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="WH6958"
**                   /isolation_source="Han from Wuhan, Hubei"
**   CDS             9217..9997
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9997,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66845.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16568
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GD7829"
**                   /isolation_source="Han from Zhanjiang, Guangdong"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66858.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16567
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="SD10352"
**                   /isolation_source="Han from Tai'an, Shandong"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9985,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66871.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="XJ8420"
**                   /isolation_source="Han from Yili, Xinjiang"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66884.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16576
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="SD10334"
**                   /isolation_source="Han from Tai'an, Shandong"
**   CDS             9214..9994
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9994,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66897.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16568
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="WH6979"
**                   /isolation_source="Han from Wuhan, Hubei"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66910.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="SD10324"
**                   /isolation_source="Han from Tai'an, Shandong"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66923.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="XJ8416"
**                   /isolation_source="Han from Yili, Xinjiang"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66936.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16567
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="LN7711"
**                   /isolation_source="Han from Fengcheng, Liaoning"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9985,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66949.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="QD8166"
**                   /isolation_source="Han from Qingdao, Shandong"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO66975.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16567
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GD7837n"
**                   /isolation_source="Han from Zhanjiang, Guangdong"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9985,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67001.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16575
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="QD8168"
**                   /isolation_source="Han from Qingdao, Shandong"
**   CDS             9213..9993
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9993,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67014.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16566
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GD7811"
**                   /isolation_source="Han from Zhanjiang, Guangdong"
**   CDS             9204..9984
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9984,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67027.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16558
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GD7830"
**                   /isolation_source="Han from Zhanjiang, Guangdong"
**   CDS             9196..9976
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9976,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67040.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16566
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="WH6980"
**                   /isolation_source="Han from Wuhan, Hubei"
**   CDS             9204..9984
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9984,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67053.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="XJ8451"
**                   /isolation_source="Han from Yili, Xinjiang"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67066.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16568
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GD7809"
**                   /isolation_source="Han from Zhanjiang, Guangdong"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67079.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="YN289"
**                   /isolation_source="Han from Kunming, Yunnan"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67091.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16562
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GD7813"
**                   /isolation_source="Han from Zhanjiang, Guangdong"
**   CDS             9200..9980
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9980,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67104.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="SD10362"
**                   /isolation_source="Han from Tai'an, Shandong"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67117.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16568
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GD7825"
**                   /isolation_source="Han from Zhanjiang, Guangdong"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67130.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="XJ8435"
**                   /isolation_source="Han from Yili, Xinjiang"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67156.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16567
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GD7824"
**                   /isolation_source="Han from Zhanjiang, Guangdong"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9985,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67169.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="LN7710"
**                   /isolation_source="Han from Fengcheng, Liaoning"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67182.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16568
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="QD8167"
**                   /isolation_source="Han from Qingdao, Shandong"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67195.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16576
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="YN163"
**                   /isolation_source="Han from Kunming, Yunnan"
**   CDS             9214..9994
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9994,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67208.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16559
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="WH6973"
**                   /isolation_source="Han from Wuhan, Hubei"
**   CDS             9197..9977
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9977,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67221.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16568
**                   /country="China"
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="QD8147"
**                   /isolation_source="Han from Qingdao, Shandong"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAO67234.1"
**   18-JUL-2003 Last updated, EMBL entry
**   source          1..16572
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Aus14"
**                   /isolation_source="Australian Aborigine"
**   CDS             9210..9990
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9990,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP47899.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16571
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Aus15"
**                   /isolation_source="Australian Aborigine"
**   CDS             9209..9989
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9989,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP47912.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Aus16"
**                   /isolation_source="Australian Aborigine"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP47925.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Aus17"
**                   /isolation_source="Australian Aborigine"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP47938.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Aus20"
**                   /isolation_source="Australian Aborigine"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP47951.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Aus21"
**                   /isolation_source="Australian Aborigine"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP47964.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Aus22"
**                   /isolation_source="Australian Aborigine"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP47977.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16575
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Aus23"
**                   /isolation_source="Australian Aborigine"
**   CDS             9212..9992
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9992,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP47990.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="B2"
**                   /isolation_source="Australian Aborigine"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48003.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="B4"
**                   /isolation_source="Australian Aborigine"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48016.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="B6"
**                   /isolation_source="Australian Aborigine"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48029.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="E4"
**                   /isolation_source="Australian Aborigine"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48042.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="E9"
**                   /isolation_source="Australian Aborigine"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48055.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16572
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="F5"
**                   /isolation_source="Australian Aborigine"
**   CDS             9210..9990
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9990,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48068.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16573
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Y6"
**                   /isolation_source="Australian Aborigine"
**   CDS             9211..9991
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9991,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48081.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16572
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Y7"
**                   /isolation_source="Australian Aborigine"
**   CDS             9210..9990
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9990,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48094.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16559
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="C1112"
**                   /isolation_source="Cook Islander"
**   CDS             9197..9977
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9977,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48107.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16560
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="C1190"
**                   /isolation_source="Cook Islander"
**   CDS             9198..9978
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9978,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48120.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="CAM"
**                   /isolation_source="Filipino"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48133.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16567
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="T1331"
**                   /isolation_source="Southern Indian (Kannada)"
**   CDS             9205..9985
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9985,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48146.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16568
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="K11b"
**                   /isolation_source="Southern Indian (Koraga)"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48159.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="M306"
**                   /isolation_source="Southern Indian (Mullukurunan)"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48185.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16571
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="961"
**                   /isolation_source="Nasioi"
**   CDS             9209..9989
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9989,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48198.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16560
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="100"
**                   /isolation_source="Papua New Guinean (Coastal)"
**   CDS             9197..9977
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9977,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48211.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16561
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="CP8"
**                   /isolation_source="Papua New Guinean (Coastal)"
**   CDS             9198..9978
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9978,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48224.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="GP4"
**                   /isolation_source="Papua New Guinean (Coastal)"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48237.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="WE16"
**                   /isolation_source="Papua New Guinean (Coastal)"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48250.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16561
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="WE18"
**                   /isolation_source="Papua New Guinean (Coastal)"
**   CDS             9198..9978
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9978,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48263.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16568
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="WE23"
**                   /isolation_source="Papua New Guinean (Coastal)"
**   CDS             9206..9986
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9986,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48276.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="WE4"
**                   /isolation_source="Papua New Guinean (Coastal)"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48289.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16561
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="WE7"
**                   /isolation_source="Papua New Guinean (Coastal)"
**   CDS             9198..9978
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9978,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48302.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="36"
**                   /isolation_source="Papua New Guinean (Highland)"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48315.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="NG12"
**                   /isolation_source="Papua New Guinean (Highland)"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48328.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="NG29"
**                   /isolation_source="Papua New Guinean (Highland)"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48341.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16572
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="SH10"
**                   /isolation_source="Papua New Guinean (Highland)"
**   CDS             9210..9990
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9990,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48354.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16570
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="SH17"
**                   /isolation_source="Papua New Guinean (Highland)"
**   CDS             9208..9988
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9988,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48367.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="SH19"
**                   /isolation_source="Papua New Guinean (Highland)"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48380.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="SH23"
**                   /isolation_source="Papua New Guinean (Highland)"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48393.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16571
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="SH29"
**                   /isolation_source="Papua New Guinean (Highland)"
**   CDS             9209..9989
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9989,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48406.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="SH33"
**                   /isolation_source="Papua New Guinean (Highland)"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48419.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16561
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="S1216"
**                   /isolation_source="Samoan"
**   CDS             9198..9978
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9978,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48431.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16561
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="S1220"
**                   /isolation_source="Samoan"
**   CDS             9198..9978
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9978,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48444.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16574
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="496"
**                   /isolation_source="Taiwanese Indian"
**   CDS             9212..9992
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9992,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48457.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16572
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="513"
**                   /isolation_source="Taiwanese Indian"
**   CDS             9210..9990
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9990,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48470.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16569
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="DCH002"
**                   /isolation_source="Thai"
**   CDS             9207..9987
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9987,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48509.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16561
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Sb13"
**                   /isolation_source="Thai"
**   CDS             9199..9979
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9979,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48522.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16560
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolate="Sb29"
**                   /isolation_source="Thai"
**   CDS             9198..9978
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9978,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48535.1"
**   08-JUL-2003 Last updated, EMBL entry
**   source          1..16560
**                   /db_xref="taxon:9606"
**                   /mol_type="genomic DNA"
**                   /organelle="mitochondrion"
**                   /organism="Homo sapiens"
**                   /isolation_source="Tongan"
**   CDS             9198..9978
**                   /codon_start=1
**                   /note="TAA stop codon is completed by the addition of 3' A
**                   residues to the mRNA"
**                   /transl_except=(pos:9978,aa:TERM)
**                   /transl_table=2
**                   /gene="COX3"
**                   /product="cytochrome c oxidase subunit III"
**                   /protein_id="AAP48548.1"
**   08-JUL-2003 Last updated, EMBL entry
**   #################    INTERNAL SECTION    ##################
**EV EA1; Rulebase; -; RU003375V0.42; 18-NOV-2002.
**EV EI2; EMBL; -; AAL54806.1; 07-FEB-2002.
**EV EI3; EMBL; -; AAK17889.2; 26-APR-2001.
**EV EI4; EMBL; -; AAK17213.1; 26-APR-2001.
**EV EI5; EMBL; -; AAK17226.2; 26-APR-2001.
**EV EI6; EMBL; -; AAK17252.1; 26-APR-2001.
**EV EI7; EMBL; -; AAK17265.2; 26-APR-2001.
**EV EI8; EMBL; -; AAK17278.2; 26-APR-2001.
**EV EI9; EMBL; -; AAK17291.2; 26-APR-2001.
**EV EI10; EMBL; -; AAK17304.2; 26-APR-2001.
**EV EI11; EMBL; -; AAK17317.2; 26-APR-2001.
**EV EI12; EMBL; -; AAK17330.2; 26-APR-2001.
**EV EI13; EMBL; -; AAK17343.2; 26-APR-2001.
**EV EI14; EMBL; -; AAK17356.2; 26-APR-2001.
**EV EI15; EMBL; -; AAK17369.2; 26-APR-2001.
**EV EI16; EMBL; -; AAK17382.1; 26-APR-2001.
**EV EI17; EMBL; -; AAK17395.1; 26-APR-2001.
**EV EI18; EMBL; -; AAK17408.1; 26-APR-2001.
**EV EI19; EMBL; -; AAK17421.1; 26-APR-2001.
**EV EI20; EMBL; -; AAK17434.2; 26-APR-2001.
**EV EI21; EMBL; -; AAK17447.2; 26-APR-2001.
**EV EI22; EMBL; -; AAK17460.1; 26-APR-2001.
**EV EI23; EMBL; -; AAK17473.1; 26-APR-2001.
**EV EI24; EMBL; -; AAK17486.2; 26-APR-2001.
**EV EI25; EMBL; -; AAK17499.2; 26-APR-2001.
**EV EI26; EMBL; -; AAK17512.2; 26-APR-2001.
**EV EI27; EMBL; -; AAK17525.2; 26-APR-2001.
**EV EI28; EMBL; -; AAK17564.1; 26-APR-2001.
**EV EI29; EMBL; -; AAK17577.2; 26-APR-2001.
**EV EI30; EMBL; -; AAK17590.2; 26-APR-2001.
**EV EI31; EMBL; -; AAK17603.2; 26-APR-2001.
**EV EI32; EMBL; -; AAK17616.2; 26-APR-2001.
**EV EI33; EMBL; -; AAK17629.2; 26-APR-2001.
**EV EI34; EMBL; -; AAK17642.2; 26-APR-2001.
**EV EI35; EMBL; -; AAK17655.2; 26-APR-2001.
**EV EI36; EMBL; -; AAK17668.2; 26-APR-2001.
**EV EI37; EMBL; -; AAK17681.2; 26-APR-2001.
**EV EI38; EMBL; -; AAK17694.1; 26-APR-2001.
**EV EI39; EMBL; -; AAK17707.2; 26-APR-2001.
**EV EI40; EMBL; -; AAK17720.2; 26-APR-2001.
**EV EI41; EMBL; -; AAK17733.2; 26-APR-2001.
**EV EI42; EMBL; -; AAK17746.2; 26-APR-2001.
**EV EI43; EMBL; -; AAK17759.2; 26-APR-2001.
**EV EI44; EMBL; -; AAK17772.2; 26-APR-2001.
**EV EI45; EMBL; -; AAK17785.2; 26-APR-2001.
**EV EI46; EMBL; -; AAK17798.2; 26-APR-2001.
**EV EI47; EMBL; -; AAK17811.2; 26-APR-2001.
**EV EI48; EMBL; -; AAK17837.2; 26-APR-2001.
**EV EI49; EMBL; -; AAK17876.2; 26-APR-2001.
**EV EI50; EMBL; -; AAL54393.1; 07-FEB-2002.
**EV EI51; EMBL; -; AAL54403.1; 07-FEB-2002.
**EV EI52; EMBL; -; AAL54416.1; 07-FEB-2002.
**EV EI53; EMBL; -; AAL54429.1; 07-FEB-2002.
**EV EI54; EMBL; -; AAL54442.1; 07-FEB-2002.
**EV EI55; EMBL; -; AAL54455.1; 07-FEB-2002.
**EV EI56; EMBL; -; AAL54468.1; 07-FEB-2002.
**EV EI57; EMBL; -; AAL54481.1; 07-FEB-2002.
**EV EI58; EMBL; -; AAL54507.1; 07-FEB-2002.
**EV EI59; EMBL; -; AAL54520.1; 07-FEB-2002.
**EV EI60; EMBL; -; AAL54546.1; 07-FEB-2002.
**EV EI61; EMBL; -; AAL54559.1; 07-FEB-2002.
**EV EI62; EMBL; -; AAL54572.1; 07-FEB-2002.
**EV EI63; EMBL; -; AAL54585.1; 07-FEB-2002.
**EV EI64; EMBL; -; AAL54611.1; 07-FEB-2002.
**EV EI65; EMBL; -; AAL54624.1; 07-FEB-2002.
**EV EI66; EMBL; -; AAL54637.1; 07-FEB-2002.
**EV EI67; EMBL; -; AAL54650.1; 07-FEB-2002.
**EV EI68; EMBL; -; AAL54663.1; 07-FEB-2002.
**EV EI69; EMBL; -; AAL54676.1; 07-FEB-2002.
**EV EI70; EMBL; -; AAL54689.1; 07-FEB-2002.
**EV EI71; EMBL; -; AAL54702.1; 07-FEB-2002.
**EV EI72; EMBL; -; AAL54715.1; 07-FEB-2002.
**EV EI73; EMBL; -; AAL54728.1; 07-FEB-2002.
**EV EI74; EMBL; -; AAL54741.1; 07-FEB-2002.
**EV EI75; EMBL; -; AAL54754.1; 07-FEB-2002.
**EV EI76; EMBL; -; AAL54767.1; 07-FEB-2002.
**EV EI77; EMBL; -; AAL54780.1; 07-FEB-2002.
**EV EI78; EMBL; -; AAL54793.1; 07-FEB-2002.
**EV EI79; EMBL; -; AAN14542.1; 12-DEC-2002.
**EV EI80; EMBL; -; AAN14553.1; 12-DEC-2002.
**EV EI81; EMBL; -; AAN14564.1; 12-DEC-2002.
**EV EI82; EMBL; -; AAN14575.1; 12-DEC-2002.
**EV EI83; EMBL; -; AAN14586.1; 12-DEC-2002.
**EV EI84; EMBL; -; AAN14597.1; 12-DEC-2002.
**EV EI85; EMBL; -; AAN14608.1; 12-DEC-2002.
**EV EI86; EMBL; -; AAN14619.1; 12-DEC-2002.
**EV EI87; EMBL; -; AAN14630.1; 12-DEC-2002.
**EV EI88; EMBL; -; AAN14641.1; 12-DEC-2002.
**EV EI89; EMBL; -; AAN14652.1; 12-DEC-2002.
**EV EI90; EMBL; -; AAN14663.1; 12-DEC-2002.
**EV EI91; EMBL; -; AAN14674.1; 12-DEC-2002.
**EV EI92; EMBL; -; AAN14685.1; 12-DEC-2002.
**EV EI93; EMBL; -; AAN14696.1; 12-DEC-2002.
**EV EI94; EMBL; -; AAN14707.1; 12-DEC-2002.
**EV EI95; EMBL; -; AAN14729.1; 12-DEC-2002.
**EV EI96; EMBL; -; AAN14740.1; 12-DEC-2002.
**EV EI97; EMBL; -; AAN14751.1; 12-DEC-2002.
**EV EI98; EMBL; -; AAN14762.1; 12-DEC-2002.
**EV EI99; EMBL; -; AAN14773.1; 12-DEC-2002.
**EV EI100; EMBL; -; AAN14784.1; 12-DEC-2002.
**EV EI101; EMBL; -; AAN14795.1; 12-DEC-2002.
**EV EI102; EMBL; -; AAN14806.1; 12-DEC-2002.
**EV EI103; EMBL; -; AAN14817.1; 12-DEC-2002.
**EV EI104; EMBL; -; AAN14828.1; 12-DEC-2002.
**EV EI105; EMBL; -; AAN14839.1; 12-DEC-2002.
**EV EI106; EMBL; -; AAN14850.1; 12-DEC-2002.
**EV EI107; EMBL; -; AAN14861.1; 12-DEC-2002.
**EV EI108; EMBL; -; AAN14872.1; 12-DEC-2002.
**EV EI109; EMBL; -; AAN14883.1; 12-DEC-2002.
**EV EI110; EMBL; -; AAN14894.1; 12-DEC-2002.
**EV EI111; EMBL; -; AAN14905.1; 12-DEC-2002.
**EV EI112; EMBL; -; AAN14916.1; 12-DEC-2002.
**EV EI113; EMBL; -; AAN14927.1; 12-DEC-2002.
**EV EI114; EMBL; -; AAN14938.1; 12-DEC-2002.
**EV EI115; EMBL; -; AAN14949.1; 12-DEC-2002.
**EV EI116; EMBL; -; AAN14960.1; 12-DEC-2002.
**EV EI117; EMBL; -; AAN14971.1; 12-DEC-2002.
**EV EI118; EMBL; -; AAO88286.1; 14-APR-2003.
**EV EI119; EMBL; -; AAO88312.1; 14-APR-2003.
**EV EI120; EMBL; -; AAO88351.1; 14-APR-2003.
**EV EI121; EMBL; -; AAO88364.1; 14-APR-2003.
**EV EI122; EMBL; -; AAO88377.1; 14-APR-2003.
**EV EI123; EMBL; -; AAO88403.1; 14-APR-2003.
**EV EI124; EMBL; -; AAO88416.1; 14-APR-2003.
**EV EI125; EMBL; -; AAO88442.1; 14-APR-2003.
**EV EI126; EMBL; -; AAO88455.1; 14-APR-2003.
**EV EI127; EMBL; -; AAO88468.1; 14-APR-2003.
**EV EI128; EMBL; -; AAO88481.1; 14-APR-2003.
**EV EI129; EMBL; -; AAO88494.1; 14-APR-2003.
**EV EI130; EMBL; -; AAO88507.1; 14-APR-2003.
**EV EI131; EMBL; -; AAO88520.1; 14-APR-2003.
**EV EI132; EMBL; -; AAO88533.1; 14-APR-2003.
**EV EI133; EMBL; -; AAO88546.1; 14-APR-2003.
**EV EI134; EMBL; -; AAO88559.1; 14-APR-2003.
**EV EI135; EMBL; -; AAO88572.1; 14-APR-2003.
**EV EI136; EMBL; -; AAO88585.1; 14-APR-2003.
**EV EI137; EMBL; -; AAO88624.1; 14-APR-2003.
**EV EI138; EMBL; -; AAO88637.1; 14-APR-2003.
**EV EI139; EMBL; -; AAO88650.1; 14-APR-2003.
**EV EI140; EMBL; -; AAO88663.1; 14-APR-2003.
**EV EI141; EMBL; -; AAO88676.1; 14-APR-2003.
**EV EI142; EMBL; -; AAO88689.1; 14-APR-2003.
**EV EI143; EMBL; -; AAO88702.1; 14-APR-2003.
**EV EI144; EMBL; -; AAO88715.1; 14-APR-2003.
**EV EI145; EMBL; -; AAO88728.1; 14-APR-2003.
**EV EI146; EMBL; -; AAO88741.1; 14-APR-2003.
**EV EI147; EMBL; -; AAO88754.1; 14-APR-2003.
**EV EI148; EMBL; -; AAO88767.1; 14-APR-2003.
**EV EI149; EMBL; -; AAO88780.1; 14-APR-2003.
**EV EI150; EMBL; -; AAO88793.1; 14-APR-2003.
**EV EI151; EMBL; -; AAO88819.1; 14-APR-2003.
**EV EI152; EMBL; -; AAO88832.1; 14-APR-2003.
**EV EI153; EMBL; -; AAO88845.1; 14-APR-2003.
**EV EI154; EMBL; -; AAO88871.1; 14-APR-2003.
**EV EI155; EMBL; -; AAO88884.1; 14-APR-2003.
**EV EI156; EMBL; -; AAO88897.1; 14-APR-2003.
**EV EI157; EMBL; -; AAO66624.1; 23-JUL-2003.
**EV EI158; EMBL; -; AAO66637.1; 23-JUL-2003.
**EV EI159; EMBL; -; AAO66650.1; 23-JUL-2003.
**EV EI160; EMBL; -; AAO66663.1; 23-JUL-2003.
**EV EI161; EMBL; -; AAO66689.1; 23-JUL-2003.
**EV EI162; EMBL; -; AAO66702.1; 23-JUL-2003.
**EV EI163; EMBL; -; AAO66715.1; 23-JUL-2003.
**EV EI164; EMBL; -; AAO66728.1; 23-JUL-2003.
**EV EI165; EMBL; -; AAO66741.1; 23-JUL-2003.
**EV EI166; EMBL; -; AAO66754.1; 23-JUL-2003.
**EV EI167; EMBL; -; AAO66767.1; 23-JUL-2003.
**EV EI168; EMBL; -; AAO66780.1; 23-JUL-2003.
**EV EI169; EMBL; -; AAO66793.1; 23-JUL-2003.
**EV EI170; EMBL; -; AAO66806.1; 23-JUL-2003.
**EV EI171; EMBL; -; AAO66819.1; 23-JUL-2003.
**EV EI172; EMBL; -; AAO66845.1; 23-JUL-2003.
**EV EI173; EMBL; -; AAO66858.1; 23-JUL-2003.
**EV EI174; EMBL; -; AAO66871.1; 23-JUL-2003.
**EV EI175; EMBL; -; AAO66884.1; 23-JUL-2003.
**EV EI176; EMBL; -; AAO66897.1; 23-JUL-2003.
**EV EI177; EMBL; -; AAO66910.1; 23-JUL-2003.
**EV EI178; EMBL; -; AAO66923.1; 23-JUL-2003.
**EV EI179; EMBL; -; AAO66936.1; 23-JUL-2003.
**EV EI180; EMBL; -; AAO66949.1; 23-JUL-2003.
**EV EI181; EMBL; -; AAO66975.1; 23-JUL-2003.
**EV EI182; EMBL; -; AAO67001.1; 23-JUL-2003.
**EV EI183; EMBL; -; AAO67014.1; 23-JUL-2003.
**EV EI184; EMBL; -; AAO67027.1; 23-JUL-2003.
**EV EI185; EMBL; -; AAO67040.1; 23-JUL-2003.
**EV EI186; EMBL; -; AAO67053.1; 23-JUL-2003.
**EV EI187; EMBL; -; AAO67066.1; 23-JUL-2003.
**EV EI188; EMBL; -; AAO67079.1; 23-JUL-2003.
**EV EI189; EMBL; -; AAO67091.1; 23-JUL-2003.
**EV EI190; EMBL; -; AAO67104.1; 23-JUL-2003.
**EV EI191; EMBL; -; AAO67117.1; 23-JUL-2003.
**EV EI192; EMBL; -; AAO67130.1; 23-JUL-2003.
**EV EI193; EMBL; -; AAO67156.1; 23-JUL-2003.
**EV EI194; EMBL; -; AAO67169.1; 23-JUL-2003.
**EV EI195; EMBL; -; AAO67182.1; 23-JUL-2003.
**EV EI196; EMBL; -; AAO67195.1; 23-JUL-2003.
**EV EI197; EMBL; -; AAO67208.1; 23-JUL-2003.
**EV EI198; EMBL; -; AAO67221.1; 23-JUL-2003.
**EV EI199; EMBL; -; AAO67234.1; 23-JUL-2003.
**EV EI200; EMBL; -; AAP47899.1; 14-JUL-2003.
**EV EI201; EMBL; -; AAP47912.1; 14-JUL-2003.
**EV EI202; EMBL; -; AAP47925.1; 14-JUL-2003.
**EV EI203; EMBL; -; AAP47938.1; 14-JUL-2003.
**EV EI204; EMBL; -; AAP47951.1; 14-JUL-2003.
**EV EI205; EMBL; -; AAP47964.1; 14-JUL-2003.
**EV EI206; EMBL; -; AAP47977.1; 14-JUL-2003.
**EV EI207; EMBL; -; AAP47990.1; 14-JUL-2003.
**EV EI208; EMBL; -; AAP48003.1; 14-JUL-2003.
**EV EI209; EMBL; -; AAP48016.1; 14-JUL-2003.
**EV EI210; EMBL; -; AAP48029.1; 14-JUL-2003.
**EV EI211; EMBL; -; AAP48042.1; 14-JUL-2003.
**EV EI212; EMBL; -; AAP48055.1; 14-JUL-2003.
**EV EI213; EMBL; -; AAP48068.1; 14-JUL-2003.
**EV EI214; EMBL; -; AAP48081.1; 14-JUL-2003.
**EV EI215; EMBL; -; AAP48094.1; 14-JUL-2003.
**EV EI216; EMBL; -; AAP48107.1; 14-JUL-2003.
**EV EI217; EMBL; -; AAP48120.1; 14-JUL-2003.
**EV EI218; EMBL; -; AAP48133.1; 14-JUL-2003.
**EV EI219; EMBL; -; AAP48146.1; 14-JUL-2003.
**EV EI220; EMBL; -; AAP48159.1; 14-JUL-2003.
**EV EI221; EMBL; -; AAP48185.1; 14-JUL-2003.
**EV EI222; EMBL; -; AAP48198.1; 14-JUL-2003.
**EV EI223; EMBL; -; AAP48211.1; 14-JUL-2003.
**EV EI224; EMBL; -; AAP48224.1; 14-JUL-2003.
**EV EI225; EMBL; -; AAP48237.1; 14-JUL-2003.
**EV EI226; EMBL; -; AAP48250.1; 14-JUL-2003.
**EV EI227; EMBL; -; AAP48263.1; 14-JUL-2003.
**EV EI228; EMBL; -; AAP48276.1; 14-JUL-2003.
**EV EI229; EMBL; -; AAP48289.1; 14-JUL-2003.
**EV EI230; EMBL; -; AAP48302.1; 14-JUL-2003.
**EV EI231; EMBL; -; AAP48315.1; 14-JUL-2003.
**EV EI232; EMBL; -; AAP48328.1; 14-JUL-2003.
**EV EI233; EMBL; -; AAP48341.1; 14-JUL-2003.
**EV EI234; EMBL; -; AAP48354.1; 14-JUL-2003.
**EV EI235; EMBL; -; AAP48367.1; 14-JUL-2003.
**EV EI236; EMBL; -; AAP48380.1; 14-JUL-2003.
**EV EI237; EMBL; -; AAP48393.1; 14-JUL-2003.
**EV EI238; EMBL; -; AAP48406.1; 14-JUL-2003.
**EV EI239; EMBL; -; AAP48419.1; 14-JUL-2003.
**EV EI240; EMBL; -; AAP48431.1; 14-JUL-2003.
**EV EI241; EMBL; -; AAP48444.1; 14-JUL-2003.
**EV EI242; EMBL; -; AAP48457.1; 14-JUL-2003.
**EV EI243; EMBL; -; AAP48470.1; 14-JUL-2003.
**EV EI244; EMBL; -; AAP48509.1; 14-JUL-2003.
**EV EI245; EMBL; -; AAP48522.1; 14-JUL-2003.
**EV EI246; EMBL; -; AAP48535.1; 14-JUL-2003.
**EV EI247; EMBL; -; AAP48548.1; 14-JUL-2003.
**EV EP248; TrEMBL; -; AAK17889.2; 26-APR-2001.
**EV EP249; Merge; -; -; 18-JUN-2003.
**ID XXXX_HUMAN
**PM ProDom; PD000382; CytC_oxdse_III; 6; 260; T; 14-APR-2003;
**PM Pfam; PF00510; COX3; 6; 260; T; 06-NOV-2003;
**PM PROSITE; PS50253; COX3; 4; 260; T; 07-NOV-2003;
**PM TIGRFAMs; TIGR01732; tiny_TM_bacill; 190; 215; T; 23-OCT-2003;
SQ   SEQUENCE   260 AA;  29864 MW;  1385EF748B7C1488 CRC64;
     MTHQSHAYHM VKPSPWPLTG ALSALLMTSG LAMWFHFHSM TLLMLGLLTN TLTMYQWWRD
     VTRESTYQGH HTPPVQKGLR YGMILFITSE VFFFAGFFWA FYHSSLAPTP QLGGHWPPTG
     ITPLNPLEVP LLNTSVLLAS GVSITWAHHS LMENNRNQMI QALLITILLG LYFTLLQASE
     YFESPFTISD GIYGSTFFVA TGFHGLHVII GSTFLTICFI RQLMFHFTSK HHFGFEAAAW
     YWHFVDVVWL FLYVSIYWWG
//
ID   3BHS_BOVIN     STANDARD;      PRT;   372 AA.
AC   P14893;
DT   01-APR-1990 (Rel. 14, Created)
DT   01-APR-1990 (Rel. 14, Last sequence update)
DT   15-JUN-2004 (Rel. 44, Last annotation update)
DE   3 beta-hydroxysteroid dehydrogenase/delta 5-->4-isomerase (3Beta-HSD)
DE   [Includes: 3-beta-hydroxy-delta(5)-steroid dehydrogenase
DE   (EC 1.1.1.145) (3-beta-hydroxy-5-ene steroid dehydrogenase)
DE   (Progesterone reductase); Steroid delta-isomerase (EC 5.3.3.1) (Delta-
DE   5-3-ketosteroid isomerase)].
GN   HSD3B.
OS   Bos taurus (Bovine).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Cetartiodactyla; Ruminantia; Pecora; Bovidae;
OC   Bovinae; Bos.
OX   NCBI_TaxID=9913;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=Ovary;
RX   MEDLINE=90092517; PubMed=2599102;
RA   Zhao H.-F., Simard J., Labrie C., Breton N., Rheaume E., Luu-The V.,
RA   Labrie F.;
RT   "Molecular cloning, cDNA structure and predicted amino acid sequence
RT   of bovine 3 beta-hydroxy-5-ene steroid dehydrogenase/delta 5-delta 4
RT   isomerase.";
RL   FEBS Lett. 259:153-157(1989).
RN   [2]
RP   PARTIAL SEQUENCE, AND CD STUDIES.
RC   TISSUE=Adrenal gland;
RX   MEDLINE=91329389; PubMed=1868086;
RA   Rutherfurd K.J., Chen S., Shively J.E.;
RT   "Isolation and amino acid sequence analysis of bovine adrenal 3 beta-
RT   hydroxysteroid dehydrogenase/steroid isomerase.";
RL   Biochemistry 30:8108-8116(1991).
CC   -!- FUNCTION: 3beta-HSD is a bifunctional enzyme, that catalyzes the
CC       oxidative conversion of delta(5)-ene-3-beta-hydroxy steroid, and
CC       the oxidative conversion of ketosteroids. The 3beta-HSD enzymatic
CC       system plays a crucial role in the biosynthesis of all classes of
CC       hormonal steroids.
CC   -!- CATALYTIC ACTIVITY: 3-beta-hydroxy-delta(5)-steroid + NAD(+) = 3-
CC       oxo-delta(5)-steroid + NADH.
CC   -!- CATALYTIC ACTIVITY: A 3-oxo-delta(5)-steroid = a 3-oxo-delta(4)-
CC       steroid.
CC   -!- PATHWAY: Steroid biosynthesis.
CC   -!- SUBCELLULAR LOCATION: Endoplasmic reticulum and mitochondrial
CC       membrane-bound protein.
CC   -!- SIMILARITY: Belongs to the 3beta-HSD family.
DR   EMBL; X17614; CAA35615.1; -.
DR   PIR; S07102; DEBOHS.
DR   InterPro; IPR002225; 3Beta_HSD.
DR   Pfam; PF01073; 3Beta_HSD; 1.
KW   Steroidogenesis; Oxidoreductase; NAD; Isomerase; Mitochondrion;
KW   Multifunctional enzyme; Transmembrane; Endoplasmic reticulum;
KW   Direct protein sequencing.
FT   INIT_MET      0      0
FT   TRANSMEM     74     91       0 (Potential).
FT   TRANSMEM    287    305       Potential.
FT   NP_BIND       5     36       NAD (Potential).
FT   CONFLICT    141    219       AKLRKELVETSEVRKAVSIETALEHKVVNGNSADAAYAQVE
FT                                IQPRANIQLDFPELKPYKQVKQIAPAELEGLLDLERVI ->
FT                                CLNCVKSWLKLLKLERQFPSKLLWSIRLSMAIALMLHMLKS
FT                                KFNQELTFNWTSQNRNHTNRLNKLLPLSLRVCWIWKELF
FT                                (in Ref. 1).
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   372 AA;  42088 MW;  5B8481DEEA5807BC CRC64;
     AGWSCLVTGG GGFLGQRIIC LLVEEKDLQE IRVLDKVFRP EVREEFSKLQ SKIKLTLLEG
     DILDEQCLKG ACQGTSVVIH TASVIDVRNA VPRETIMNVN VKGTQLLLEA CVQASVPVFI
     HTSTIEVAGP NSYREIIQDG REEEHHESAW SSPYPYSKKL AEKAVLGANG WALKNGGTLY
     TCALRPMYIY GEGSPFLSAY MHGALNNNGI LTNHCKFSRV NPVYVGNVAW AHILALRALR
     DPKKVPNIQG QFYYISDDTP HQSYDDLNYT LSKEWGFCLD SRMSLPISLQ YWLAFLLEIV
     SFLLSPIYKY NPCFNRHLVT LSNSVFTFSY KKAQRDLGYE PLYTWEEAKQ KTKEWIGSLV
     KQHKETLKTK IH
//
ID   CBP1_HORVU     STANDARD;      PRT;   499 AA.
AC   P07519; P07520;
DT   01-APR-1988 (Rel. 07, Created)
DT   01-NOV-1997 (Rel. 35, Last sequence update)
DT   15-JUN-2004 (Rel. 44, Last annotation update)
DE   Serine carboxypeptidase I precursor (EC 3.4.16.5) (Carboxypeptidase C)
DE   (CP-MI).
GN   Name=CBP1; Synonyms=CXP;1;
OS   Hordeum vulgare (Barley).
OC   Eukaryota; Viridiplantae; Streptophyta; Embryophyta; Tracheophyta;
OC   Spermatophyta; Magnoliophyta; Liliopsida; Poales; Poaceae; Pooideae;
OC   Triticeae; Hordeum.
OX   NCBI_TaxID=4513;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=Aleurone;
RA   Rocher A., Lok F., Cameron-Mills V., von Wettstein D.;
RL   Submitted (DEC-1996) to the EMBL/GenBank/DDBJ databases.
RN   [2]
RP   SEQUENCE OF 88-499 FROM N.A.
RX   MEDLINE=88298749; PubMed=3403516;
RA   Doan N.P., Fincher G.B.;
RT   "The A- and B-chains of carboxypeptidase I from germinated barley
RT   originate from a single precursor polypeptide.";
RL   J. Biol. Chem. 263:11106-11110(1988).
RN   [3]
RP   SEQUENCE OF 31-296 AND 352-499.
**   MEDLINE=None; PubMed=None;
RA   Soerensen S.B., Breddam K., Svendsen I.;
RT   "Primary structure of carboxypeptidase I from malted barley.";
RL   Carlsberg Res. Commun. 51:475-485(1986).
CC   -!- FUNCTION: May be involved in the degradation of small peptides (2-
CC       5 residues) or in the degradation of storage proteins in the
CC       embryo.
CC   -!- CATALYTIC ACTIVITY: Release of a C-terminal amino acid with a
CC       broad specificity.
CC   -!- SUBUNIT: Carboxypeptidase I is a dimer, where each monomer is
CC       composed of two chains linked by disulfide bonds.
CC   -!- SUBCELLULAR LOCATION: Secreted into the endosperm.
CC   -!- DEVELOPMENTAL STAGE: After one day of germination, mainly found in
CC       the scutellum of the developing grain; barely detectable after
CC       four days, and absent from the mature grain. A lower level of
CC       expression is seen in the aleurone both during development and
CC       germination.
CC   -!- PTM: Three disulfide bonds are present.
CC   -!- PTM: The linker peptide is endoproteolytically excised during
CC       enzyme maturation.
CC   -!- SIMILARITY: Belongs to peptidase family S10.
DR   EMBL; Y09603; CAA70816.1; -.
DR   EMBL; J03897; AAA32940.1; -.
DR   PIR; T05367; CPBHS.
DR   HSSP; P08819; 1WHT.
DR   MEROPS; S10.004; -.
DR   InterPro; IPR001563; Peptidase_S10.
DR   InterPro; IPR000379; Ser_estrs.
DR   Pfam; PF00450; Peptidase_S10; 1.
DR   PRINTS; PR00724; CRBOXYPTASEC.
DR   ProDom; PD001189; Serine_carbpept; 2.
DR   PROSITE; PS00560; CARBOXYPEPT_SER_HIS; 1.
DR   PROSITE; PS00131; CARBOXYPEPT_SER_SER; 1.
KW   Hydrolase; Carboxypeptidase; Glycoprotein; Zymogen; Signal;
KW   Direct protein sequencing.
FT   SIGNAL        1     30       Potential.
FT   CHAIN        31    296       Serine carboxypeptidase I chain A.
FT   PROPEP      297    351       Linker peptide.
FT   CHAIN       352    499       Serine carboxypeptidase I chain B.
FT   ACT_SITE    188    188       By similarity.
FT   ACT_SITE    423    423       By similarity.
FT   ACT_SITE    476    476       By similarity.
FT   SITE        497    499       Microbody targeting signal (Potential).
FT   CARBOHYD    148    148       N-linked (GlcNAc...).
FT   CARBOHYD    262    262       N-linked (GlcNAc...).
FT   CARBOHYD    407    407       N-linked (GlcNAc...).
FT   CONFLICT    102    102       H -> P (in Ref. 3).
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   499 AA;  54096 MW;  9C6674B14D9DB9BF CRC64;
     MARCRRRSGC TAGAALLLLL ALALSGGGGA APQGAEVTGL PGFDGALPSK HYAGYVTVDE
     GHGRNLFYYV VESERDPGKD PVVLWLNGGP GCSSFDGFVY EHGPFNFESG GSVKSLPKLH
     LNPYAWSKVS TMIYLDSPAG VGLSYSKNVS DYETGDLKTA TDSHTFLLKW FQLYPEFLSN
     PFYIAGESYA GVYVPTLSHE VVKGIQGGAK PTINFKGYMV GNGVCDTIFD GNALVPFAHG
     MGLISDEIYQ QASTSCHGNY WNATDGKCDT AISKIESLIS GLNIYDILEP CYHSRSIKEV
     NLQNSKLPQS FKDLGTTNKP FPVRTRMLGR AWPLRAPVKA GRVPSWQEVA SGVPCMSDEV
     ATAWLDNAAV RSAIHAQSVS AIGPWLLCTD KLYFVHDAGS MIAYHKNLTS QGYRAIIFSG
     DHDMCVPFTG SEAWTKSLGY GVVDSWRPWI TNGQVSGYTE GYEHGLTFAT IKGAGHTVPE
     YKPQEAFAFY SRWLAGSKL
//
ID   YCXD_CYAPA     STANDARD;      PRT;   244 AA.
AC   P48334;
DT   01-FEB-1996 (Rel. 33, Created)
DT   01-FEB-1996 (Rel. 33, Last sequence update)
DT   29-MAR-2004 (Rel. 43, Last annotation update)
DE   Probable ABC transporter ATP-binding protein in ycf23-apcF intergenic
DE   region (ORF244).
OS   Cyanophora paradoxa.
OG   Cyanelle.
OC   Eukaryota; Glaucocystophyceae; Cyanophoraceae; Cyanophora.
OX   NCBI_TaxID=2762;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=UTEX LB 555 / Pringsheim;
**   MEDLINE=None; PubMed=None;
RA   Stirewalt V.L., Michalowski C.B., Loeffelhardt W., Bohnert H.J.,
RA   Bryant D.A.;
RT   "Nucleotide sequence of the cyanelle DNA from Cyanophora paradoxa.";
RL   Plant Mol. Biol. Rep. 13:327-332(1995).
RN   [2]
RP   SEQUENCE FROM N.A.
RC   STRAIN=UTEX LB 555 / Pringsheim;
RA   Loeffelhardt W., Stirewalt V.L., Michalowski C.B., Annarella M.,
RA   Farley J.Y., Schluchter W.M., Chung S., Newmann-Spallart C.,
RA   Steiner J.M., Jakowitsch J., Bohnert H.J., Bryant D.A.;
RT   "The complete sequence of the cyanelle genome of Cyanophora paradoxa:
RT   the genetic complexity of a primitive plastid.";
RL   (In) Schenk H.E.A., Herrmann R., Jeon K.W., Mueller N.E.,
RL   Schwemmler W. (eds.);
RL   Eukaryotism and Symbiosis, pp.40-48, Springer-Verlag, Heidelberg
RL   (1997).
CC   -!- SUBCELLULAR LOCATION: Cyanelle.
CC   -!- SIMILARITY: Belongs to the ABC transporter family.
DR   EMBL; U30821; AAA81304.1; -.
DR   PIR; T06961; T06961.
DR   HSSP; P58301; 1F2T.
DR   InterPro; IPR003593; AAA_ATPase.
DR   InterPro; IPR003439; ABC_transporter.
DR   Pfam; PF00005; ABC_tran; 1.
DR   ProDom; PD000006; ABC_transporter; 1.
DR   SMART; SM00382; AAA; 1.
DR   PROSITE; PS00211; ABC_TRANSPORTER_1; 1.
DR   PROSITE; PS50893; ABC_TRANSPORTER_2; 1.
KW   Hypothetical protein; ATP-binding; Transport; Cyanelle.
FT   NP_BIND      41     48       ATP (Potential).
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   244 AA;  27747 MW;  4C5B357FF9C55D3B CRC64;
     MFYTLPKQLE INNLTVSYPH GTVLQNIFLT IESGKLIGII GPNGAGKSTL LKTIIEQIKP
     ISGEIFYQGA PLKNQRARIG YVPQRAQVDW DFPINVWDVV MMARLKKIGW FSSYSKKSYE
     CVKAALEKVD MLKYKDRNIR ELSGGQQQRV FLARLLAQEA DLLLLDEPFT GVDFQTQKII
     FSLLKEQIAS NKIVIVIHHD LGESIINFDE LILLNKKIIS HDLTTKILNS KKLSTLFGEH
     IYAN
//
ID   CYC_ARUMA      STANDARD;      PRT;   111 AA.
AC   P00065;
DT   21-JUL-1986 (Rel. 01, Created)
DT   21-JUL-1986 (Rel. 01, Last sequence update)
DT   15-JUN-2004 (Rel. 44, Last annotation update)
DE   Cytochrome c.
OS   Arum maculatum (Cuckoo-pint).
OC   Eukaryota; Viridiplantae; Streptophyta; Embryophyta; Tracheophyta;
OC   Spermatophyta; Magnoliophyta; Liliopsida; Araceae; Arum.
OX   NCBI_TaxID=4458;
RN   [1]
RP   SEQUENCE.
RA   Boulter D.;
**   /NO TITLE.
RL   Unpublished results, cited by:
RL   Dickerson R.E., Timkovich R.;
RL   (In) Boyer P.D. (eds.);
RL   The enzymes (3rd ed.), pp.11:397-547, Academic Press, New York (1975).
CC   -!- FUNCTION: Electron carrier protein. The oxidized form of the
CC       cytochrome c heme group can accept an electron from the heme group
CC       of the cytochrome c1 subunit of cytochrome reductase. Cytochrome c
CC       then transfers this electron to the cytochrome oxidase complex,
CC       the final protein carrier in the mitochondrial electron-transport
CC       chain.
CC   -!- SUBCELLULAR LOCATION: Mitochondrial matrix.
CC   -!- PTM: Binds 1 heme group per subunit.
CC   -!- SIMILARITY: Belongs to the cytochrome c family.
DR   PIR; A00057; CCRM.
DR   HSSP; P00055; 1CCR.
DR   InterPro; IPR000345; CytC_heme_BS.
DR   InterPro; IPR009056; Cytochrome_c.
DR   InterPro; IPR003088; Cyt_CI.
DR   InterPro; IPR002327; Cyt_CIAB.
DR   Pfam; PF00034; Cytochrom_C; 1.
DR   PRINTS; PR00604; CYTCHRMECIAB.
DR   ProDom; PD000375; Cyt_CIAB; 1.
DR   PROSITE; PS00190; CYTOCHROME_C; 1.
KW   Mitochondrion; Electron transport; Respiratory chain; Heme;
KW   Acetylation; Direct protein sequencing.
FT   METAL        26     26       Iron (heme axial ligand).
FT   METAL        88     88       Iron (heme axial ligand).
FT   BINDING      22     22       Heme (covalent).
FT   BINDING      25     25       Heme (covalent).
FT   MOD_RES       1      1       N-acetylalanine.
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   111 AA;  12020 MW;  6F7C201451D52E47 CRC64;
     ASFAEAPPGN PKAGEKIFKT KCAQCHTVEK GAGHKQGPNL NGLFGRQSGT TAGYSYSAAN
     KNMAVIWEES TLYDYLLNPX KYIPGTKMVF PGLXKPQERA DLIAYLKEST A
//
ID   TAP2_HUMAN     STANDARD;      PRT;   686 AA.
AC   Q03519; Q9UQ83;
DT   01-JUN-1994 (Rel. 29, Created)
DT   01-JUN-1994 (Rel. 29, Last sequence update)
DT   15-JUN-2004 (Rel. 44, Last annotation update)
DE   Antigen peptide transporter 2 (APT2) (Peptide transporter TAP2)
DE   (Peptide transporter PSF2) (Peptide supply factor 2) (PSF-2) (Peptide
DE   transporter involved in antigen processing 2).
GN   Name=TAP2; Synonyms=ABCB3, PSF2, RING11, Y1;
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606;
RN   [1]
RP   SEQUENCE FROM N.A. (TAP2*0101).
RX   MEDLINE=93085727; PubMed=1453454;
RA   Beck S., Kelly A., Radley E., Khurshid F., Alderton R.P.,
RA   Trowsdale J.;
RT   "DNA sequence analysis of 66 kb of the human MHC class II region
RT   encoding a cluster of genes for antigen processing.";
RL   J. Mol. Biol. 228:433-441(1992).
RN   [2]
RP   SEQUENCE FROM N.A. (TAP2*0101/TAP2*0201).
RX   MEDLINE=92159069; PubMed=1741401;
RA   Powis S.H., Mockridge I., Kelly A., Kerr L.-A., Glynne R.J.,
RA   Gileadi U., Beck S., Trowsdale J.;
RT   "Polymorphism in a second ABC transporter gene located within the
RT   class II region of the human major histocompatibility complex.";
RL   Proc. Natl. Acad. Sci. U.S.A. 89:1463-1467(1992).
RN   [3]
RP   SEQUENCE FROM N.A. (TAP2*0201).
RX   MEDLINE=92052217; PubMed=1946428;
RA   Bahram S., Arnold D., Bresnahan M., Strominger J.L., Spies T.;
RT   "Two putative subunits of a peptide pump encoded in the human major
RT   histocompatibility complex class II region.";
RL   Proc. Natl. Acad. Sci. U.S.A. 88:10094-10098(1991).
RN   [4]
RP   SEQUENCE FROM N.A. (TAP2*0102).
RX   MEDLINE=93154779; PubMed=8428770;
RA   Powis S.H., Tonks S., Mockridge I., Kelly A.P., Bodmer J.G.,
RA   Trowsdale J.;
RT   "Alleles and haplotypes of the MHC-encoded ABC transporters TAP1 and
RT   TAP2.";
RL   Immunogenetics 37:373-380(1993).
RN   [5]
RP   SEQUENCE FROM N.A. (TAP2*0101).
RX   MEDLINE=96144827; PubMed=8568858;
RA   Beck S., Abdulla S., Alderton R.P., Glynne R.J., Gut I.G.,
RA   Hosking L.K., Jackson A., Kelly A., Newell W.R., Sanseau P.,
RA   Radley E., Thorpe K.L., Trowsdale J.;
RT   "Evolutionary dynamics of non-coding sequences within the class II
RT   region of the human MHC.";
RL   J. Mol. Biol. 255:1-13(1996).
RN   [6]
RP   SEQUENCE FROM N.A.
RC   TISSUE=Brain;
RX   MEDLINE=22388257; PubMed=12477932; DOI=10.1073/pnas.242603899;
RA   Strausberg R.L., Feingold E.A., Grouse L.H., Derge J.G.,
RA   Klausner R.D., Collins F.S., Wagner L., Shenmen C.M., Schuler G.D.,
RA   Altschul S.F., Zeeberg B., Buetow K.H., Schaefer C.F., Bhat N.K.,
RA   Hopkins R.F., Jordan H., Moore T., Max S.I., Wang J., Hsieh F.,
RA   Diatchenko L., Marusina K., Farmer A.A., Rubin G.M., Hong L.,
RA   Stapleton M., Soares M.B., Bonaldo M.F., Casavant T.L., Scheetz T.E.,
RA   Brownstein M.J., Usdin T.B., Toshiyuki S., Carninci P., Prange C.,
RA   Raha S.S., Loquellano N.A., Peters G.J., Abramson R.D., Mullahy S.J.,
RA   Bosak S.A., McEwan P.J., McKernan K.J., Malek J.A., Gunaratne P.H.,
RA   Richards S., Worley K.C., Hale S., Garcia A.M., Gay L.J., Hulyk S.W.,
RA   Villalon D.K., Muzny D.M., Sodergren E.J., Lu X., Gibbs R.A.,
RA   Fahey J., Helton E., Ketteman M., Madan A., Rodrigues S., Sanchez A.,
RA   Whiting M., Madan A., Young A.C., Shevchenko Y., Bouffard G.G.,
RA   Blakesley R.W., Touchman J.W., Green E.D., Dickson M.C.,
RA   Rodriguez A.C., Grimwood J., Schmutz J., Myers R.M.,
RA   Butterfield Y.S.N., Krzywinski M.I., Skalska U., Smailus D.E.,
RA   Schnerch A., Schein J.E., Jones S.J.M., Marra M.A.;
RT   "Generation and initial analysis of more than 15,000 full-length human
RT   and mouse cDNA sequences.";
RL   Proc. Natl. Acad. Sci. U.S.A. 99:16899-16903(2002).
RN   [7]
RP   SEQUENCE OF 65-686 FROM N.A. (TAP2*0103).
RC   TISSUE=Blood;
RX   MEDLINE=95313033; PubMed=7792761;
RA   Cano P., Baxter-Lowe L.A.;
RT   "Novel human TAP2*103 allele shows further polymorphism in the ATP-
RT   binding domain.";
RL   Tissue Antigens 45:139-142(1995).
RN   [8]
RP   SEQUENCE OF 204-686 FROM N.A., AND VARIANTS THR-374 AND ILE-467.
RA   Tang J., Allen S., Karita E., Musonda R., Kaslow R.A.;
RT   "New TAP2 polymorphisms in Africans.";
RL   Submitted (OCT-1998) to the EMBL/GenBank/DDBJ databases.
RN   [9]
RP   SEQUENCE OF 517-645 FROM N.A. (TAP2*0101/TAP2*0102).
RA   Singal D.P., Ye M., D'Souza M.;
RL   Submitted (FEB-1993) to the EMBL/GenBank/DDBJ databases.
RN   [10]
RP   SEQUENCE OF 517-645 FROM N.A. (TAP2*0101/TAP2*0102).
RX   MEDLINE=94215245; PubMed=8162639;
RA   Singal D.P., Ye M., Qiu X., D'Souza M.;
RT   "Polymorphisms in the TAP2 gene and their association with rheumatoid
RT   arthritis.";
RL   Clin. Exp. Rheumatol. 12:29-33(1994).
RN   [11]
RP   PEPTIDE-BINDING SITE.
RX   PubMed=8955196;
RA   Nijenhuis M., Hammerling G.J.;
RT   "Multiple regions of the transporter associated with antigen
RT   processing (TAP) contribute to its peptide binding site.";
RL   J. Immunol. 157:5467-5477(1996).
RN   [12]
RP   INHIBITION BY ICP47.
RX   PubMed=8670825;
RA   Ahn K., Meyer T.H., Uebel S., Sempe P., Djaballah H., Yang Y.,
RA   Peterson P.A., Frueh K., Tampe R.;
RT   "Molecular mechanism and species specificity of TAP inhibition by
RT   herpes simplex virus ICP47.";
RL   EMBO J. 15:3247-3255(1996).
RN   [13]
RP   INHIBITION BY US6 GLYCOPROTEIN.
RX   PubMed=9175839;
RA   Ahn K., Gruhler A., Galocha B., Jones T.R., Wiertz E.J.H.J.,
RA   Ploegh H.L., Peterson P.A., Yang Y., Frueh K.;
RT   "The ER-luminal domain of the HCMV glycoprotein US6 inhibits peptide
RT   translocation by TAP.";
RL   Immunity 6:613-621(1997).
RN   [14]
RP   INHIBITION BY US6 GLYCOPROTEIN.
RX   PubMed=11157746;
RA   Hewitt E.W., Gupta S.S., Lehner P.J.;
RT   "The human cytomegalovirus gene product US6 inhibits ATP binding by
RT   TAP.";
RL   EMBO J. 20:387-396(2001).
RN   [15]
RP   INHIBITION BY E3-19K GLYCOPROTEIN.
RX   PubMed=10227971;
RA   Bennett E.M., Bennink J.R., Yewdell J.W., Brodsky F.M.;
RT   "Cutting edge: adenovirus E19 has two mechanisms for affecting class I
RT   MHC expression.";
RL   J. Immunol. 162:5049-5052(1999).
RN   [16]
RP   VARIANTS ILE-379 AND ALA-665.
RX   MEDLINE=92237283; PubMed=1570316;
RA   Colonna M., Bresnahan M., Bahram S., Strominger J.L., Spies T.;
RT   "Allelic variants of the human putative peptide transporter involved
RT   in antigen processing.";
RL   Proc. Natl. Acad. Sci. U.S.A. 89:3932-3936(1992).
RN   [17]
RP   VARIANT TAP2*BKY2 VAL-577.
RX   MEDLINE=97464203; PubMed=9324024;
RA   Kumagai S., Kanagawa S., Morinobu A., Takada M., Nakamura K.,
RA   Sugai S., Maruya E., Saji H.;
RT   "Association of a new allele of the TAP2 gene, TAP2*Bky2 (Val577),
RT   with susceptibility to Sjogren's syndrome.";
RL   Arthritis Rheum. 40:1685-1692(1997).
RN   [18]
RP   VARIANTS THR-374; ILE-379; ILE-467; SER-513 THR-565; CYS-651; ALA-665
RP   AND GLN-GLU-GLY-GLN-ASP-LEU-TYR-SER-ARG-LEU-VAL-GLN-GLN-ARG-LEU-MET-
RP   ASP-686 INS, AND DEFINITION OF ALLELES.
RX   MEDLINE=21190086; PubMed=11294565; DOI=10.1038/sj/gene/6363731;
RA   Tang J., Freedman D.O., Allen S., Karita E., Musonda R., Braga C.,
RA   Jamieson B.D., Louie L., Kaslow R.A.;
RT   "Genotyping TAP2 variants in North American Caucasians, Brazilians,
RT   and Africans.";
RL   Genes Immun. 2:32-40(2001).
CC   -!- FUNCTION: Involved in the transport of antigens from the cytoplasm
CC       to the endoplasmic reticulum for association with MHC class I
CC       molecules. Also acts as a molecular scaffold for the final stage
CC       of MHC class I folding, namely the binding of peptide. Nascent MHC
CC       class I molecules associate with TAP via tapasin. Inhibited by the
CC       covalent attachment of herpes simplex virus ICP47 protein, which
CC       blocks the peptide-binding site of TAP. Inhibited by human
CC       cytomegalovirus US6 glycoprotein, which binds to the lumenal side
CC       of the TAP complex and inhibits peptide translocation by
CC       specifically blocking ATP-binding to TAP1 and prevents the
CC       conformational rearrangement of TAP induced by peptide binding.
CC       Inhibited by human adenovirus E3-19K glycoprotein, which binds the
CC       TAP complex and acts as a tapasin inhibitor, preventing MHC class
CC       I/TAP association.
CC   -!- SUBUNIT: Heterodimer of TAP1 and TAP2.
CC   -!- SUBCELLULAR LOCATION: Integral membrane protein. Endoplasmic
CC       reticulum. The transmembrane segments seem to form a pore in the
CC       membrane.
CC   -!- INDUCTION: By interferon gamma.
CC   -!- DOMAIN: The peptide-binding site is shared between the cytoplasmic
CC       loops of TAP1 and TAP2.
CC   -!- POLYMORPHISM: 4 common alleles are officially recognized:
CC       TAP2*0101 (TAP2A or PSF2A or RING11A), TAP2*0102 (TAP2E),
CC       TAP2*0103 (TAP2F), and TAP2*0201 (TAP2B or PSF2B or RING11B).
CC       Other relatively common alleles have been identified: TAP2*01D,
CC       TAP2*01E, TAP2*01F, TAP2*01G, TAP2*01H, TAP2*02B, TAP2*02C
CC       (TAP2*0202), TAP2*02D, TAP2*02E, TAP2*02F, TAP2*03A and TAP2*04A.
CC       The sequence shown is that of TAP2*0101.
CC   -!- POLYMORPHISM: The allele TAP2*Bky2 is commonly found only in the
CC       Japanese population. It may be associated with susceptibility to
CC       Sjoegren's syndrome, an autoimmune disorder characterized by
CC       abnormal dryness of the conjunctiva, cornea and mouth due to
CC       exocrine glands dysfunction.
CC   -!- SIMILARITY: Belongs to the ABC transporter family. MDR subfamily.
DR   EMBL; X66401; CAA47027.1; -.
DR   EMBL; M84748; -; NOT_ANNOTATED_CDS.
DR   EMBL; M74447; AAA59841.1; -.
DR   EMBL; Z22935; CAA80522.1; -.
DR   EMBL; Z22936; CAA80523.1; -.
DR   EMBL; X87344; CAA60788.1; -.
DR   EMBL; U07844; AAA79901.1; -.
DR   EMBL; BC002751; AAH02751.1; -.
DR   EMBL; AF100418; AAD23381.1; -.
DR   EMBL; AF100415; AAD23381.1; JOINED.
DR   EMBL; AF100416; AAD23381.1; JOINED.
DR   EMBL; AF100417; AAD23381.1; JOINED.
DR   EMBL; L09191; AAA58648.1; -.
DR   EMBL; L10287; AAA58649.1; -.
DR   PIR; B41538; B41538.
DR   HSSP; Q03518; 1JJ7.
DR   HGNC; HGNC:44; TAP2.
DR   MIM; 170261; -.
DR   GO; GO:0005829; C:cytosol; NAS.
DR   GO; GO:0005788; C:endoplasmic reticulum lumen; IMP.
DR   GO; GO:0016021; C:integral to membrane; NAS.
DR   GO; GO:0042825; C:TAP complex; NAS.
DR   GO; GO:0005524; F:ATP binding; NAS.
DR   GO; GO:0004409; F:homoaconitate hydratase activity; NAS.
DR   GO; GO:0042288; F:MHC class I protein binding; NAS.
DR   GO; GO:0042605; F:peptide antigen binding; NAS.
DR   GO; GO:0015433; F:peptide antigen transporter activity; NAS.
DR   GO; GO:0042301; F:phosphate binding; NAS.
DR   GO; GO:0046982; F:protein heterodimerization activity; IPI.
DR   GO; GO:0046980; F:tapasin binding; IPI.
DR   GO; GO:0048004; P:antigen presentation, endogenous peptide an...; NAS.
DR   GO; GO:0019885; P:antigen processing, endogenous antigen via ...; NAS.
DR   GO; GO:0046967; P:cytosol to ER transport; NAS.
DR   GO; GO:0006886; P:intracellular protein transport; IMP.
DR   GO; GO:0015833; P:peptide transport; NAS.
DR   GO; GO:0006461; P:protein complex assembly; NAS.
DR   InterPro; IPR003593; AAA_ATPase.
DR   InterPro; IPR001140; ABC_TM_transpt.
DR   InterPro; IPR003439; ABC_transporter.
DR   InterPro; IPR005293; Ag_transporter2.
DR   Pfam; PF00664; ABC_membrane; 1.
DR   Pfam; PF00005; ABC_tran; 1.
DR   ProDom; PD000006; ABC_transporter; 1.
DR   SMART; SM00382; AAA; 1.
DR   TIGRFAMs; TIGR00958; 3a01208; 1.
DR   PROSITE; PS50929; ABC_TM1F; 1.
DR   PROSITE; PS00211; ABC_TRANSPORTER_1; 1.
DR   PROSITE; PS50893; ABC_TRANSPORTER_2; 1.
KW   Immune response; Transport; Peptide transport; Endoplasmic reticulum;
KW   ATP-binding; Transmembrane; Polymorphism.
FT   TOPO_DOM      1      6       Lumenal (Potential).
FT   TRANSMEM      7     27       1 (Potential).
FT   TOPO_DOM     28     56       Cytoplasmic (Potential).
FT   TRANSMEM     57     77       2 (Potential).
FT   TOPO_DOM     78     98       Lumenal (Potential).
FT   TRANSMEM     99    119       3 (Potential).
FT   TOPO_DOM    120    148       Cytoplasmic (Potential).
FT   TRANSMEM    149    169       4 (Potential).
FT   TOPO_DOM    170    187       Lumenal (Potential).
FT   TRANSMEM    188    208       5 (Potential).
FT   TOPO_DOM    209    266       Cytoplasmic (Potential).
FT   TRANSMEM    267    287       6 (Potential).
FT   TOPO_DOM    288    293       Lumenal (Potential).
FT   TRANSMEM    294    314       7 (Potential).
FT   TOPO_DOM    315    374       Cytoplasmic (Potential).
FT   TRANSMEM    375    395       8 (Potential).
FT   TOPO_DOM    396    408       Lumenal (Potential).
FT   TRANSMEM    409    429       9 (Potential).
FT   TOPO_DOM    430    686       Cytoplasmic (Potential).
FT   NP_BIND     503    510       ATP (Potential).
FT   REGION      301    389       Involved in peptide-binding site.
FT   REGION      414    433       Involved in peptide-binding site.
FT   REGION      468    686       ABC transporter.
FT   VARIANT     374    374       A -> T (in allele TAP2*01F, allele
FT                                TAP2*01G, allele TAP2*01H, allele
FT                                TAP2*02B and allele TAP2*02D).
FT                                /FTId=VAR_014997.
FT   VARIANT     379    379       V -> I (in allele TAP2*01D, allele
FT                                TAP2*01E, allele TAP2*01G, allele
FT                                TAP2*02C and allele TAP2*02F;
FT                                dbSNP:1800454).
FT                                /FTId=VAR_000094.
FT   VARIANT     467    467       V -> I (in allele TAP2*01F and allele
FT                                TAP2*02D).
FT                                /FTId=VAR_014998.
FT   VARIANT     513    513       A -> S (rare polymorphism).
FT                                /FTId=VAR_014999.
FT   VARIANT     565    565       A -> T (in allele TAP2*0102, allele
FT                                TAP2*01D, allele TAP2*02E and allele
FT                                TAP2*02F).
FT                                /FTId=VAR_000095.
FT   VARIANT     577    577       M -> V (in allele TAP2*BKY2).
FT                                /FTId=VAR_015000.
FT   VARIANT     651    651       R -> C (in allele TAP2*0103 and allele
FT                                TAP2*01G).
FT                                /FTId=VAR_000096.
FT   VARIANT     665    665       T -> A (in allele TAP2*0201, allele
FT                                TAP2*02B, allele TAP2*02C, allele
FT                                TAP2*02D, allele TAP2*02E, allele
FT                                TAP2*02F, allele TAP2*04A and allele
FT                                TAP2*Bky2; dbSNP:241447).
FT                                /FTId=VAR_000097.
FT   VARIANT     686    686       L -> LQEGQDLYSRLVQQRLMD (in allele
FT                                TAP2*0201, allele TAP2*02B, allele
FT                                TAP2*02C, allele TAP2*02D, allele
FT                                TAP2*02E, allele TAP2*02F, allele
FT                                TAP2*03A and allele TAP2*Bky2).
FT                                /FTId=VAR_000098.
**
**   #################    INTERNAL SECTION    ##################
**CL 6p21.3;
**ZB CHH, 8-JAN-2004;
SQ   SEQUENCE   686 AA;  75664 MW;  E7E4A7F6A2A3B48B CRC64;
     MRLPDLRPWT SLLLVDAALL WLLQGPLGTL LPQGLPGLWL EGTLRLGGLW GLLKLRGLLG
     FVGTLLLPLC LATPLTVSLR ALVAGASRAP PARVASAPWS WLLVGYGAAG LSWSLWAVLS
     PPGAQEKEQD QVNNKVLMWR LLKLSRPDLP LLVAAFFFLV LAVLGETLIP HYSGRVIDIL
     GGDFDPHAFA SAIFFMCLFS FGSSLSAGCR GGCFTYTMSR INLRIREQLF SSLLRQDLGF
     FQETKTGELN SRLSSDTTLM SNWLPLNANV LLRSLVKVVG LYGFMLSISP RLTLLSLLHM
     PFTIAAEKVY NTRHQEVLRE IQDAVARAGQ VVREAVGGLQ TVRSFGAEEH EVCRYKEALE
     QCRQLYWRRD LERALYLLVR RVLHLGVQML MLSCGLQQMQ DGELTQGSLL SFMIYQESVG
     SYVQTLVYIY GDMLSNVGAA EKVFSYMDRQ PNLPSPGTLA PTTLQGVVKF QDVSFAYPNR
     PDRPVLKGLT FTLRPGEVTA LVGPNGSGKS TVAALLQNLY QPTGGQVLLD EKPISQYEHC
     YLHSQVVSVG QEPVLFSGSV RNNIAYGLQS CEDDKVMAAA QAAHADDFIQ EMEHGIYTDV
     GEKGSQLAAG QKQRLAIARA LVRDPRVLIL DEATSALDVQ CEQALQDWNS RGDRTVLVIA
     HRLQTVQRAH QILVLQEGKL QKLAQL
//
ID   CIN5_HUMAN     STANDARD;      PRT;  2016 AA.
AC   Q14524;
DT   15-DEC-1998 (Rel. 37, Created)
DT   15-DEC-1998 (Rel. 37, Last sequence update)
DT   15-JUN-2004 (Rel. 44, Last annotation update)
DE   Sodium channel protein type V alpha subunit (Voltage-gated sodium
DE   channel alpha subunit Nav1.5) (Sodium channel protein, cardiac muscle
DE   alpha-subunit) (HH1).
GN   Name=SCN5A;
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   TISSUE=Heart;
RX   MEDLINE=92115699; PubMed=1309946;
RA   Gellens M.E., George A.L. Jr., Chen L.Q., Chahine M., Horn R.,
RA   Barchi R.L., Kallen R.G.;
RT   "Primary structure and functional expression of the human cardiac
RT   tetrodotoxin-insensitive voltage-dependent sodium channel.";
RL   Proc. Natl. Acad. Sci. U.S.A. 89:554-558(1992).
RN   [2]
RP   VARIANTS LQT3.
RX   MEDLINE=95196273; PubMed=7889574;
RA   Wang Q., Shen J., Splawski I., Atkinson D., Li Z., Robinson J.L.,
RA   Moss A.J., Towbin J.A., Keating M.T.;
RT   "SCN5A mutations associated with an inherited cardiac arrhythmia, long
RT   QT syndrome.";
RL   Cell 80:805-811(1995).
RN   [3]
RP   VARIANTS LQT3.
RX   MEDLINE=96081224; PubMed=8541846;
RA   Wang Q., Shen J., Li Z., Timothy K.W., Vincent G.M., Priori S.G.,
RA   Schwartz P.J., Keating M.T.;
RT   "Cardiac sodium channel mutations in patients with long QT syndrome,
RT   an inherited cardiac arrhythmia.";
RL   Hum. Mol. Genet. 4:1603-1607(1995).
RN   [4]
RP   VARIANT LQT3 1505-LYS--GLN-1507 DEL.
RX   MEDLINE=95379949; PubMed=7651517;
RA   Bennett P.B., Yazawa K., Makita N., George A.L. Jr.;
RT   "Molecular mechanism for an inherited cardiac arrhythmia.";
RL   Nature 376:683-685(1995).
RN   [5]
RP   VARIANT LQT3 GLY-1790.
RX   MEDLINE=98349542; PubMed=9686753;
RA   An R.H., Wang X.L., Kerem B., Benhorin J., Medina A., Goldmit M.,
RA   Kass R.S.;
RT   "Novel LQT-3 mutation affects Na+ channel activity through
RT   interactions between alpha- and beta1-subunits.";
RL   Circ. Res. 83:141-146(1998).
RN   [6]
RP   VARIANT LQT3 GLN-1623.
RX   MEDLINE=98165676; PubMed=9506831;
RA   Makita N., Shirai N., Nagashima M., Matsuoka R., Yamada Y., Tohse N.,
RA   Kitabatake A.;
RT   "A de novo missense mutation of human cardiac Na(+) channel exhibiting
RT   novel molecular mechanisms of long QT syndrome.";
RL   FEBS Lett. 423:5-9(1998).
RN   [7]
RP   VARIANT LQT3 GLY-1839.
**   MEDLINE=None; PubMed=None;
RA   Benhorin J., Goldmit M., Maccluer J.W., Blangero J., Goffen R.,
RA   Leibovitch A., Rahat A., Wang Q., Medina A., Towbin J.A., Kerem B.;
RT   "Identification of a new SCN5A mutation, D1840G, associated with the
RT   long QT syndrome.";
RL   Hum. Mutat. 12:72-72(1998).
RN   [8]
RP   VARIANT LQT3 GLN-1623.
**   MEDLINE=None; PubMed=None;
RA   Yamagishi H., Furutani M., Kamisago M., Morikawa Y., Kojima Y.,
RA   Hino Y., Furutani Y., Kimura M., Imamura S.-I., Takao A., Momma K.,
RA   Matsuoka R.;
RT   "A De Novo missense mutation (R1623Q) of the SCN5A gene in a Japanese
RT   girl with sporadic long QT syndrome.";
RL   Hum. Mutat. 12:481-481(1998).
RN   [9]
RP   VARIANTS BRUGADA SYNDROME TRP-1232 AND MET-1620.
RX   PubMed=9521325; DOI=10.1038/32675;
RA   Chen Q., Kirsch G.E., Zhang D., Brugada R., Brugada J., Brugada P.,
RA   Potenza D., Moya A., Borggrefe M., Breithardt G., Ortiz-Lopez R.,
RA   Wang Z., Antzelevitch C., O'Brien R.E., Schulze-Bahr E., Keating M.T.,
RA   Towbin J.A., Wang Q.;
RT   "Genetic basis and molecular mechanism for idiopathic ventricular
RT   fibrillation.";
RL   Nature 392:293-296(1998).
RN   [10]
RP   VARIANTS LQT3 MET-1304 AND MET-1645, AND VARIANT ASN-1500.
RX   MEDLINE=99439526; PubMed=10508990;
RA   Wattanasirichaigoon D., Vesely M.R., Duggal P., Levine J.C.,
RA   Blume E.D., Wolff G.S., Edwards S.B., Beggs A.H.;
RT   "Sodium channel abnormalities are infrequent in patients with long QT
RT   syndrome: identification of two novel SCN5A mutations.";
RL   Am. J. Med. Genet. 86:470-476(1999).
RN   [11]
RP   CHARATERIZATION OF VARIANTS BRUGADA SYNDROME TRP-1512 AND THR-1924.
RX   PubMed=10690282;
RA   Rook M.B., Bezzina Alshinawi C., Groenewegen W.A., van Gelder I.C.,
RA   van Ginneken A.C.G., Jongsma H.J., Mannens M.M.A.M., Wilde A.A.M.;
RT   "Human SCN5A gene mutations alter cardiac sodium channel kinetics and
RT   are associated with the Brugada syndrome.";
RL   Cardiovasc. Res. 44:507-517(1999).
RN   [12]
RP   VARIANT LQT3 LYS-1784.
RX   MEDLINE=99307063; PubMed=10377081;
RA   Wei J., Wang D.W., Alings M., Fish F., Wathen M., Roden D.M.,
RA   George A.L. Jr.;
RT   "Congenital long-QT syndrome caused by a novel mutation in a conserved
RT   acidic domain of the cardiac Na+ channel.";
RL   Circulation 99:3165-3171(1999).
RN   [13]
RP   CHARACTERIZATION OF VARIANT BRUGADA SYNDROME MET-1620.
RX   PubMed=10532948;
RA   Dumaine R., Towbin J.A., Brugada P., Vatta M., Nesterenko D.V.,
RA   Nesterenko V.V., Brugada J., Brugada R., Antzelevitch C.;
RT   "Ionic mechanisms responsible for the electrocardiographic phenotype
RT   of the Brugada syndrome are temperature dependent.";
RL   Circ. Res. 85:803-809(1999).
RN   [14]
RP   CHARACTERIZATION OF VARIANT LQT3/BRUGADA SYNDROME ASP-1795 INS.
RX   PubMed=10590249;
RA   Bezzina C.R., Veldkamp M.W., van Den Berg M.P., Postma A.V.,
RA   Rook M.B., Viersma J.-W., van Langen I.M., Tan-Sindhunata G.,
RA   Bink-Boelkens M.T.E., van Der Hout A.H., Mannens M.M.A.M.,
RA   Wilde A.A.M.;
RT   "A single Na(+) channel mutation causing both long-QT and Brugada
RT   syndromes.";
RL   Circ. Res. 85:1206-1213(1999).
RN   [15]
RP   DISEASE.
RX   PubMed=10471492; DOI=10.1038/12618;
RA   Schott J.-J., Alshinawi C., Kyndt F., Probst V., Hoorntje T.M.,
RA   Hulsbeek M., Wilde A.A.M., Escande D., Mannens M.M.A.M., Le Marec H.;
RT   "Cardiac conduction defects associate with mutations in SCN5A.";
RL   Nat. Genet. 23:20-21(1999).
RN   [16]
RP   CHARACTERIZATION OF VARIANT BRUGADA SYNDROME MET-1620.
RX   PubMed=10618304;
RA   Makita N., Shirai N., Wang D.W., Sasaki K., George A.L. Jr., Kanno M.,
RA   Kitabatake A.;
RT   "Cardiac Na(+) channel dysfunction in Brugada syndrome is aggravated
RT   by beta(1)-subunit.";
RL   Circulation 101:54-60(2000).
RN   [17]
RP   VARIANTS LQT3 ASN-1114; VAL-1501; LEU-1623; HIS-1644 AND ASN-1787.
RX   MEDLINE=20432616; PubMed=10973849;
RA   Splawski I., Shen J., Timothy K.W., Lehmann M.H., Priori S.,
RA   Robinson J.L., Moss A.J., Schwartz P.J., Towbin J.A., Vincent G.M.,
RA   Keating M.T.;
RT   "Spectrum of mutations in long-QT syndrome genes. KVLQT1, HERG, SCN5A,
RT   KCNE1, and KCNE2.";
RL   Circulation 102:1178-1185(2000).
RN   [18]
RP   VARIANT IVF LEU-1710.
RX   PubMed=10940383;
RA   Akai J., Makita N., Sakurada H., Shirai N., Ueda K., Kitabatake A.,
RA   Nakazawa K., Kimura A., Hiraoka M.;
RT   "A novel SCN5A mutation associated with idiopathic ventricular
RT   fibrillation without typical ECG findings of Brugada syndrome.";
RL   FEBS Lett. 479:29-34(2000).
RN   [19]
RP   VARIANT LQT3 ASN-941.
RX   PubMed=10911008;
RA   Schwartz P.J., Priori S.G., Dumaine R., Napolitano C.,
RA   Antzelevitch C., Stramba-Badiale M., Richard T.A., Berti M.R.,
RA   Bloise R.;
RT   "A molecular link between the sudden infant death syndrome and the
RT   long-QT syndrome.";
RL   N. Engl. J. Med. 343:262-267(2000).
RN   [20]
RP   CHARACTERIZATION OF VARIANTS LQT3 CYS-1795 AND BRUGADA SYNDROME
RP   HIS-1795.
RX   PubMed=11410597; DOI=10.1074/jbc.M104471200;
RA   Rivolta I., Abriel H., Tateyama M., Liu H., Memmi M., Vardas P.,
RA   Napolitano C., Priori S.G., Kass R.S.;
RT   "Inherited Brugada and long QT-3 syndrome mutations of a single
RT   residue of the cardiac sodium channel confer distinct channel and
RT   clinical phenotypes.";
RL   J. Biol. Chem. 276:30623-30630(2001).
RN   [21]
RP   VARIANT SSS1/BRUGADA SYNDROME ARG-1408.
RX   PubMed=11748104;
RA   Kyndt F., Probst V., Potet F., Demolombe S., Chevallier J.-C.,
RA   Baro I., Moisan J.-P., Boisseau P., Schott J.-J., Escande D.,
RA   Le Marec H.;
RT   "Novel SCN5A mutation leading either to isolated cardiac conduction
RT   defect or Brugada syndrome in a large French family.";
RL   Circulation 104:3081-3086(2001).
RN   [22]
RP   CHARACTERIZATION OF VARIANTS LQT3 SER-997 AND HIS-1826.
RX   PubMed=11710892;
RA   Ackerman M.J., Siu B.L., Sturner W.Q., Tester D.J., Valdivia C.R.,
RA   Makielski J.C., Towbin J.A.;
RT   "Postmortem molecular analysis of SCN5A defects in sudden infant death
RT   syndrome.";
RL   JAMA 286:2264-2269(2001).
RN   [23]
RP   CHARACTERIZATION OF VARIANT CARDIAC CONDUCTION DEFECT CYS-514.
RX   PubMed=11234013; DOI=10.1038/35059090;
RA   Tan H.L., Bink-Boelkens M.T.E., Bezzina C.R., Viswanathan P.C.,
RA   Beaufort-Krol G.C.M., van Tintelen P.J., van den Berg M.P.,
RA   Wilde A.A.M., Balser J.R.;
RT   "A sodium-channel mutation causes isolated cardiac conduction
RT   disease.";
RL   Nature 409:1043-1047(2001).
RN   [24]
RP   CHARATCTERIZATION OF VARIANTS PROGRESSIVE FAMILIAL HEART BLOCK TYPE I
RP   SER-298 AND ASN-1595.
RX   PubMed=11804990;
RA   Wang D.W., Viswanathan P.C., Balser J.R., George A.L. Jr.,
RA   Benson D.W.;
RT   "Clinical, genetic and biophysical characterisation of SCN5A mutations
RT   associated with atrioventricular block.";
RL   Circulation 105:341-346(2002).
RN   [25]
RP   VIRTUAL MODELING OF VARIANT LQT3/BRUGADA SYNDROME ASP-1795 INS.
RX   PubMed=11889015;
RA   Clancy C.E., Rudy Y.;
RT   "Na(+) channel mutation that causes both Brugada and long-QT syndrome
RT   phenotypes: a simulation study of mechanism.";
RL   Circulation 105:1208-1213(2002).
RN   [26]
RP   CHARACTERIZATION OF VARIANTS BRUGADA SYNDROME HIS-367; VAL-735 AND
RP   GLN-1193.
RX   PubMed=11823453;
RA   Vatta M., Dumaine R., Varghese G., Richard T.A., Shimizu W.,
RA   Aihara N., Nademanee K., Brugada R., Brugada J., Veerakul G., Li H.,
RA   Bowles N.E., Brugada P., Antzelevitch C., Towbin J.A.;
RT   "Genetic and biophysical basis of sudden unexplained nocturnal death
RT   syndrome (SUNDS), a disease allelic to Brugada syndrome.";
RL   Hum. Mol. Genet. 11:337-345(2002).
RN   [27]
RP   VARIANT ACQUIRED ARRHYTHMIA TYR-1103.
RX   PubMed=12471205;
RA   Chen S., Chung M.K., Martin D., Rozich R., Tchou P.J., Wang Q.;
RT   "SNP S1103Y in the cardiac sodium channel gene SCN5A is associated
RT   with cardiac arrhythmias and sudden death in a white family.";
RL   J. Med. Genet. 39:913-915(2002).
RN   [28]
RP   VARIANT ACQUIRED ARRHYTHMIA TYR-1103.
RX   PubMed=12193783; DOI=10.1126/science.1073569;
RA   Splawski I., Timothy K.W., Tateyama M., Clancy C.E., Malhotra A.,
RA   Beggs A.H., Cappuccio F.P., Sagnella G.A., Kass R.S., Keating M.T.;
RT   "Variant of SCN5A sodium channel implicated in risk of cardiac
RT   arrhythmia.";
RL   Science 297:1333-1336(2002).
RN   [29]
RP   VARIANT LQT3 PHE-619.
RX   MEDLINE=22560398; PubMed=12673799; DOI=10.1002/humu.9136;
RA   Wehrens X.H., Rossenbacker T., Jongbloed R.J., Gewillig M.,
RA   Heidbuchel H., Doevendans P.A., Vos M.A., Wellens H.J., Kass R.S.;
RT   "A novel mutation L619F in the cardiac Na+ channel SCN5A associated
RT   with long-QT syndrome (LQT3): a role for the I-II linker in
RT   inactivation gating.";
RL   Hum. Mutat. 21:552-552(2003).
RN   [30]
RP   VARIANTS SSS1 ILE-220; LEU-1298 AND ARG-1408.
RX   PubMed=14523039; DOI=10.1172/JCI200318062;
RA   Benson D.W., Wang D.W., Dyment M., Knilans T.K., Fish F.A.,
RA   Strieper M.J., Rhodes T.H., George A.L. Jr.;
RT   "Congenital sick sinus syndrome caused by recessive mutations in the
RT   cardiac sodium channel gene (SCN5A).";
RL   J. Clin. Invest. 112:1019-1028(2003).
CC   -!- FUNCTION: This protein mediates the voltage-dependent sodium ion
CC       permeability of excitable membranes. Assuming opened or closed
CC       conformations in response to the voltage difference across the
CC       membrane, the protein forms a sodium-selective channel through
CC       which Na+ ions may pass in accordance with their electrochemical
CC       gradient. It is a tetrodotoxin-resistant Na+ channel isoform. This
CC       channel is responsible for the initial upstroke of the action
CC       potential in the electrocardiogram.
CC   -!- SUBUNIT: Interacts with the PDZ domain of the syntrophin SNTA1,
CC       SNTB1 and SNTB2 (By similarity).
CC   -!- SUBCELLULAR LOCATION: Integral membrane protein.
CC   -!- TISSUE SPECIFICITY: Expressed in human atrial and ventricular
CC       cardiac muscle but not in adult skeletal muscle, brain,
CC       myometrium, liver, or spleen.
CC   -!- DOMAIN: The sequence contains 4 internal repeats, each with 5
CC       hydrophobic segments (S1,S2,S3,S5,S6) and one positively charged
CC       segment (S4). Segments S4 are probably the voltage-sensors and are
CC       characterized by a series of positively charged amino acids at
CC       every third position.
CC   -!- DISEASE: Defects in SCN5A are a cause of progressive familial
CC       heart block type I (PFHBI) [MIM:113900]; also known as Lenegre-Lev
CC       disease or progressive cardiac conduction defect (PCCD). PFHBI is
CC       characterized by progressive alteration of cardiac conduction
CC       through the His-Purkinje system with right or left bundle branch
CC       block and widening of QRS complexes, leading to complete atrio-
CC       ventricular block and causing syncope and sudden death. PFHBI
CC       inheritance is autosomal dominant.
CC   -!- DISEASE: Defects in SCN5A are the cause of long QT syndrome type 3
CC       (LQT3) [MIM:603830]. LQT3 is an autosomal dominant cardiac disease
CC       characterized by prolonged QT interval on electrocardiogram,
CC       recurrent syncope and sudden cardiac death.
CC   -!- DISEASE: Defects in SCN5A are the cause of Brugada syndrome
CC       [MIM:601144]. Brugada syndrome is an autosomal dominant inherited
CC       arrhythmia that causes the ventricles to beat so fast that they
CC       can prevent the blood from circulating efficiently in the body.
CC       When this situation occurs (called ventricular fibrillation), the
CC       individual will faint and may die in a few minutes if the heart is
CC       not reset. Brugada syndrome is an idiopathic ventricular
CC       fibrillation (IVF) syndrome characterized by right bundle branch
CC       block and ST elevation on an electrocardiogram (ECG). While
CC       Brugada syndrome is a disease that usually affects people in their
CC       30's, it has actually been described at all ages.
CC   -!- DISEASE: Defects in SCN5A are the cause of autosomal recessive
CC       sick sinus syndrome 1 (SSS1) [MIM:608567]. The term 'sick sinus
CC       syndrome' encompasses a variety of conditions caused by sinus node
CC       dysfunction. The most common clinical manifestations are syncope,
CC       presyncope, dizziness, and fatigue. Electrocardiogram typically
CC       shows sinus bradycardia, sinus arrest, and/or sinoatrial block.
CC       Episodes of atrial tachycardias coexisting with sinus bradycardia
CC       ('tachycardia-bradycardia syndrome') are also common in this
CC       disorder. SSS occurs most often in the elderly associated with
CC       underlying heart disease or previous cardiac surgery, but can also
CC       occur in the fetus, infant, or child without heart disease or
CC       other contributing factors, in which case it is considered to be a
CC       congenital disorder.
CC   -!- DISEASE: Defects in SCN5A are a cause of idiopathic ventricular
CC       fibrillation (IVF) [MIM:603829]; also called paroxysmal familial
CC       ventricular fibrillation. IVF is a self originated, of unknown
CC       causation, ventricular fibrillation that causes the ventricles to
CC       beat so fast that they can prevent the blood from circulating
CC       efficiently in the body. This disorder is not truly idiopathic in
CC       many cases but can be caused by specific mutations such as those
CC       in the SCN5A gene. IVF is said to cause more than 300,000 sudden
CC       deaths each year in the United States alone. In approximately 5 to
CC       12% of cases, there are no demonstrable cardiac or noncardiac
CC       causes to account for the episode, which is therefore classified
CC       as idiopathic ventricular fibrillation.
CC   -!- DISEASE: Defects in SCN5A are a cause of sudden infant death
CC       syndrome (SIDS) [MIM:272120]. SIDS remains elusive in its causes
CC       and devastating in its consequences. Despite the impressive
CC       decline in the incidence of SIDS since the recommendation to avoid
CC       the prone sleep position, SIDS remains a leading cause of death in
CC       the first year of life.
CC   -!- MISCELLANEOUS: Na+ channels in mammalian cardiac membrane have
CC       functional properties quite distinct from Na+ channels in nerve
CC       and skeletal muscle.
CC   -!- SIMILARITY: Belongs to the sodium channel family.
CC   -!- SIMILARITY: Contains 1 IQ domain.
CC   -!- WEB RESOURCE: Name=LQTSdb; Note=SCN5A mutations page;
CC       URL="http://www.ssi.dk/en/forskning/lqtsdb/scn5a.htm";
CC   -!- WEB RESOURCE: Name=CaBP; Note=Calpain;
CC       URL="http://structbio.vanderbilt.edu/cabp_database/general/prot_pages/calpain.html";
DR   EMBL; M77235; AAA58644.1; -.
DR   PIR; A38195; A38195.
DR   HSSP; P04775; 1BYY.
DR   HGNC; HGNC:10593; SCN5A.
DR   MIM; 600163; -.
DR   MIM; 113900; -.
DR   MIM; 603830; -.
DR   MIM; 601144; -.
DR   MIM; 608567; -.
DR   MIM; 603829; -.
DR   MIM; 272120; -.
DR   GO; GO:0005248; F:voltage-gated sodium channel activity; TAS.
DR   GO; GO:0006936; P:muscle contraction; TAS.
DR   GO; GO:0008016; P:regulation of heart rate; TAS.
DR   GO; GO:0006814; P:sodium ion transport; TAS.
DR   InterPro; IPR001682; Ca/Na_pore.
DR   InterPro; IPR002111; Cat_channel_TrpL.
DR   InterPro; IPR005821; Ion_trans.
DR   InterPro; IPR000048; IQ_region.
DR   InterPro; IPR005820; M+channel_nlg.
DR   InterPro; IPR001696; Na_channel.
DR   InterPro; IPR008053; Na_channel5.
DR   InterPro; IPR010526; Na_trans_assoc.
DR   Pfam; PF00520; Ion_trans; 4.
DR   Pfam; PF00612; IQ; 1.
DR   Pfam; PF06512; Na_trans_assoc; 1.
DR   PRINTS; PR00170; NACHANNEL.
DR   PRINTS; PR01666; NACHANNEL5.
DR   PROSITE; PS50096; IQ; FALSE_NEG.
KW   Ionic channel; Transmembrane; Ion transport; Voltage-gated channel;
KW   Glycoprotein; Repeat; Multigene family; Phosphorylation; Polymorphism;
KW   Disease mutation; Long QT syndrome; Sodium channel.
FT   TRANSMEM    127    150       S1 of repeat I (Potential).
FT   TRANSMEM    159    178       S2 of repeat I (Potential).
FT   TRANSMEM    192    210       S3 of repeat I (Potential).
FT   TRANSMEM    217    236       S4 of repeat I (Potential).
FT   TRANSMEM    253    276       S5 of repeat I (Potential).
FT   TRANSMEM    390    415       S6 of repeat I (Potential).
FT   TRANSMEM    712    736       S1 of repeat II (Potential).
FT   TRANSMEM    748    771       S2 of repeat II (Potential).
FT   TRANSMEM    780    799       S3 of repeat II (Potential).
FT   TRANSMEM    806    825       S4 of repeat II (Potential).
FT   TRANSMEM    842    862       S5 of repeat II (Potential).
FT   TRANSMEM    914    939       S6 of repeat II (Potential).
FT   TRANSMEM   1201   1224       S1 of repeat III (Potential).
FT   TRANSMEM   1238   1263       S2 of repeat III (Potential).
FT   TRANSMEM   1270   1291       S3 of repeat III (Potential).
FT   TRANSMEM   1296   1317       S4 of repeat III (Potential).
FT   TRANSMEM   1337   1359       S5 of repeat III (Potential).
FT   TRANSMEM   1444   1470       S6 of repeat III (Potential).
FT   TRANSMEM   1524   1547       S1 of repeat IV (Potential).
FT   TRANSMEM   1559   1582       S2 of repeat IV (Potential).
FT   TRANSMEM   1589   1612       S3 of repeat IV (Potential).
FT   TRANSMEM   1623   1644       S4 of repeat IV (Potential).
FT   TRANSMEM   1660   1682       S5 of repeat IV (Potential).
FT   TRANSMEM   1748   1772       S6 of repeat IV (Potential).
FT   CARBOHYD    214    214       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    283    283       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    288    288       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    291    291       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    318    318       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    328    328       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    548    548       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    592    592       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    740    740       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    803    803       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    841    841       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    864    864       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    946    946       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD   1365   1365       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD   1374   1374       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD   1380   1380       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD   1388   1388       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD   1736   1736       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD   1774   1774       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD   1955   1955       N-linked (GlcNAc...) (Potential).
FT   VARIANT     220    220       T -> I (in SSS1).
FT                                /FTId=VAR_017670.
FT   VARIANT     298    298       G -> S (in progressive familial heart
FT                                block type I; significant defect in the
FT                                kinetics of fast-channel inactivation
FT                                distinct from mutations reported in
FT                                LQT3).
FT                                /FTId=VAR_017671.
FT   VARIANT     367    367       R -> H (in Brugada syndrome; express no
FT                                current).
FT                                /FTId=VAR_017672.
FT   VARIANT     425    425       R -> H (in AT-III deficiency; type-II;
FT                                Glasgow/Sheffield/Chicago/Avranches/
FT                                Kumamoto-2; increases affinity for
FT                                heparin).
FT                                /FTId=VAR_007074.
FT   VARIANT     514    514       G -> C (in cardiac conduction defect).
FT                                /FTId=VAR_017673.
FT   VARIANT     558    558       H -> R (in dbSNP:1805124).
FT                                /FTId=VAR_008955.
FT   VARIANT     619    619       L -> F (in LQT3).
FT                                /FTId=VAR_015682.
FT   VARIANT     735    735       A -> V (in Brugada syndrome; expresses
FT                                currents with steady state activation
FT                                voltage shifted to more positive
FT                                potentials and exhibit reduced sodium
FT                                channel current at the end of phase I of
FT                                the action potential).
FT                                /FTId=VAR_017674.
FT   VARIANT     941    941       S -> N (in LQT3; also in SIDS).
FT                                /FTId=VAR_017675.
FT   VARIANT     997    997       A -> S (in LQT3; sodium current
FT                                characterized by slower decay and a 2- to
FT                                3-fold increase in late sodium current).
FT                                /FTId=VAR_017676.
FT   VARIANT    1090   1090       P -> L (in dbSNP:1805125).
FT                                /FTId=VAR_014464.
FT   VARIANT    1103   1103       S -> Y (in acquired arrhythmia;
FT                                susceptibility to).
FT                                /FTId=VAR_017677.
FT   VARIANT    1114   1114       D -> N (in LQT3).
FT                                /FTId=VAR_009935.
FT   VARIANT    1193   1193       R -> Q (in Brugada syndrome; accelerates
FT                                the inactivation of the sodium channel
FT                                current and exhibit reduced sodium
FT                                channel current at the end of phase I of
FT                                the action potential).
FT                                /FTId=VAR_017678.
FT   VARIANT    1232   1232       R -> W (in Brugada syndrome; could be a
FT                                rare polymorphism).
FT                                /FTId=VAR_017679.
FT   VARIANT    1298   1298       P -> L (in SSS1).
FT                                /FTId=VAR_017680.
FT   VARIANT    1304   1304       T -> M (in LQT3).
FT                                /FTId=VAR_008956.
FT   VARIANT    1325   1325       N -> S (in LQT3).
FT                                /FTId=VAR_001577.
FT   VARIANT    1408   1408       G -> R (in SSS1 and Brugada syndrome;
FT                                also in cardiac conduction defect).
FT                                /FTId=VAR_017681.
FT   VARIANT    1500   1500       K -> N.
FT                                /FTId=VAR_008957.
FT   VARIANT    1501   1501       L -> V (in LQT3).
FT                                /FTId=VAR_009936.
FT   VARIANT    1505   1507       Missing (in LQT3).
FT                                /FTId=VAR_001576.
FT   VARIANT    1512   1512       R -> W (in Brugada syndrome;
FT                                significantly affects cardiac sodium
FT                                channel characteristics; associated with
FT                                an increase in inward sodium current
FT                                during the action potential upstroke).
FT                                /FTId=VAR_017682.
FT   VARIANT    1595   1595       D -> N (in progressive familial heart
FT                                block type I; significant defect in the
FT                                kinetics of fast-channel inactivation
FT                                distinct from mutations reported in
FT                                LQT3).
FT                                /FTId=VAR_017683.
FT   VARIANT    1620   1620       T -> M (in Brugada syndrome;
FT                                arrhythmogenicity revealed only at
FT                                temperatures approaching the physiologic
FT                                range).
FT                                /FTId=VAR_017684.
FT   VARIANT    1623   1623       R -> L (in LQT3).
FT                                /FTId=VAR_009937.
FT   VARIANT    1623   1623       R -> Q (in LQT3).
FT                                /FTId=VAR_001578.
FT   VARIANT    1644   1644       R -> H (in LQT3).
FT                                /FTId=VAR_001579.
FT   VARIANT    1645   1645       T -> M (in LQT3).
FT                                /FTId=VAR_008958.
FT   VARIANT    1710   1710       S -> L (in IVF).
FT                                /FTId=VAR_017685.
FT   VARIANT    1784   1784       E -> K (in LQT3).
FT                                /FTId=VAR_008959.
FT   VARIANT    1787   1787       S -> N (in LQT3).
FT                                /FTId=VAR_009938.
FT   VARIANT    1790   1790       D -> G (in LQT3).
FT                                /FTId=VAR_001580.
FT   VARIANT    1795   1795       Y -> C (in LQT3; slows the onset of
FT                                activation, but does not cause a marked
FT                                negative shift in the voltage dependence
FT                                of inactivation or affect the kinetics of
FT                                the recovery from inactivation; increases
FT                                the expression of sustained Na(+) channel
FT                                activity and promotes entrance into an
FT                                intermediate or slowly developing
FT                                inactivated state).
FT                                /FTId=VAR_019123.
FT   VARIANT    1795   1795       Y -> H (in Brugada syndrome; accelerates
FT                                the onset of activation and causes a
FT                                marked negative shift in the voltage
FT                                dependence of inactivation; does not
FT                                affect the kinetics of the recovery from
FT                                inactivation; increases the expression of
FT                                sustained Na(+) channel activity and
FT                                promotes entrance into an intermediate or
FT                                slowly developing inactivated state).
FT                                /FTId=VAR_019124.
FT   VARIANT    1795   1795       Y -> YD (in LQT3 and Brugada syndrome;
FT                                7.3-mV negative shift of the steady-state
FT                                inactivation curve and 8.1-mV positive
FT                                shift of the steady-state activation
FT                                curve; may reduced sodium current during
FT                                the upstroke of the action potential).
FT                                /FTId=VAR_017686.
FT   VARIANT    1826   1826       R -> H (in LQT3; sodium current
FT                                characterized by slower decay and a 2- to
FT                                3-fold increase in late sodium current).
FT                                /FTId=VAR_017687.
FT   VARIANT    1839   1839       D -> G (in LQT3).
FT                                /FTId=VAR_001581.
FT   VARIANT    1924   1924       A -> T (in Brugada syndrome;
FT                                significantly affect cardiac sodium
FT                                channel characteristics; associated with
FT                                an increase in inward sodium current
FT                                during the action potential upstroke).
FT                                /FTId=VAR_017688.
**
**   #################    INTERNAL SECTION    ##################
**CL 3p21;
**ZB SYP, 23-JUL-2002;
SQ   SEQUENCE   2016 AA;  227162 MW;  ED97598D215E349E CRC64;
     MANFLLPRGT SSFRRFTRES LAAIEKRMAE KQARGSTTLQ ESREGLPEEE APRPQLDLQA
     SKKLPDLYGN PPQELIGEPL EDLDPFYSTQ KTFIVLNKGK TIFRFSATNA LYVLSPFHPV
     RRAAVKILVH SLFNMLIMCT ILTNCVFMAQ HDPPPWTKYV EYTFTAIYTF ESLVKILARA
     FCLHAFTFLR DPWNWLDFSV IIMAYTTEFV DLGNVSALRT FRVLRALKTI SVISGLKTIV
     GALIQSVKKL ADVMVLTVFC LSVFALIGLQ LFMGNLRHKC VRNFTALNGT NGSVEADGLV
     WESLDLYLSD PENYLLKNGT SDVLLCGNSS DAGTCPEGYR CLKAGENPDH GYTSFDSFAW
     AFLALFRLMT QDCWERLYQQ TLRSAGKIYM IFFMLVIFLG SFYLVNLILA VVAMAYEEQN
     QATIAETEEK EKRFQEAMEM LKKEHEALTI RGVDTVSRSS LEMSPLAPVN SHERRSKRRK
     RMSSGTEECG EDRLPKSDSE DGPRAMNHLS LTRGLSRTSM KPRSSRGSIF TFRRRDLGSE
     ADFADDENST ARESESHHTS LLVPWPLRRT SAQGQPSPGT SAPGHALHGK KNSTVDCNGV
     VSLLGAGDPE ATSPGSHLLR PVMLEHPPDT TTPSEEPGGP QMLTSQAPCV DGFEEPGARQ
     RALSAVSVLT SALEELEESR HKCPPCWNRL AQRYLIWECC PLWMSIKQGV KLVVMDPFTD
     LTITMCIVLN TLFMALEHYN MTSEFEEMLQ VGNLVFTGIF TAEMTFKIIA LDPYYYFQQG
     WNIFDSIIVI LSLMELGLSR MSNLSVLRSF RLLRVFKLAK SWPTLNTLIK IIGNSVGALG
     NLTLVLAIIV FIFAVVGMQL FGKNYSELRD SDSGLLPRWH MMDFFHAFLI IFRILCGEWI
     ETMWDCMEVS GQSLCLLVFL LVMVIGNLVV LNLFLALLLS SFSADNLTAP DEDREMNNLQ
     LALARIQRGL RFVKRTTWDF CCGLLRHRPQ KPAALAAQGQ LPSCIATPYS PPPPETEKVP
     PTRKETQFEE GEQPGQGTPG DPEPVCVPIA VAESDTDDQE EDEENSLGTE EESSKQQESQ
     PVSGWPRGPP DSRTWSQVSA TASSEAEASA SQADWRQQWK AEPQAPGCGE TPEDSCSEGS
     TADMTNTAEL LEQIPDLGQD VKDPEDCFTE GCVRRCPCCA VDTTQAPGKV WWRLRKTCYH
     IVEHSWFETF IIFMILLSSG ALAFEDIYLE ERKTIKVLLE YADKMFTYVF VLEMLLKWVA
     YGFKKYFTNA WCWLDFLIVD VSLVSLVANT LGFAEMGPIK SLRTLRALRP LRALSRFEGM
     RVVVNALVGA IPSIMNVLLV CLIFWLIFSI MGVNLFAGKF GRCINQTEGD LPLNYTIVNN
     KSQCESLNLT GELYWTKVKV NFDNVGAGYL ALLQVATFKG WMDIMYAAVD SRGYEEQPQW
     EYNLYMYIYF VIFIIFGSFF TLNLFIGVII DNFNQQKKKL GGQDIFMTEE QKKYYNAMKK
     LGSKKPQKPI PRPLNKYQGF IFDIVTKQAF DVTIMFLICL NMVTMMVETD DQSPEKINIL
     AKINLLFVAI FTGECIVKLA ALRHYYFTNS WNIFDFVVVI LSIVGTVLSD IIQKYFFSPT
     LFRVIRLARI GRILRLIRGA KGIRTLLFAL MMSLPALFNI GLLLFLVMFI YSIFGMANFA
     YVKWEAGIDD MFNFQTFANS MLCLFQITTS AGWDGLLSPI LNTGPPYCDP TLPNSNGSRG
     DCGSPAVGIL FFTTYIIISF LIVVNMYIAI ILENFSVATE ESTEPLSEDD FDMFYEIWEK
     FDPEATQFIE YSVLSDFADA LSEPLRIAKP NQISLINMDL PMVSGDRIHC MDILFAFTKR
     VLGESGEMDA LKIQMEEKFM AANPSKISYE PITTTLRRKH EEVSAMVIQR AFRRHLLQRS
     LKHASFLFRQ QAGSGLSEED APEREGLIAY VMSENFSRPL GPPSSSSISS TSFPPSYDSV
     TRATSDNLQV RGSDYSHSED LADFPPSPDR DRESIV
//
ID   Q9DIX3      PRELIMINARY;      PRT;    56 AA.
AC   Q9DIX3;
DT   01-MAR-2001 (TrEMBLrel. 16, Created)
DT   01-MAR-2001 (TrEMBLrel. 16, Last sequence update)
DT   01-MAR-2004 (TrEMBLrel. 26, Last annotation update)
DE   VP1 capsid protein and P2A protease (Fragment){EI4}.
GN   Name=VP1 and P2A{EI4};
OS   Hepatitis A virus.
OC   Viruses; ssRNA positive-strand viruses, no DNA stage; Picornaviridae;
OC   Hepatovirus.
OX   NCBI_TaxID=12092{EI4};
RN   [1]{EI4}
RP   SEQUENCE FROM N.A.
RX   MEDLINE=21259955; PubMed=11360240;
RA   Diaz B.I., Sariol C.A., Normann A., Rodriguez L.A., Flehmig B.;
RT   "Genetic relatedness of Cuban HAV wild-type isolates.";
RL   J. Med. Virol. 64:96-103(2001).
DR   EMBL; AJ245534; CAC17887.1; -.{EI4}
DR   PIR; PQ0427; PQ0427.
DR   PIR; PQ0428; PQ0428.
DR   GO; GO:0008233; F:peptidase activity; IEA.
DR   InterPro; IPR000886; ER_target_S.
DR   PROSITE; PS00014; ER_TARGET; UNKNOWN_1.
KW   Protease{EP2}.
FT   NON_TER       1      1       {EI4}
FT   NON_TER      56     56       {EI4}
**
**   #################    INTERNAL SECTION    ##################
**EV EI4; EMBL; -; CAC17887.1; 11-MAY-2004.
**EV EP2; TrEMBL; -; CAC17887.1; 11-MAY-2004.
**PM PROSITE; PS00014; ER_TARGET; 53; 56; ?; 27-APR-2004;
SQ   SEQUENCE   56 AA;  6628 MW;  465CF4B35C1EF4BC CRC64;
     ESMMSRIAAG DLESSVDDPR SDEDRRFESH IECRKPYKEL RLEVGKQRLK YAQEEL
//


ID   Q27383      PRELIMINARY;      PRT;   378 AA.
AC   Q27383;
DT   01-JUN-1998 (TrEMBLrel. 06, Created)
DT   01-JUN-1998 (TrEMBLrel. 06, Last sequence update)
DT   01-MAR-2004 (TrEMBLrel. 26, Last annotation update)
DE   Hypothetical 40.7 kDa protein R09F10.2 in chromosome X precursor.
GN   Name=R09F10.2;
GN   and
GN   Name=abu-9{EI1}; ORFNames=R09F10.2{EI1}, R09F10.7{EI2};
OS   Caenorhabditis elegans.
OC   Eukaryota; Metazoa; Nematoda; Chromadorea; Rhabditida; Rhabditoidea;
OC   Rhabditidae; Peloderinae; Caenorhabditis.
OX   NCBI_TaxID=6239;
RN   [1]
RP   SEQUENCE FROM N.A.
RC   STRAIN=BRISTOL N2;
RA   Couch J.;
RL   Submitted (JUL-1996) to the EMBL/GenBank/DDBJ databases.
RN   [2]
RP   REVISIONS TO C-TERMINUS.
**   MEDLINE=None; PubMed=None;
RA   Ambler R.P.;
**   /NO TITLE.
RL   Unpublished results, cited by:
RL   McGinnis J., Sinclair-day J.D., Sykes A.G., Powls R., Moore J.,
RL   Wright P.D.;
RL   Inorg. Chem. 27:2306-2312(1988).
RN   [3]
RP   SEQUENCE.
RC   TISSUE=Plasma;
RA   Moorad D.R., Luo C., Garcia G.E., Doctor B.P.;
RT   "Amino acid sequence of horse serum butyrycholinesterase.";
RL   (In) Doctor B.P., Taylor P., Quinn D.M., Rotundo R.L., Gentry M.K.
RL   (eds.);
RL   Structure and function of cholinesterases and related proteins,
RL   pp.145-146, Plenum Press, New York and London (1998).
CC   -!- WEB RESOURCE: Name=Androgen receptor gene mutations database;
CC       URL="http://www.mcgill.ca/androgendb/";
CC   -!- SIMILARITY: BELONGS TO FAMILY UPF.
DR   EMBL; U64859; AAC69090.1; -.
DR   PIR; B89588; B89588.
DR   WormBase; R09F10.2; CE07436.
DR   WormBase; R09F10.7; CE07436.
DR   InterPro; IPR009475; DUF1096.
DR   InterPro; IPR003341; DUF139.
DR   Pfam; PF02363; C_tripleX; 9.
DR   Pfam; PF06493; DUF1096; 1.
KW   Hypothetical protein; Signal.
FT   SIGNAL        1     18       POTENTIAL.
FT   CHAIN        19    378       HYPOTHETICAL PROTEIN R09F10.2.
**
**   #################     SOURCE SECTION     ##################
**   Caenorhabditis elegans cosmid R09F10.
**   [1]
**   1-31934
**   MEDLINE; 94150718.
**   Wilson R., Ainscough R., Anderson K., Baynes C., Berks M., Bonfield
**   J.,
**   Burton J., Connell M., Copsey T., Cooper J., Coulson A., Craxton M.,
**   Dear S., Du Z., Durbin R., Favello A., Fulton L., Gardner A., Green
**   P.,
**   Hawkins T., Hillier L., Jier M., Johnston L., Jones M., Kershaw J.,
**   Kirsten J., Laister N., Latreille P., Lightning J., Lloyd C.,
**   McMurray A., Mortimore B., O'Callaghan M., Parsons J., Percy C.,
**   Rifken L., Roopra A., Saunders D., Shownkeen R., Smaldon N., Smith A.,
**   Sonnhammer E., Staden R., Sulston J., Thierry-Mieg J., Thomas K.,
**   Vaudin M., Vaughan K., Waterston R., Watson A., Weinstock L.,
**   Wilkinson-Sproat J., Wohldman P.;
**   "2.2 Mb of contiguous nucleotide sequence from chromosome III of C.
**   elegans";
**   Nature 368:32-38(1994).
**   [2]
**   1-31934
**   Couch J.;
**   "The sequence of C. elegans cosmid R09F10";
**   Unpublished.
**   [3]
**   1-31934
**   Waterston R.;
**   ;
**   Submitted (22-JUL-1996) to the EMBL/GenBank/DDBJ databases.
**   Genome Sequencing Center Department of Genetics, Washington
**   University,
**   St. Louis, MO 63110, USA, and Sanger Centre, Hinxton Hall Cambridge
**   CB10
**   IRQ, England e-mail: rw@nematode.wustl.edu and jes@sanger.ac.uk
**   Submitted by: Genome Sequencing Center Department of Genetics,
**   Washington University, St. Louis, MO 63110, USA, and Sanger Centre,
**   Hinxton Hall Cambridge CB10 IRQ, England e-mail:
**   rw@nematode.wustl.edu and jes@sanger.ac.uk NEIGHBORING COSMID
**   INFORMATION: The 5' cosmid is F13B9, 600 bp overlap;3' cosmid is
**   F55E10, 200 bp overlap. Actual start of this cosmid is at base
**   position 595 of CELR09F10; actual end is at 7938 of CELF55E10
**   NOTES: Coding sequences below are predicted from computer analysis,
**   using the program Genefinder(P. Green and L. Hillier, ms in
**   preparation).
**   source          1..31934
**                   /organism="Caenorhabditis elegans"
**                   /chromosome="X"
**                   /strain="Bristol N2"
**                   /clone="R09F10"
**   CDS             join(complement(26227..26277),complement(26009.
**   .26179),
**                   complement(25045..25959))
**                   /codon_start=1
**                   /db_xref="PID:g1465855"
**                   /evidence=NOT_EXPERIMENTAL
**                   /note="glutamine-rich protein"
**                   /gene="R09f10.7"
**   CDS_IN_EMBL_ENTRY 8
**   01-OCT-1996 (Rel. 49, Last updated, Version 5)
**   Caenorhabditis elegans cosmid R09F10.
**   [1]
**   1-31934
**   MEDLINE; 94150718.
**   Wilson R., Ainscough R., Anderson K., Baynes C., Berks M., Bonfield
**   J.,
**   Burton J., Connell M., Copsey T., Cooper J., Coulson A., Craxton M.,
**   Dear S., Du Z., Durbin R., Favello A., Fulton L., Gardner A., Green
**   P.,
**   Hawkins T., Hillier L., Jier M., Johnston L., Jones M., Kershaw J.,
**   Kirsten J., Laister N., Latreille P., Lightning J., Lloyd C.,
**   McMurray A., Mortimore B., O'Callaghan M., Parsons J., Percy C.,
**   Rifken L., Roopra A., Saunders D., Shownkeen R., Smaldon N., Smith A.,
**   Sonnhammer E., Staden R., Sulston J., Thierry-Mieg J., Thomas K.,
**   Vaudin M., Vaughan K., Waterston R., Watson A., Weinstock L.,
**   Wilkinson-Sproat J., Wohldman P.;
**   "2.2 Mb of contiguous nucleotide sequence from chromosome III of C.
**   elegans";
**   Nature 368:32-38(1994).
**   [2]
**   1-31934
**   Couch J.;
**   "The sequence of C. elegans cosmid R09F10";
**   Unpublished.
**   [3]
**   1-31934
**   Waterston R.;
**   ;
**   Submitted (22-JUL-1996) to the EMBL/GenBank/DDBJ databases.
**   Genome Sequencing Center Department of Genetics, Washington
**   University,
**   St. Louis, MO 63110, USA, and Sanger Centre, Hinxton Hall Cambridge
**   CB10
**   IRQ, England e-mail: rw@nematode.wustl.edu and jes@sanger.ac.uk
**   Submitted by: Genome Sequencing Center Department of Genetics,
**   Washington University, St. Louis, MO 63110, USA, and Sanger Centre,
**   Hinxton Hall Cambridge CB10 IRQ, England e-mail:
**   rw@nematode.wustl.edu and jes@sanger.ac.uk NEIGHBORING COSMID
**   INFORMATION: The 5' cosmid is F13B9, 600 bp overlap;3' cosmid is
**   F55E10, 200 bp overlap. Actual start of this cosmid is at base
**   position 595 of CELR09F10; actual end is at 7938 of CELF55E10
**   NOTES: Coding sequences below are predicted from computer analysis,
**   using the program Genefinder(P. Green and L. Hillier, ms in
**   preparation).
**   source          1..31934
**                   /organism="Caenorhabditis elegans"
**                   /chromosome="X"
**                   /strain="Bristol N2"
**                   /clone="R09F10"
**   CDS             join(27488..27538,27586..27756,27806..28720)
**                   /codon_start=1
**                   /db_xref="PID:g1465856"
**                   /evidence=NOT_EXPERIMENTAL
**                   /note="glutamine-rich protein"
**                   /gene="R09f10.2"
**   CDS_IN_EMBL_ENTRY 8
**   01-OCT-1996 (Rel. 49, Last updated, Version 5)
**   FAMILY UPF CONSISTS OF Q17400, Q17401, Q19919, Q19594 AND Q27383.
**   FOR THIS ENTRY AMOS, THERE ARE TWO CDS IN THE SAME COSMID WHICH
**   ENCODE IDENTICAL PROTEINS AND SO I HAVE MERGED. I HAVE USED THE
**   FIRST CDS FOR NOMENCLATURE.
**   #################    INTERNAL SECTION    ##################
**EV EI1; WormBase_ADD; -; R09F10.2; 23-MAY-2004.
**EV EI2; WormBase_ADD; -; R09F10.7; 23-MAY-2004.
**ID XXXX_CAEEL
**PM Pfam; PF02363; C_tripleX; 131; 147; T; 06-MAY-2004;
**PM Pfam; PF02363; C_tripleX; 153; 169; T; 06-MAY-2004;
**PM Pfam; PF02363; C_tripleX; 191; 207; T; 06-MAY-2004;
**PM Pfam; PF02363; C_tripleX; 213; 229; T; 06-MAY-2004;
**PM Pfam; PF02363; C_tripleX; 259; 275; T; 06-MAY-2004;
**PM Pfam; PF02363; C_tripleX; 305; 321; T; 06-MAY-2004;
**PM Pfam; PF02363; C_tripleX; 327; 343; T; 06-MAY-2004;
**PM Pfam; PF02363; C_tripleX; 78; 94; T; 06-MAY-2004;
**PM Pfam; PF02363; C_tripleX; 99; 115; T; 06-MAY-2004;
**PM Pfam; PF06493; DUF1096; 3; 65; T; 06-MAY-2004;
**ZZ CREATED AND FINISHED BY CLAIRE.
SQ   SEQUENCE   378 AA;  40683 MW;  E58B416BFE3A7610 CRC64;
     MRFITLAVFF ACALVASSSV LREKRHCGCA QPQQSQCSCQ QVQQTQSCSC QSAPVQQQAP
     SCSCAQPQQT QTVQVQSTQC APACQQSCRQ QCQSAPAVSQ CQPMCQQQCQ SQCTPMYNPP
     ATTTTTPAPV VQCQPMCQQQ CQSTCVQQQQ PVSQCQPQCQ QQCNVACDAT TTTTSAPQVI
     HIQLEIQQAQ VQCQPACQQQ CQSSCVQQQQ PAKQCASSCN TQCTNACQQT AQATQQVIYG
     QNSNTQMYDP YNNQQQQQAN CAPACQPACD NSCIQQTAAP IYNPTTTSAP QVVQIVLQAS
     VAQSSQCAPQ CEQSCQQQCV QQQQPVAQCQ SACQSSCSSS CQAAQPATVA CQQAPQSNQC
     SCQSNYSPCG QGQCCRRK
//
ID   TAU_HUMAN      STANDARD;      PRT;   757 AA.
AC   P10636; P18518; Q14799; Q15549; Q15550; Q15551; Q9UDJ3; Q9UMH0;
AC   Q9UQ96;
DT   01-JUL-1989 (Rel. 11, Created)
DT   16-OCT-2001 (Rel. 40, Last sequence update)
DT   01-OCT-2004 (Rel. 45, Last annotation update)
DE   Microtubule-associated protein tau (Neurofibrillary tangle protein)
DE   (Paired helical filament-tau) (PHF-tau).
GN   Name=MAPT; Synonyms=MTBT1, TAU;
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Primates; Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606;
RN   [1]
RP   SEQUENCE FROM N.A. (ISOFORMS PNS-TAU; TAU-A AND TAU-F).
RA   Andreadis A.;
RL   Submitted (FEB-1998) to the EMBL/GenBank/DDBJ databases.
RN   [2]
RP   SEQUENCE FROM N.A. (ISOFORM TAU-A).
RC   TISSUE=Brain;
RX   MEDLINE=88234557; PubMed=3131773;
RA   Goedert M., Wischik C., Crowther R., Walker J., Klug A.;
RT   "Cloning and sequencing of the cDNA encoding a core protein of the
RT   paired helical filament of Alzheimer disease: identification as the
RT   microtubule-associated protein tau.";
RL   Proc. Natl. Acad. Sci. U.S.A. 85:4051-4055(1988).
RN   [3]
RP   SEQUENCE FROM N.A. (ISOFORMS TAU-B; TAU-C; TAU-E AND TAU-F).
RC   TISSUE=Brain;
RX   MEDLINE=90380393; PubMed=2484340;
RA   Goedert M., Spillantini M.G., Jakes R., Rutherford D., Crowther R.A.;
RT   "Multiple isoforms of human microtubule-associated protein tau:
RT   sequences and localization in neurofibrillary tangles of Alzheimer's
RT   disease.";
RL   Neuron 3:519-526(1989).
RN   [4]
RP   SEQUENCE FROM N.A. (ISOFORM TAU-D).
RC   TISSUE=Brain;
RX   MEDLINE=89251564; PubMed=2498079;
RA   Goedert M., Spillantini M.G., Potier M.C., Ulrich J., Crowther R.A.;
RT   "Cloning and sequencing of the cDNA encoding an isoform of
RT   microtubule-associated protein tau containing four tandem repeats:
RT   differential expression of tau protein mRNAs in human brain.";
RL   EMBO J. 8:393-399(1989).
RN   [5]
RP   SEQUENCE FROM N.A. (ISOFORMS TAU-A AND FETAL-TAU).
RC   TISSUE=Fetal brain;
RX   MEDLINE=90180482; PubMed=2516729;
RA   Lee G., Neve R.L., Kosik K.S.;
RT   "The microtubule binding domain of tau protein.";
RL   Neuron 2:1615-1624(1989).
RN   [6]
RP   SEQUENCE FROM N.A. (ISOFORM TAU-F), AND ALTERNATIVE SPLICING.
RX   MEDLINE=93041757; PubMed=1420178;
RA   Andreadis A., Brown W.M., Kosik K.S.;
RT   "Structure and novel exons of the human tau gene.";
RL   Biochemistry 31:10626-10633(1992).
RN   [7]
RP   SEQUENCE FROM N.A. (ISOFORM TAU-A).
RC   TISSUE=Brain;
RX   MEDLINE=22388257; PubMed=12477932; DOI=10.1073/pnas.242603899;
RA   Strausberg R.L., Feingold E.A., Grouse L.H., Derge J.G.,
RA   Klausner R.D., Collins F.S., Wagner L., Shenmen C.M., Schuler G.D.,
RA   Altschul S.F., Zeeberg B., Buetow K.H., Schaefer C.F., Bhat N.K.,
RA   Hopkins R.F., Jordan H., Moore T., Max S.I., Wang J., Hsieh F.,
RA   Diatchenko L., Marusina K., Farmer A.A., Rubin G.M., Hong L.,
RA   Stapleton M., Soares M.B., Bonaldo M.F., Casavant T.L., Scheetz T.E.,
RA   Brownstein M.J., Usdin T.B., Toshiyuki S., Carninci P., Prange C.,
RA   Raha S.S., Loquellano N.A., Peters G.J., Abramson R.D., Mullahy S.J.,
RA   Bosak S.A., McEwan P.J., McKernan K.J., Malek J.A., Gunaratne P.H.,
RA   Richards S., Worley K.C., Hale S., Garcia A.M., Gay L.J., Hulyk S.W.,
RA   Villalon D.K., Muzny D.M., Sodergren E.J., Lu X., Gibbs R.A.,
RA   Fahey J., Helton E., Ketteman M., Madan A., Rodrigues S., Sanchez A.,
RA   Whiting M., Madan A., Young A.C., Shevchenko Y., Bouffard G.G.,
RA   Blakesley R.W., Touchman J.W., Green E.D., Dickson M.C.,
RA   Rodriguez A.C., Grimwood J., Schmutz J., Myers R.M.,
RA   Butterfield Y.S.N., Krzywinski M.I., Skalska U., Smailus D.E.,
RA   Schnerch A., Schein J.E., Jones S.J.M., Marra M.A.;
RT   "Generation and initial analysis of more than 15,000 full-length human
RT   and mouse cDNA sequences.";
RL   Proc. Natl. Acad. Sci. U.S.A. 99:16899-16903(2002).
RN   [8]
RP   SEQUENCE OF 591-621 FROM N.A.
RC   TISSUE=Brain;
RX   MEDLINE=89193714; PubMed=2495000;
RA   Mori H., Hamada Y., Kawaguchi M., Honda T., Kondo J., Ihara Y.;
RT   "A distinct form of tau is selectively incorporated into Alzheimer's
RT   paired helical filaments.";
RL   Biochem. Biophys. Res. Commun. 159:1221-1226(1989).
RN   [9]
RP   SEQUENCE OF 1-72; 102-380; 467-496; 507-570; 576-582; 591-606;
RP   615-633; 638-656; 660-663; 670-699 AND 702-757.
RC   TISSUE=Brain;
RX   MEDLINE=92381012; PubMed=1512244;
RA   Hasegawa M., Morishima-Kawashima M., Takio K., Suzuki M., Titani K.,
RA   Ihara Y.;
RT   "Protein sequence and mass spectrometric analyses of tau in the
RT   Alzheimer's disease brain.";
RL   J. Biol. Chem. 267:17047-17054(1992).
RN   [10]
RP   SEQUENCE OF 576-583; 607-610; 615-627; 638-647 AND 670-685,
RP   PHOSPHORYLATION, AND MUTAGENESIS.
RX   MEDLINE=95221434; PubMed=7706316;
RA   Drewes G., Trinczek B., Illenberger S., Biernat J., Schmitt-Ulms G.,
RA   Meyer H.E., Mandelkow E.-M., Mandelkow E.;
RT   "Microtubule-associated protein/microtubule affinity-regulating kinase
RT   (p110mark). A novel protein kinase that regulates tau-microtubule
RT   interactions and dynamic instability by phosphorylation at the
RT   Alzheimer-specific site serine 262.";
RL   J. Biol. Chem. 270:7679-7688(1995).
RN   [11]
RP   REVIEW.
RX   MEDLINE=91320377; PubMed=1713721; DOI=10.1016/0166-2236(91)90105-4;
RA   Goedert M., Crowther R.A., Garner C.C.;
RT   "Molecular characterization of microtubule-associated proteins tau and
RT   MAP2.";
RL   Trends Neurosci. 14:193-199(1991).
RN   [12]
RP   SUBCELLULAR LOCATION, AND PHOSPHORYLATION.
RX   MEDLINE=20283597; PubMed=10747907; DOI=10.1074/jbc.M000389200;
RA   Maas T., Eidenmueller J., Brandt R.;
RT   "Interaction of tau with the neural membrane cortex is regulated by
RT   phosphorylation at sites that are modified in paired helical
RT   filaments.";
RL   J. Biol. Chem. 275:15733-15740(2000).
RN   [13]
RP   PHOSPHORYLATION, AND MUTAGENESIS.
RX   MEDLINE=98413833; PubMed=9735171;
RA   Sengupta A., Kabat J., Novak M., Wu Q., Grundke-Iqbal I., Iqbal K.;
RT   "Phosphorylation of tau at both Thr 231 and Ser 262 is required for
RT   maximal inhibition of its binding to microtubules.";
RL   Arch. Biochem. Biophys. 357:299-309(1998).
RN   [14]
RP   PHOSPHORYLATION, AND MUTAGENESIS.
RX   MEDLINE=98278959; PubMed=9614189;
RA   Illenberger S., Zheng-Fischhofer Q., Preuss U., Stamer K., Baumann K.,
RA   Trinczek B., Biernat J., Godemann R., Mandelkow E.-M., Mandelkow E.;
RT   "The endogenous and cell cycle-dependent phosphorylation of tau
RT   protein in living cells: implications for Alzheimer's disease.";
RL   Mol. Biol. Cell 9:1495-1512(1998).
RN   [15]
RP   GLYCATION.
RX   MEDLINE=97465580; PubMed=9326300;
RA   Nacharaju P., Ko L., Yen S.H.;
RT   "Characterization of in vitro glycation sites of tau.";
RL   J. Neurochem. 69:1709-1719(1997).
RN   [16]
RP   REVIEW ON VARIANTS.
RX   MEDLINE=20437008; PubMed=10899436; DOI=10.1016/S0925-4439(00)00037-5;
RA   Goedert M., Spillantini M.G.;
RT   "Tau mutations in frontotemporal dementia FTDP-17 and their relevance
RT   for Alzheimer's disease.";
RL   Biochim. Biophys. Acta 1502:110-121(2000).
RN   [17]
RP   VARIANT FTDP17 MET-653, AND VARIANTS ASN-284; ALA-288; TYR-440 AND
RP   PRO-446.
RX   MEDLINE=98291804; PubMed=9629852;
RA   Poorkaj P., Bird T.D., Wijsman E., Nemens E., Garruto R.M.,
RA   Anderson L., Andreadis A., Wiederholt W.C., Raskind M.,
RA   Schellenberg G.D.;
RT   "Tau is a candidate gene for chromosome 17 frontotemporal dementia.";
RL   Ann. Neurol. 43:815-825(1998).
RN   [18]
RP   ERRATUM.
**   MEDLINE=None; PubMed=None;
RA   Poorkaj P., Bird T.D., Wijsman E., Nemens E., Garruto R.M.,
RA   Anderson L., Andreadis A., Wiederholt W.C., Raskind M.,
RA   Schellenberg G.D.;
**   /NO TITLE.
RL   Ann. Neurol. 44:428-428(1998).
RN   [19]
RP   VARIANT FTDP17 LEU-617.
RX   MEDLINE=98409513; PubMed=9736786;
RA   Dumanchin C., Camuzat A., Campion D., Verpillat P., Hannequin D.,
RA   Dubois B., Saugier-Veber P., Martin C., Penet C., Charbonnier F.,
RA   Agid Y., Frebourg T., Brice A.;
RT   "Segregation of a missense mutation in the microtubule-associated
RT   protein tau gene with familial frontotemporal dementia and
RT   parkinsonism.";
RL   Hum. Mol. Genet. 7:1825-1829(1998).
RN   [20]
RP   VARIANTS FTDP17 VAL-588; LEU-617 AND TRP-722.
RX   MEDLINE=98303385; PubMed=9641683; DOI=10.1038/31508;
RA   Hutton M., Lendon C.L., Rizzu P., Baker M., Froelich S., Houlden H.,
RA   Pickering-Brown S., Chakraverty S., Isaacs A., Grover A., Hackett J.,
RA   Adamson J., Lincoln S., Dickson D., Davies P., Petersen R.C.,
RA   Stevens M., de Graaff E., Wauters E., van Baren J., Hillebrand M.,
RA   Joosse M., Kwon J.M., Nowotny P., Che L.K., Norton J., Morris J.C.,
RA   Reed L.A., Trojanowski J., Basun H., Lannfelt L., Neystat M., Fahn S.,
RA   Dark F., Tannenberg T., Dodd P.R., Hayward N., Kwok J.B.J.,
RA   Schofield P.R., Andreadis A., Snowden J., Craufurd D., Neary D.,
RA   Owen F., Oostra B.A., Hardy J., Goate A., van Swieten J., Mann D.,
RA   Lynch T., Heutink P.;
RT   "Association of missense and 5'-splice-site mutations in tau with the
RT   inherited dementia FTDP-17.";
RL   Nature 393:702-705(1998).
RN   [21]
RP   VARIANT PPND LYS-595, AND VARIANT FTDP17 LEU-617.
RX   MEDLINE=99007274; PubMed=9789048;
RA   Clark L.N., Poorkaj P., Wszolek Z., Geschwind D.H., Nasreddine Z.S.,
RA   Miller B., Li D., Payami H., Awert F., Markopoulou K., Andreadis A.,
RA   D'Souza I., Lee V.M.-Y., Reed L., Trojanowski J.Q., Zhukareva V.,
RA   Bird T., Schellenberg G., Wilhelmsen K.C.;
RT   "Pathogenic implications of mutations in the tau gene in pallido-
RT   ponto-nigral degeneration and related neurodegenerative disorders
RT   linked to chromosome 17.";
RL   Proc. Natl. Acad. Sci. U.S.A. 95:13103-13107(1998).
RN   [22]
RP   VARIANTS FTDP17 VAL-588; LYS-596 DEL; LEU-617 AND TRP-722.
RX   MEDLINE=99138654; PubMed=9973279;
RA   Rizzu P., Van Swieten J.C., Joosse M., Hasegawa M., Stevens M.,
RA   Tibben A., Niermeijer M.F., Hillebrand M., Ravid R., Oostra B.A.,
RA   Goedert M., van Duijn C.M., Heutink P.;
RT   "High prevalence of mutations in the microtubule-associated protein
RT   tau in a population study of frontotemporal dementia in the
RT   Netherlands.";
RL   Am. J. Hum. Genet. 64:414-421(1999).
RN   [23]
RP   VARIANTS FTDP17 LEU-617; MET-653 AND TRP-722.
RX   MEDLINE=99229757; PubMed=10214944;
RA   Nacharaju P., Lewis J., Easson C., Yen S., Hackett J., Hutton M.,
RA   Yen S.H.;
RT   "Accelerated filament formation from tau protein with specific FTDP-17
RT   missense mutations.";
RL   FEBS Lett. 447:195-199(1999).
RN   [24]
RP   VARIANT FTDP17/CBD SER-617.
RX   MEDLINE=99301293; PubMed=10374757;
RA   Bugiani O., Murrell J.R., Giaccone G., Hasegawa M., Ghigo G.,
RA   Tabaton M., Morbin M., Primavera A., Carella F., Solaro C.,
RA   Grisoli M., Savoiardo M., Spillantini M.G., Tagliavini F., Goedert M.,
RA   Ghetti B.;
RT   "Frontotemporal dementia and corticobasal degeneration in a family
RT   with a P301S mutation in tau.";
RL   J. Neuropathol. Exp. Neurol. 58:667-677(1999).
RN   [25]
RP   VARIANT DEMENTIA ARG-705.
RX   MEDLINE=20068246; PubMed=10604746;
RA   Murrell J.R., Spillantini M.G., Zolo P., Guazzelli M., Smith M.J.,
RA   Hasegawa M., Redi F., Crowther R.A., Pietrini P., Ghetti B.,
RA   Goedert M.;
RT   "Tau gene mutation G389R causes a tauopathy with abundant pick body-
RT   like inclusions and axonal deposits.";
RL   J. Neuropathol. Exp. Neurol. 58:1207-1226(1999).
RN   [26]
RP   VARIANTS PSP ASN-284 AND ALA-288.
RX   MEDLINE=20001812; PubMed=10534245;
RA   Higgins J.J., Adler R.L., Loveless J.M.;
RT   "Mutational analysis of the tau gene in progressive supranuclear
RT   palsy.";
RL   Neurology 53:1421-1424(1999).
RN   [27]
RP   VARIANT FTDP17 ASN-621.
RX   MEDLINE=99223277; PubMed=10208578;
RA   Iijima M., Tabira T., Poorkaj P., Schellenberg G.D., Trojanowski J.Q.,
RA   Lee V.M.-Y., Schmidt M.L., Takahashi K., Nabika T., Matsumoto T.,
RA   Yamashita Y., Yoshioka S., Ishino H.;
RT   "A distinct familial presenile dementia with a novel missense mutation
RT   in the tau gene.";
RL   NeuroReport 10:497-501(1999).
RN   [28]
RP   VARIANT DEMENTIA THR-573.
RX   MEDLINE=20539309; PubMed=11089577;
RA   Rizzini C., Goedert M., Hodges J.R., Smith M.J., Jakes R., Hills R.,
RA   Xuereb J.H., Crowther R.A., Spillantini M.G.;
RT   "Tau gene mutation K257T causes a tauopathy similar to Pick's
RT   disease.";
RL   J. Neuropathol. Exp. Neurol. 59:990-1001(2000).
CC   -!- FUNCTION: Promotes microtubule assembly and stability, and might
CC       be involved in the establishment and maintenance of neuronal
CC       polarity. The C-terminus binds axonal microtubules while the N-
CC       terminus binds neural plasma membrane components, suggesting that
CC       tau functions as a linker protein between both. Axonal polarity is
CC       predetermined by tau localization (in the neuronal cell) in the
CC       domain of the cell body defined by the centrosome. The short
CC       isoforms allow plasticity of the cytoskeleton whereas the longer
CC       isoforms may preferentially play a role in its stabilization.
CC   -!- SUBCELLULAR LOCATION: Mostly found in the axons of neurons, in the
CC       cytosol and in association with plasma membrane components.
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing; Named isoforms=8;
CC         Comment=Additional isoforms seem to exist. Isoforms differ from
CC         each other by the presence or absence of up to 5 of the 15
CC         exons. One of these optional exons contains the additional
CC         tau/MAP repeat;
CC       Name=PNS-tau;
CC         IsoId=P10636-1; Sequence=Displayed;
CC       Name=Fetal-tau;
CC         IsoId=P10636-2; Sequence=VSP_003175, VSP_003176, VSP_003177,
CC                                  VSP_003178, VSP_003179, VSP_003180,
CC                                  VSP_003181;
CC       Name=Tau-A;
CC         IsoId=P10636-3; Sequence=VSP_003176, VSP_003177, VSP_003179,
CC                                  VSP_003180, VSP_003181;
CC       Name=Tau-B;
CC         IsoId=P10636-4; Sequence=VSP_003177, VSP_003179, VSP_003180,
CC                                  VSP_003181;
CC       Name=Tau-C; Synonyms=Tau-3;
CC         IsoId=P10636-5; Sequence=VSP_003179, VSP_003180, VSP_003181;
CC       Name=Tau-D;
CC         IsoId=P10636-6; Sequence=VSP_003176, VSP_003177, VSP_003179,
CC                                  VSP_003180;
CC       Name=Tau-E;
CC         IsoId=P10636-7; Sequence=VSP_003177, VSP_003179, VSP_003180;
CC       Name=Tau-F; Synonyms=Tau-4;
CC         IsoId=P10636-8; Sequence=VSP_003179, VSP_003180;
CC   -!- TISSUE SPECIFICITY: Expressed in neurons. PNS-tau is expressed in
CC       the peripheral nervous system while the others are expressed in
CC       the central nervous system.
CC   -!- DEVELOPMENTAL STAGE: Four-repeat (type II) tau is expressed in an
CC       adult-specific manner and is not found in fetal brain, whereas
CC       three-repeat (type I) tau is found in both adult and fetal brain.
CC   -!- DOMAIN: The tau/MAP repeat binds to tubulin. Type I isoforms
CC       contain 3 repeats while type II isoforms contain 4 repeats.
CC   -!- PTM: Phosphorylation at serine and threonine residues in S-P or T-
CC       P motifs by proline-directed protein kinases (PDPK: CDC2, CDK5,
CC       GSK-3, MAPK) (only 2-3 sites per protein in interphase, seven-fold
CC       increase in mitosis, and in PHF-tau), and at serine residues in K-
CC       X-G-S motifs by MAP/microtubule affinity-regulating kinase (MARK)
CC       in Alzheimer's diseased brains. Phosphorylation decreases with
CC       age. Phosphorylation within tau's repeat domain or in flanking
CC       regions seems to reduce tau's interaction with, respectively,
CC       microtubules or plasma membrane components.
CC   -!- PTM: Glycation of PHF-tau, but not normal brain tau. Glycation is
CC       a non-enzymatic posttranslational modification that involves a
CC       covalent linkage between a sugar and an amino group of a protein
CC       molecule forming ketoamine. Subsequent oxidation, fragmentation
CC       and/or crosslinking of ketoamine leads to the production of
CC       advanced glycation endproducts (AGES). Glycation may play a role
CC       in stabilizing PHF aggregation leading to tangle formation in AD.
CC   -!- PTM: Phosphorylation on Ser-609, Ser-621, Ser-640 and Ser-672 in
CC       several isoforms during mitosis.
CC   -!- DISEASE: In Alzheimer disease, the neuronal cytoskeleton in the
CC       brain is progressively disrupted and replaced by tangles of paired
CC       helical filaments (PHF) and straight filaments, mainly composed of
CC       hyperphosphorylated forms of TAU (PHF-TAU or AD P-TAU).
CC   -!- DISEASE: Defects in MAPT are a cause of frontotemporal dementia
CC       and parkinsonism linked to chromosome 17 (FTDP17) [MIM:600274];
CC       also historically termed Pick's disease. This form of
CC       frontotemporal dementia is characterized by presenile dementia
CC       with behavioral changes, deterioration of cognitive capacities and
CC       loss of memory. In some cases, parkinsonian symptoms are
CC       prominent. Neuropathological changes include frontotemporal
CC       atrophy often associated with atrophy of the basal ganglia,
CC       substantia nigra, amygdala. In most cases, protein tau deposits
CC       are found in glial cells and/or neurons.
CC   -!- DISEASE: Defects in MAPT are a cause of pallido-ponto-nigral
CC       degeneration (PPND) [MIM:168610]. The clinical features include
CC       ocular motility abnormalities, dystonia and urinary incontinence,
CC       beside progressive parkinsonism and dementia.
CC   -!- DISEASE: Defects in MAPT are a cause of corticobasal degeneration
CC       (CBD). It is marked by extrapyramidal signs and apraxia and can be
CC       associated with memory loss. Neuropathologic features may overlap
CC       Alzheimer's disease, progressive supranuclear palsy, and
CC       Parkinson's disease.
CC   -!- DISEASE: Defects in MAPT may predispose to progressive
CC       supranuclear palsy (PSP) [MIM:601104]; also known as steele-
CC       richardson-olszewski syndrome. It is characterized by akinetic-
CC       rigid syndrome, supranuclear gaze palsy, pyramidal tract
CC       dysfunction, pseudobulbar signs and cognitive capacities
CC       deterioration. Neurofibrillary tangles and gliosis but no amyloid
CC       plaques are found in diseased brains.
CC   -!- DISEASE: Defects in MAPT may be a cause of hereditary dysphasic
CC       disinhibition dementia (HDDD) [MIM:607485], a frontotemporal
CC       dementia characterized by progressive cognitive deficits with
CC       memory loss and personality changes, severe dysphasic disturbances
CC       leading to mutism, and hyperphagia.
CC   -!- SIMILARITY: Contains 4 Tau/MAP repeats.
CC   -!- WEB RESOURCE: Name=HotMolecBase; Note=Tau entry;
CC       URL="http://bioinformatics.weizmann.ac.il/hotmolecbase/entries/tau.htm";
CC   -!- WEB RESOURCE: Name=Alzheimer Research Forum; Note=Tau mutations;
CC       URL="http://www.alzforum.org/res/com/mut/tau/default.asp";
CC   -!- 100% SWISS-PROT IDENTITY: Q8X251.
CC   -!- INTERACTION:
CC       Q9H074:dkfzp586c051; NbExp=3; IntAct=EBI-81531, EBI-81519;
CC       Q04637:EIF4G1; NbExp=1; IntAct=EBI-81531, EBI-73711;
CC   -!- INTERACTION:
CC       Q8NI08:-; NbExp=1; IntAct=EBI-80809, EBI-80799;
CC       Q9Y618:ncor2; NbExp=1; IntAct=EBI-80809, EBI-80830;
CC   -!- INTERACTION:
CC       Self; NbExp=1; IntAct=EBI-123485, EBI-123485;
CC       Q9W158:cg4612; NbExp=1; IntAct=EBI-123485, EBI-89895;
CC       Q9VYI0:fne; NbExp=1; IntAct=EBI-123485, EBI-126770;
CC   -!- INTERACTION:
CC       Q8C1S0:2410018m14rik (xeno); NbExp=1; IntAct=EBI-394562, EBI-398761;
CC       Q15528:surf5; NbExp=1; IntAct=EBI-394562, EBI-394687;
CC       Q9CQI9:thrap6 (xeno); NbExp=1; IntAct=EBI-394562, EBI-309220;
CC   -!- INTERACTION:
CC       P13607:atp-alpha; NbExp=1; IntAct=EBI-126914, EBI-213208;
CC       P11450:fcp3c; NbExp=1; IntAct=EBI-126914, EBI-159556;
DR   EMBL; AF047863; AAC04277.1; -.
DR   EMBL; AF027491; AAC04277.1; JOINED.
DR   EMBL; AF047856; AAC04277.1; JOINED.
DR   EMBL; AF047857; AAC04277.1; JOINED.
DR   EMBL; AF027492; AAC04277.1; JOINED.
DR   EMBL; AF047858; AAC04277.1; JOINED.
DR   EMBL; AF027493; AAC04277.1; JOINED.
DR   EMBL; AF047859; AAC04277.1; JOINED.
DR   EMBL; AF047860; AAC04277.1; JOINED.
DR   EMBL; AF047862; AAC04277.1; JOINED.
DR   EMBL; AF027494; AAC04277.1; JOINED.
DR   EMBL; AF027495; AAC04277.1; JOINED.
DR   EMBL; AF027496; AAC04277.1; JOINED.
DR   EMBL; J03778; AAA60615.1; -.
DR   EMBL; X14474; CAA32636.1; -.
DR   EMBL; AF027491; AAC04279.1; -.
DR   EMBL; AF047856; AAC04279.1; JOINED.
DR   EMBL; AF047857; AAC04279.1; JOINED.
DR   EMBL; AF027492; AAC04279.1; JOINED.
DR   EMBL; AF027493; AAC04279.1; JOINED.
DR   EMBL; AF047860; AAC04279.1; JOINED.
DR   EMBL; AF047862; AAC04279.1; JOINED.
DR   EMBL; AF027494; AAC04279.1; JOINED.
DR   EMBL; AF027495; AAC04279.1; JOINED.
DR   EMBL; AF027496; AAC04279.1; JOINED.
DR   EMBL; AF047863; AAC04279.1; JOINED.
DR   EMBL; AF027491; AAC04278.1; -.
DR   EMBL; AF027492; AAC04278.1; JOINED.
DR   EMBL; AF027493; AAC04278.1; JOINED.
DR   EMBL; AF047860; AAC04278.1; JOINED.
DR   EMBL; AF047862; AAC04278.1; JOINED.
DR   EMBL; AF027495; AAC04278.1; JOINED.
DR   EMBL; AF027496; AAC04278.1; JOINED.
DR   EMBL; AF047863; AAC04278.1; JOINED.
DR   EMBL; BC000558; AAH00558.1; -.
DR   EMBL; M25298; AAA57264.1; -.
DR   PIR; I52232; I52232.
DR   PIR; JS0370; QRHUT1.
DR   PDB; 1I8H; NMR; A=541-553.
DR   HGNC; HGNC:6893; MAPT.
DR   MIM; 157140; -.
DR   MIM; 168610; -.
DR   MIM; 600274; -.
DR   MIM; 172700; -.
DR   MIM; 601104; -.
DR   MIM; 607485; -.
DR   GO; GO:0030424; C:axon; NAS.
DR   GO; GO:0005829; C:cytosol; TAS.
DR   GO; GO:0005875; C:microtubule associated complex; TAS.
DR   GO; GO:0005886; C:plasma membrane; TAS.
DR   GO; GO:0005200; F:structural constituent of cytoskeleton; TAS.
DR   GO; GO:0007026; P:microtubule stabilization; NAS.
DR   InterPro; IPR002955; Tau_protein.
DR   InterPro; IPR001084; Tubulin_Tau.
DR   Pfam; PF00418; Tubulin-binding; 4.
DR   PRINTS; PR01261; TAUPROTEIN.
**   PRINTS; PR01217; PRICHEXTENSN; FALSE_POS_1.
DR   PROSITE; PS00229; TAU_MAP; 4.
KW   3D-structure; Acetylation; Alternative splicing; Alzheimer's disease;
KW   Cytoskeleton; Direct protein sequencing; Disease mutation;
KW   Glycoprotein; Microtubule; Phosphorylation; Polymorphism; Repeat.
FT   INIT_MET      0      0
FT   REPEAT      560    590       Tau/MAP motif 1.
FT   REPEAT      591    621       Tau/MAP motif 2.
FT   REPEAT      622    652       Tau/MAP motif 3.
FT   REPEAT      653    684       Tau/MAP motif 4.
FT   MOD_RES       1      1       N-acetylalanine.
FT   MOD_RES      45     45       Phosphoserine (by PDPK) (partial).
FT   MOD_RES      49     49       Phosphothreonine (by PDPK) (partial).
FT   MOD_RES     469    469       Phosphothreonine (by PDPK) (partial).
FT   MOD_RES     483    483       Deamidated asparagine (in form Tau and
FT                                form PHF-Tau) (partial).
FT   MOD_RES     491    491       Phosphothreonine (by PDPK) (partial).
FT   MOD_RES     497    497       Phosphothreonine (by PDPK) (partial).
FT   MOD_RES     514    514       Phosphoserine (by PDPK) (partial).
FT   MOD_RES     515    515       Phosphoserine (by PDPK) (partial).
FT   MOD_RES     518    518       Phosphoserine (by PDPK) (partial).
FT   MOD_RES     521    521       Phosphothreonine (by PDPK) (partial).
FT   MOD_RES     528    528       Phosphothreonine (by PDPK) (partial).
FT   MOD_RES     530    530       Phosphoserine (by PKA) (partial).
FT   MOD_RES     533    533       Phosphothreonine (by PDPK) (partial).
FT   MOD_RES     547    547       Phosphothreonine (by PDPK) (partial).
FT   MOD_RES     551    551       Phosphoserine (by PDPK) (partial).
FT   MOD_RES     578    578       Phosphoserine (by MARK1).
FT   MOD_RES     595    595       Deamidated asparagine (in form Tau and
FT                                form PHF-Tau) (partial).
FT   MOD_RES     609    609       Phosphoserine (by MARK1) (in PHF-tau).
FT   MOD_RES     621    621       Phosphoserine (by MARK1) (in PHF-tau).
FT   MOD_RES     640    640       Phosphoserine (by MARK1) (in PHF-tau).
FT   MOD_RES     672    672       Phosphoserine (by MARK1) (in PHF-tau).
FT   MOD_RES     712    712       Phosphoserine (by PDPK) (partial).
FT   MOD_RES     720    720       Phosphoserine (by PDPK) (partial).
FT   MOD_RES     725    725       Phosphoserine (Potential).
FT   MOD_RES     729    729       Phosphoserine (Potential).
FT   MOD_RES     732    732       Phosphoserine (by CaMK2).
FT   MOD_RES     738    738       Phosphoserine (by PDPK) (partial).
FT   CARBOHYD     86     86       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    382    382       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    466    466       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    479    479       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    490    490       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    541    541       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    550    550       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    575    575       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    596    596       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    597    597       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    663    663       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    669    669       N-linked (Glc) (glycation); in PHF-tau.
FT   CARBOHYD    685    685       N-linked (Glc) (glycation); in PHF-tau.
FT   DISULFID    607    638       By similarity.
FT   VAR_SEQ       1     43       AEPRQEFEVMEDHAGTYGLGDRKDQGGYTMHQDQEGDTDAG
FT                                LK -> LRALQQRKR (in isoform Fetal-tau).
FT                                /FTId=VSP_003175.
FT   VAR_SEQ      44     72       Missing (in isoform Tau-A, isoform Tau-D
FT                                and isoform Fetal-tau).
FT                                /FTId=VSP_003176.
FT   VAR_SEQ      73    101       Missing (in isoform Tau-A, isoform Tau-B,
FT                                isoform Tau-D, isoform Tau-E and isoform
FT                                Fetal-tau).
FT                                /FTId=VSP_003177.
FT   VAR_SEQ     102    103       Missing (in isoform Fetal-tau).
FT                                /FTId=VSP_003178.
FT   VAR_SEQ     124    374       Missing (in isoform Tau-A, isoform Tau-B,
FT                                isoform Tau-C, isoform Tau-D, isoform
FT                                Tau-E, isoform Tau-F and isoform Fetal-
FT                                tau).
FT                                /FTId=VSP_003179.
FT   VAR_SEQ     394    459       Missing (in isoform Tau-A, isoform Tau-B,
FT                                isoform Tau-C, isoform Tau-D, isoform
FT                                Tau-E, isoform Tau-F and isoform Fetal-
FT                                tau).
FT                                /FTId=VSP_003180.
FT   VAR_SEQ     591    621       Missing (in isoform Tau-A, isoform Tau-B,
FT                                isoform Tau-C and isoform Fetal-tau).
FT                                /FTId=VSP_003181.
FT   VARIANT     284    284       D -> N (risk factor for progressive
FT                                supranuclear palsy).
FT                                /FTId=VAR_010340.
FT   VARIANT     288    288       V -> A (risk factor for progressive
FT                                supranuclear palsy).
FT                                /FTId=VAR_010341.
FT   VARIANT     440    440       H -> Y.
FT                                /FTId=VAR_010342.
FT   VARIANT     446    446       S -> P.
FT                                /FTId=VAR_010343.
FT   VARIANT     573    573       K -> T (in a dementia resembling Pick's
FT                                disease).
FT                                /FTId=VAR_010344.
FT   VARIANT     588    588       G -> V (in FTDP17).
FT                                /FTId=VAR_010345.
FT   VARIANT     595    595       N -> K (in PPND).
FT                                /FTId=VAR_010346.
FT   VARIANT     596    596       Missing (in FTDP17).
FT                                /FTId=VAR_010347.
FT   VARIANT     617    617       P -> L (in FTDP17; most common mutation;
FT                                reduction in the ability to promote
FT                                microtubule assembly; accelerated
FT                                aggregation of Tau into filaments).
FT                                /FTId=VAR_010348.
FT   VARIANT     617    617       P -> S (in FTDP17 and in CBD; reduction
FT                                in the ability to promote microtubule
FT                                assembly).
FT                                /FTId=VAR_010349.
FT   VARIANT     621    621       S -> N (in FTDP17; minimal parkinsonism;
FT                                very early age of onset).
FT                                /FTId=VAR_010350.
FT   VARIANT     653    653       V -> M (in FTDP17; ultrastructural and
FT                                biochemical characteristics
FT                                indistinguishable from Alzheimer's
FT                                disease; accelerated aggregation of Tau
FT                                into filaments).
FT                                /FTId=VAR_010351.
FT   VARIANT     705    705       G -> R (in FTDP17; in a dementia
FT                                resembling Pick's disease).
FT                                /FTId=VAR_010352.
FT   VARIANT     722    722       R -> W (in FTDP17; accelerated
FT                                aggregation of Tau into filaments).
FT                                /FTId=VAR_010353.
FT   MUTAGEN     514    514       S->E: No association with plasma
FT                                membrane.
FT   MUTAGEN     515    515       S->E: No association with plasma
FT                                membrane.
FT   MUTAGEN     518    518       S->E: No association with plasma
FT                                membrane.
FT   MUTAGEN     530    530       S->A: No decrease in microtubule-binding
FT                                and nucleation activity after in vitro
FT                                phosphorylation of mutant protein.
FT   MUTAGEN     547    547       T->A: 50% Decrease in microtubule-binding
FT                                after in vitro phosphorylation of mutant
FT                                protein.
FT   MUTAGEN     547    547       T->E: No association with plasma
FT                                membrane.
FT   MUTAGEN     551    551       S->A: 70% decrease in microtubule-binding
FT                                after in vitro phosphorylation of mutant
FT                                protein.
FT   MUTAGEN     551    551       S->E: No association with plasma
FT                                membrane.
FT   MUTAGEN     573    573       K->T: Reduced ability to promote
FT                                microtubule assembly.
FT   MUTAGEN     578    578       S->A: 8% decrease in microtubule-binding
FT                                after in vitro phosphorylation of mutant
FT                                protein.
FT   MUTAGEN     712    712       S->E: No association with plasma
FT                                membrane.
FT   MUTAGEN     720    720       S->E: No association with plasma
FT                                membrane.
FT   MUTAGEN     725    725       S->E: No association with plasma
FT                                membrane.
FT   MUTAGEN     729    729       S->E: No association with plasma
FT                                membrane.
FT   MUTAGEN     738    738       S->E: No association with plasma
FT                                membrane.
**
**   #################    INTERNAL SECTION    ##################
**CL 17q21.1;
**IS P10636-9
**ZC EMBL; BC000558; AAH00558.1; -.   11-05-2003
SQ   SEQUENCE   757 AA;  78746 MW;  1426CEB011C1CC73 CRC64;
     AEPRQEFEVM EDHAGTYGLG DRKDQGGYTM HQDQEGDTDA GLKESPLQTP TEDGSEEPGS
     ETSDAKSTPT AEDVTAPLVD EGAPGKQAAA QPHTEIPEGT TAEEAGIGDT PSLEDEAAGH
     VTQEPESGKV VQEGFLREPG PPGLSHQLMS GMPGAPLLPE GPREATRQPS GTGPEDTEGG
     RHAPELLKHQ LLGDLHQEGP PLKGAGGKER PGSKEEVDED RDVDESSPQD SPPSKASPAQ
     DGRPPQTAAR EATSIPGFPA EGAIPLPVDF LSKVSTEIPA SEPDGPSVGR AKGQDAPLEF
     TFHVEITPNV QKEQAHSEEH LGRAAFPGAP GEGPEARGPS LGEDTKEADL PEPSEKQPAA
     APRGKPVSRV PQLKARMVSK SKDGTGSDDK KAKTSTRSSA KTLKNRPCLS PKLPTPGSSD
     PLIQPSSPAV CPEPPSSPKH VSSVTSRTGS SGAKEMKLKG ADGKTKIATP RGAAPPGQKG
     QANATRIPAK TPPAPKTPPS SGEPPKSGDR SGYSSPGSPG TPGSRSRTPS LPTPPTREPK
     KVAVVRTPPK SPSSAKSRLQ TAPVPMPDLK NVKSKIGSTE NLKHQPGGGK VQIINKKLDL
     SNVQSKCGSK DNIKHVPGGG SVQIVYKPVD LSKVTSKCGS LGNIHHKPGG GQVEVKSEKL
     DFKDRVQSKI GSLDNITHVP GGGNKKIETH KLTFRENAKA KTDHGAEIVY KSPVVSGDTS
     PRHLSNVSST GSIDMVDSPQ LATLADEVSA SLAKQGL
//
ID   ZEA1_MAIZE     STANDARD;      PRT;   234 AA.
AC   P02859;
DT   21-JUL-1986 (Rel. 01, Created)
DT   13-AUG-1987 (Rel. 05, Last sequence update)
DT   01-MAY-2005 (Rel. 47, Last annotation update)
DE   Zein-alpha precursor (19 kDa) (Clone A30).
OS   Zea mays (Maize).
OC   Eukaryota; Viridiplantae; Streptophyta; Embryophyta; Tracheophyta;
OC   Spermatophyta; Magnoliophyta; Liliopsida; Poales; Poaceae;
OC   PACCAD clade; Panicoideae; Andropogoneae; Zea.
OX   NCBI_TaxID=4577;
RN   [1]
RP   NUCLEOTIDE SEQUENCE.
RX   MEDLINE=82081837; PubMed=6895552;
RA   Geraghty D., Peifer M.A., Rubenstein I., Messing J.;
RT   "The primary structure of a plant storage protein: zein.";
RL   Nucleic Acids Res. 9:5163-5174(1981).
RN   [2]
RP   NUCLEOTIDE SEQUENCE.
RX   MEDLINE=84207882; PubMed=6233138;
RA   Hu N.T., Peifer M.A., Heidecker G., Messing J., Rubenstein I.;
RT   "Primary structure of a genomic zein sequence of maize.";
RL   EMBO J. 1:1337-1342(1982).
CC   -!- FUNCTION: Zeins are major seed storage proteins.
CC   -!- MISCELLANEOUS: The alpha zeins of 19 kDa and 22 kDa account for
CC       70% of the total zein fraction. They are encoded by a large
CC       multigene family.
CC   -!- MISCELLANEOUS: Structurally, 19 kDa and 19 kDa zeins are composed
CC       of nine adjacent, topologically antiparallel helices clustered
CC       within a distorted cylinder.
CC   -!- SIMILARITY: Belongs to the zein family.
DR   EMBL; V01481; CAA24728.1; -; mRNA.
DR   EMBL; V01481; CAA24728.1; -; mRNA.{EP1}
DR   PIR; A90967; ZIZM3.
DR   MaizeGDB; 58096; -.
DR   InterPro; IPR002530; Zein.
DR   Pfam; PF01559; Zein; 1.
KW   Multigene family; Repeat; Seed storage protein; Signal;
KW   Storage protein.
FT   SIGNAL        1     21
FT   CHAIN        22    234       Zein-alpha.
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   234 AA;  25404 MW;  502A99D438CA5DAA CRC64;
     MAAKIFCLLM LLGLSASAAT ATIFPQCSQA PIASLLPPYL SPAVSSVCEN PILQPYRIQQ
     AIAAGILPLS PLFLQQSSAL LQQLPLVHLL AQNIRAQQLQ QLVLANLAAY SQQQQFLPFN
     QLAALNSASY LQQQQLPFSQ LPAAYPQQFL PFNQLAALNS PAYLQQQQLL PFSQLAGVSP
     ATFLTQPQLL PFYQHAAPNA GTLLQLQQLL PFNQLALTNL AAFYQQPIIG GALF
//
ID   ENTK_HUMAN              Reviewed;        1019 AA.
AC   P98073; Q2NKL7;
DT   01-FEB-1996, integrated into UniProtKB/Swiss-Prot.
DT   01-FEB-1996, sequence version 1.
DT   22-JUL-2008, entry version 94.
DE   RecName: Full=Enteropeptidase;
DE            EC=3.4.21.9;
DE   AltName: Full=Enterokinase;
DE   AltName: Full=Serine protease 7;
DE   Contains:
DE     RecName: Full=Enteropeptidase non-catalytic heavy chain;
DE   Contains:
DE     RecName: Full=Enteropeptidase catalytic light chain;
DE   Flags: Precursor;
GN   Name=PRSS7; Synonyms=ENTK;
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Euarchontoglires; Primates; Haplorrhini;
OC   Catarrhini; Hominidae; Homo.
OX   NCBI_TaxID=9606;
RN   [1]
RP   NUCLEOTIDE SEQUENCE [MRNA].
RC   TISSUE=Duodenum;
RX   MEDLINE=95234679; PubMed=7718557; DOI=10.1021/bi00014a008;
RA   Kitamoto Y., Veile R.A., Donis-Keller H., Sadler J.E.;
RT   "cDNA sequence and chromosomal localization of human enterokinase, the
RT   proteolytic activator of trypsinogen.";
RL   Biochemistry 34:4562-4568(1995).
RN   [2]
RP   NUCLEOTIDE SEQUENCE [GENOMIC DNA], AND INVOLVEMENT IN ENTEROKINASE
RP   DEFICIENCY.
RX   MEDLINE=21606074; PubMed=11719902; DOI=10.1086/338456;
RA   Holzinger A., Maier E.M., Buck C., Mayerhofer P.U., Kappler M.,
RA   Haworth J.C., Moroz S.P., Hadorn H.-B., Sadler J.E., Roscher A.A.;
RT   "Mutations in the proenteropeptidase gene are the molecular cause of
RT   congenital enteropeptidase deficiency.";
RL   Am. J. Hum. Genet. 70:20-25(2002).
RN   [3]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA], AND VARIANTS GLU-134
RP   AND PRO-732.
RX   MEDLINE=20289799; PubMed=10830953; DOI=10.1038/35012518;
RA   Hattori M., Fujiyama A., Taylor T.D., Watanabe H., Yada T.,
RA   Park H.-S., Toyoda A., Ishii K., Totoki Y., Choi D.-K., Groner Y.,
RA   Soeda E., Ohki M., Takagi T., Sakaki Y., Taudien S., Blechschmidt K.,
RA   Polley A., Menzel U., Delabar J., Kumpf K., Lehmann R., Patterson D.,
RA   Reichwald K., Rump A., Schillhabel M., Schudy A., Zimmermann W.,
RA   Rosenthal A., Kudoh J., Shibuya K., Kawasaki K., Asakawa S.,
RA   Shintani A., Sasaki T., Nagamine K., Mitsuyama S., Antonarakis S.E.,
RA   Minoshima S., Shimizu N., Nordsiek G., Hornischer K., Brandt P.,
RA   Scharfe M., Schoen O., Desario A., Reichelt J., Kauer G., Bloecker H.,
RA   Ramser J., Beck A., Klages S., Hennig S., Riesselmann L., Dagand E.,
RA   Wehrmeyer S., Borzym K., Gardiner K., Nizetic D., Francis F.,
RA   Lehrach H., Reinhardt R., Yaspo M.-L.;
RT   "The DNA sequence of human chromosome 21.";
RL   Nature 405:311-319(2000).
RN   [4]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA].
RX   PubMed=15489334; DOI=10.1101/gr.2596504;
RG   The MGC Project Team;
RT   "The status, quality, and expansion of the NIH full-length cDNA
RT   project: the Mammalian Gene Collection (MGC).";
RL   Genome Res. 14:2121-2127(2004).
RN   [5]
RP   NUCLEOTIDE SEQUENCE [MRNA] OF 749-1019.
RC   TISSUE=Duodenum;
RX   MEDLINE=94329561; PubMed=8052624;
RA   Kitamoto Y., Yuan X., Wu Q., McCourt D.W., Sadler J.E.;
RT   "Enterokinase, the initiator of intestinal digestion, is a mosaic
RT   protease composed of a distinctive assortment of domains.";
RL   Proc. Natl. Acad. Sci. U.S.A. 91:7588-7592(1994).
CC   -!- FUNCTION: Responsible for initiating activation of pancreatic
CC       proteolytic proenzymes (trypsin, chymotrypsin and carboxypeptidase
CC       A). It catalyzes the conversion of trypsinogen to trypsin which in
CC       turn activates other proenzymes including chymotrypsinogen,
CC       procarboxypeptidases, and proelastases.
CC   -!- CATALYTIC ACTIVITY: Activation of trypsinogen by selective
CC       cleavage of 6-Lys-|-Ile-7 bond.
CC   -!- SUBUNIT: Heterodimer of a catalytic (light) chain and a
CC       multidomain (heavy) chain linked by a disulfide bond.
CC   -!- SUBCELLULAR LOCATION: Membrane; Single-pass type II membrane
CC       protein (Probable).
CC   -!- TISSUE SPECIFICITY: Intestinal brush border.
CC   -!- PTM: The chains are derived from a single precursor that is
CC       cleaved by a trypsin-like protease.
CC   -!- DISEASE: Defects in PRSS7 are a cause of enterokinase deficiency
CC       [MIM:226200]; a life-threatening intestinal malabsorption disorder
CC       characterized by diarrhea and failure to thrive.
CC   -!- SIMILARITY: Belongs to the peptidase S1 family.
CC   -!- SIMILARITY: Contains 2 CUB domains.
CC   -!- SIMILARITY: Contains 2 LDL-receptor class A domains.
CC   -!- SIMILARITY: Contains 1 MAM domain.
CC   -!- SIMILARITY: Contains 1 peptidase S1 domain.
CC   -!- SIMILARITY: Contains 1 SEA domain.
CC   -!- SIMILARITY: Contains 1 SRCR domain.
DR   EMBL; U09860; AAC50138.1; -; mRNA.
DR   EMBL; Y19124; CAB65555.1; -; Genomic_DNA.
DR   EMBL; Y19125; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19126; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19127; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19128; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19129; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19130; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19131; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19132; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19133; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19134; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19135; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19136; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19137; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19138; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19139; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19140; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19141; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19142; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; Y19143; CAB65555.1; JOINED; Genomic_DNA.
DR   EMBL; AL163218; CAB90392.1; -; Genomic_DNA.
DR   EMBL; AL163217; CAB90389.1; -; Genomic_DNA.
DR   EMBL; BC111749; AAI11750.1; -; mRNA.
DR   PIR; A56318; A56318.
DR   RefSeq; NP_002763.1; -.
DR   UniGene; Hs.149473; -.
DR   HSSP; P98072; 1EKB.
DR   SMR; P98073; 785-1019.
DR   MEROPS; S01.156; -.
DR   Ensembl; ENSG00000154646; Homo sapiens.
DR   GeneID; 5651; -.
DR   KEGG; hsa:5651; -.
DR   H-InvDB; HIX0040924; -.
DR   HGNC; HGNC:9490; PRSS7.
DR   HPA; HPA015611; -.
DR   MIM; 226200; phenotype.
DR   MIM; 606635; gene.
DR   PharmGKB; PA33839; -.
DR   HOGENOM; P98073; -.
DR   HOVERGEN; P98073; -.
DR   LinkHub; P98073; -.
DR   ArrayExpress; P98073; -.
DR   CleanEx; HS_PRSS7; -.
DR   GermOnline; ENSG00000154646; Homo sapiens.
DR   GO; GO:0005903; C:brush border; TAS:ProtInc.
DR   InterPro; IPR000859; CUB.
DR   InterPro; IPR002172; LDL_rcpt_classA_cys-rich.
DR   InterPro; IPR000998; MAM.
DR   InterPro; IPR011163; Pept_S1A_enterop.
DR   InterPro; IPR001254; Peptidase_S1_S6.
DR   InterPro; IPR001314; Peptidase_S1A.
DR   InterPro; IPR000082; SEA.
DR   InterPro; IPR001190; Srcr_rcpt.
DR   InterPro; IPR017448; Srcr_rcpt-rel.
DR   Gene3D; G3DSA:2.60.120.290; CUB; 2.
DR   Gene3D; G3DSA:4.10.400.10; LDL_rcpt_classA_cys-rich; 1.
DR   Pfam; PF00431; CUB; 2.
DR   Pfam; PF00057; Ldl_recept_a; 2.
DR   Pfam; PF00629; MAM; 1.
DR   Pfam; PF01390; SEA; 1.
DR   Pfam; PF00530; SRCR; 1.
DR   Pfam; PF00089; Trypsin; 1.
DR   PIRSF; PIRSF001138; Enteropeptidase; 1.
DR   PRINTS; PR00722; CHYMOTRYPSIN.
DR   PRINTS; PR00261; LDLRECEPTOR.
DR   PRINTS; PR00020; MAMDOMAIN.
DR   SMART; SM00042; CUB; 2.
DR   SMART; SM00192; LDLa; 2.
DR   SMART; SM00137; MAM; 1.
DR   SMART; SM00200; SEA; 1.
DR   SMART; SM00020; Tryp_SPc; 1.
DR   PROSITE; PS01180; CUB; 2.
DR   PROSITE; PS01209; LDLRA_1; 2.
DR   PROSITE; PS50068; LDLRA_2; 2.
DR   PROSITE; PS00740; MAM_1; 1.
DR   PROSITE; PS50060; MAM_2; 1.
DR   PROSITE; PS50024; SEA; 1.
DR   PROSITE; PS00420; SRCR_1; FALSE_NEG.
DR   PROSITE; PS50287; SRCR_2; 1.
DR   PROSITE; PS50240; TRYPSIN_DOM; 1.
DR   PROSITE; PS00134; TRYPSIN_HIS; 1.
DR   PROSITE; PS00135; TRYPSIN_SER; 1.
PE   2: Evidence at transcript level;
KW   Glycoprotein; Hydrolase; Lipoprotein; Membrane; Myristate;
KW   Polymorphism; Protease; Repeat; Serine protease; Signal-anchor;
KW   Transmembrane; Zymogen.
FT   CHAIN         1    784       Enteropeptidase non-catalytic heavy
FT                                chain.
FT                                /FTId=PRO_0000027719.
FT   CHAIN       785   1019       Enteropeptidase catalytic light chain.
FT                                /FTId=PRO_0000027720.
FT   TOPO_DOM      1     18       Cytoplasmic (Potential).
FT   TRANSMEM     19     47       Signal-anchor for type II membrane
FT                                protein (Potential).
FT   TOPO_DOM     48   1019       Extracellular (Potential).
FT   DOMAIN       52    169       SEA.
FT   DOMAIN      182    223       LDL-receptor class A 1.
FT   DOMAIN      225    334       CUB 1.
FT   DOMAIN      342    504       MAM.
FT   DOMAIN      524    634       CUB 2.
FT   DOMAIN      641    679       LDL-receptor class A 2.
FT   DOMAIN      678    771       SRCR.
FT   DOMAIN      785   1019       Peptidase S1.
FT   ACT_SITE    825    825       Charge relay system (By similarity).
FT   ACT_SITE    876    876       Charge relay system (By similarity).
FT   ACT_SITE    971    971       Charge relay system (By similarity).
FT   LIPID         2      2       N-myristoyl glycine (Potential).
FT   CARBOHYD    116    116       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    147    147       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    179    179       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    328    328       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    335    335       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    388    388       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    440    440       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    470    470       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    503    503       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    534    534       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    630    630       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    682    682       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    706    706       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    725    725       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    848    848       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    887    887       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    909    909       N-linked (GlcNAc...) (Potential).
FT   CARBOHYD    949    949       N-linked (GlcNAc...) (Potential).
FT   DISULFID    184    197       By similarity.
FT   DISULFID    191    210       By similarity.
FT   DISULFID    204    221       By similarity.
FT   DISULFID    225    253       By similarity.
FT   DISULFID    524    552       By similarity.
FT   DISULFID    643    655       By similarity.
FT   DISULFID    650    668       By similarity.
FT   DISULFID    662    677       By similarity.
FT   DISULFID    757    767       By similarity.
FT   DISULFID    772    896       Interchain (between heavy and light
FT                                chains) (By similarity).
FT   DISULFID    810    826       By similarity.
FT   DISULFID    910    977       By similarity.
FT   DISULFID    941    956       By similarity.
FT   DISULFID    967    995       By similarity.
FT   VARIANT      65     65       T -> I (in dbSNP:rs35987974).
FT                                /FTId=VAR_031686.
FT   VARIANT      77     77       K -> R (in dbSNP:rs2824804).
FT                                /FTId=VAR_021940.
FT   VARIANT     134    134       Q -> E (in dbSNP:rs2824790).
FT                                /FTId=VAR_031687.
FT   VARIANT     545    545       S -> C (in dbSNP:rs8134187).
FT                                /FTId=VAR_031688.
FT   VARIANT     641    641       E -> K (in dbSNP:rs2273204).
FT                                /FTId=VAR_020175.
FT   VARIANT     660    660       N -> H (in dbSNP:rs11088674).
FT                                /FTId=VAR_024292.
FT   VARIANT     732    732       S -> P (in dbSNP:rs2824721).
FT                                /FTId=VAR_031689.
FT   VARIANT     828    828       Y -> C (in dbSNP:rs8130110).
FT                                /FTId=VAR_031690.
FT   CONFLICT    754    771       SQQCLQDSLIRLQCNHKS -> RRNAKNEIDALSPIILIA
FT                                (in Ref. 3; CAB90389).
**
**   #################    INTERNAL SECTION    ##################
**CL 21q21.1;
**YY VAR_031687: AA shown in Swiss-Prot is the most frequent; 19-MAR-2007.
**YY VAR_031688: AA shown in Swiss-Prot is the most frequent; 19-MAR-2007.
**ZB NAG, 23-Mar-2007;
SQ   SEQUENCE   1019 AA;  112924 MW;  B6AAA245F6D4A563 CRC64;
     MGSKRGISSR HHSLSSYEIM FAALFAILVV LCAGLIAVSC LTIKESQRGA ALGQSHEARA
     TFKITSGVTY NPNLQDKLSV DFKVLAFDLQ QMIDEIFLSS NLKNEYKNSR VLQFENGSII
     VVFDLFFAQW VSDQNVKEEL IQGLEANKSS QLVTFHIDLN SVDILDKLTT TSHLATPGNV
     SIECLPGSSP CTDALTCIKA DLFCDGEVNC PDGSDEDNKM CATVCDGRFL LTGSSGSFQA
     THYPKPSETS VVCQWIIRVN QGLSIKLSFD DFNTYYTDIL DIYEGVGSSK ILRASIWETN
     PGTIRIFSNQ VTATFLIESD ESDYVGFNAT YTAFNSSELN NYEKINCNFE DGFCFWVQDL
     NDDNEWERIQ GSTFSPFTGP NFDHTFGNAS GFYISTPTGP GGRQERVGLL SLPLDPTLEP
     ACLSFWYHMY GENVHKLSIN ISNDQNMEKT VFQKEGNYGD NWNYGQVTLN ETVKFKVAFN
     AFKNKILSDI ALDDISLTYG ICNGSLYPEP TLVPTPPPEL PTDCGGPFEL WEPNTTFSST
     NFPNSYPNLA FCVWILNAQK GKNIQLHFQE FDLENINDVV EIRDGEEADS LLLAVYTGPG
     PVKDVFSTTN RMTVLLITND VLARGGFKAN FTTGYHLGIP EPCKADHFQC KNGECVPLVN
     LCDGHLHCED GSDEADCVRF FNGTTNNNGL VRFRIQSIWH TACAENWTTQ ISNDVCQLLG
     LGSGNSSKPI FSTDGGPFVK LNTAPDGHLI LTPSQQCLQD SLIRLQCNHK SCGKKLAAQD
     ITPKIVGGSN AKEGAWPWVV GLYYGGRLLC GASLVSSDWL VSAAHCVYGR NLEPSKWTAI
     LGLHMKSNLT SPQTVPRLID EIVINPHYNR RRKDNDIAMM HLEFKVNYTD YIQPICLPEE
     NQVFPPGRNC SIAGWGTVVY QGTTANILQE ADVPLLSNER CQQQMPEYNI TENMICAGYE
     EGGIDSCQGD SGGPLMCQEN NRWFLAGVTS FGYKCALPNR PGVYARVSRF TEWIQSFLH
//
ID   Q27861_9HYMN            Unreviewed;        41 AA.
AC   Q27861;
DT   01-NOV-1996, integrated into UniProtKB/TrEMBL.
DT   01-NOV-1996, sequence version 1.
DT   22-JUL-2008, entry version 32.
DE   SubName: Full=Histone H3{EI1};
DE            EC=3.4.21.9;
DE   AltName: Full=Enterokinase{EI5};
DE   Flags:  Fragment{EI8}; Precursor{EI7};
GN   Name=histone H3II{EI1};
OS   Tetrahymena hegewischi.
OC   Eukaryota; Alveolata; Ciliophora; Intramacronucleata;
OC   Oligohymenophorea; Hymenostomatida; Tetrahymenina; Tetrahymenidae;
OC   Tetrahymena.
OX   NCBI_TaxID=5923{EI1};
RN   [1]{EI1}
RP   NUCLEOTIDE SEQUENCE.
RC   STRAIN=KP7{EI1};
RX   MEDLINE=92203991; PubMed=1552842;
RA   Sadler L.A., Brunk C.F.;
RT   "Phylogenetic relationships and unusual diversity in histone H4
RT   proteins within the Tetrahymena pyriformis complex.";
RL   Mol. Biol. Evol. 9:70-84(1992).
CC   -!- SIMILARITY: Belongs to the histone H3 family{EA4}.
DR   EMBL; M73210; AAA75643.1; -; Genomic_DNA.{EI1}
DR   GO; GO:0000786; C:nucleosome; IEA:InterPro.
DR   GO; GO:0005634; C:nucleus; IEA:InterPro.
DR   GO; GO:0003677; F:DNA binding; IEA:InterPro.
DR   GO; GO:0006334; P:nucleosome assembly; IEA:InterPro.
DR   InterPro; IPR009072; Histone-fold.
DR   InterPro; IPR000164; Histone_H3.
DR   Gene3D; G3DSA:1.10.20.10; Histone-fold; 1.
DR   PANTHER; PTHR11426; Histone_H3; 1.
DR   PRINTS; PR00622; HISTONEH3.
DR   PROSITE; PS00322; HISTONE_H3_1; UNKNOWN_1.
PE   3: Inferred from homology;
FT   NON_TER      41     41       {EI1}
**
**   #################    INTERNAL SECTION    ##################
**EV EA4; Rulebase; TREMBL; RU004194V0.230S0031185; 11-MAR-2008.
**EV EI1; EMBL; -; AAA75643.1; 23-APR-2004.
**PM Gene3D; G3DSA:1.10.20.10; Histone-fold; 2; 40; T; 16-NOV-2006;
**PM PANTHER; PTHR11426; Histone_H3; 1; 40; T; 27-APR-2007;
**PM PRINTS; PR00622; HISTONEH3; 17; 31; T; 30-SEP-2005;
**PM PRINTS; PR00622; HISTONEH3; 34; 41; T; 30-SEP-2005;
**PM PRINTS; PR00622; HISTONEH3; 3; 17; T; 30-SEP-2005;
**PM PROSITE; PS00322; HISTONE_H3_1; 15; 21; ?; 07-OCT-2007;
SQ   SEQUENCE   41 AA;  4316 MW;  E343DF37507B58D9 CRC64;
     MARTKQTARK STGAKAPRKQ LASKAARKSA PATGGIKKPH E
//
ID   CAPSD_PCV2              Reviewed;         233 AA.
AC   O56129; Q8BB11;
DT   25-OCT-2005, integrated into UniProtKB/Swiss-Prot.
DT   01-JUN-1998, sequence version 1.
DT   05-APR-2011, entry version 41.
DE   RecName: Full=Capsid protein;
GN   Name=Cap; ORFNames=ORF2;
OS   Porcine circovirus 2 (PCV2).
OC   Viruses; ssDNA viruses; Circoviridae; Circovirus.
OX   NCBI_TaxID=85708;
OH   NCBI_TaxID=9823; Sus scrofa (Pig).
DR   EMBL; AF027217; AAC59463.1; -; Genomic_DNA.
DR   EMBL; AY094619; AAM21849.1; -; Genomic_DNA.
DR   GO; GO:0042025; C:host cell nucleus; IEA:UniProtKB-SubCell.
DR   GO; GO:0019028; C:viral capsid; IEA:UniProtKB-KW.
DR   GO; GO:0044419; P:interspecies interaction between organisms; IEA:UniProtKB-KW.
DR   InterPro; IPR003383; Circovirus_ORF2_capsid.
DR   Pfam; PF02443; Circo_capsid; 1.
PE   1: Evidence at protein level;
KW   Capsid protein; Clathrin- and
KW   caveolin-independent endocytosis of virus by host; Complete proteome;
KW   DNA-binding; Host nucleus; Host-virus interaction;
KW   Initiation of viral infection; T=1 icosahedral capsid protein;
KW   Viral attachment to host cell; Viral penetration into host cytoplasm;
KW   Viral penetration into host nucleus; Virion;
KW   Virus endocytosis by host.
FT   CHAIN         1    233       Capsid protein.
FT                                /FTId=PRO_0000133085.
**
**   #################    INTERNAL SECTION    ##################
**ZA PHL, 07-SEP-2005;
**ZB CHH, 25-MAY-2009; CHH, 25-JAN-2011; CHH, 14-MAR-2011;
SQ   SEQUENCE   233 AA;  27897 MW;  3C664C4B4E83AB58 CRC64;
     MTYPRRRYRR RRHRPRSHLG QILRRRPWLV HPRHRYRWRR KNGIFNTRLS RTFGYTVKAT
     TVRTPSWAVD MMRFNIDDFV PPGGGTNKIS IPFEYYRIRK VKVEFWPCSP ITQGDRGVGS
     TAVILDDNFV TKATALTYDP YVNYSSRHTI PQPFSYHSRY FTPKPVLDST IDYFQPNNKR
     TQLWLRLQTS RNVDHVGLGT AFENSIYDQD YNIRVTMYVQ FREFNLKDPP LKP
//
ID   PCLO_RAT                Reviewed;        1380 AA.
AC   Q9JKS6; Q9JLT1;
DT   19-OCT-2002, integrated into UniProtKB/Swiss-Prot.
DT   01-OCT-2000, sequence version 1.
DT   14-MAY-2014, entry version 108.
DE   RecName: Full=Protein piccolo {ECO:0000269|PubMed:11285225};
DE   AltName: Full=Aczonin;
DE   AltName: Full=Multidomain presynaptic cytomatrix protein;
GN   Name=vcp {ECO:0000313|EMBL:AAH50488.1,
GN   ECO:0000313|ZFIN:ZDB-GENE-030131-5408}; Synonyms=cdc48;
GN   ORFNames=si:ch211-113n10.2;
OG   Plasmid pCP301 {ECO:0000313|EMBL:AAH50488.1,
OG   ECO:0000269|PubMed:11285225}, Plasmid pWR100, Plasmid pINV_F6_M1382,
OG   and Plasmid pSF5; Chloroplast.
OS   Rattus norvegicus (Rat).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
OC   Mammalia; Eutheria; Euarchontoglires; Glires; Rodentia; Sciurognathi;
OC   Muroidea; Muridae; Murinae; Rattus.
OX   NCBI_TaxID=10116;
RN   [1] {ECO:0000269|PubMed:11285225}
RP   NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 2), AND INTERACTION WITH RABAC1.
RX   PubMed=10707984; DOI=10.1016/S0896-6273(00)80883-1;
RA   Fenster S.D., Chung W.J., Zhai R., Cases-Langhoff C., Voss B.,
RA   Garner A.M., Kaempf U., Kindler S., Gundelfinger E.D., Garner C.C.;
RT   "Piccolo, a presynaptic zinc finger protein structurally related to
RT   bassoon.";
RL   Neuron 25:203-214(2000).
RN   [2]
RP   NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 1).
RA   Fenster S.D., Cases-Langhoff C., Gundelfinger E.D., Garner C.C.;
RL   Submitted (JAN-2000) to the EMBL/GenBank/DDBJ databases.
CC   -!- FUNCTION: May act as a scaffolding protein involved in the
CC       organization of synaptic active zones and in synaptic vesicle
CC       trafficking (By similarity). {ECO:0000250|UniProtKB:Q9QYX7}.
CC   -!- SUBUNIT: Interacts with RABAC1/PRA1, RIMS2 and profilin (By
CC       similarity). {ECO:0000269|PubMed:10707984}.
CC   -!- INTERACTION:
CC       Q920M9:Siah1; NbExp=2; IntAct=EBI-2271602, EBI-957514;
CC   -!- SUBCELLULAR LOCATION: Integral membrane protein.
CC   -!- SUBCELLULAR LOCATION: Cell junction, synapse. Note=Concentrated at
CC       presynaptic side of synaptic junctions.
CC   -!- SUBCELLULAR LOCATION: Plastid, chloroplast membrane
CC       {ECO:0000269|PubMed:12766230, ECO:0000269|Ref.11}; Peripheral
CC       membrane protein {ECO:0000269|PubMed:12766230,
CC       ECO:0000269|PubMed:18431481}. Plastid, chloroplast stroma.
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing; Named isoforms=3;
CC       Name=1;
CC         IsoId=Q9JKS6-1; Sequence=Displayed;
CC       Name=2;
CC         IsoId=Q9JKS6-2; Sequence=VSP_003930, VSP_003931;
CC       Name=3;
CC         IsoId=Q9JKS6-3; Sequence=VSP_018194;
CC   -!- RNA EDITING: Modified_positions=1, 4, 18, 33
CC       {ECO:0000269|PubMed:10707984, ECO:0000269|PubMed:11285225}, 34,
CC       72, 80, 86, 95, 121, 123, 154, 155, 156, 163, 169, 193, 196, 233,
CC       295, 346; Note=The initiator methionine is created by RNA editing.
CC       The nonsense codons at positions 72, 121, 169, 193 and 346 are
CC       modified to sense codons. {ECO:0000269|PubMed:10707984}.
CC   -!- RNA EDITING: Modified_positions=Not_applicable; Note=a.
CC   -!- RNA EDITING: [Isoform 3]: Modified_positions=Not_applicable; Note=Exon
CC       13 is extensively edited in brain. {ECO:0000269|PubMed:20835228};
CC   -!- DOMAIN: C2 domain 1 is involved in binding calcium and
CC       phospholipids. Calcium binds with low affinity but with high
CC       specificity and induces a large conformational change.
CC       {ECO:0000269|PubMed:11285225}.
CC   -!- DISEASE: Spastic paraplegia 4, autosomal dominant (SPG4)
CC       [MIM:182601]: A form of spastic paraplegia, a neurodegenerative
CC       disorder characterized by a slow, gradual, progressive weakness
CC       and spasticity of the lower limbs. Rate of progression and the
CC       severity of symptoms are quite variable. Initial symptoms may
CC       include difficulty with balance, weakness and stiffness in the
CC       legs, muscle spasms, and dragging the toes when walking. In some
CC       forms of the disorder, bladder symptoms (such as incontinence) may
CC       appear, or the weakness and stiffness may spread to other parts of
CC       the body. {ECO:0000269|PubMed:10610178,
CC       ECO:0000269|PubMed:10699187, ECO:0000269|PubMed:11015453,
CC       ECO:0000269|PubMed:11039577, ECO:0000269|PubMed:11087788,
CC       ECO:0000269|PubMed:11309678, ECO:0000269|PubMed:11843700,
CC       ECO:0000269|PubMed:11985387, ECO:0000269|PubMed:12124993,
CC       ECO:0000269|PubMed:12161613, ECO:0000269|PubMed:12163196,
CC       ECO:0000269|PubMed:12202986, ECO:0000269|PubMed:12460147,
CC       ECO:0000269|PubMed:12552568, ECO:0000269|PubMed:12939659,
CC       ECO:0000269|PubMed:14732620, ECO:0000269|PubMed:15159500,
CC       ECO:0000269|PubMed:15210521, ECO:0000269|PubMed:15248095,
CC       ECO:0000269|PubMed:15326248, ECO:0000269|PubMed:15482961,
CC       ECO:0000269|PubMed:16682546, ECO:0000269|PubMed:16684598,
CC       ECO:0000269|PubMed:17594340, ECO:0000269|PubMed:20214791,
CC       ECO:0000269|PubMed:20550563, ECO:0000269|PubMed:20562464,
CC       ECO:0000269|PubMed:20718791, ECO:0000269|PubMed:20932283}.
CC       Note=The disease is caused by mutations affecting the gene
CC       represented in this entry.
CC   -!- BIOPHYSICOCHEMICAL PROPERTIES:
CC       Kinetic parameters:
CC         KM=0.45 mM for ATP {ECO:0000269|PubMed:15716377,
CC         ECO:0000269|PubMed:17389232, ECO:0000269|PubMed:22637577};
CC         Vmax=1.2 nmol/min/ug enzyme {ECO:0000269|PubMed:15716377,
CC         ECO:0000269|PubMed:17389232, ECO:0000269|PubMed:22637577};
CC         Note=Kinetic parameters shown are for full length enzyme. N-
CC         terminally truncated spastin (residues 228-616), which has been
CC         shown to exhibit full severing activity, shows a basal ATP
CC         turnover rate of 0.78 sec(-1) in the absence of microtubules, a
CC         KM of 0.16 mM for ATP, and the ATP turnover rate is extrapolated
CC         to 3.83 sec(-1) in the presence of microtubules. ATPase activity
CC         shows non-Michaelis-Menten kinetics in the presence of
CC         microtubules, but is close to non-cooperative behavior in their
CC         absence (PubMed:22637577). {ECO:0000269|PubMed:15716377,
CC         ECO:0000269|PubMed:17389232, ECO:0000269|PubMed:22637577};
CC   -!- SIMILARITY: Contains 2 C2 domains. {ECO:0000269|PubMed:11285225}.
CC   -!- SIMILARITY: Contains 1 PDZ (DHR) domain.
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing; Named isoforms=2;
CC         Comment=This alternative splicing is restricted to C4 species of
CC         Flaveria. {ECO:0000269|PubMed:7550380,
CC         ECO:0000269|PubMed:8771790, ECO:0000269|Ref.3};
CC       Name=1; Synonyms=Long, Long variant 1;
CC         IsoId=Q9UBP0-1; Sequence=Displayed;
CC         Note=No experimental confirmation available. {ECO:0000305};
CC       Name=2; Synonyms=Long variant 2;
CC         IsoId=Q9UBP0-2; Sequence=VSP_000024;
CC   -!- SEQUENCE CAUTION:
CC       Sequence=AAH52843.1; Type=Miscellaneous discrepancy; Note=Contaminating sequence. Potential poly-A sequence.; Evidence={ECO:0000305};
CC       Sequence=AAI06097.1; Type=Miscellaneous discrepancy; Note=Contaminating sequence. Potential poly-A sequence.; Evidence={ECO:0000305};
CC       Sequence=BAC33868.1; Type=Frameshift; Positions=1759;
CC   -!- SEQUENCE CAUTION: [Isoform 1]:
CC       Sequence=AAH17572.1; Type=Erroneous initiation; Note=Truncated N-terminus.; Evidence={ECO:0000305};
CC   -!- RNA EDITING: Modified_positions=207 {ECO:0000269|PubMed:17018572,
CC       ECO:0000269|Ref.6}; Note=Partially edited. Target of Adar.
CC   -----------------------------------------------------------------------
CC   Copyrighted by the UniProt Consortium, see http://www.uniprot.org/terms
CC   Distributed under the Creative Commons Attribution-NoDerivs License
CC   -----------------------------------------------------------------------
DR   Pfam; PF00168; C2; 2.
DR   Pfam; PF00595; PDZ; 1.
DR   Pfam; PF05715; zf-piccolo; 2.
DR   SMART; SM00239; C2; 2.
DR   SMART; SM00228; PDZ; 1.
DR   SUPFAM; SSF49562; SSF49562; 2.
DR   SUPFAM; SSF50156; SSF50156; 1.
DR   SUPFAM; SSF57903; SSF57903; 2.
DR   PROSITE; PS50004; C2; 2.
DR   PROSITE; PS50106; PDZ; 1.
PE   1: Evidence at protein level;
KW   3D-structure; Alternative splicing; Calcium;
KW   Calcium/phospholipid-binding; Cell junction; Complete proteome;
KW   Glycoprotein; Metal-binding; Phosphoprotein; Reference proteome;
KW   Repeat; Synapse; Zinc; Zinc-finger.
FT   CHAIN         1   5085       Protein piccolo.
FT                                /FTId=PRO_0000058252.
FT   TRANSIT       1     29       MITOCHONDRION{EC2}.
FT   INIT_MET      1      1       Removed. {ECO:0000269|PubMed:22814378}.
FT   DOMAIN     4442   4536       PDZ.{ECO:0000269|PubMed:11285225}.
FT   DOMAIN     4653   4752       C2 1. {ECO:0000269|PubMed:11285225}.
FT   DOMAIN     4968   5059       C2 2. {ECO:0000269|PubMed:11285225}.
FT   ZN_FING     523    547       C4-type (Potential).
FT                                {ECO:0000269|PubMed:10707984}.
FT   ZN_FING    1010   1033       C4-type (Potential).
FT                                {ECO:0000269|PubMed:10707984}.
FT   REGION      372    491       12 X 10 AA tandem approximate repeats of
FT                                P-A-K-P-Q-P-Q-Q-P-X.
FT   COMPBIAS   2351   2362       Poly-Pro. {ECO:0000269|PubMed:11285225}.
FT   MOD_RES       3      3       Phosphoserine.
FT   MOD_RES    1778   1778       Phosphoserine (By similarity).
FT   MOD_RES    3374   3374       Phosphoserine (By similarity).
FT   MOD_RES    3392   3392       Phosphothreonine (By similarity).
FT   CARBOHYD   2702   2702       O-linked (GlcNAc) (By similarity).
FT   CARBOHYD   2976   2976       O-linked (GlcNAc) (By similarity).
FT   VAR_SEQ    4687   4695       Missing (in isoform 3).
FT                                /FTId=VSP_018194.
FT   VAR_SEQ    4876   4880       TKPTN -> SKRRK (in isoform 2).
FT                                {ECO:0000269|PubMed:11285225}.
FT                                /FTId=VSP_003930.
FT   VAR_SEQ    4881   5085       Missing (in isoform 2).
FT                                {ECO:0000269|PubMed:11285225}.
FT                                /FTId=VSP_003931.
FT   VARIANT       3      3       A -> L{EC2}.
FT                                /FTId=VAR_000001.
FT   VARIANT       3      3       A -> LFSJFSHLJSDHFLSJLFHSJLLSJKDHFLSKJFHJ
FT                                .
FT   VARIANT      23     23       Missing. {ECO:0000269|PubMed:15489334,
FT                                ECO:0000269|PubMed:8906617,
FT                                ECO:0000269|PubMed:9009220,
FT                                ECO:0000269|PubMed:9921897}.
FT   VARIANT     386    386       N -> S (in SPG4).
FT   MUTAGEN    4668   4668       D->A: Complete loss of calcium-binding
FT                                and calcium-dependent phospholipid
FT                                binding activity.
FT                                {ECO:0000269|PubMed:11285225}.
FT   MUTAGEN    4674   4674       D->A: Complete loss of calcium-binding
FT                                and calcium-dependent phospholipid
FT                                binding activity.
FT                                {ECO:0000269|PubMed:11285225}.
FT   MUTAGEN    4688   4689       VM->SS: 10-fold increase in affinity for
FT                                calcium. {ECO:0000269|PubMed:11285225}.
FT   MUTAGEN    4688   4688       V->S: Small increase in affinity for
FT                                calcium. {ECO:0000269|PubMed:11285225}.
FT   MUTAGEN    4689   4689       M->S: Increased affinity for calcium.
FT                                {ECO:0000269|PubMed:11285225}.
FT   MUTAGEN    4690   4691       VV->SS: 10-fold increase in affinity for
FT                                calcium. {ECO:0000269|PubMed:11285225}.
FT   MUTAGEN    4692   4693       QN->AA: Moderate increase in affinity for
FT                                calcium. {ECO:0000269|PubMed:11285225}.
FT   MUTAGEN    4694   4694       A->S: No effect on calcium-binding
FT                                activity. {ECO:0000269|PubMed:11285225}.
**
**   #################    INTERNAL SECTION    ##################
SQ   SEQUENCE   1380 AA;  144367 MW;  13497D39F4CE32B8 CRC64;
     MGNEASLEGE GLPEGLAAAA GAGGSGSALH PGIPAGMEAD LSQLSEEERR QIAAVMSRAQ
     GLPKGSVPPA AAESPSMHRK QELDSSQAPQ QPGKPPDPGR PTQPGLSKSR TTDTFRSEQK
     LPGRSPSTIS LKESKSRTDF KEEYKSSMMP GFFSDVNPLS AVSSVVNKFN PFDLISDSEA
     SQEETTKKQK VVQKEQGKSE GMAKPPLQQP SPKPIPKQQG QVKEVIQQDS SPKSVSSQQA
     EKVKPQAPGT GKPSQQSPAQ TPAQQASPGK PVAQQPGSAK ATVQQPGPAK SPAQPAGTGK
     SPAQPPAKTP GQQAGLEKTS SSQQPGPKSL AQTPGHGKFP LGPVKSPAQQ PGTAKHPAQQ
     PGPQTAAKVP GPTKTPAQQS GPGKTPAQQP GPTKPSPQQP IPAKPQPQQP VATKTQPQQS
     APAKPQPQQP APAKPQPQQP TPAKPQPQPP TPAKPQPQPP TATKPQPQPP TATKPHHQQP
     GLAKPSAQQP TKSISQTVTG RPLQPPPTSA AQTPAQGLSK TICPLCNTTE LLLHIPEKAN
     FNTCTECQST VCSLCGFNPN PHLTEIKEWL CLNCQMQRAL GGDLAAAIPS SPQPTPKAAT
     APTATASKSP VPSQQASPKK EPPSKQDSPK ALESKKPPEP KKPPEPKKPP EPKKPPPLVK
     QPTLHGPTPA TAPQLPVAEA LPEPAPPKEP SGPLPEQAKA PVGDVEPKQP KMTETRADIQ
     SSSTTKPDIL SSQVQSQAQV KTASPLKTDS AKPSQSFPPT GEKTTPLDSK AMPRPASDSK
     IISQPGPGSE SKDPKHIDPI QKKDEPKKAQ PKGSPKPETK PVPKGSPTPS GTRPTAGQAA
     PPSQQPPKPQ EQSRRFSLNL GGITDAPKSQ PTTPQETVTG KLFGFGASIF SQASNLISTA
     GQQGPHPQTG PAAPSKQAPT PSQSPAAQGP AKSTGQLPPA PAKATAVKKE AKAAAAENLE
     SKPEQAPTAK KTEKDKKPPP AKVGKPPPSE PEKAVPAHKP DKTTKPKPAC PLCRTELNLG
     SQEPPNFNTC TECKNQVCNL CGFNPTPHLT EIQEWLCLNC QTQRAISGQL GDMGKMPPAP
     SGPKASPMPA PAEPSSQKTP TGTQVKGKKK EAEGKTEAEK PVPEKETASI EKTPPMVTTD
     QKLEESEGKK SKVSALPEKK PSEEEKAISA DKKERKPPAE EKPPLEEKKP IPVDKKLPPE
     AKPLSSEGEE KHEILKAHVQ IPEEEPTGKV AAKAGEEEQQ PDSRPEALPG ATPLTLPKAG
     EKERAVAQPQ AEGSSKDGQG ERSKEKTEKE EDKSDTSSSQ QPKSPQGLSD TGYSSDGISG
     SLGEIPSLIP SDEKDLLKGL KKDSFSQESS PSSPSDLAKL ESTVLSILEA QASTLVGEKA
//
ID   RIBA1_ARATH             Reviewed;         543 AA.
AC   P47924; Q9SBA8;
DT   01-FEB-1996, integrated into UniProtKB/Swiss-Prot.
DT   10-JAN-2003, sequence version 2.
DT   24-JUN-2015, entry version 119.
DE   RecName: Full=Bifunctional riboflavin biosynthesis protein RIBA 1, chloroplastic;
DE            Short=AtRIBA1;
DE   Includes:
DE     RecName: Full=3,4-dihydroxy-2-butanone 4-phosphate synthase;
DE              Short=DHBP synthase;
DE              EC=4.1.99.12;
DE   Flags: Precursor;
GN   Name=RIBA1; Synonyms=RIBBA; OrderedLocusNames=At5g64300;
GN   ORFNames=MSJ1.14;
OS   Arabidopsis thaliana (Mouse-ear cress).
OC   Eukaryota; Viridiplantae; Streptophyta; Embryophyta; Tracheophyta;
OC   Spermatophyta; Magnoliophyta; eudicotyledons; Gunneridae;
OC   Pentapetalae; rosids; malvids; Brassicales; Brassicaceae; Camelineae;
OC   Arabidopsis.
OX   NCBI_TaxID=3702;
RN   [1]
RP   NUCLEOTIDE SEQUENCE [GENOMIC DNA].
RX   PubMed=10783978; DOI=10.1016/S0031-9422(00)00013-3;
RA   Herz S.W., Eberhardt S., Bacher A.;
RT   "Biosynthesis of riboflavin in plants. The ribA gene of Arabidopsis
RT   thaliana specifies a bifunctional GTP cyclohydrolase II/3,4-dihydroxy-
RT   2-butanone 4-phosphate synthase.";
RL   Phytochemistry 53:723-731(2000).
RN   [2]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA].
RC   STRAIN=cv. Columbia;
RX   PubMed=9501997; DOI=10.1093/dnares/4.6.401;
RA   Nakamura Y., Sato S., Kaneko T., Kotani H., Asamizu E., Miyajima N.,
RA   Tabata S.;
RT   "Structural analysis of Arabidopsis thaliana chromosome 5. III.
RT   Sequence features of the regions of 1,191,918 bp covered by seventeen
RT   physically assigned P1 clones.";
RL   DNA Res. 4:401-414(1997).
RN   [3]
RP   GENOME REANNOTATION.
RC   STRAIN=cv. Columbia;
RG   The Arabidopsis Information Resource (TAIR);
RL   Submitted (APR-2011) to the EMBL/GenBank/DDBJ databases.
RN   [4]
RP   NUCLEOTIDE SEQUENCE [MRNA] OF 279-543.
RX   PubMed=7642114; DOI=10.1016/0378-1119(95)00246-3;
RA   Kobayashi M., Sugiyama M., Yamamoto K.;
RT   "Isolation of cDNAs encoding GTP cyclohydrolase II from Arabidopsis
RT   thaliana.";
RL   Gene 160:303-304(1995).
RN   [5]
RP   NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 2).
RX   PubMed=10835424; DOI=10.1074/jbc.M003127200;
RA   Turk E., Kim O., Le Coutre J., Whitelegge J.P., Eskandari S.,
RA   Lam J.T., Kreman M., Zampighi G., Faull K.F., Wright E.M.;
RT   "Molecular characterization of Vibrio parahaemolyticus vSGLT: a model
RT   for sodium-coupled sugar cotransporters.";
RL   J. Biol. Chem. 275:25711-25716(2000).
RN   [6]
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RX   PubMed=19376835; DOI=10.1104/pp.109.138677;
RA   Reiland S., Messerli G., Baerenfaller K., Gerrits B., Endler A.,
RA   Grossmann J., Gruissem W., Baginsky S.;
RT   "Large-scale Arabidopsis phosphoproteome profiling reveals novel
RT   chloroplast kinase substrates and phosphorylation networks.";
RL   Plant Physiol. 150:889-903(2009).
RN   [7]
RP   VARIANT GLN-229 (ISOFORM 2).
RX   PubMed=18514161; DOI=10.1016/j.ajhg.2008.04.020;
RA   Tanaka M., Olsen R.W., Medina M.T., Schwartz E., Alonso M.E.,
RA   Duron R.M., Castro-Ortega R., Martinez-Juarez I.E.,
RA   Pascual-Castroviejo I., Machado-Salas J., Silva R., Bailey J.N.,
RA   Bai D., Ochoa A., Jara-Prado A., Pineda G., Macdonald R.L.,
RA   Delgado-Escueta A.V.;
RT   "Hyperglycosylation and reduced GABA currents of mutated GABRB3
RT   polypeptide in remitting childhood absence epilepsy.";
RL   Am. J. Hum. Genet. 82:1249-1261(2008).
RN   [8]
RP   FUNCTION, CATALYTIC ACTIVITY, COFACTOR, TISSUE SPECIFICITY, AND
RP   SUBCELLULAR LOCATION.
RX   PubMed=23203051; DOI=10.3390/ijms131114086;
RA   Hiltunen H.M., Illarionov B., Hedtke B., Fischer M., Grimm B.;
RT   "Arabidopsis RIBA Proteins: two out of three isoforms have lost their
RT   bifunctional activity in riboflavin biosynthesis.";
RL   Int. J. Mol. Sci. 13:14086-14105(2012).
RN   [9]
RP   FUNCTION, AND DISRUPTION PHENOTYPE.
RX   PubMed=22081402; DOI=10.1007/s11103-011-9846-1;
RA   Hedtke B., Alawady A., Albacete A., Kobayashi K., Melzer M.,
RA   Roitsch T., Masuda T., Grimm B.;
RT   "Deficiency in riboflavin biosynthesis affects tetrapyrrole
RT   biosynthesis in etiolated Arabidopsis tissue.";
RL   Plant Mol. Biol. 78:77-93(2012).
RN   [10]
RP   DISRUPTION PHENOTYPE.
RX   PubMed=19797057; DOI=10.1074/jbc.M109.030247;
RA   Nagy R., Grob H., Weder B., Green P., Klein M., Frelet-Barrand A.,
RA   Schjoerring J.K., Brearley C., Martinoia E.;
RT   "The Arabidopsis ATP-binding cassette protein AtMRP5/AtABCC5 is a high
RT   affinity inositol hexakisphosphate transporter involved in guard cell
RT   signaling and phytate storage.";
RL   J. Biol. Chem. 284:33614-33622(2009).
RN   [11]
RP   ALLERGEN.
RX   PubMed=21848516; DOI=10.1111/j.1398-9995.2011.02683.x;
RA   An S., Ma D., Wei J.F., Yang X., Yang H.W., Yang H., Xu X., He S.,
RA   Lai R.;
RT   "A novel allergen Tab y 1 with inhibitory activity of platelet
RT   aggregation from salivary glands of horseflies.";
RL   Allergy 66:1420-1427(2011).
RN   [12]
RP   ALLERGEN.
RX   PubMed=12100054; DOI=10.1046/j.1365-2222.2002.01411.x;
RA   Codina R., Ardusso L., Lockey R.F., Crisci C.D., Jaen C.,
RA   Bertoya N.H.;
RT   "Identification of the soybean hull allergens involved in
RT   sensitization to soybean dust in a rural population from Argentina and
RT   N-terminal sequence of a major 50 KD allergen.";
RL   Clin. Exp. Allergy 32:1059-1063(2002).
RN   [13]
RP   SUBUNIT, SUBCELLULAR LOCATION, INDUCTION, AND DOMAIN.
RX   PubMed=24389466; DOI=10.1038/nsmb.2740;
RA   Boel G., Smith P.C., Ning W., Englander M.T., Chen B., Hashem Y.,
RA   Testa A.J., Fischer J.J., Wieden H.J., Frank J., Gonzalez R.L. Jr.,
RA   Hunt J.F.;
RT   "The ABC-F protein EttA gates ribosome entry into the translation
RT   elongation cycle.";
RL   Nat. Struct. Mol. Biol. 21:143-151(2014).
RN   [14]
RP   SUBUNIT, AND DOMAIN.
RX   PubMed=24389465; DOI=10.1038/nsmb.2741;
RA   Chen B., Boel G., Hashem Y., Ning W., Fei J., Wang C.,
RA   Gonzalez R.L. Jr., Hunt J.F., Frank J.;
RT   "EttA regulates translation by binding the ribosomal E site and
RT   restricting ribosome-tRNA dynamics.";
RL   Nat. Struct. Mol. Biol. 21:152-159(2014).
RN   [15]
RP   INDUCTION.
RX   PubMed=21094157; DOI=10.1016/j.febslet.2010.11.025;
RA   Shin R., Jez J.M., Basra A., Zhang B., Schachtman D.P.;
RT   "14-3-3 proteins fine-tune plant nutrient metabolism.";
RL   FEBS Lett. 585:143-147(2011).
RN   [16]
RP   COFACTOR, AND BIOTECHNOLOGY.
RX   PubMed=23269834; DOI=10.1073/pnas.1214159110;
RA   Kim S., Yamaoka Y., Ono H., Kim H., Shim D., Maeshima M.,
RA   Martinoia E., Cahoon E.B., Nishida I., Lee Y.;
RT   "AtABCA9 transporter supplies fatty acids for lipid synthesis to the
RT   endoplasmic reticulum.";
RL   Proc. Natl. Acad. Sci. U.S.A. 110:773-778(2013).
RN   [17]
RP   BIOTECHNOLOGY.
RX   PubMed=16244144; DOI=10.1104/pp.105.068684;
RA   Van Hoewyk D., Garifullina G.F., Ackley A.R., Abdel-Ghany S.E.,
RA   Marcus M.A., Fakra S., Ishiyama K., Inoue E., Pilon M., Takahashi H.,
RA   Pilon-Smits E.A.;
RT   "Overexpression of AtCpNifS enhances selenium tolerance and
RT   accumulation in Arabidopsis.";
RL   Plant Physiol. 139:1518-1528(2005).
RN   [18]
RP   DEVELOPMENTAL STAGE.
RX   PubMed=9675899; DOI=10.1046/j.1365-313X.1998.00151.x;
RA   Hirner B., Fischer W.-N., Rentsch D., Kwart M., Frommer W.B.;
RT   "Developmental control of H+/amino acid permease gene expression
RT   during seed development of Arabidopsis.";
RL   Plant J. 14:535-544(1998).
RN   [19]
RP   DEVELOPMENTAL STAGE.
RX   PubMed=12376641; DOI=10.1104/pp.008110;
RA   Fatland B.L., Ke J., Anderson M.D., Mentzen W.I., Cui L.W.,
RA   Allred C.C., Johnston J.L., Nikolau B.J., Wurtele E.S.;
RT   "Molecular characterization of a heteromeric ATP-citrate lyase that
RT   generates cytosolic acetyl-coenzyme A in Arabidopsis.";
RL   Plant Physiol. 130:740-756(2002).
RN   [20]
RP   DISEASE.
RX   PubMed=17315199; DOI=10.1002/hipo.20268;
RA   Almgren M., Persson A.S., Fenghua C., Witgen B.M., Schalling M.,
RA   Nyengaard J.R., Lavebratt C.;
RT   "Lack of potassium channel induces proliferation and survival causing
RT   increased neurogenesis and two-fold hippocampus enlargement.";
RL   Hippocampus 17:292-304(2007).
RN   [21]
RP   DISEASE.
RX   PubMed=8995755; DOI=10.1007/s003359900259;
RA   Donahue L.R., Cook S.A., Johnson K.R., Bronson R.T., Davisson M.T.;
RT   "Megencephaly: a new mouse mutation on chromosome 6 that causes
RT   hypertrophy of the brain.";
RL   Mamm. Genome 7:871-876(1996).
RN   [22]
RP   DISEASE.
RX   PubMed=21966978; DOI=10.1111/j.1460-9568.2011.07834.x;
RA   Fisahn A., Lavebratt C., Canlon B.;
RT   "Acoustic startle hypersensitivity in Mceph mice and its effect on
RT   hippocampal excitability.";
RL   Eur. J. Neurosci. 34:1121-1130(2011).
RN   [23]
RP   ENZYME REGULATION, CATALYTIC ACTIVITY, AND BIOPHYSICOCHEMICAL
RP   PROPERTIES.
RX   PubMed=6750031;
RA   Ogrydziak D.M., Scharf S.J.;
RT   "Alkaline extracellular protease produced by Saccharomycopsis
RT   lipolytica CX161-1B.";
RL   J. Gen. Microbiol. 128:1225-1234(1982).
RN   [24]
RP   ENZYME REGULATION, CATALYTIC ACTIVITY, AND PROTEOLYTIC PROCESSING.
RX   PubMed=2649495;
RA   Matoba S., Ogrydziak D.M.;
RT   "A novel location for dipeptidyl aminopeptidase processing sites in
RT   the alkaline extracellular protease of Yarrowia lipolytica.";
RL   J. Biol. Chem. 264:6037-6043(1989).
RN   [25]
RP   FUNCTION, CATALYTIC ACTIVITY, AND BIOPHYSICOCHEMICAL PROPERTIES.
RX   PubMed=17432872; DOI=10.1021/jf0633894;
RA   Poza M., Sestelo A.B., Ageitos J.M., Vallejo J.A., Veiga-Crespo P.,
RA   Villa T.G.;
RT   "Cloning and expression of the XPR2 gene from Yarrowia lipolytica in
RT   Pichia pastoris.";
RL   J. Agric. Food Chem. 55:3944-3948(2007).
RN   [26]
RP   PROTEOLYTIC PROCESSING.
RX   PubMed=9353927;
RA   Matoba S., Morano K.A., Klionsky D.J., Kim K., Ogrydziak D.M.;
RT   "Dipeptidyl aminopeptidase processing and biosynthesis of alkaline
RT   extracellular protease from Yarrowia lipolytica.";
RL   Microbiology 143:3263-3272(1997).
RN   [27]
RP   PHARMACEUTICAL.
RX   PubMed=21949405; DOI=10.1073/pnas.1108495108;
RA   Crawford M.A., Lowe D.E., Fisher D.J., Stibitz S., Plaut R.D.,
RA   Beaber J.W., Zemansky J., Mehrad B., Glomski I.J., Strieter R.M.,
RA   Hughes M.A.;
RT   "Identification of the bacterial protein FtsX as a unique target of
RT   chemokine-mediated antimicrobial activity against Bacillus
RT   anthracis.";
RL   Proc. Natl. Acad. Sci. U.S.A. 108:17159-17164(2011).
RN   [28]
RP   PHARMACEUTICAL.
RX   PubMed=16632613; DOI=10.1073/pnas.0509392103;
RA   Rutten L., Geurtsen J., Lambert W., Smolenaers J.J., Bonvin A.M.,
RA   de Haan A., van der Ley P., Egmond M.R., Gros P., Tommassen J.;
RT   "Crystal structure and catalytic mechanism of the LPS 3-O-deacylase
RT   PagL from Pseudomonas aeruginosa.";
RL   Proc. Natl. Acad. Sci. U.S.A. 103:7071-7076(2006).
RN   [29]
RP   INVOLVEMENT IN ASTHMA RESPONSE TO GLUCOCORTICOID TREATMENT, AND TISSUE
RP   SPECIFICITY.
RX   PubMed=21991891; DOI=10.1056/NEJMoa0911353;
RA   Tantisira K.G., Lasky-Su J., Harada M., Murphy A., Litonjua A.A.,
RA   Himes B.E., Lange C., Lazarus R., Sylvia J., Klanderman B., Duan Q.L.,
RA   Qiu W., Hirota T., Martinez F.D., Mauger D., Sorkness C., Szefler S.,
RA   Lazarus S.C., Lemanske R.F. Jr., Peters S.P., Lima J.J., Nakamura Y.,
RA   Tamari M., Weiss S.T.;
RT   "Genomewide association between GLCCI1 and response to glucocorticoid
RT   therapy in asthma.";
RL   N. Engl. J. Med. 365:1173-1183(2011).
RN   [30]
RP   INVOLVEMENT IN CHRONIC OBSTRUCTIVE PULMONARY DISEASE RESPONSE TO
RP   GLUCOCORTICOID TREATMENT.
RX   PubMed=22187997; DOI=10.1056/NEJMc1112547#SA2;
RA   Van den Berge M., Hiemstra P.S., Postma D.S.;
RT   "Genetics of glucocorticoids in asthma.";
RL   N. Engl. J. Med. 365:2434-2435(2011).
RN   [31]
RP   RNA EDITING, AND SUBCELLULAR LOCATION.
RC   TISSUE=Ehrlich ascites tumor cell;
RX   PubMed=9372446;
RA   Petzelt C., Joswig G., Mincheva A., Lichter P., Stammer H., Werner D.;
RT   "The centrosomal protein centrosomin A and the nuclear protein
RT   centrosomin B derive from one gene by post-transcriptional processes
RT   involving RNA editing.";
RL   J. Cell Sci. 110:2573-2578(1997).
RN   [32] {ECO:0000305}
RP   RNA EDITING.
RX   PubMed=17018572; DOI=10.1261/rna.254306;
RA   Stapleton M., Carlson J.W., Celniker S.E.;
RT   "RNA editing in Drosophila melanogaster: new targets and functional
RT   consequences.";
RL   RNA 12:1922-1932(2006).
RN   [33]
RP   TOXIC DOSE.
RC   TISSUE=Venom;
RX   PubMed=3075905;
RA   Masci P.P., Whitaker A.N., de Jersey J.;
RT   "Purification and characterization of a prothrombin activator from the
RT   venom of the Australian brown snake, Pseudonaja textilis textilis.";
RL   Biochem. Int. 17:825-835(1988).
RN   [34]
RP   TOXIC DOSE.
RC   TISSUE=Venom gland;
RX   PubMed=15351847; DOI=10.1267/THRO04090509;
RA   Rao V.S., Swarup S., Kini R.M.;
RT   "The catalytic subunit of pseutarin C, a group C prothrombin activator
RT   from the venom of Pseudonaja textilis, is structurally similar to
RT   mammalian blood coagulation factor Xa.";
RL   Thromb. Haemost. 92:509-521(2004).
RN   [35]
RP   BIOPHYSICOCHEMICAL PROPERTIES.
RX   PubMed=12111146; DOI=10.1007/s00253-002-1012-x;
RA   Kiiskinen L.-L., Viikari L., Kruus K.;
RT   "Purification and characterisation of a novel laccase from the
RT   ascomycete Melanocarpus albomyces.";
RL   Appl. Microbiol. Biotechnol. 59:198-204(2002).
RN   [36]
RP   BIOPHYSICOCHEMICAL PROPERTIES.
RX   PubMed=12118243; DOI=10.1038/nsb823;
RA   Hakulinen N., Kiiskinen L.-L., Kruus K., Saloheimo M., Paananen A.,
RA   Koivula A., Rouvinen J.;
RT   "Crystal structure of a laccase from Melanocarpus albomyces with an
RT   intact trinuclear copper site.";
RL   Nat. Struct. Biol. 9:601-605(2002).
RN   [37]
RP   BIOPHYSICOCHEMICAL PROPERTIES.
RX   PubMed=21592966; DOI=10.1074/jbc.M110.212183;
RA   Kilmartin J.R., Maher M.J., Krusong K., Noble C.J., Hanson G.R.,
RA   Bernhardt P.V., Riley M.J., Kappler U.;
RT   "Insights into structure and function of the active site of SoxAX
RT   cytochromes.";
RL   J. Biol. Chem. 286:24872-24881(2011).
RN   [38] {ECO:0000305}
RP   BIOPHYSICOCHEMICAL PROPERTIES.
RX   PubMed=18552405; DOI=10.1074/jbc.M800315200;
RA   Kappler U., Bernhardt P.V., Kilmartin J., Riley M.J., Teschner J.,
RA   McKenzie K.J., Hanson G.R.;
RT   "SoxAX cytochromes, a new type of heme copper protein involved in
RT   bacterial energy generation from sulfur compounds.";
RL   J. Biol. Chem. 283:22206-22214(2008).
RN   [39]
RP   MASS SPECTROMETRY OF FORMYLATED FORM.
RX   PubMed=10757971; DOI=10.1021/bi000150m;
RA   le Coutre J., Whitelegge J.P., Gross A., Turk E., Wright E.M.,
RA   Kaback H.R., Faull K.F.;
RT   "Proteomics on full-length membrane proteins using mass
RT   spectrometry.";
RL   Biochemistry 39:4237-4242(2000).
CC   -!- FUNCTION: Involved in the retrieval of endoplasmic reticulum
CC       membrane proteins from the early Golgi compartment. Required for
CC       correct localization of SEC12, SEC71 and SEC63 in the endoplasmic
CC       reticulum (By similarity). {ECO:0000250}. RIBA2 and RIBA3
CC       together are not able to complement the loss of function of RIBA1.
CC       {ECO:0000269|PubMed:22081402}.
CC   -!- CATALYTIC ACTIVITY: D-ribulose 5-phosphate = formate + L-3,4-
CC       dihydroxybutan-2-one 4-phosphate. {ECO:0000269|PubMed:17432872,
CC       ECO:0000269|PubMed:23203051}.
CC   -!- CATALYTIC ACTIVITY:
CC       Reaction=acetyl-CoA + H2O + oxaloacetate = citrate + CoA + H(+);
CC         Xref=Rhea:RHEA:16845, ChEBI:CHEBI:15377, ChEBI:CHEBI:15378,
CC         ChEBI:CHEBI:16452, ChEBI:CHEBI:16947, ChEBI:CHEBI:57287,
CC         ChEBI:CHEBI:57288; EC=2.3.3.16;
CC         Evidence={ECO:0000269|PubMed:11111111};
CC       PhysiologicalDirection=left-to-right; Xref=Rhea:RHEA:16846;
CC         Evidence={ECO:0000269|PubMed:22222222};
CC       PhysiologicalDirection=right-to-left; Xref=Rhea:RHEA:16847;
CC         Evidence={ECO:0000269|PubMed:33333333};
CC   -!- CATALYTIC ACTIVITY:
CC       Reaction=Sugar phosphate + H(2)O = sugar + phosphate.;
CC         EC=3.1.3.23; Evidence={ECO:0000269|PubMed:25848029};
CC   -!- CATALYTIC ACTIVITY: [Serine protease NS3]:
CC       Reaction=Selective hydrolysis of -Xaa-Xaa-|-Yaa- bonds in which each of
CC         the Xaa can be either Arg or Lys and Yaa can be either Ser or Ala.;
CC         EC=3.4.21.91;
CC   -!- COFACTOR:
CC       Name=Mg(2+); Xref=ChEBI:CHEBI:18420; Evidence={ECO:0000250};
CC       Name=Mn(2+); Xref=ChEBI:CHEBI:29035; Evidence={ECO:0000250};
CC       Note=Binds 2 divalent metal cations per subunit. Magnesium or
CC       manganese. {ECO:0000250};
CC   -!- COFACTOR:
CC       Name=Zn(2+); Xref=ChEBI:CHEBI:29105;
CC         Evidence={ECO:0000269|PubMed:23203051,
CC         ECO:0000269|PubMed:23269834};
CC       Note=Binds 1 zinc ion per subunit. {ECO:0000269|PubMed:23269834}.
CC       The purified enzyme contains a mixture of Fe(2+) and Zn(2+) bound
CC       in the active site, and a single equivalent of metal is required
CC       for full catalytic activity. {ECO:0000269|PubMed:23203051};
CC   -!- COFACTOR: [Serine protease NS3]:
CC       Name=Zn(2+); Xref=ChEBI:CHEBI:29105;
CC         Evidence={ECO:0000269|PubMed:9060645};
CC       Note=Binds 1 zinc ion. {ECO:0000269|PubMed:9060645};
CC   -!- ENZYME REGULATION: The protease activity is completely inhibited
CC       by the serine inhibitor PMSF but is not affected by thiol group
CC       inhibitors and in the presence of dithiothreitol. In the presence
CC       of high concentrations of o-phenanthroline the protease activity
CC       is only partially inhibited. {ECO:0000269|PubMed:6750031}. The
CC       pro-region plays an inhibitory role and may provide a mechanism
CC       for preventing premature activation in the secretory pathway.
CC       {ECO:0000269|PubMed:2649495}.
CC   -!- BIOPHYSICOCHEMICAL PROPERTIES:
CC       Absorption:
CC         Abs(max)=~280 nm {ECO:0000269|PubMed:12111146,
CC         ECO:0000269|PubMed:12118243};
CC         Note=Exhibits a shoulder at 360 nm, a smaller absorption peak at
CC         450 nm, and a second, larger peak at 590 nm.
CC         {ECO:0000269|PubMed:12118243}. Absorption peak at 450 nm
CC         disappears at high pH. {ECO:0000269|PubMed:12111146};
CC       Kinetic parameters:
CC         KM=0.49 mM for glutathione (at pH 6.0)
CC         {ECO:0000269|PubMed:18552405, ECO:0000269|PubMed:21592966};
CC         KM=0.19 mM for glutathione (at pH 7.0)
CC         {ECO:0000269|PubMed:18552405, ECO:0000269|PubMed:21592966};
CC         Vmax=0.124 uM/min/mg enzyme (copper-free) at pH 6.2
CC         {ECO:0000269|PubMed:18552405, ECO:0000269|PubMed:21592966};
CC         Vmax=1.54 uM/min/mg enzyme (copper-loaded) at pH 6.2
CC         {ECO:0000269|PubMed:18552405, ECO:0000269|PubMed:21592966};
CC         Note=kcat is 8.72 s(-1) and 5.7 s(-1) for glutathione at pH 6.0
CC         and pH 7.0, respectively. {ECO:0000269|PubMed:18552405}. Vmax
CC         decreases a lot at pH 7.0. {ECO:0000269|PubMed:21592966};
CC       pH dependence:
CC         Optimum pH is 9.0-10.0. {ECO:0000269|PubMed:6750031}. Inactive
CC         when pH is greater than 8.0. {ECO:0000269|PubMed:17432872};
CC       Redox potential:
CC         E(0) is -479 mV for heme at pH 8.0.
CC         {ECO:0000269|PubMed:21592966}. E(0) is -430 mV for heme at pH 6
CC         with copper-loaded SoxAX, -455 mV for heme at pH 7 with copper-
CC         loaded SoxAX, -486 mV for heme at pH 8 with copper-loaded SoxAX,
CC         -424 mV for heme at pH 6 with copper-free SoxAX, -479 mV for
CC         heme at pH 7 with copper-free SoxAX, -507 mV for heme at pH 8
CC         with copper-free SoxAX and -196 mV for copper center.
CC         {ECO:0000269|PubMed:18552405}. The Fe (III/II) potentials are
CC         +133 mV at pH 6.0, +104 mV at pH 7.0, +49 at pH 7.9 and +10 mV
CC         at pH 8.7. {ECO:0000269|PubMed:18552405,
CC         ECO:0000269|PubMed:21592966};
CC       Temperature dependence:
CC         Optimum temperature is 40 degrees Celsius.
CC         {ECO:0000269|PubMed:6750031}. Inactive when temperature drops
CC         below 20 degrees Celsius. {ECO:0000269|PubMed:17432872};
CC   -!- BIOPHYSICOCHEMICAL PROPERTIES: [Isoform 1]:
CC       Kinetic parameters:
CC         KM=0.11 uM for palmitic acid (at pH 7.5)
CC         {ECO:0000269|PubMed:17681178};
CC         KM=0.38 uM for palmitic acid (at pH 9.5)
CC         {ECO:0000269|PubMed:17681178};
CC       pH dependence:
CC         Optimum pH is 9.5. {ECO:0000269|PubMed:17681178};
CC   -!- PATHWAY: Cofactor biosynthesis; riboflavin biosynthesis; 2-
CC       hydroxy-3-oxobutyl phosphate from D-ribulose 5-phosphate: step
CC       1/1.
CC   -!- PATHWAY: Cofactor biosynthesis; riboflavin biosynthesis; 5-amino-
CC       6-(D-ribitylamino)uracil from GTP: step 1/4.
CC   -!- SUBUNIT: Interacts with AXL1, AXL2, IQG1 and SEC3.
CC       {ECO:0000269|PubMed:24389465}. Probably contacts ribosomal
CC       proteins L1, L5, L33 and S7, the 16S and 23S rRNA and the P site
CC       containing tRNA(fMet). {ECO:0000269|PubMed:24389466}.
CC   -!- SUBCELLULAR LOCATION: Plastid, chloroplast {ECO:0000255|HAMAP-
CC       Rule:MF_03000}. Cytoplasm, cytoskeleton, microtubule organizing
CC       center, centrosome {ECO:0000269|PubMed:23203051}. Nucleus
CC       {ECO:0000269|PubMed:9372446}. Note=Centrosomin-A is found in the
CC       centrosome. {ECO:0000269|PubMed:23203051}. Centrosomin-B is found
CC       in the nucleus. {ECO:0000269|PubMed:9372446}.
CC   -!- SUBCELLULAR LOCATION: [Isoform 1]: Cytoplasm. Nucleus.
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing; Named isoforms=2;
CC         Comment=Additional isoforms may be produced by alternative
CC         initiation, both from non-canonical start codons upstream of the
CC         initiator methionine displayed and from other canonical start
CC         codons downstream of that displayed.
CC         {ECO:0000303|PubMed:10835424}. The precise sites of translation
CC         initiation have not been unambiguously identified.
CC         {ECO:0000269|PubMed:10835424};
CC       Name=1;
CC         IsoId=P47924-1; Sequence=Displayed;
CC       Name=2;
CC         IsoId=P47924-2; Sequence=VSP_002721;
CC         Note=Most abundantly expressed isoform, at mRNA level.
CC         {ECO:0000269|PubMed:10835424}. Contains a phosphothreonine at
CC         position 214. {ECO:0000269|PubMed:10835424}. Ref.5 (AAF80602)
CC         sequence is in conflict in position: 223:K->E. {ECO:0000305}.
CC         Variant in position: 229:P->Q (in dbSNP:rs1141138).
CC         {ECO:0000269|PubMed:18514161}. Ref.5 (AAF80602) sequence differs
CC         from that shown due to several frameshifts. {ECO:0000305};
CC   -!- TISSUE SPECIFICITY: Expressed in leaves, shoots, roots, flowers
CC       and siliques. {ECO:0000269|PubMed:23203051}. Predominantly
CC       expressed in lung, spleen, thymus and testis and, at lower levels,
CC       in brain, bone marrow, peripheral leukocytes, skin and trachea.
CC       {ECO:0000269|PubMed:21991891}.
CC   -!- DEVELOPMENTAL STAGE: Expressed in endosperm during early stages of
CC       seed development. Strongly induced at heart stage of
CC       embryogenesis. {ECO:0000269|PubMed:9675899}. Expressed in flower
CC       buds at stage 6 of development in tapetal cells and at stage 10 in
CC       the epidermal cells of growing petals and ovaries. In young
CC       siliques, expressed transiently in the inner integument of the
CC       ovules just prior to testal deposition.
CC       {ECO:0000269|PubMed:12376641}.
CC   -!- INDUCTION: Constitutively expressed, increases in stationary phase
CC       (at protein level). {ECO:0000269|PubMed:24389466}. Induced by
CC       nitrogen (N) and phosphate (P) deprivation in leaves, but
CC       repressed by potassium (K) and N deprivation in roots.
CC       {ECO:0000269|PubMed:21094157}.
CC   -!- DOMAIN: The arm domain (residues 95-139) is inserted in the first
CC       ABC transporter domain. Its deletion abrogates the growth arrest
CC       and translation inhibition effect of the double Q-188/Q-470
CC       mutation. When deleted impairs fitness in long-term (up to 6 days)
CC       growth in stationary phase. {ECO:0000269|PubMed:24389466}.
CC       Probably contacts ribosomal protein L1.
CC       {ECO:0000269|PubMed:24389465}.
CC   -!- DOMAIN: [Envelope glycoprotein E1]: The transmembrane regions of
CC       envelope E1 and E2 glycoproteins are involved in heterodimer formation,
CC       ER localization, and assembly of these proteins.
CC       {ECO:0000269|PubMed:11145889}.
CC   -!- PTM: The 10 consecutive -X-Ala- or -X-Pro- dipeptides located over
CC       100 amino acids upstream of the N-terminal of mature XPR2 are
CC       subject to dipeptidyl aminopeptidase (DPAPase)-processing.
CC       {ECO:0000269|PubMed:9353927}. DPAPase activity is not necessary
CC       for XPR6 cleavage and for secretion of mature active XPR2.
CC       {ECO:0000269|PubMed:2649495}.
CC   -!- RNA EDITING: Modified_positions=Not_applicable; Note=Some
CC       positions are modified by RNA editing via nucleotide deletion, up
CC       to position 787. The unedited version gives rise to centrosomin-B
CC       (shown here). The fully edited version gives rise to centrosomin-
CC       A, in which the C-terminal sequence is replaced up to position 787
CC       by Ser-Ile-Val-Ala-STOP. A combination of alternative splicing and
CC       RNA editing resulting in this template G deletion could also
CC       explain the generation of centrosomin-A mRNA.
CC       {ECO:0000269|PubMed:9372446}. Target of Adar.
CC       {ECO:0000269|PubMed:17018572};
CC   -!- MASS SPECTROMETRY: Mass=60680; Method=Electrospray; Range=1-543;
CC       Note=Formylated form.; Evidence={ECO:0000269|PubMed:10757971};
CC   -!- POLYMORPHISM: Polymorphisms dbSNP:rs37972 and dbSNP:rs37973,
CC       located in GLCCI1 promoter region, are associated with a decreased
CC       response to glucorticoid treatment [MIM:614400] in asthma
CC       patients. {ECO:0000269|PubMed:21991891}. Same observations have
CC       been found in chronic obstructive pulmonary disease patients.
CC       {ECO:0000269|PubMed:22187997}. The mean increase in forced
CC       expiratory volume in 1 second in glucorticoid treated subjects who
CC       are homozygous for the mutant (G) rs37973 allele is only about
CC       one-third of that seen in similarly treated subjects who are
CC       homozygous for the wild-type allele (A).
CC       {ECO:0000269|PubMed:21991891}. These polymorphisms affect GLCCI1
CC       transcription level. {ECO:0000269|PubMed:21991891,
CC       ECO:0000269|PubMed:22187997}.
CC   -!- DISEASE: Note=A spontaneous mutation leading to a frameshift and
CC       truncation of Kcna2 causes megencephaly with a 25% increase of
CC       brain weight relative to wild-type. Especially the hippocampus
CC       shows increased proliferation of neurons and astrocytes, leading
CC       to increased brain volume. {ECO:0000269|PubMed:17315199}. Mutant
CC       mice appear normal at birth. After 3-4 weeks, they display low
CC       body weight, a subtle shakiness in their gait, a preference for a
CC       strange sitting position that is maintained for periods ranging
CC       from 30 seconds to several minutes, excessive lacrimation and
CC       acoustic startle hypersensitivity. {ECO:0000269|PubMed:21966978,
CC       ECO:0000269|PubMed:8995755}.
CC   -!- DISRUPTION PHENOTYPE: No visible phenotype when heterozygous.
CC       Bleached phenotype and no viable seeds produced when homozygous.
CC       {ECO:0000269|PubMed:22081402}. Low phytic acid levels in seed
CC       tissue. {ECO:0000269|PubMed:19797057}.
CC   -!- ALLERGEN: Causes an allergic reaction in human. Binds to IgE.
CC       {ECO:0000269|PubMed:21848516}. Sensitization is common in subjects
CC       who are repeatedly exposed to G.max dust. Common symptoms are
CC       bronchial asthma and allergic rhinitis.
CC       {ECO:0000269|PubMed:12100054}.
CC   -!- TOXIC DOSE: Intravenous injection (23 ug/kg bodyweight) of the
CC       group C prothrombin activator causes death in rats through
CC       disseminated intravascular coagulopathy.
CC       {ECO:0000269|PubMed:3075905}. To the contrary, pseutarin-C is not
CC       lethal even at 10 mg/kg in mice when injected intraperitoneally.
CC       {ECO:0000269|PubMed:15351847}.
CC   -!- BIOTECHNOLOGY: Overexpression of ABCA9 increases seed oil content.
CC       {ECO:0000269|PubMed:23269834}. NFS2-overexpressing transgenic
CC       plants have enhanced ability to tolerate and accumulate Se and may
CC       be used in phytoremediation. {ECO:0000269|PubMed:16244144}.
CC   -!- PHARMACEUTICAL: Potential therapeutic target of chemokine-mediated
CC       antimicrobial activity against B.anthracis to treat infection.
CC       {ECO:0000269|PubMed:21949405}. Might be useful for the development
CC       of new vaccines or adjuvants because of its LPS-modifying
CC       properties. May be used for the design of inhibitors with possible
CC       therapeutic value. {ECO:0000303|PubMed:16632613}.
CC   -!- SIMILARITY: In the N-terminal section; belongs to the DHBP
CC       synthase family. {ECO:0000305}.
CC   -!- SIMILARITY: In the C-terminal section; belongs to the GTP
CC       cyclohydrolase II family. {ECO:0000305}.
CC   -!- SIMILARITY: Contains 1 HNH domain. {ECO:0000305}.
CC   -!- SIMILARITY: To phage T4 mobB and mobD. {ECO:0000305}.
CC   -!- CAUTION: The active site cysteine and glutamate residues are not
CC       conserved in this protein. Its activity is therefore unsure.
CC       {ECO:0000305}. Dermonecrotic toxins were previously known as
CC       sphingomyelin phosphodiesterase D based on their ability to
CC       hydrolyze sphingomyelin into choline and acylsphingosine
CC       phosphate. Based on additional biochemical analysis, the enzymes
CC       have been renamed phospholipase D to represent a more accurate and
CC       broader denomination. {ECO:0000305}.
CC   -!- SEQUENCE CAUTION:
CC       Sequence=BAA08113.1; Type=Erroneous initiation; Note=Translation N-terminally extended.; Evidence={ECO:0000305};
DR   EMBL; AJ000053; CAA03884.1; -; Genomic_DNA.
DR   EMBL; AB008268; BAB09861.1; -; Genomic_DNA.
DR   EMBL; CP002688; AED97867.1; -; Genomic_DNA.
DR   EMBL; D45165; BAA08113.1; ALT_INIT; mRNA.
DR   EMBL; AF255301; AAF80602.1; -; mRNA.
DR   PIR; JC4209; JC4209.
DR   RefSeq; NP_201235.4; NM_125826.4.
DR   UniGene; At.49217; -.
DR   ProteinModelPortal; P47924; -.
DR   SMR; P47924; 125-502.
DR   MINT; MINT-8063173; -.
DR   STRING; 3702.AT5G64300.1; -.
DR   PaxDb; P47924; -.
DR   PRIDE; P47924; -.
DR   EnsemblPlants; AT5G64300.1; AT5G64300.1; AT5G64300.
DR   GeneID; 836551; -.
DR   KEGG; ath:AT5G64300; -.
DR   TAIR; AT5G64300; -.
DR   eggNOG; COG0108; -.
DR   HOGENOM; HOG000115440; -.
DR   InParanoid; P47924; -.
DR   KO; K14652; -.
DR   OMA; LMVDRNT; -.
DR   PhylomeDB; P47924; -.
DR   BioCyc; ARA:AT5G64300-MONOMER; -.
DR   BioCyc; MetaCyc:AT5G64300-MONOMER; -.
DR   BRENDA; 4.1.99.12; 399.
DR   UniPathway; UPA00275; UER00399.
DR   UniPathway; UPA00275; UER00400.
DR   PRO; PR:P47924; -.
DR   Proteomes; UP000006548; Chromosome 5.
DR   GO; GO:0009507; C:chloroplast; IDA:UniProtKB.
DR   GO; GO:0009570; C:chloroplast stroma; IDA:TAIR.
DR   GO; GO:0016020; C:membrane; IDA:TAIR.
DR   GO; GO:0008686; F:3,4-dihydroxy-2-butanone-4-phosphate synthase activity; IDA:UniProtKB.
DR   GO; GO:0005525; F:GTP binding; IEA:UniProtKB-KW.
DR   GO; GO:0003935; F:GTP cyclohydrolase II activity; IDA:UniProtKB.
DR   GO; GO:0046872; F:metal ion binding; IEA:UniProtKB-KW.
DR   GO; GO:0009231; P:riboflavin biosynthetic process; IMP:UniProtKB.
DR   Gene3D; 3.90.870.10; -; 1.
DR   HAMAP; MF_00179; RibA; 1.
DR   HAMAP; MF_00180; RibB; 1.
DR   HAMAP; MF_01283; RibBA; 1.
DR   InterPro; IPR017945; DHBP_synth_RibB-like_a/b_dom.
DR   InterPro; IPR000422; DHBP_synthase_RibB.
DR   InterPro; IPR000926; GTP_CycHdrlaseII_RibA.
DR   InterPro; IPR016299; Riboflavin_synth_RibBA.
DR   Pfam; PF00926; DHBP_synthase; 1.
DR   Pfam; PF00925; GTP_cyclohydro2; 1.
DR   SUPFAM; SSF55821; SSF55821; 1.
DR   TIGRFAMs; TIGR00505; ribA; 1.
DR   TIGRFAMs; TIGR00506; ribB; 1.
PE   1: Evidence at protein level;
KW   Allergen; Alternative splicing; Chloroplast; Complete proteome;
KW   Cytoplasm; Cytoskeleton; GTP-binding; Hydrolase; Lyase; Magnesium;
KW   Manganese; Metal-binding; Multifunctional enzyme; Nucleotide-binding;
KW   Nucleus; Pharmaceutical; Plastid; Reference proteome;
KW   Riboflavin biosynthesis; Transit peptide; Zinc.
FT   TRANSIT       1     56       Chloroplast. {ECO:0000255|HAMAP-
FT                                Rule:MF_03000}.
FT   CHAIN        57    543       Bifunctional riboflavin biosynthesis
FT                                protein RIBA 1, chloroplastic.
FT                                /FTId=PRO_0000030436.
FT   NP_BIND     379    383       GTP. {ECO:0000250}.
FT   NP_BIND     423    425       GTP. {ECO:0000250}.
FT   REGION       57    328       DHBP synthase.
FT   REGION      152    153       D-ribulose 5-phosphate binding.
FT                                {ECO:0000250}.
FT   REGION      267    271       D-ribulose 5-phosphate binding.
FT                                {ECO:0000250}.
FT   REGION      329    543       GTP cyclohydrolase II.
FT   COMPBIAS      7     10       Poly-Ser.
FT   COMPBIAS    144    147       Poly-Val.
FT   ACT_SITE    457    457       Proton acceptor; for GTP cyclohydrolase
FT                                activity. {ECO:0000255}.
FT   ACT_SITE    459    459       Nucleophile; for GTP cyclohydrolase
FT                                activity. {ECO:0000250}.
FT   METAL       153    153       Magnesium or manganese 1. {ECO:0000250}.
FT   METAL       153    153       Magnesium or manganese 2. {ECO:0000250}.
FT   METAL       270    270       Magnesium or manganese 2. {ECO:0000250}.
FT   METAL       384    384       Zinc; catalytic. {ECO:0000250}.
FT   METAL       395    395       Zinc; catalytic. {ECO:0000250}.
FT   METAL       397    397       Zinc; catalytic. {ECO:0000250}.
FT   BINDING     157    157       D-ribulose 5-phosphate. {ECO:0000250}.
FT   BINDING     291    291       D-ribulose 5-phosphate. {ECO:0000250}.
FT   BINDING     400    400       GTP. {ECO:0000250}.
FT   BINDING     445    445       GTP. {ECO:0000250}.
FT   BINDING     480    480       GTP. {ECO:0000250}.
FT   BINDING     485    485       GTP. {ECO:0000250}.
FT   SITE        253    253       Essential for DHBP synthase activity.
FT                                {ECO:0000250}.
FT   SITE        291    291       Essential for DHBP synthase activity.
FT                                {ECO:0000250}.
FT   VAR_SEQ     203    203       E -> ETYMAVSFIGGTDGWFAGVSKMVDAAPGHFEMILDQ
FT                                SNPQYMNLPGIAVLIGGLWVANLYYWGFNQYIIQRTLAAK
FT                                (in isoform 2).
FT                                /FTId=VSP_002721.
**
**   #################    INTERNAL SECTION    ##################
**DR TAIR-CDS; AT5G64300.1; TAIR10; P47924-1.
**EV ECO:0000250; -; XXX; 01-JAN-1900.
**EV ECO:0000255; -; XXX; 01-JAN-1900.
**EV ECO:0000255; HAMAP-Rule:MF_03000; SEG; 30-JUN-2015.
**EV ECO:0000269; PubMed:10757971; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:10835424; SEG; 30-JUN-2015.
**EV ECO:0000269; PubMed:12100054; EMB; 09-APR-2013.
**EV ECO:0000269; PubMed:12111146; JUJ; 19-APR-2006.
**EV ECO:0000269; PubMed:12118243; JUJ; 19-APR-2006.
**EV ECO:0000269; PubMed:12376641; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:15351847; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:16244144; MIS; 14-JAN-2015.
**EV ECO:0000269; PubMed:17018572; ELS; 27-MAR-2008.
**EV ECO:0000269; PubMed:17315199; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:17432872; SEG; 29-JUN-2015.
**EV ECO:0000269; PubMed:18514161; SEG; 30-JUN-2015.
**EV ECO:0000269; PubMed:18552405; KAL; 17-JUN-2013.
**EV ECO:0000269; PubMed:19797057; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:21094157; MAF; 23-APR-2015.
**EV ECO:0000269; PubMed:21592966; KAL; 13-JUN-2013.
**EV ECO:0000269; PubMed:21848516; MNS; 14-OCT-2011.
**EV ECO:0000269; PubMed:21949405; KAL; 09-JAN-2013.
**EV ECO:0000269; PubMed:21966978; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:21991891; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:22081402; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:22187997; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:23203051; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:23269834; MIT; 19-JUN-2010.
**EV ECO:0000269; PubMed:24389465; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:24389466; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:2649495; MAF; 23-APR-2015.
**EV ECO:0000269; PubMed:3075905; SEG; 30-JUN-2015.
**EV ECO:0000269; PubMed:6750031; MAF; 23-APR-2015.
**EV ECO:0000269; PubMed:8995755; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:9353927; SEG; 29-JUN-2015.
**EV ECO:0000269; PubMed:9372446; XXX; 01-JAN-1900.
**EV ECO:0000269; PubMed:9675899; XXX; 01-JAN-1900.
**EV ECO:0000303; PubMed:10835424; SEG; 30-JUN-2015.
**EV ECO:0000303; PubMed:16632613; KAL; 30-APR-2013.
**EV ECO:0000305; -; SEG; 30-JUN-2015.
**IS P47924-3
**ZB MIT, 21-JUL-2004; ELC, 15-NOV-2007; MIS, 02-MAY-2013;
SQ   SEQUENCE   543 AA;  59056 MW;  31D89A500E42BF81 CRC64;
     MSSINLSSSS PSTISLSRSR LSQSSTTLLH GLHRVTLPSN HPLSTFSIKT NTGKVKAAVI
     SREDDLLSFT NGNTPLSNGS LIDDRTEEPL EADSVSLGTL AADSAPAPAN GFVAEDDDFE
     LDLPTPGFSS IPEAIEDIRQ GKLVVVVDDE DRENEGDLVM AAQLATPEAM AFIVRHGTGI
     VCVSMKEDDL ERLHLPLMVN QKENEEKLST AFTVTVDAKH GTTTGVSARD RATTILSLAS
     RDSKPEDFNR PGHIFPLKYR EGGVLKRAGH TEASVDLTVL AGLDPVGVLC EIVDDDGSMA
     RLPKLREFAA ENNLKVVSIA DLIRYRRKRD KLVERASAAR IPTMWGPFTA YCYRSILDGI
     EHIAMVKGEI GDGQDILVRV HSECLTGDIF GSARCDCGNQ LALSMQQIEA TGRGVLVYLR
     GHEGRGIGLG HKLRAYNLQD AGRDTVEANE ELGLPVDSRE YGIGAQIIRD LGVRTMKLMT
     NNPAKYVGLK GYGLAIVGRV PLLSLITKEN KRYLETKRTK MGHMYGLKFK GDVVEKIESE
     SES
//
